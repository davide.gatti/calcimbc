USE grid
USE riblets
USE dns
USE coco

IMB = STRUCTURE[ POINTER TO ARRAY(*,*) OF POINTER TO ARRAY(*) OF REAL imbc;
               ]
		
SUBROUTINE allocate(IMB this^; DNS dns; GRID grid; RIBLETS riblet; BOOLEAN useCoco) FOLLOWS
REAL FUNCTION calcCoeff(GRID grid; GRID prGrid; DNS dns; RIBLETS riblet; INTEGER VARIABLE ix, iy, iz; BOOLEAN useCoco) FOLLOWS
SUBROUTINE populateCoeff(IMB this^; DNS dns; GRID grid; GRID pgrid; RIBLETS riblet; BOOLEAN useCoco) FOLLOWS

MODULE imbc

	dir        = (     99,       0,        0,       1,        1,        2,       2 )
	neighbours = ( (0,0,0), (-1,0,0), (1,0,0), (0,-1,0), (0,1,0), (0,0,-1), (0,0,1) )
	
	SUBROUTINE allocate(IMB this^; DNS dns; GRID grid; RIBLETS riblet; BOOLEAN useCoco)
        	this.imbc = NEW ARRAY(dns.localSize(0,0)..dns.localSize(0,1), dns.localSize(1,0)..dns.localSize(1,1)) OF POINTER TO ARRAY(*) OF REAL
		LOOP FOR ix=this.imbc.LO1 TO this.imbc.HI1 AND iy=this.imbc.LO2 TO this.imbc.HI2
			INTEGER izlo=0
			DO
				INC izlo
				ARRAY(0..2) OF REAL coord = (grid.x(0,ix), grid.x(1,iy), grid.x(2,izlo))
			WHILE isInBody[riblet, coord]
			INTEGER izhi = izlo
			LOOP FOR iz = izlo+1 TO dns.size(2)-1-grid.di(2)
				BOOLEAN moleculeCutsBody = FALSE
				ARRAY(0..2) OF INTEGER indxA = (ix,iy,iz) 
				ARRAY(0..2) OF REAL coordA = (grid.x(0,indxA(0)), grid.x(1,indxA(1)), grid.x(2,indxA(2)))
				LOOP FOR iN = neighbours.LO1 TO neighbours.HI1
					ARRAY(0..2) OF INTEGER indxB = (ix,iy,iz) + neighbours(iN,*)
					ARRAY(0..2) OF REAL coordB = (grid.x(0,indxB(0)), grid.x(1,indxB(1)), grid.x(2,indxB(2)))
					cordMod(riblet, coordA, coordB)
					moleculeCutsBody = moleculeCutsBody OR isInBody[riblet, coordB]
				REPEAT
				IF NOT useCoco THEN IF moleculeCutsBody THEN izhi=iz
				STRUCTURED ARRAY(x,y,z,beta) OF REAL edgeCoord=findEdge(riblet, dns, coordA)
				IF useCoco THEN IF moleculeCutsBody OR applyCoco(coordA, edgeCoord, grid, dns, riblet, useCoco) THEN izhi=iz
			REPEAT
			this.imbc(ix,iy)=NEW ARRAY(izlo..izhi) OF REAL
		REPEAT
	END allocate
	
	REAL FUNCTION calcCoeff(GRID grid; GRID prGrid; DNS dns; RIBLETS riblet; INTEGER VARIABLE ix, iy, iz; BOOLEAN useCoco)
		INTEGER iN0
		LOOP FOR ALL ii
			IF grid.di(ii) # 0 THEN  iN0 = ii						! To find the equation we are computing the coefficients for
		REPEAT
		ARRAY(0..2) OF INTEGER indx_in = (ix, iy, iz)						! Definitions of coordinates
		ARRAY(0..2) OF REAL coord_in = (grid.x(0, indx_in(0)), grid.x(1, indx_in(1)), grid.x(2, indx_in(2)))
		ARRAY(0..2) OF REAL coord_p1 = (prGrid.x(0, indx_in(0)), prGrid.x(1, indx_in(1)), prGrid.x(2, indx_in(2)))
		ARRAY(0..2) OF REAL coord_p2 = (prGrid.x(0, indx_in(0)+grid.di(0)), prGrid.x(1, indx_in(1)+grid.di(1)), prGrid.x(2, indx_in(2)+grid.di(2)))
		cordMod(riblet, coord_in, coord_p1); cordMod(riblet, coord_in, coord_p2)
		prGridDelta = ABS(coord_p2-coord_p1)
		REAL rsd=0, corrLapl, corrStokes, p1, p2, symmetry
        	INLINE REAL FUNCTION delta(INTEGER iN) = grid.delta(dir(iN),indx_in(dir(iN)),SIGN(neighbours(iN,dir(iN)+1)))
		INLINE REAL FUNCTION d2(INTEGER iN) = grid.d2(dir(iN),indx_in(dir(iN)),SIGN(neighbours(iN,dir(iN)+1)))
		
		STRUCTURED ARRAY(x,y,z,beta) OF REAL edgeCoord=findEdge(riblet, dns, coord_in)
		REAL laplaceSol0, stokesSol0								! Laplace & Stokes solution @ central point, computed later
		ARRAY(0..2) OF REAL Lweight = (COS(edgeCoord(3))**2, SIN(edgeCoord(3))**2, 0)		! Rotation coefficients
		ARRAY(0..2) OF REAL Sweight = (SIN(edgeCoord(3))**2, COS(edgeCoord(3))**2, 1)		! "
		ARRAY(0..2) OF REAL pSweight = (SIN(edgeCoord(3)), COS(edgeCoord(3)), 1) 		! "
		
		IF applyCoco(coord_in, edgeCoord, grid, dns, riblet, useCoco) THEN 			! Pressure contribution to Coco
		        laplaceSol0 = stokesSolution(dns, riblet, coord_in)(0) 				! Laplace solution @ central point
		        stokesSol0 = stokesSolution(dns, riblet, coord_in)(IF iN0#2 THEN 1 ELSE iN0)	! Stokes solution @ central point
			p1 = stokesSolution(dns, riblet, coord_p1)(3)
			p2 = stokesSolution(dns, riblet, coord_p2)(3)
			symmetry = stokesSolution(dns, riblet, coord_in)(4)				! To account for periodic bc (for w)
			ARRAY(0..1) OF REAL Psigns = signP(riblet, coord_in, coord_p1, coord_p2, edgeCoord, p1, p2)
			rsd = pSweight(iN0)*(Psigns(0)*p1+Psigns(1)*p2)/stokesSol0/prGridDelta * (IF iN0=2 THEN symmetry ELSE 1)
		END IF
		
		LOOP FOR iN = neighbours.LO1+1 TO neighbours.HI1					! Loop to account for all stencil points
			ARRAY(0..2) OF INTEGER indx = (ix,iy,iz) + neighbours(iN,*)
			ARRAY(0..2) OF REAL coord = (grid.x(0,indx(0)), grid.x(1,indx(1)), grid.x(2,indx(2)))
			cordMod(riblet, coord_in, coord)
			REAL d0 = ABS(coord-coord_in)
			REAL d = d0
			IF isInBody(riblet,coord) THEN d = ABS(coord_in-Bisection(<isInBody(riblet,s)>,coord_in,coord))

			IF NOT applyCoco(coord_in, edgeCoord, grid, dns, riblet, useCoco) THEN		! Points without Coco
				rsd = ~+delta(iN)/d*d2(iN)
			ELSE										! Points with Coco
				IF ABS(ribletsCrossed(riblet, coord) - ribletsCrossed(riblet, coord_in)) = 1 AND iN0 = 2 THEN ! when w stencil intersects with riblets symmetry axis
					rsd = ~
				ELSE IF d = d0 THEN
					corrLapl = stokesSolution(dns, riblet, coord)(0)/laplaceSol0*d2(iN)
					corrStokes = ABS(stokesSolution(dns, riblet, coord)(IF iN0#2 THEN 1 ELSE iN0)/stokesSol0*d2(iN))
					rsd = ~ + Lweight(iN0)*corrLapl + Sweight(iN0)*corrStokes
				ELSE									! Stencil crosses the boundary
					rsd = ~
				END IF
			END IF 	
		REPEAT
		
        	RESULT = {rsd-[SUM grid.d2(iC,indx_in(iC),i) FOR ALL iC,i]}*dns.nu			! Add -2/dx/dx -2/dy/dy -2/dz/dz
	END calcCoeff
	
	SUBROUTINE populateCoeff(IMB this^; DNS dns; GRID grid; GRID prGrid; RIBLETS riblet; BOOLEAN useCoco)
		LOOP FOR ix=this.imbc.LO1 TO this.imbc.HI1 AND iy=this.imbc.LO2 TO this.imbc.HI2
			LOOP FOR iz = this.imbc(ix, iy).LO TO this.imbc(ix, iy).HI
				this.imbc(ix, iy)(iz) = calcCoeff(grid, prGrid, dns, riblet, ix, iy, iz, useCoco)
				IF this.imbc(ix, iy)(iz) <-1E2 THEN this.imbc(ix,iy)(iz)=-1E2
				!IF iy MOD 16 = 7 AND iz = 15 AND grid.di(2) = 1 THEN this.imbc(ix, iy)(iz) = 1353.96
				!IF iy MOD 16 = 9 AND iz = 15 AND grid.di(2) = 1 THEN this.imbc(ix, iy)(iz) = 1353.96
			REPEAT
		REPEAT
	END populateCoeff
			
END imbc
