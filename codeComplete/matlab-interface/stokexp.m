clc 
close all
clear

input_angle = linspace(10*pi/180,110*pi/180,10000);
stokesexp = zeros(1,10000);

stokdisp    = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);
options         = optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);

for ii = 1:10000
    phiwall = pi-input_angle(ii)*0.5;
    stokesexp(ii)     = fsolve( @(x) stokdisp(x,phiwall), 0.51, options);
end

coeff2 = polyfit(input_angle,stokesexp,2);
stkexp_fit2 = polyval(coeff2,input_angle);

coeff4 = polyfit(input_angle,stokesexp,4);
stkexp_fit4 = polyval(coeff4,input_angle);

coeff6 = polyfit(input_angle,stokesexp,6);
stkexp_fit6 = polyval(coeff6,input_angle);

figure(); hold on; grid on
plot(input_angle*180/pi,stokesexp,'r*')
plot(input_angle*180/pi,stkexp_fit2,'g')
plot(input_angle*180/pi,stkexp_fit4,'b')
plot(input_angle*180/pi,stkexp_fit6,'k')
