%%
addpath('./base/')
clear
clc
disp('|-------------------')
disp('|     RIBLETS 3D   |')
disp('|-------------------')
disp('| ')
disp('| Simulation set-up ')
disp('| initialisation    ')
disp('| and checks        ')
disp('| ')
disp('  ')


%% Physical Inputs

% LAMINAR or TURBULENT ?

dns.laminar         = false;             % this setting allow the field generator to chose among the possible laminar cases
dns.couette 	    = false;             % if true, a couette flow is imposed with centerline velocity = 1 and headx=heady=0

% CONSTANT PRESSURE GRADIENT or FLOW RATE ? 

dns.type 	        = 'CPG';            % 'CFR'= constant flow rate; 'CPG'= constant pressure gradient

dns.headx 	        = 1.0;				% x-component of the pressure gradient
dns.heady 	        = 0.0;  			% y-component of the pressure gradient

% REYNOLDS NUMBER ?

dns.re 		        = 200;              % re_bulk if CFR, re_tau if CPG


%% Geometrical Inputs

% HALF or FULL CHANNEL ?

dns.walls           = 2;                % one wall -> half channel, two walls -> full channel

% DOMAIN SIZE ?

dns.domain          = 'full';           % 'full'= physical dimensions chosen by user, 'minimal'= minimal domain size

dns.Lx_plus         = 1500;             % relevant only if dns.type is 'full' (wall-units)
dns.Ly_plus         = 750;              % relevant only if dns.type is 'full' (wall-units)

% RIBLETS GEOMETRY

rib.alfa            = 60;               % rib tip angle (degrees)
rib.s_plus          = 16;               % riblets spacing (wall-units), s_plus=0 TO SIMULATE A SMOOTH CHANNEL 

rib.lambda_plus     = ones(1,100);      % initial values for lambda (will be modified subsequently)
rib.max_beta        = -1*ones(1,100);   % initial values for max angles (will be modified subsequently)
rib.type            = 0;                % riblet shape: 0 = triangular, 1 = parabolic, 2 = blade
rib.s_t             = 5;                % ratio between spacing and thickness for blade riblets

rib.lambda_plus(1)  = dns.Lx_plus;      % values for the riblets wavelengths (wall-units)
rib.lambda_plus(2)  = dns.Lx_plus/6;
rib.lambda_plus(3)  = dns.Lx_plus/3;
rib.lambda_plus(4)  = dns.Lx_plus/6;
rib.lambda_plus(5)  = dns.Lx_plus/3;

rib.max_beta(1)     = 0;                % values for the maximum riblet angles (degrees), at least one value for max_beta has to be defined
rib.max_beta(2)     = 0;
rib.max_beta(3)     = 0;
rib.max_beta(4)     = 0;
rib.max_beta(5)     = 0;

rib.r_s = 0.025;			% ratio between tip radius and spacing
rib.r  = rib.r_s*rib.s_plus/dns.re;     % riblet tip radius
rib.yC = 0.5*rib.s_plus/dns.re;         % circle X center coordinate

%% Channel
% --------------------------
% If you want to simulate channel, then set channel = 0, otherwise channel
% = 1
channel = 1;

%% Corner Correction Inputs

dns.useCoco         = 'yes';            % if 'yes' then corner correction is applied

dns.radius_plus     = 2;                % CoCo's radius of aplication around the tip (wall-units)

dns.extra_diag      = 'no';             % if 'yes' then the extra-diagonal terms of the corner correction are applied


%% Numerical Inputs

% SPATIAL RESOLUTION

dns.dx_plus        = 2.0;              % streamwise resolution (wall-units)
dns.dy_plus        = 1.0;              % spanwise resolution (wall-units)
dns.dz_plus        = 0.8;              % wall normal resolution at the wall (wall-units)
dns.nppr           = 2;                % minimum number of points per riblet

dns.xstretch       = false;           
dns.ystretch       = false;
dns.zstretch       = true;

dns.xstr           = 7.0;              
dns.ystr           = 7.0;
dns.zstr           = 3.0;

% TEMPORAL RESOLUTION

dns.cflmax          = 1.2;              % maximum cfl number
dns.fouriermax      = 0.628;            % maximum fourier number

% CHECKS and SAVES

dns.it_save         = 10000;            % number of iterations between two flow snapshots. It can be changed in the ontherun.in file while tthe simulation is going
dns.it_stat         = 10000;            % number of iteration between two 1d flow statistics
dns.it_max          = 100000;           % maximum number of iterations
dns.it_check        = 100;              % number of iterations between two readings of the on the fly adaptation. This can also be changed on the run
dns.it_cfl          = 10;               % how often to compute CFL; can also be 0 to stop the simulation after it_check iterations and save the statistics.

dns.it_runtimestats = 5;                % number of iterations between two runtimestats acquisitons
dns.it_start        = 90000;            % start iteration to save runtimestats
dns.it_end          = 100000;           % end iteration to save runtimestats
dns.iz_stats        = 22;               % z index at which you want to save runtimestats (right nowabout 3 units+ above riblet tip)

dns.maxTime         = 250;              % maximum time before the simulation stops, expressed in seconds


%% Starting Field Input

dns.generateInitialField    = true;                         % do you want to generate the initial field?
dns.fieldToInterpolate      = '../../../../initialFields/riblets200.field';


%% Derived Quantities

% WALL-UNITS

dns.nu          = (1/dns.re);                               % kinematic viscosity

if strcmp(dns.type,'CFR')
    tmp.Cf      = 0.073*((2*dns.re)^-(0.25));
    dns.retau   = sqrt(0.5*tmp.Cf)*dns.re;
else
    dns.retau   = dns.re;
end

dns.utau        = dns.retau*dns.nu;
dns.deltani     = dns.nu/dns.utau;
dns.tni         = dns.nu/dns.utau^2;
dns.dt          = (0.01*dns.tni);

rib.s_h         = rib.s_plus*dns.nu/dns.utau;               % ratio between riblet spacing and channel height
rib.s_k         = 2*tan(pi*rib.alfa/2/180);                 % ratio between riblet spacing and height
rib.k_h         = rib.s_h/rib.s_k;                          % ratio between riblet and channel height

rib.index = 0;                                                  % create arrays containing values for lambda, amp; this is here because it is needed before defining laminar domain
rib.lambda = [];
while rib.index < length(rib.lambda_plus)
    rib.lambda = [rib.lambda rib.lambda_plus(rib.index+1)*dns.nu/dns.utau];
    if mod(dns.Lx_plus,rib.lambda_plus(rib.index+1)) ~= 0
        error('Incorrect lambda value: no periodicity along the streamwise direction');
    end
    rib.index = rib.index+1;
    if rib.max_beta(rib.index) < 0
        rib.lambda = rib.lambda(1:end-1);
        rib.final_index = rib.index-1;
        break
    end
end
if isempty(rib.lambda)
    rib.lambda = 0;
    rib.amp = 0;
else
    rib.amp = rib.lambda.*tan(rib.max_beta(1:rib.final_index)*pi/180)/2/pi;
end
rib.wave_n = length(rib.amp);                               % number of wavelength in the geometry

% PRESSURE GRADIENT

if dns.couette
     dns.headx 	= 0.0;
     dns.heady 	= 0.0;
end

% DOMAIN SIZE

dns.zcp         = 400;                                      % height up to which you want an healthy mean velocity profile (minimal domain)

if dns.laminar
    dns.nonLaminar = 0;
    dns.Ly_plus = rib.s_plus;                               % to simulate one riblet
    if rib.amp == zeros(1,length(rib.amp))
        dns.Lx_plus = 8*dns.dx_plus;                        % to use a smaller domain
        dns.Lx = dns.Lx_plus*dns.deltani;
    end
else
    dns.nonLaminar = 1;
    if strcmp(dns.domain,'minimal')
        dns.walls   = 1;
        dns.zcp 	= max([4*rib.s_plus/rib.s_k, 100]);
        dns.Ly_plus = ceil(max([0.5*rib.s_plus/0.4, dns.zcp/0.4])/rib.s_plus)*rib.s_plus;   % Spanwise size of the box in wall units.
                                                                                            % Criteria according to (eq. 2.3) of Endrikat et al (2021), JFM.
									                                                    % Rounded so to have an integer number of riblets in the box.
        dns.Lx_plus = max([1000, 3*dns.Ly_plus]);               % streamwise size of the box in wall units: criteria according to (eq. 2.3) of Endrikat et al (2021), JFM
        dns.Ly      = dns.Ly_plus*dns.deltani;                  % spanwise size of the box as fraction of h
        dns.Lx      = dns.Lx_plus*dns.deltani;                  % streamwise size of the box as fraction of h
    else
        dns.Lx          = dns.Lx_plus*dns.nu/dns.utau;              % relevant only if dns.type is 'full'
    end 
end

rib.number      = round(dns.Ly_plus/rib.s_plus);
dns.Ly_plus     = rib.number*rib.s_plus;                    % to ensure periodicity
dns.Ly          = dns.Ly_plus*dns.nu/dns.utau;              % relevant only if dns.type is 'full'

dns.radius      = dns.radius_plus/dns.re;

dns.cellAR      = dns.dy_plus/dns.dz_plus;                  % aspect ratio of the cells within the riblets (dy+/dz+)

dns.dx          = dns.dx_plus*dns.nu/dns.utau;
dns.dy          = dns.dy_plus*dns.nu/dns.utau;
dns.dz          = dns.dz_plus*dns.nu/dns.utau;

rib.s = rib.s_plus/dns.retau;                               % riblet spacing
rib.h = 1/rib.s_k*rib.s;                                    % riblet height

deltaQ = @(zC) (rib.yC+rib.h^2/rib.s+2*rib.h*zC./rib.s).^2-(1+4*(1/rib.s_k)^2)*(rib.yC^2+(0.5*rib.h)^2+zC.^2+rib.h.*zC-rib.r^2);
rib.zC = fzero(deltaQ, 0.01);                               % circle Y center coordinate
rib.yTan = (0.5*rib.h + (rib.zC+ 0.5*rib.s*rib.yC/rib.h))/(2*1/rib.s_k+0.5*rib.s_k);  % intersection between riblet sidewall and tip circle

% COCO VARIABLES

tmp.phirib      = atan(0.5*rib.s_k);                        % semi-angle at the riblet tip
rib.phiwall     = pi - tmp.phirib;                          % angle wrt the horizontal wall
rib.stokdisp    = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);
rib.options     = optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);
rib.stokexp     = fsolve( @(x) rib.stokdisp(x,rib.phiwall), 0.51, rib.options); % Stokes eq. coefficient to evaluate the corner correction

if strcmp(dns.useCoco,'no') || rib.s_plus == 0
    dns.coco = false;                                        % corner correction activated
else
    dns.coco = true;
end

if strcmp(dns.extra_diag,'yes')
    dns.extra = true;                                       % extra diagonal terms considered
else
    dns.extra = false;
end


% COEFFICIENTS TO FIND TIP ANGLE FOR SINUSOIDAL RIBLETS, AT THE MOMENT THIS
% WORKS FOR ONE WAVELENGTH ONLY, NO COMPOSITION OF MORE

coeff = calcCocoCoeff(rib);


%% GRID

grid = createGrid(dns,rib);

% Grid origin computation:
if rib.type == 0
    grid.eqHeight = rib.h/2;
end
if rib.type == 1
    grid.eqHeight = rib.h/3;
end
if rib.type == 2
    rib.t = rib.s_h/rib.s_t;
    grid.eqHeight = rib.k_h/(1+rib.s_h - rib.t);
end


%% Echo
% --------------------------
% clear tmp i j options
clear i j options
disp(' ')
disp('Flow parameters')
disp(['    nx = ' num2str(grid.nx)])
disp(['    ny = ' num2str(grid.ny)])
disp(['    nz = ' num2str(grid.nz)])
disp(['    nu = ' num2str(1/dns.retau)])
disp(['    Lx = ' num2str(dns.Lx) '  (Lx+ = ' num2str(dns.Lx_plus) ')'])
disp(['    Ly = ' num2str(dns.Ly) '  (Ly+ = ' num2str(dns.Ly_plus) ')'])
disp(['    Lz = ' num2str(grid.zd(end))])
disp(['    dx+ = ' num2str(dns.dx_plus)])
disp(['    dy+ = ' num2str(dns.dy_plus)])
disp(['    dz+min = ' num2str(min(abs(diff(grid.zc))*dns.retau))])
disp(['    dz+max = ' num2str(max(diff(grid.zc)*dns.retau))])
disp(['    deltat = ' num2str(dns.dt)])
disp(['    dt+ = ' num2str(dns.dt*dns.retau)])
disp(['    headx = ' num2str(dns.headx)])
disp(['    heady = ' num2str(dns.heady)])
disp(['    AR = ' num2str(dns.cellAR)])
disp(['    walls = ' num2str(dns.walls)])
disp(' ')
disp('Riblets parameters')
disp(['    alfa = ' num2str(rib.alfa) '°'])
disp(['    k/s = ' num2str(channel*rib.s_k)])
disp(['    s/h = ' num2str(rib.s_h) '  (s+ = ' num2str(rib.s_plus) ')'])
disp(['    stokexp = ' num2str(rib.stokexp) '  (err = ' num2str(rib.stokdisp(rib.stokexp,rib.phiwall)) ')'])
disp(['    nppr = ' num2str(rib.s_plus/dns.dy_plus)])
disp(['    type = ' num2str(rib.type) '  (0 = Triangular, 1 = Parabolic, 2 = Blade) '])

%% Create initial condition
% --------------------------
if dns.generateInitialField
  disp(' ')
  disp('Generating initial field')
  field=fieldGenerator('../fields/riblets.field',dns.fieldToInterpolate,dns,grid);
end


%% Create initial parameter file
% --------------------------
f=fopen('../input/riblets.in','w+');
fprintf(f,'non_laminar = %d\t\t nu = %f\t\t headx = %f\t\t heady = %f\n\n',dns.nonLaminar, dns.nu, dns.headx, dns.heady);
fprintf(f,'walls = %d\t\t spacing = %f\t ratiohs = %.12f\n',dns.walls, rib.s_h, channel/rib.s_k);
fprintf(f,'waven = %d\n',rib.wave_n);
fprintf(f,'lambda\t= ');
if length(rib.lambda) ~= 1
    fprintf(f,'%f \t',rib.lambda(1:end-1));
    fprintf(f,'%f \n',rib.lambda(end));
else
    fprintf(f,'%f \n',rib.lambda(1));
end
fprintf(f,'ampl\t= ');
if length(rib.lambda) ~= 1
    fprintf(f,'%f \t',rib.amp(1:end-1));
    fprintf(f,'%f \n\n',rib.amp(end));
else
    fprintf(f,'%f \n\n',rib.amp(1));
end
fprintf(f,'use_coco = %d\t\t radius = %f\t stokexp = %.12f\t extra = %d\n',dns.coco, dns.radius, rib.stokexp, dns.extra);
fprintf(f,'phiC\t= ');
fprintf(f,'%f \t',coeff.coeff_phi(1:end-1));
fprintf(f,'%f \n',coeff.coeff_phi(end));
fprintf(f,'gC\t= ');
fprintf(f,'%f \t',coeff.coeff_gamma(1:end-1));
fprintf(f,'%f \n\n',coeff.coeff_gamma(end));
fprintf(f,'tip_radius = %.12f\t\t zCirc = %.12f\t\t yTan = %.12f\t\t eqHeight = %.12f\t\t s_t = %.12f\n\n',rib.r, rib.zC, rib.yTan, grid.eqHeight, rib.s_t);
fprintf(f,'nx = %d\t\t ny = %d\t\t nz = %d\t\t\t Lx = %f\t\t Ly = %f\n',grid.nx, grid.ny, grid.nz, dns.Lx, dns.Ly);
fprintf(f,'cflmax = %g\t\t fouriermax = %g\t deltat = %f\t\t maxTime = %g\n',dns.cflmax, dns.fouriermax, dns.dt, dns.maxTime);
fprintf(f,'it_save=%d\t\t it_stat=%d\t\t it_max=%d\t\t\t it_cfl=%d\t\t it_check=%d\n',dns.it_save, dns.it_stat,dns.it_max,dns.it_cfl,dns.it_check);
fprintf(f,'it_runtimestats=%d\t it_start=%d\t\t it_end=%d\t\t\t iz_stats=%d\n',dns.it_runtimestats,dns.it_start,dns.it_end,dns.iz_stats);
fprintf(f,'continue=FALSE\n');
fclose(f);
% clear f



%% Tschüß
% ---------------------------
disp(' ')
disp('Remember to check:  ')
disp('   1. that all input parameters are correct and as desired')
disp('   2. that the grid looks fine')
disp('   3. that the definition of the body and edge in riblets.cpl is as intended,')
disp('      in fact it is implemented for triangular riblets at the moment but can easily modified.')
disp('   4. that the HPC batch job scripts match what input in parallelbcs.h')
disp(' ')

disp('Tschüß!')
