clear
clc
% close all

% % primo indice field: pressione(1), u(2), v(3), w(4) (penso)
% % secondo indice field: distanza da parete
% % terzo indice field: coordinata spanwise
% % ultimo indice field: coordinata streamwise

% Example of a postprocessing: it just reads a file at the moment

% figure()
% pcolor(dns.yc(3:end-2),dns.zc(5:end-25),v(5:end-25,:))
% shading interp
% axis equal
% colorbar()

addpath('./base/')
[field, dns] = readfield('../fields/ribletsTurb.field');

p = squeeze(field(1,:,:,3));
u = squeeze(field(2,:,:,3));
v = squeeze(field(3,:,:,3));
w = squeeze(field(4,:,:,3));

figure(); hold on; axis equal
pcolor(dns.yc(3:end-2),dns.zc,u)
shading interp
axis equal
colorbar()

% myString1 = strcat('../fields/riblets_test', num2str(200));
% [field, dns] = readfield('../fields/riblets_test1.field');

% s = 0.08*6.25;
% height = s*cos(30*pi/180);
% y1 = linspace(0,s/2,100);
% y2 = linspace(s/2,s,100);
% [Y,Z] = meshgrid(dns.yc(3:end-2),dns.zc);
% 
% for i = 1:10
% 
%     myString1 = strcat('../fields/riblets_test', num2str(i*50));
%     myString2 = '.field';
%     finalString = [myString1 myString2];
%     [field, dns] = readfield(finalString);
% 
%     v = squeeze(field(3,:,:,3));
%     w = squeeze(field(4,:,:,3));
% 
%     mag = (v.^2+w.^2).^(0.5);
% 
%     figure(i); hold on
%     axis equal
%     contourf(Y(5:end-12,:),Z(5:end-12,:),mag(5:end-12,:),100,'LineColor','none')
%     h = streamslice(Y(5:end-12,:),Z(5:end-12,:),v(5:end-12,:),w(5:end-12,:));
% %     contourf(Y,Z,mag,100,'LineColor','none')
% %     h = streamslice(Y,Z,v,w);
%     plot(y1,tan(60*pi/180)*y1-height/2,'k','linewidth',3)
%     plot(y2,-tan(60*pi/180)*y1+height/2,'k','linewidth',3)
% %     plot(Y,Z,'o')
%     set(h,'Color','k')
%     clim([0 1e-5])
%     colormap(flipud(hot))
%     colorbar()
% 
% end