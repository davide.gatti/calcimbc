function var_new=fieldGenerator(filenameout,filenamein,dns,grid)

% STARTING FIELD CREATION FROM RIBLETS_INIZIALE.FIELD

i = fopen(filenamein,'r');
nx = fread(i,1,'int');
ny = fread(i,1,'int');
nz = fread(i,1,'int');
it = fread(i,1,'int');
Lx = fread(i,1,'double');
Ly = fread(i,1,'double');
headx = fread(i,1,'double');
heady = fread(i,1,'double');
nu = fread(i,1,'double');
dt = fread(i,1,'double');
zd=fread(i, 2*nz+1, 'double');
var=reshape(fread(i, 'double'), [4, nz+1, ny, nx]);
disp('    Input field read')

x_new = grid.x;
y_new = grid.y;
z_new = grid.z;

x_original = linspace(0,Lx,nx)*x_new(end)/Lx;
y_original = linspace(0,Ly,ny)*y_new(end)/Ly;
z_original = zd(1:2:end)'; 

[X_original, Y_original, Z_original] = meshgrid(x_original, y_original, z_original);
[X_new, Y_new, Z_new] = ndgrid(x_new, y_new, z_new);

nxn=grid.nx;
nyn=grid.ny;
nzn=grid.nz;

var_new=zeros(4,nzn+1,nyn,nxn);

if dns.laminar == false

    for i = 1:4
        f_initial = permute(squeeze(var(i,:,:,:)), [2, 3, 1]);
        var_new(i,:,:,:) = permute(interp3(X_original, Y_original, Z_original, f_initial, X_new, Y_new, Z_new, 'linear', 0), [3, 2, 1]);
    end
    
else

    if dns.couette == false
    	laminar_profile = -dns.headx*0.5/dns.nu*zz_new.*(zz_new - zz_new(end)*2); %parabola
    else
        if dns.walls == 2 
            laminar_profile(1:length(grid.zc)/2) = grid.zc(1:length(grid.zc)/2);
            laminar_profile(length(grid.zc)/2+1:length(grid.zc)) = fliplr(grid.zc(1:length(grid.zc)/2));
        else
            laminar_profile = grid.zc;
        end
    end
    for i = 1:nyn
        for j = 1:nxn
                var_new(2,:,i,j) = laminar_profile;
                var_new(3,:,i,j) = laminar_profile;
        end
    end
    
end

% WRITE FILE
d = fopen(filenameout,'w');
it=0;
domain = [nxn,nyn,nzn,it]';
headx=dns.headx;
heady=dns.heady;
nu=dns.nu;
deltat=dns.dt;
Lx=dns.Lx;
Ly=dns.Ly;
inputs= [Lx,Ly,headx,heady,nu,deltat];
fwrite(d,domain,'int');
fwrite(d,inputs,'double');
fwrite(d,grid.x_v_periodic,'double');
fwrite(d,grid.y_v_periodic,'double');
fwrite(d,grid.zd,'double');
fwrite(d,var_new,'double');
fclose(d);
disp('    Field written')

