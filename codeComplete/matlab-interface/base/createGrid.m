function grid = createGrid(dns,rib)

if rib.s_h == 0
    rib.s_h = 0.08;
end

meshx = @(x,deltax) x+0.5*deltax;
meshy = @(x,yold,deltay,radius,s,yratio,slope) x-yold-0.5*deltay*(1-(1-1/yratio)/(1+exp(-slope*(-abs(yold+x-s)/2+radius))));
% meshz = @(x,zold,deltaz,tip,zstr,radius,yratio,slope,slope_long,point_long) x-zold-0.5*deltaz*(1+zstr/(1+exp(-slope_long*((x+zold)/2-point_long)))-(1-1/yratio)/(1+exp(-slope*(-abs(zold+x-tip)/2+radius))));
meshz = @(x,zold,deltaz,tip,zstr,radius,yratio,slope,slope_long,point_long) x-zold-0.5*deltaz*(1+zstr/(1+exp(-slope_long*((x+zold)/2-point_long))));

xold = 0;
xnew = 0;

yold = 0;
ynew = 0;

zold = -rib.s_h*sqrt(3)/2/2;
znew = -rib.s_h*sqrt(3)/2/2;

x_v = 0;
y_v_rib = 0;
z_v_half = -rib.s_h*sqrt(3)/2/2;
dx_v = [];
dy_v = [];
dz_v = [];

ix = 1;
iy = 1;
iz = 1;

xratio = 3;
yratio = 10;

while xnew < dns.Lx || mod(ix,4) ~= 1
    if dns.xstretch
        xnew = fsolve(@(x) meshx(x,0.5*dns.dx),xold,rib.options);
    else
        xnew = xold+dns.dx/2;
    end
    x_v(ix+1) = xnew;
    dx_v(ix) = xnew-xold;
    xold = xnew;
    ix = ix+1;
end

while ynew < rib.s_h/2 || mod(iy,4) ~= 1
    if dns.ystretch
        ynew = fsolve(@(x) meshy(x,yold,dns.dy,dns.radius,rib.s_h,25,700),yold,rib.options); % era 2 -> 25, ora 0.5 -> 6
    else
        ynew = yold+dns.dy/2;
    end
    y_v_rib(iy+1) = ynew;
    dy_v(iy) = ynew-yold;
    yold = ynew;
    iy = iy+1;
end

while znew < 1 || mod(iz,2) == 1
    if dns.zstretch
        znew = fsolve(@(x) meshz(x,zold,dns.dz,rib.k_h/2,dns.zstr,dns.radius,25,600,10,0.7),zold,rib.options);
%         znew = fsolve(@(x) meshz(x,zold,dns.dz,rib.k_h/2,dns.zstretch,dns.radius,1,600,10,0.7),zold,rib.options);
    else
        znew = zold+dns.dz/2;
    end
    z_v_half(iz+1) = znew;
    dz_v(iz) = znew-zold;
    zold = znew;
    iz = iz+1;
end

if x_v(end) ~= dns.Lx
    cx = sum(dx_v)/dns.Lx;
    dx_v = dx_v/cx;
end

if y_v_rib(end) ~= rib.s_h/2
    cy = sum(dy_v)/(rib.s_h/2);
    dy_v = dy_v/cy;
end

if z_v_half(end) ~= 1
    cz = sum(dz_v)/(1+rib.s_h*sqrt(3)/2/2);
    dz_v = dz_v/cz;
end

for i = 1:length(dx_v)
    x_v(i+1) = x_v(i)+dx_v(i);
end

for i = 1:length(dy_v)
    y_v_rib(i+1) = y_v_rib(i)+dy_v(i);
end

for i = 1:length(dz_v)
    z_v_half(i+1) = z_v_half(i)+dz_v(i);
end

y_v_rib = [y_v_rib, 2*y_v_rib(end)-fliplr(y_v_rib(1:end-1))];

if dns.Ly ~= rib.s_h
    y_v = [y_v_rib(1), repmat(y_v_rib(2:end), 1, dns.Ly/rib.s_h) + kron(0:dns.Ly/rib.s_h-1, rib.s_h*ones(1, numel(y_v_rib)-1))];
else
    y_v = y_v_rib;
end

if dns.walls == 2
    z_v = zeros(1,2*length(z_v_half)-1);
    z_v(1:length(z_v_half)) = z_v_half;
    for i = 1:length(z_v_half)-1
        z_v(length(z_v_half)+i) = 1+(1-z_v_half(end-i));
    end
else
    z_v = zeros(1,length(z_v_half)+1);
    z_v(1:length(z_v_half)) = z_v_half;
    z_v(end) = 2*z_v(end-1)-z_v(end-2);
end

grid.dy_min = 2*min(dy_v)*dns.re;
grid.dy_max = 2*max(dy_v)*dns.re;
grid.dz_min = 2*min(dz_v)*dns.re;
grid.dz_max = 2*max(dz_v)*dns.re;

grid.nx = numel(x_v(1:2:end))-1;
grid.ny = numel(y_v(1:2:end))-1;

x_v_periodic = [x_v(1)-(x_v(end)-x_v(end-2)), x_v(1)-(x_v(end)-x_v(end-1)), x_v];
x_v_periodic = [x_v_periodic, x_v_periodic(end)+(x_v(2)-x_v(1)), x_v_periodic(end)+(x_v(3)-x_v(1)), x_v_periodic(end)+(x_v(4)-x_v(1)), x_v_periodic(end)+(x_v(5)-x_v(1)), x_v_periodic(end)+(x_v(6)-x_v(1))];

y_v_periodic = [y_v(1)-(y_v(end)-y_v(end-2)), y_v(1)-(y_v(end)-y_v(end-1)), y_v];
y_v_periodic = [y_v_periodic, y_v_periodic(end)+(y_v(2)-y_v(1)), y_v_periodic(end)+(y_v(3)-y_v(1)), y_v_periodic(end)+(y_v(4)-y_v(1)), y_v_periodic(end)+(y_v(5)-y_v(1)), y_v_periodic(end)+(y_v(6)-y_v(1))];

grid.x = x_v(3:2:end);
grid.y = y_v(3:2:end);
grid.z = z_v(1:2:end);

grid.x_v_periodic = x_v_periodic;
grid.y_v_periodic = y_v_periodic;

grid.zd = z_v;
grid.zc = grid.zd(1:2:end);
grid.nz = numel(grid.zc)-1;
