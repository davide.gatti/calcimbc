function dns = wallNormalGrid_geometric(dns)

% The wall-normal grid will go from -k/2 to 1, so that the 
% meltdown height is exactly 1. For the wall-normal grid I use an
% equispaced grid until the riblet tip and then a stretching through a
% geometric series. The stretching ratio will be slightly adjusted, so that
% we get an integer number of points in the wall-normal direction. All
% support variable I need for the grid are saved in the structure tmp,
% which I will delete at the end.
% 
tmp.nz_rib 	= ceil(2*rib.k_h*dns.retau/dns.dzmin);

%if mod(tmp.nz_rib,2) == 0
%    dns.dzmin = dns.dzmin*(tmp.nz_rib + 1)/tmp.nz_rib;
%    dns.cellAR = dns.dyp/dns.dzmin;
%end

if mod(tmp.nz_rib,2) == 0
	tmp.nz_rib = tmp.nz_rib + 1;
end
dns.nz_rib = tmp.nz_rib;
dns.dzmin = (2*dns.retau*(rib.k_h - 1/dns.retau*(0.15))/tmp.nz_rib);%*0.95; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dns.cellAR = dns.dyp/dns.dzmin;

tmp.z0 		= -rib.k_h/2+(tmp.nz_rib*dns.dzmin*0.5)/dns.retau;
tmp.z1 		= 1;
tmp.L 	= (tmp.z1-tmp.z0)*dns.retau;
tmp.dzmax = 0.5*dns.dzmin*dns.zstretch;
tmp.beta = (tmp.L-0.5*dns.dzmin)/(tmp.L-tmp.dzmax);
tmp.n = ceil(1+log(dns.zstretch)/log(tmp.beta));
tmp.n = tmp.n + mod(tmp.n + tmp.nz_rib,2);
tmp.fun = @(x,n,L,dzmin) n-1-log(x)/log((L-dzmin)/(L-dzmin*x));
tmp.r = fsolve( @(x) tmp.fun(x,tmp.n-1,tmp.L,0.5*dns.dzmin), dns.zstretch,options);
tmp.beta = tmp.r^(1/(tmp.n-2));
tmp.nz = tmp.nz_rib+tmp.n;              
dns.dzmax = dns.dzmin*tmp.r;            % maximum grid spacing in the wall-normal direction
dns.zd = zeros(tmp.nz+1,1);             % grid in the wall-normal direction (all nodes)
for i=0:tmp.nz_rib
    dns.zd(i+1)=(0.5*i*dns.dzmin-rib.k_h/2*dns.retau)/dns.retau;
end
for i=tmp.nz_rib+1:tmp.nz
    j=i-tmp.nz_rib-1;
    tmp.d = (0.5*dns.dzmin)*tmp.beta^j;
    dns.zd(i+1)=dns.zd(i)+tmp.d/dns.retau;
end

% tmp.dzmin_ph = dns.dzmin/dns.retau;         % minimum delta_z in physical units
% tmp.nz_rib 	= ceil(1.5*rib.k_h/tmp.dzmin_ph);   % (ceil-ed) number of
% % points in wall-normal direction below (safety factor)*h_riblet 
% 
% tmp.zstart	= tmp.dzmin_ph*tmp.nz_rib - rib.k_h/2;
% tmp.zend	= 1;
% tmp.L       	= tmp.zend-tmp.zstart;
% tmp.below 	= -rib.k_h/2:tmp.dzmin_ph:tmp.zstart;
% % dns.exp_ratio = 1.0035;
% % zzd = expansion (dns.exp_ratio, tmp.below, tmp.dzmin_ph, tmp.zend);
% % 
% % iinc = 0.00001;
% % inc = 0.00001;
% % parit = mod(numel(zzd),2);
% % while parit == 0
% %     zzd = expansion (dns.exp_ratio + inc*(-1)^(inc/iinc), tmp.below, tmp.dzmin_ph, tmp.zend);
% %     inc = inc + iinc;
% %     parit = mod(numel(zzd),2);
% % end
% % 
% % dns.zd = zzd;
% 
% tmp.fun = @(x) tmp.L - tmp.dzmin_ph*(1 - dns.zstretch^(x/(x-1)))/(1 - dns.zstretch^(1/(x -1)));
% tmp.n = ceil(fsolve(tmp.fun, tmp.nz_rib, options));
% tmp.n = tmp.n + mod(tmp.n + tmp.nz_rib,2);
% tmp.beta = dns.zstretch^(1/(tmp.n-1));
% tmp.nz = tmp.nz_rib+tmp.n;              
% % dns.dzmax = dns.dzmin_ph*tmp.dns.zstretch;            % maximum grid spacing in the wall-normal direction
% dns.zd = zeros(tmp.nz+1,1);             % grid in the wall-normal direction (all nodes)
% 
% for i=0:tmp.nz_rib
%     dns.zd(i+1)=tmp.below(i+1);
% end
% for i=tmp.nz_rib+1:tmp.nz
%     j=i-tmp.nz_rib-1;
%     tmp.d = (tmp.dzmin_ph)*tmp.beta^j;
%     dns.zd(i+1)=dns.zd(i)+tmp.d;
% end
% 
dns.zd(end-1)=1.0;
dns.zd(end)=dns.zd(end-1)+(dns.zd(end-1)-dns.zd(end-2));


% dns.zc = dns.zd(1:2:end);               % grid in wall-normal direction (w nodes)
% dns.nz = numel(dns.zc)-1;               % number of points in wall-normal direction
% % dns.zd
% % numel(dns.zd)
% % return
% where to end the stretch (default = 1)
dns.zd(end-1)=1.0; dns.zd(end)=dns.zd(end-1)+(dns.zd(end-1)-dns.zd(end-2));
dns.zc = dns.zd(1:2:end);               % grid in wall-normal direction (w nodes)
dns.nz = numel(dns.zc)-1;               % number of points in wall-normal direction

% close all
% figure(11)
% subplot(2,1,1)
% plot(dns.zd)
% grid on
% xlabel('i')
% ylabel('z/h')
% subplot(2,1,2)
% grid on
% hold on
% plot(dns.zc(2:end)*dns.retau/dns.retau,diff(dns.zc)*dns.retau)
% if strcmp(dns.domain, 'minimal')
%     plot([1 1]*dns.zcp, [1 dns.zstretch])
% end
% xlim([dns.zd(1) dns.zd(end)]*dns.retau/dns.retau)
% xlabel('z/h')
% ylabel('dz+')