function coeff = calcCocoCoeff(rib)

coeff.beta = zeros(10000,1);
coeff.phi = zeros(10000,1);

coeff.findEdge = @(x,xp,yp,A,lambda,ae) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-ae);
coeff.findValley = @(x,xp,yp,A,lambda,av) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-av);

for i = 1:length(rib.max_beta(1:rib.final_index))
    if rib.max_beta(i) ~= 0
        coeff.A = rib.amp(i);
    end
end

coeff.A = 1.1677582641186;
coeff.lambda = rib.lambda(i);
coeff.ae = rib.s_h/2;
coeff.av = coeff.ae-rib.s_h/2;

for ii = 1:10000

    coeff.xp = coeff.lambda/10000*ii;
    coeff.yp = coeff.A*sin(2*pi/coeff.lambda*coeff.xp)+0.5*(coeff.ae+coeff.av)+(rand-0.5)*2*rib.s_h;
    
    coeff.edgex = fsolve(@(x) coeff.findEdge(x,coeff.xp,coeff.yp,coeff.A,coeff.lambda,coeff.ae), coeff.xp, rib.options); 
    coeff.edgey = coeff.A*sin(2*pi/coeff.lambda*coeff.edgex)+coeff.ae;

    coeff.vallx = fsolve( @(x) coeff.findValley(x,coeff.xp,coeff.yp,coeff.A,coeff.lambda,coeff.av), coeff.xp, rib.options);
    coeff.vally = coeff.A*sin(2*pi/coeff.lambda*coeff.edgex)+coeff.av;

    coeff.beta(ii) = atan((coeff.edgex-coeff.vallx)/(coeff.vally-coeff.edgey));
    coeff.phi(ii) = 2*atan(abs(coeff.edgey-coeff.vally)/cos(coeff.beta(ii))/rib.k_h);

end

coeff.coeff_phi = polyfit(coeff.beta,coeff.phi,6);


% COEFFICIENTS TO FIND STOKES EXPONENT FOR SINUSOIDAL RIBLETS

coeff.input_angle = linspace(10*pi/180,110*pi/180,10000);
coeff.stokesexp = zeros(1,10000);

coeff.stokdisp = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);

for ii = 1:10000
    coeff.phiwall = pi-0.5*coeff.input_angle(ii);
    coeff.stokesexp(ii) = fsolve( @(x) coeff.stokdisp(x,coeff.phiwall), 0.51, rib.options);
end

coeff.coeff_gamma = polyfit(coeff.input_angle,coeff.stokesexp,6);
