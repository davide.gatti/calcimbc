clc

dns.filenamein = '../../../initialFields/riblets_iniziale.field';
filenameout = '../fields/riblets.field';

% STARTING FIELD CREATION FROM RIBLETS_INIZIALE.FIELD

i = fopen(dns.filenamein,'r');
nx = fread(i,1,'int');
ny = fread(i,1,'int');
nz = fread(i,1,'int'); % questo nz salvato nel field conta in realta' un elemento in meno della reale dimensione del field
it = fread(i,1,'int');
Lx = fread(i,1,'double');
Ly = fread(i,1,'double');
headx = fread(i,1,'double');
heady = fread(i,1,'double');
nu = fread(i,1,'double');
dt = fread(i,1,'double');
zd=fread(i, 2*nz+1, 'double');

var=reshape(fread(i, 'double'), [4, nz+1, ny, nx]);



x=linspace(0,Lx,nx+1); x=x(1:end-1);   % queste 3 righe creano vettori x, y, z da quello che vedono nel campo iniziale, input nuovi non ancora visti
y=linspace(0,Ly,ny+1); y=y(1:end-1);
z = zd(1:2:end)'; 

%%


if z(end-1)<1.5 
    zvec = [z(1:end-1)*(1-dns.zd(1))+dns.zd(1), 2-(z(end-1:-1:1)*(1-dns.zd(1))+dns.zd(1))];
    indx = [(1:(numel(z)-1)), ((numel(z)-1):-1:1)];
else
    zvec = z;
    indx = 1:numel(z);
end

dns.laminar = false;


if dns.laminar == false

    var = fft(fft(var,[],4),[],3);

    
    nxn=dns.nx;
    nyn=dns.ny;
    nzn=dns.nz;

    zz_new=dns.zc;
    z_new=dns.zd;
    var_new=zeros(4,nzn+1,nyn,nxn);
    
    
    iiix = [0:floor(nx/2), -floor(nx/2)+1-mod(nx,2):-1];
    iiiy = [0:floor(ny/2), -floor(ny/2)+1-mod(ny,2):-1];
    
    for iV=1:4
        for ix=1:floor(min(nx,nxn)/2)+1
            if iiix(ix)<0
                IX=nxn+iiix(ix)+1;
            else
                IX=iiix(ix)+1;
            end
%           ny
%           nyn
            for iy=1:min(ny,nyn)
                if iiiy(iy)<0
                    IY=nyn+iiiy(iy)+1;
%                 else
%                     IY=iiiy(iy)+1;
                end
%                 if iV==4 || iV==1
%                     vvec = [squeeze(var(iV,1:end-1,iy,ix))', -squeeze(var(iV,end-1:-1:1,iy,ix))'];
%                 else
%                     vvec = var(iV,indx,iy,ix);
%                 end
%                 var_new(iV,:,IY,IX) = interp1(zvec,var(iV,indx,iy,ix),zz_new,'linear',0);
            end
        end
%         disp(['    Component ' num2str(iV) ' interpolated'])
    end
%     
%     var_new = (ifft(ifft(var_new,[],3),[],4,'symmetric'))*nxn*nyn/nx/ny;
%     var_new = real(var_new);
%     
%     if dns.walls==1
%         var_new([1,4],:,:,end-1:end)=0;
%         var_new(:,:,:,1)=0;
%     else
%         var_new([1,4],:,:,end-1:end)=0;
%         var_new([2,3],:,:,end)=0;
%         var_new(:,:,:,1)=0;
%     end
    
else
   
    
    nxn=dns.nx;
    nyn=dns.ny;
    nzn=dns.nz;
    zz_new=dns.zc - dns.zc(1);
    z_new=dns.zd;
    
    if dns.couette == false
    	laminar_profile = -dns.headx*0.5/dns.nu*zz_new.*(zz_new - zz_new(end)*2); %parabola
    else
	laminar_profile = zz_new/zz_new(end);
    end

    var_new=zeros(4,nzn+1,nyn,nxn);
    for i = 1:nyn
        for j = 1:nxn
		var_new(2,:,i,j) = laminar_profile;
		var_new(3,:,i,j) = laminar_profile;
        end
    end
    
end


%% WRITE FILE
d = fopen(filenameout,'w');
it=0;
domain = [nxn,nyn,nzn,it]';
headx=dns.headx;
heady=dns.heady;
nu=dns.nu;
deltat=dns.dt;
Lx=dns.Lx;
Ly=dns.Ly;
inputs= [Lx,Ly,headx,heady,nu,deltat];
fwrite(d,domain,'int');
fwrite(d,inputs,'double');
fwrite(d,z_new,'double');
fwrite(d,var_new,'double');
fclose(d);
disp('    Field written')
