clc
clear
close all

lam = 7.5;
ampl = 0.521044976361029;
s = 0.08;
h = s*cos(30*pi/180);
amp = ampl*s;

M = readmatrix('../COCO.txt');

x = linspace(0,M(end-25,1),240);

num = zeros(length(x),1);

for i = 1:length(x)

    for j = 1:length(M(1:end-21,1))

        if x(i) == M(j,1)

            num(i) = num(i)+1;

        end

    end

end

n = max(num);
yM = zeros(length(x),n);
z1M = zeros(length(x),n);
z2M = zeros(length(x),n);

for i = 1:length(x)

    yM(i,1:num(i)) = M(sum(num(1:i-1))+1:sum(num(1:i)),2);
    z1M(i,1:num(i)) = M(sum(num(1:i-1))+1:sum(num(1:i)),3);
    z2M(i,1:num(i)) = M(sum(num(1:i-1))+1:sum(num(1:i)),4);

end


y1 = linspace(0,s/2,100);
y2 = linspace(s/2,s,100);
y3 = linspace(s,3*s/2,100);
y4 = linspace(3*s/2,2*s,100);
y5 = linspace(-s,-s/2,100);
y6 = linspace(-s/2,0,100);

b1y = zeros(100,1);
b2y = 0.08*ones(100,1);
bz = linspace(-0.05,0.05,100);

for i = 1:10

    deltay = amp*sin(2*pi*x(i)/lam);

    yy1 = linspace(deltay,s/2+deltay,100);
    yy2 = linspace(s/2+deltay,s+deltay,100);
    yy3 = linspace(s+deltay,3*s/2+deltay,100);
    yy4 = linspace(3*s/2+deltay,2*s+deltay,100);
    yy5 = linspace(-s+deltay,-s/2+deltay,100);
    yy6 = linspace(-s/2+deltay,deltay,100);

    m1 = tan(60*pi/180);
    m2 = -tan(60*pi/180);
    m3 = tan(60*pi/180);
    m4 = -tan(60*pi/180);
    m5 = tan(60*pi/180);
    m6 = -tan(60*pi/180);
    q1 = h/2-tan(60*pi/180)*(s/2+deltay);
    q2 = h/2+tan(60*pi/180)*(s/2+deltay);
    q3 = h/2-tan(60*pi/180)*(3*s/2+deltay);
    q4 = h/2+tan(60*pi/180)*(3*s/2+deltay);
    q5 = h/2+tan(60*pi/180)*(s/2-deltay);
    q6 = h/2-tan(60*pi/180)*(s/2-deltay);

    figure(i); grid on; hold on;
    plot(yy1,m1*yy1+q1,'k','linewidth',1)
    plot(yy2,m2*yy2+q2,'k','linewidth',1)
    plot(yy3,m3*yy3+q3,'k','linewidth',1)
    plot(yy4,m4*yy4+q4,'k','linewidth',1)
    plot(yy5,m5*yy5+q5,'k','linewidth',1)
    plot(yy6,m6*yy6+q6,'k','linewidth',1)
    plot(yM(i,1:num(i)),z1M(i,1:num(i)),'o')
    plot(yM(i,1:num(i)),z2M(i,1:num(i)),'o')
    plot(b1y,bz,'g','linewidth',2);
    plot(b2y,bz,'g','linewidth',2)
    axis equal

end







