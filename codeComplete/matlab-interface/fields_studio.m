clear
clc
close all

addpath('./base/')
fields_name = dir('../fields/*.field');
field_file = fields_name(1).name;
[field,dns] = readfield("../fields/" + field_file);
s = 0.08;
dns.ppr = 16;
u = zeros(dns.ppr,length(dns.zc)/2);
v = zeros(dns.ppr,length(dns.zc)/2);
w = zeros(dns.ppr,length(dns.zc)/2);
u_2d = zeros(length(dns.ny),length(dns.zc));
v_2d = zeros(length(dns.ny),length(dns.zc));
w_2d = zeros(length(dns.ny),length(dns.zc));

clear field_file
clear field
clear dns

for f=1:length(fields_name)

    field_file = fields_name(f).name;

    [field, dns] = readfield("../fields/" + field_file);

    u_2d = u_2d+squeeze(mean(permute(squeeze(field(2,:,:,:)),[3 2 1])));
    v_2d = v_2d+squeeze(mean(permute(squeeze(field(3,:,:,:)),[3 2 1])));
    w_2d = w_2d+squeeze(mean(permute(squeeze(field(4,:,:,:)),[3 2 1])));

end

u_2d = u_2d/length(fields_name);
v_2d = v_2d/length(fields_name);
w_2d = w_2d/length(fields_name);

for i = 1:length(dns.ny)

    for j = 1:dns.ppr

        if mod(i-j,dns.ppr) == 0

            u(j,:) = u(j,:)+u_2d(i,:);
            v(j,:) = v(j,:)+v_2d(i,:);
            w(j,:) = w(j,:)+w_2d(i,:);

        end

    end

end

u = u/(length(dns.ny)/dns.ppr);
v = v/(length(dns.ny)/dns.ppr);
w = w/(length(dns.ny)/dns.ppr);




