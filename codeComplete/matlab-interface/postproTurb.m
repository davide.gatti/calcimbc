clear
clc
close all

% % primo indice field: pressione(1), u(2), v(3), w(4) (penso)
% % secondo indice field: distanza da parete
% % terzo indice field: coordinata spanwise
% % ultimo indice field: coordinata streamwise

addpath('./base/')
[field, dns] = readfield('../fields/riblets.field');

s = 0.08;
height = s*cos(30*pi/180);
y1 = linspace(0,s/2,100);
y2 = linspace(s/2,s,100);
[Y,Z] = meshgrid(dns.yc(3:end-2),dns.zc);

p = squeeze(field(1,:,:,3));
u = squeeze(field(2,:,:,3));
v = squeeze(field(3,:,:,3));
w = squeeze(field(4,:,:,3));

figure()
pcolor(dns.yc(3:end-2),dns.zc,u)
shading interp
axis equal
colorbar()

figure()
pcolor(dns.yc(3:end-2),dns.zc,v)
shading interp
axis equal
colorbar()

figure()
pcolor(dns.yc(3:end-2),dns.zc,w)
shading interp
axis equal
colorbar()

figure()
pcolor(dns.yc(3:end-2),dns.zc,p)
shading interp
axis equal
colorbar()