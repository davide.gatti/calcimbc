import matplotlib.pyplot as plt
data = {}

for i in range(1, 10):
    folder_name = f"./mesh{i}"
    
    # Read 'protrusionHeightsResults.txt' file
    results_file_path = f"{folder_name}/protrusionHeightsResults.txt"
    with open(results_file_path, 'r') as results_file:
        lines = results_file.readlines()
        l_parallel = float(lines[0].split('=')[1].strip().split()[0])
        l_perpendicular = float(lines[1].split('=')[1].strip().split()[0])
        error_parallel = float(lines[0].split('=')[2].strip('()%\n'))
        error_perpendicular = float(lines[1].split('=')[2].strip('()%\n'))

    # Read 'mesh_stats.txt' file
    stats_file_path = f"{folder_name}/mesh_stats.txt"
    with open(stats_file_path, 'r') as stats_file:
        lines = stats_file.readlines()
        mesh_parameter = float(lines[0].split(': ')[1].strip())
        n_triangles = int(lines[1].split(': ')[1].strip())
    
    # Store data for the current index
    data[i] = {
        'l_parallel': l_parallel,
        'l_perpendicular': l_perpendicular,
        'error_parallel': error_parallel,
        'error_perpendicular': error_perpendicular,
        'mesh_parameter': mesh_parameter,
        'n_triangles': n_triangles,
    }


#for i, values in data.items():
#    print(f"Index {i}: {values}")
# Extract data for plotting
indices = sorted(data.keys())
n_triangles = [data[i]['n_triangles'] for i in indices]
errors_parallel = [data[i]['error_parallel'] for i in indices]
errors_perpendicular = [data[i]['error_perpendicular'] for i in indices]

# Plotting
plt.figure(figsize=(10, 6))
plt.plot(n_triangles, errors_parallel, marker='o', label='Error (l_parallel)')
plt.plot(n_triangles, errors_perpendicular, marker='o', label='Error (l_perpendicular)')

# Adding labels and title
plt.xlabel('N Triangles')
plt.ylabel('Error (%)')
plt.title('Errors vs. N Triangles for l_parallel and l_perpendicular')
plt.legend()
plt.grid(True)

# Show the plot
plt.show()
