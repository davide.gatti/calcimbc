// Mesh parameter: 0.007, Riblet Height: 0.0346, Riblet Spacing: 0.01
// Riblet parameters
h_s = 0.866025403784;
s = 0.03;
h = h_s*s;
p = 0.0085;
// Domain Definition
Point(1) = {0.0, -h/2, 0, p};
Point(2) = {s, -h/2, 0, p};
Point(3) = {s, 1, 0, p};
Point(4) = {0, 1, 0, p};
// Riblet Definition
Point(5) = {s/2, h/2, 0, p};
// Riblet
Line(1) = {1, 5};
Line(2) = {5, 2};
// Domain
Line(3) = {2, 3};
Line(4) = {3, 4};
Line(5) = {4, 1};
// Boundary
Curve Loop(1) = {1, 2, 3, 4, 5};

Plane Surface(1) = {1};
// Line tags for boundary conditions
Physical Curve("body", 6) = {1, 2};
Physical Curve("right", 7) = {3};
Physical Curve("top", 8) = {4};
Physical Curve("left", 9) = {5};
Physical Surface("volume", 10) = {1};
//Fields
Field[1] = Box;
Field[1].Thickness = 2;
Field[1].VIn = p/5;
Field[1].VOut = p;
Field[1].XMax = s;
Field[1].XMin = 0.0;
Field[1].YMax = 2*h;
Field[1].YMin = -1;

Field[2] = Box;
Field[2].Thickness = 0.3;
Field[2].VIn = p/25;
Field[2].VOut = p;
Field[2].XMax = 0.75*s;
Field[2].XMin = 0.25*s;
Field[2].YMax = 1.25*h;
Field[2].YMin = -h/4;

Field[3] = Box;
Field[3].Thickness = 0.3;
Field[3].VIn = p/125;
Field[3].VOut = p;
Field[3].XMax = 1.05*s/2;
Field[3].XMin = 0.95*s/2;
Field[3].YMax = 0.55*h;
Field[3].YMin = 0.45*h;

Field[4] = Box;
Field[4].Thickness = 0.3;
Field[4].VIn = p/2000;
Field[4].VOut = p;
Field[4].XMax = 1.005*s/2;
Field[4].XMin = 0.995*s/2;
Field[4].YMax = 0.505*h;
Field[4].YMin = 0.495*h;

Field[5] = Min;
Field[5].FieldsList = {1, 2, 3, 4};
Background Field = 5;

Mesh.Algorithm = 2;


