// Mesh parameter: 0.025, RIblet Spacing: 0.08
// Riblet parameters
p = 0.025;
h_s = 0.866025403784;
s = 0.08;
h = h_s*s;
xC = s/2;
yC = 0.030641016151361;
x1 = (0.5*h + (yC+ 0.5*s*xC/h))/(2*h_s+0.5*s/h);
x2 = xC + (xC - x1);
y1 = 2*h_s*x1 - 0.5*h;
// Domain Definition
Point(1) = {0.0, -h/2, 0, p};
Point(2) = {s, -h/2, 0, p};
Point(3) = {s, 1, 0, p};
Point(4) = {0, 1, 0, p};
// Riblet Definition
Point(5) = {x1, y1, 0, p};
Point(6) = {xC, yC, 0, p};
Point(7) = {x2, y1, 0, p};
// Riblet
Line(1) = {1, 5};
Line(2) = {7, 2};
Circle(6) = {5, 6, 7};
// Domain
Line(3) = {2, 3};
Line(4) = {3, 4};
Line(5) = {4, 1};
// Boundary
Curve Loop(1) = {1, 2, 3, 4, 5, 6};

Plane Surface(1) = {1};
// Line tags for boundary conditions
Physical Curve("body", 6) = {1, 2, 6};
Physical Curve("right", 7) = {3};
Physical Curve("top", 8) = {4};
Physical Curve("left", 9) = {5};
Physical Surface("volume", 10) = {1};
//Fields
Field[1] = Box;
Field[1].Thickness = 2;
Field[1].VIn = p/50;
Field[1].VOut = p;
Field[1].XMax = s;
Field[1].XMin = 0.0;
Field[1].YMax = 2*h;
Field[1].YMin = -1;

Field[2] = Box;
Field[2].Thickness = 0.3;
Field[2].VIn = p/75;
Field[2].VOut = p;
Field[2].XMax = 0.75*s;
Field[2].XMin = 0.25*s;
Field[2].YMax = 1.25*h;
Field[2].YMin = -h/4;

Field[3] = Box;
Field[3].Thickness = 0.3;
Field[3].VIn = p/100;
Field[3].VOut = p;
Field[3].XMax = 1.05*s/2;
Field[3].XMin = 0.95*s/2;
Field[3].YMax = 0.55*h;
Field[3].YMin = 0.45*h;



Field[4] = Min;
Field[4].FieldsList = {1, 2, 3};
Background Field = 4;

Mesh.Algorithm = 2;


