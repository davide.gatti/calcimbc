#import matplotlib.pyplot as plt
from riblet import *
import matplotlib.pyplot as plt
import numpy as np

#%% PARALLEL FLOW - PROTRUSION HEIGHTS EVALUATION 
dns = read_input('../input/riblets.in')
ratiohs = 0.866025403784
spacing = 0.08

#%%
stats2d = read_2dstats('../output/stats.bin',dns)

#%% Some statistics
stats = compute_1dstats(stats2d,dns)

#%% Plot mean velocity profile and extraction of protrusion heights
u_plot = np.mean(stats2d['U'], axis=0)
zz = stats2d['zz']
z_plot = np.linspace(zz[0], zz[-1], 10000)
sqr_err_min_u = 1000
j_opt_u = 0
ll = len(zz)
v_plot = np.mean(stats2d['V'], axis=0)
sqr_err_min_v = 1000
j_opt_v = 0





#%%

for j in range(2,ll-3):
    line_profile_u = np.polyfit(zz[j:],u_plot[j:],1)
    z_height_u = -line_profile_u[1]/line_profile_u[0]
    sqr_err_u = sum(np.power(u_plot[zz>-3*zz[0]] - zz[zz>-3*zz[0]]*line_profile_u[0] - line_profile_u[1],2))/len(zz[zz>-3*zz[0]])
    protr_height_u = abs(z_height_u - ratiohs*spacing/2)/spacing

    if sqr_err_u<sqr_err_min_u:
        sqr_err_min_u = sqr_err_u
        line_plot_u = z_plot*line_profile_u[0] + line_profile_u[1]
        j_opt_u = j
        protr_height_opt_u = protr_height_u

for j in range(2,ll-3):
    line_profile_v = np.polyfit(zz[j:],v_plot[j:],1)
    z_height_v = -line_profile_v[1]/line_profile_v[0]
    sqr_err_v = sum(np.power(v_plot[zz>-1*zz[0]] - zz[zz>-1*zz[0]]*line_profile_v[0] - line_profile_v[1],2))/len(zz[zz>-1*zz[0]])
    protr_height_v = abs(z_height_v - ratiohs*spacing/2)/spacing

    if sqr_err_v<sqr_err_min_v:
        sqr_err_min_v = sqr_err_v
        line_plot_v = z_plot*line_profile_v[0] + line_profile_v[1]
        j_opt_v = j
        protr_height_opt_v = protr_height_v
        
        
plt.figure()
plt.plot(zz,u_plot)
plt.plot(z_plot,line_profile_u[0]*z_plot+line_profile_u[1])
plt.plot(0.08*0.866025403784/2*np.array([1,1]),np.array([0,1]))
plt.plot(-0.08*0.866025403784/2*np.array([1,1]),np.array([0,1]))
plt.grid(True)

plt.figure()
plt.plot(zz,v_plot)
plt.plot(z_plot,line_profile_v[0]*z_plot+line_profile_v[1])
plt.plot(0.08*0.866025403784/2*np.array([1,1]),np.array([0,1]))
plt.plot(-0.08*0.866025403784/2*np.array([1,1]),np.array([0,1]))
plt.grid(True)



print("")
print("PARALLEL PH")
print("The minumun error occurs by starting the interpolation at z = ",zz[j_opt_u+5],)
print("Its value is ", sqr_err_min_u)
print("The parallel protrusion height is h = ",protr_height_opt_u)
print("The % error is: ", 100*(protr_height_opt_u-0.1715)/0.1715)
print("")

print("")
print("PERPENDICULAR PH")
print("The minumun error occurs by starting the interpolation at z = ",zz[j_opt_v+5],)
print("Its value is ", sqr_err_min_v)
print("The parallel protrusion height is h = ",protr_height_opt_v)
print("The % error is: ", 100*(protr_height_opt_v-0.08099)/0.08099)
print("")

print("")
print("delta_h = ",protr_height_opt_u-protr_height_opt_v)
print("The % error is: ", 100*(protr_height_opt_u-protr_height_opt_v-0.09051)/0.09051)
