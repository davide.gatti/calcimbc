# Packages and functions utilized
import matplotlib.pyplot as plt
import numpy as np
U = np.loadtxt('./riblets_code/finiteElements/mesh9/u_mean.txt')
V = np.loadtxt('./riblets_code/finiteElements/mesh9/v_mean.txt')
z = np.loadtxt('./riblets_code/finiteElements/mesh9/grid_y.txt')
print(type(U))
lz =  2*len(z)
spacing = 0.04
ratiohs = 0.866025403784
h = ratiohs*spacing
# Protrusion heights computation
indx_start = 0
err_min_U = 1000
err_min_V = 1000
for ii in range(0, int(lz/2)-indx_start):
    poly_U = np.polyfit(z[indx_start+ii:int(lz/2)], U[indx_start+ii:int(lz/2)], 1)
    poly_V = np.polyfit(z[indx_start+ii:int(lz/2)], V[indx_start+ii:int(lz/2)], 1)
    z_height_U = -poly_U[1]/poly_U[0]
    z_height_V= -poly_V[1]/poly_V[0]
    err_U = sum(np.power(U[:]-z[:]*poly_U[0] - poly_U[1], 2))/lz
    err_V = sum(np.power(V[:]-z[:]*poly_V[0] - poly_V[1], 2))/lz
    if err_U < err_min_U:
        ii_opt = ii
        err_min_U = err_U
        l_parallel = abs(z_height_U - ratiohs*spacing/2)/spacing
    if err_V < err_min_V:
        jj_opt = ii
        err_min_V = err_V
        l_perpendicular = abs(z_height_V- ratiohs*spacing/2)/spacing

delta_PH = l_parallel - l_perpendicular
poly_U_opt = np.polyfit(z[indx_start+ii_opt:int(lz/2)], U[indx_start+ii_opt:int(lz/2)], 1)
poly_V_opt = np.polyfit(z[indx_start+jj_opt:int(lz/2)], V[indx_start+jj_opt:int(lz/2)], 1)
delta_PH = l_parallel - l_perpendicular

# PLots for checks
#plt.plot(z[0:10], np.polyval(poly_U_opt, z)[0:10])
#plt.plot(z[0:10], U[0:10])
#plt.plot(zz[20], U[20], 'o')
#plt.legend("P1", "U")
#plt.show()

err_par = abs(100*(l_parallel-0.1715)/0.1715)
err_per = abs(100*(l_perpendicular-0.08099)/0.08099)
err_delta = abs(100*(delta_PH-0.09051)/0.09051)
print("The parallel protrusion height is", l_parallel)
print("The % error WRT Luchini 1991 is", err_par)
print("The perpendicular protrusion height is", l_perpendicular)
print("The % error WRT Luchini 1991 is", err_per)
print("The difference is", delta_PH)
print("The % error WRT Luchini 1991 is", err_delta)

# Write results to a text file
with open('./riblets_code/finiteElements/mesh9/protrusionHeightsResults.txt', 'w') as file:
    file.write(f'l_parallel = {l_parallel:.6f} (err% = {err_par:.6f})\n')
    file.write(f'l_perpendicular = {l_perpendicular:.6f} (err% = {err_per:.6f})\n')
    file.write(f'Delta = {delta_PH:.6f} (err% = {err_delta:.6f})\n\n')
    file.write("Obtained with P1 for Laplace and P2-P1 for Stokes")

