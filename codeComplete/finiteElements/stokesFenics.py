from fenics import * 
import matplotlib.pyplot as plt
import numpy as np
import struct
# This script solves the Laplace problem for a single riblet using Finite Elements Method
# remember to run "dolfin-convert file.msh file.xml" before using this script
spacing = 0.08
ratiohs=0.866025403784
h = ratiohs*spacing
radius =( 2/200)*1.5
mesh  = Mesh("./meshToUse/riblet.xml")
cd = MeshFunction("size_t", mesh, "./meshToUse/riblet_physical_region.xml");
fd = MeshFunction("size_t", mesh, "./meshToUse/riblet_facet_region.xml");
tol = 1e-14
class PeriodicBoundary(SubDomain):
    # Left boundary is the target domain
    def inside(self, x , on_boundary):
        return bool(x[0]<tol and x[0]>-tol and on_boundary)

    # Map right boundary on left boundary
    def map(self, x, y):
        y[0] = x[0] - spacing
        y[1] = x[1]

pbc = PeriodicBoundary()

dx = Measure('dx', domain = mesh)
ds = Measure('ds', domain = mesh, subdomain_data = fd)

V = VectorElement('P', mesh.ufl_cell(), 2)
Q = FiniteElement('P', mesh.ufl_cell(), 1)
X= FunctionSpace(mesh, MixedElement([V, Q]), constrained_domain = pbc)

# Expressions for Boundary Conditions
g_top = Expression(('1', '1'), degree = 4)
bcd_wall = Expression(('0', '0'), degree = 4)
bcd_top = Expression(('1', '0'), degree = 4) # this value must be = to domain height to have unitary velocity gradient at channel centerline
bc_u_wall = DirichletBC(X.sub(0), bcd_wall, fd, 6)
bc_u_top = DirichletBC(X.sub(0), bcd_top, fd, 8)
def pressureBC(x, on_boundary):
    return near(x[0], spacing, tol) and near(x[1], 1, tol)  # the second value must be the domain height

bc_p = DirichletBC(X.sub(1), Constant(0.), pressureBC, 'pointwise')
bc = [bc_u_wall, bc_u_top, bc_p]

# Variational formulation
u, p = TrialFunctions(X)
v, q = TestFunctions(X)

LHS = inner(grad(u), grad(v))*dx - p*div(v)*dx + q*div(u)*dx
RHS = dot(v, g_top) * ds(8)

# Solve the problem
x = Function(X)
solve(LHS == RHS, x, bc, solver_parameters=dict(linear_solver='mumps'))
u, p = x.split()

#u_plot = plot(u)
#plt.colorbar(u_plot)
#plt.show()

#p_plot = plot(p)
#plt.colorbar(p_plot)
#plt.show()

ux = u.sub(0, deepcopy=True) # v stokes
uy = u.sub(1, deepcopy=True) # w stokes
'''
v_plot = plot(ux)
plt.colorbar(v_plot)
plt.title('V component')
plt.show()

w_plot = plot(uy)
plt.colorbar(w_plot)
plt.title('W component')
plt.show()
'''
File("./meshToUse/u.pvd") << u
File("./meshToUse/p.pvd") << p

#v_st = u.sub(0, deepcopy=True).vector().get_local() # v stokes
#np.savetxt('./riblets_code/finiteElements/v_st.txt', v_st)
#w_st = u.sub(1, deepcopy=True).vector().get_local() # w stokes
#np.savetxt('./riblets_code/finiteElements/w_st.txt', w_st)
#-----------------------------------------------------------------------------------------------------------------
# Remove comments from section below to compute mean velocity to calculate protrusion heights.
#grid_x_above_riblet = np.linspace(0, 0.25*spacing, 400)
#grid_x_above_riblet = np.append(grid_x_above_riblet, np.linspace(0.25*spacing, 0.75*spacing, 1000))
#grid_x_above_riblet = np.append(grid_x_above_riblet, np.linspace(0.75*spacing, spacing, 400))
#grid_y_above_riblet = np.linspace(h, 1.5*h, 1400)
#grid_y_above_riblet =  np.append(grid_y_above_riblet, np.linspace(1.5*h, 1, 400))

#u_mean = []
#for j in range(len(grid_y_above_riblet)):
#    u_at_y = []
#    for i in range(len(grid_x_above_riblet)):
#        u_at_y.append(ux(Point((grid_x_above_riblet[i], grid_y_above_riblet[j]))))

            
#    u_mean.append(np.mean(u_at_y, axis = 0))
#plt.plot(grid_y_above_riblet, u_mean)
#plt.show()
#np.savetxt('./riblets_code/finiteElements/mesh9/grid_y.txt',grid_y_above_riblet)
#np.savetxt('./riblets_code/finiteElements/mesh9/v_mean.txt',u_mean)

#-----------------------------------------------------------------------------------------------------------------
# This section produces the necessary data to be used in Turbulent simulations.

if spacing/2<=radius:
    grid_x = np.linspace(0, spacing, 1000)
    grid_y = np.linspace(-h/2, h/2+radius, 1000)
    #print(1)
else:
    grid_x = np.linspace(spacing/2-radius, spacing/2 + radius, 2000)
    grid_y = np.linspace(h/2 - radius, h/2 + radius, 2000)
    #print(2)



output_filev = './meshToUse/v_stokes.bin'
output_filew = './meshToUse/w_stokes.bin'
output_filep = './meshToUse/p_stokes.bin'

with open(output_filev, 'wb') as file:
    for ii in grid_x:
        for jj in grid_y:
            try:
                v_P = ux(Point(ii, jj))
            except RuntimeError:
                v_P = 0
            file.write(struct.pack('d', v_P))

with open(output_filew, 'wb') as file:
    for ii in grid_x:
        for jj in grid_y:
            try:
                w_P = uy(Point(ii, jj))
            except RuntimeError:
                w_P = 0
            file.write(struct.pack('d', w_P))           
out = np.zeros([grid_x.size,grid_y.size])

with open(output_filep, 'wb') as file:
    for iC,ii in enumerate(grid_x):
        for jC,jj in enumerate(grid_y):
            try:
                p_P = p(Point(ii, jj))
            except RuntimeError:
                x = ii; y = jj
                r = sqrt((x-spacing/2)**2 + (y-h/2)**2)
                #theta = atan(y-h/2/x-spacing/2)
                theta = np.arctan2(spacing/2-x,y-h/2)
                phi_w = 5.0/6.0*np.pi
                theta_p = theta*1.0
                if theta >= phi_w:
                    theta_p = phi_w/(phi_w-np.pi)*(theta-np.pi)    # phi = phiwall/(phiwall-PI)*(phi-PI)
                elif theta <= -phi_w:
                    theta_p = phi_w/(phi_w-np.pi)*(theta+np.pi)
                x = spacing/2-r*np.sin(theta_p)
                y = h/2+r*np.cos(theta_p)
                try:
                    p_P = p(Point(x, y))
                except:
                   #print(ii,jj,r,theta)
                    p_P = 0
                    while p_P == 0 :
                        theta_p = theta_p - 1e-4 if theta_p>0 else  theta_p + 1e-4
                        x = spacing/2-r*np.sin(theta_p)
                        y = h/2+r*np.cos(theta_p)
                        try:
                            p_P = p(Point(x, y))
                        except:
                            pass
                    #print(x,y,r,theta_p)
                    #print(' ')
            out[iC,jC] = p_P
            #print(f'y index:{iC}')
            #print(f'z index:{jC}')
            #file.write(struct.pack('d', p_P))
out.tofile(output_filep)
