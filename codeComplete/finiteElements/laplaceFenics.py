from fenics import * 
import matplotlib.pyplot as plt
import numpy as np
import struct

# This script solves the Laplace problem for a single riblet using Finite Elements Method
# remember to run "dolfin-convert file.msh file.xml" before using this script
spacing = 0.08
ratiohs=0.866025403784
h = ratiohs*spacing
radius = (2/200)*1.5
mesh  = Mesh("./meshToUse/riblet.xml")
cd = MeshFunction("size_t", mesh, "./meshToUse/riblet_physical_region.xml")
fd = MeshFunction("size_t", mesh, "./meshToUse/riblet_facet_region.xml")
tol = 1e-14
class PeriodicBoundary(SubDomain):
    # Left boundary is the target domain
    def inside(self, x , on_boundary):
        return bool(x[0]<tol and x[0]>-tol and on_boundary)

    # Map right boundary on left boundary
    def map(self, x, y):
        y[0] = x[0] - spacing
        y[1] = x[1]

pbc = PeriodicBoundary()

V = FunctionSpace(mesh, 'P', 1, constrained_domain = pbc)
g_top = Constant(1.0)
bc_wall = Constant(0.)
bc_top = Constant(1.)
bc1 = DirichletBC(V, bc_wall, fd, 6)
bc2 = DirichletBC(V, bc_top, fd, 8)
bc = [bc1, bc2]
dx = Measure('dx', domain = mesh)
ds = Measure('ds', domain = mesh, subdomain_data = fd)
u = TrialFunction(V)
v = TestFunction(V)

LHS = dot(grad(u), grad(v)) * dx
RHS = g_top * v * ds(8)
#RHS = 0

u = Function(V)

solve(LHS == RHS, u, bc)

u_plot = plot(u)
plt.colorbar(u_plot)
plt.show()
#u_v = u.vector().get_local() # numpy array for u
#np.savetxt('./riblets_code/finiteElements/u_lapl.txt', u_v)


#plt.plot(mean_profiles[:, 0], mean_profiles[:,1])
#plt.show()
#np.savetxt('./riblets_code/finiteElements/grid.txt',grid)
#np.savetxt('./riblets_code/finiteElements/u_try.txt',u_v)
#plt.plot(x_coor, y_coor, '*')
#plt.show()

#-----------------------------------------------------------------------------------------------------------------
# Remove comments from section below to compute mean velocity to calculate protrusion heights.

#grid_x_above_riblet = np.linspace(0, 0.25*spacing, 400)
#grid_x_above_riblet = np.append(grid_x_above_riblet, np.linspace(0.25*spacing, 0.75*spacing, 1000))
#grid_x_above_riblet = np.append(grid_x_above_riblet, np.linspace(0.75*spacing, spacing, 400))
#grid_y_above_riblet = np.linspace(h, 1.5*h, 1400)
#grid_y_above_riblet =  np.append(grid_y_above_riblet, np.linspace(1.5*h, 1, 400))

#u_mean = []
#append1 = u_mean.append

#for j in range(len(grid_y_above_riblet)):
#    u_at_y = []
#    append2 = u_at_y.append
#   : for i in range(len(grid_x_above_riblet)):
#        #u_at_y.append(u(Point((grid_x_above_riblet[i], grid_y_above_riblet[j]))))
#        append2(u(Point((grid_x_above_riblet[i], grid_y_above_riblet[j]))))
#            
#    #u_mean.append(np.mean(u_at_y, axis = 0))
#    append1(np.mean(u_at_y, axis = 0))
#plt.plot(grid_y_above_riblet, u_mean)
#plt.show()
#np.savetxt('./riblets_code/finiteElements/grid_y.txt',grid_y_above_riblet)
#np.savetxt('./riblets_code/finiteElements/mesh9/u_mean.txt',u_mean)
#-----------------------------------------------------------------------------------------------------------------
# This section produces the necessary data to be used in Turbulent simulations.
if spacing/2<=radius:
    grid_x = np.linspace(0, spacing, 1000)
    grid_y = np.linspace(-h/2, h/2+radius, 1000)
else:
    grid_x = np.linspace(spacing/2-radius, spacing/2 + radius, 2000)
    grid_y = np.linspace(h/2 - radius, h/2 + radius, 2000)

output_fileu = './meshToUse/u_laplace.bin'
output_filegy = './meshToUse/gridy.bin'
output_filegz = './meshToUse/gridz.bin'

with open(output_fileu, 'wb') as file:
    for ii in grid_x:
        for jj in grid_y:
            try:
                u_P = u(Point(ii, jj))
            except RuntimeError:
                u_P = 0
            file.write(struct.pack('d', u_P))

with open(output_filegy, 'wb') as file:
    for ii in grid_x:
        file.write(struct.pack('d', ii))

        

with open(output_filegz, 'wb') as file:
    for jj in grid_y:
        file.write(struct.pack('d', jj))
        
            

#-----------------------------------------------------------------------------------------------------------------
