#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Post processing for turbulent simulations: l_T, l_U and DeltaU computation

@author: sb0817
"""
import numpy as np
import matplotlib.pyplot as plt
from riblet import * 
from scipy.optimize import minimize
import timeit 

#%% Simulation Data
Re_tau = 200
nu = 0.005
delta = 1
u_tau = Re_tau*nu

# Riblets
ratiohs = 0.866025403784
spacing = 0.04
lg_plus = (ratiohs*(spacing)**2/2)**0.5*Re_tau
h_rib = ratiohs*spacing
riblets_dns = read_input('../input/riblets.in')
riblets_stats2d = read_2dstats('../output/stats.bin', riblets_dns)
riblets_stats = compute_1dstats(riblets_stats2d, riblets_dns)
u_riblets = riblets_stats['U']
uw_riblets = riblets_stats['uw']
z_riblets = riblets_stats['zz']

# Channel
channel_dns = read_input('../../channel/input/riblets.in')
channel_stats2d = read_2dstats('../../channel/output/stats.bin', channel_dns)
channel_stats = compute_1dstats(channel_stats2d, channel_dns)
u_channel = channel_stats['U']
uw_channel = channel_stats['uw']
z_channel = channel_stats['zz']

#%% Data conversion in plus units
z_rib_pl = z_riblets*u_tau/nu
z_ch_pl = z_channel*u_tau/nu
u_rib_pl = u_riblets/u_tau
uw_rib_pl = -uw_riblets/(u_tau**2) # REMEMBER THAT THIS IS -uw, NOT uw
u_ch_pl = u_channel/u_tau
uw_ch_pl = -uw_channel/(u_tau**2) # REMEMBER THAT THIS IS -uw, NOT uw

#%% Useful stuff before actual computation starts
ii = 0
while uw_rib_pl[ii] < max(uw_rib_pl):
    ii+=1
peak_indx = ii

ll = 0
while z_rib_pl[ll] < 5:
    ll+=1
uw_start_indx = ll

jj = 0
while z_rib_pl[jj] < 40:
    jj+=1
lU_start_indx = jj

kk = 0
while z_rib_pl[kk] <= 150:
    kk+=1
lU_end_indx = kk
#%% Functions definition
def l_T_computation(uw_rib_pl, uw_ch_pl, z_ch_pl, z_rib_pl, u_tau, nu, h_rib):
    err= 1000
    toll = 1e-6 
    ii = 0
    nit_max = 10000
    err_v = []
    while err>toll and ii<=nit_max:
        shift = ii/1000
        z_shifted = z_rib_pl - shift
        uw_rib_pl_mod = np.interp(z_ch_pl, z_shifted, uw_rib_pl)
        err = np.mean(abs(uw_rib_pl_mod - uw_ch_pl))
        err_v.append(err)
        if min(err_v) == err:
            z_T_pl = shift
        ii+=1
        #print(f"Iteration: {ii}, error: {err}")
       
    z_T = z_T_pl*nu/u_tau
    #l_T = h_rib/2 - z_T
    #l_T_pl = l_T*u_tau/nu  
    return z_T


def u_tau_update(u_tau0, delta):
    #tic = timeit.default_timer()
    diff = 10
    toll = 1e-6
    nit_max = 10000
    ii = 0
    u_tau = u_tau0
    z_T0 = 0
    while diff>toll and ii<=nit_max:
        z_rib_pl = z_riblets*u_tau/nu
        z_ch_pl = z_channel*u_tau/nu
        uw_rib_pl = -uw_riblets/(u_tau**2) # REMEMBER THAT THIS IS -uw, NOT uw
        uw_ch_pl = -uw_channel/(u_tau**2) # REMEMBER THAT THIS IS -uw, NOT uw
        z_T = l_T_computation(uw_rib_pl[0:peak_indx], uw_ch_pl[0:peak_indx],
                              z_ch_pl[0:peak_indx], z_rib_pl[0:peak_indx], u_tau, nu, h_rib)
        u_tau = u_tau0*((delta - (z_T-z_T0))/delta)**0.5
        z_T0 = z_T
        diff = abs(u_tau - u_tau0)
        u_tau0 = u_tau
        ii+=1 
        #print(f"Iteration: {ii}, u_tau: {u_tau}, z_T: {z_T}.")
    #toc = timeit.default_timer()
    return u_tau

def DeltaU_and_l_U_computation(u_rib_pl, u_ch_pl, z_ch_pl, z_rib_pl, u_tau, nu, l_T_pl, z_T_pl):
    z_shifted = z_rib_pl - z_T_pl
    u_rib_pl_mod0 = np.interp(z_ch_pl, z_shifted, u_rib_pl)
    DeltaU_pl = -np.mean(abs(u_rib_pl_mod0 - u_ch_pl))
    l_U_pl = l_T_pl - DeltaU_pl
    l_U = l_U_pl*nu/u_tau
    DeltaU = DeltaU_pl*u_tau
    #l_T = h_rib/2 - z_T
    #l_T_pl = l_T*u_tau/nu  
    return l_U, DeltaU
    
#%% Computations


u_tau_new = u_tau_update(u_tau, delta)
z_rib_pl = z_riblets*u_tau_new/nu
z_ch_pl = z_channel*u_tau_new/nu
u_rib_pl = u_riblets/u_tau_new
uw_rib_pl = -uw_riblets/(u_tau_new**2) # REMEMBER THAT THIS IS -uw, NOT uw
u_ch_pl = u_channel/u_tau_new
uw_ch_pl = -uw_channel/(u_tau_new**2) # REMEMBER THAT THIS IS -uw, NOT uw
z_T = l_T_computation(uw_rib_pl[uw_start_indx:peak_indx], uw_ch_pl[uw_start_indx:peak_indx],
                      z_ch_pl[uw_start_indx:peak_indx], z_rib_pl[uw_start_indx:peak_indx], u_tau_new, nu, h_rib)
l_T = h_rib/2 - z_T
l_T_pl = l_T*u_tau_new/nu
z_T_pl = z_T*u_tau_new/nu  
l_U, DeltaU = DeltaU_and_l_U_computation(u_rib_pl[lU_start_indx:lU_end_indx], u_ch_pl[lU_start_indx:lU_end_indx],
                                         z_ch_pl[lU_start_indx:lU_end_indx], z_rib_pl[lU_start_indx:lU_end_indx],
                                         u_tau_new, nu, l_T_pl, z_T_pl)
DeltaU_pl = DeltaU/u_tau_new
u_tau_shift_perc = (u_tau-u_tau_new)/u_tau * 100
#%% plots

plt.semilogx(z_ch_pl, u_ch_pl)
plt.semilogx(z_rib_pl - z_T_pl, u_rib_pl+DeltaU_pl)
#plt.semilogx(z_rib_pl-z_T_pl, u_rib_pl)
plt.xlim([1, 200])
plt.ylim([0, 20])
plt.show()

plt.plot(z_ch_pl, uw_ch_pl)
plt.plot(z_rib_pl - z_T_pl, uw_rib_pl)
#plt.plot(z_rib_pl - 3, uw_rib_pl)

plt.xlim([0, 30])
plt.ylim([0, 0.8])
plt.show()


print(l_T/spacing)
print(l_U/spacing)
