import matplotlib.pyplot as plt
import numpy as np

with open('../output/runtimedata.txt', 'r') as f:
	lines = f.readlines()
x = [float(line.split()[1]) for line in lines]
y = [float(line.split()[5]) for line in lines] # y = flowrate x

fig = plt.figure()
plt.plot(x, y, linewidth=2)
plt.show()

