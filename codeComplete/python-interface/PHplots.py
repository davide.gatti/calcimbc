import matplotlib.pyplot as plt
import os

def read_protrusion_heights(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    parallel_height = float(lines[0].split(': ')[1].strip())
    perpendicular_height = float(lines[1].split(': ')[1].strip())

    return parallel_height, perpendicular_height

def create_plot(file_pattern, plot_title, save_filename):
    indices = list(range(6, 25, 2))  # Generating indices from 6 to 24 with step 2
    parallel_heights = []
    perpendicular_heights = []

    for i in indices:
        file_path = f'../data/s+{i}.txt'
        if os.path.exists(file_path):
            parallel_height, perpendicular_height = read_protrusion_heights(file_path)
            parallel_heights.append(parallel_height)
            perpendicular_heights.append(perpendicular_height)

    # Plotting the parallel protrusion heights
    plt.figure(figsize=(8, 6))
    plt.plot(indices, parallel_heights, marker='o', linestyle='-', color='b', label='Parallel Protrusion Heights')
    plt.axhline(y=0.1715, linestyle='--', color='gray', label='Luchini 1991 value')
    plt.title(f'{plot_title} - Parallel Protrusion Heights')
    plt.xlabel('s+')
    plt.ylabel('Protrusion Height')
    plt.legend()
    plt.grid(True)
    #plt.savefig(f'{save_filename}_parallel.png')
    plt.show()

    # Plotting the perpendicular protrusion heights
    plt.figure(figsize=(8, 6))
    plt.plot(indices, perpendicular_heights, marker='o', linestyle='-', color='r', label='Perpendicular Protrusion Heights')
    plt.axhline(y=0.08099, linestyle='--', color='gray', label='Luchini 1991 value')
    plt.title(f'{plot_title} - Perpendicular Protrusion Heights')
    plt.xlabel('s+')
    plt.ylabel('Protrusion Height')
    plt.legend()
    plt.grid(True)
    #plt.savefig(f'{save_filename}_perpendicular.png')
    plt.show()

if __name__ == "__main__":
    create_plot('../data/s+{}.txt', 'Protrusion Heights Plot', 'protrusion_plot')

