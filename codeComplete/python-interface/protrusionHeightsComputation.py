# Packages and functions utilized
import matplotlib.pyplot as plt
import numpy as np
from riblet import *

dns = read_input('../input/riblets.in') # reading input file
ratiohs = 0.866025403784; spacing = 0.08; # other input parameters
# 16 PPR
# Stats computation
stats2d = read_2dstats('../output/stats.bin',dns)
stats = compute_1dstats(stats2d,dns)

# Useful data
U_full  = np.mean(stats2d['U'], axis = 0)
V_full = np.mean(stats2d['V'], axis = 0)
zz_full = stats2d['zz']
err_min_U = 1000
err_min_V = 1000
lz = len(zz_full)
U = U_full[0:int(lz/2)]
V = V_full[0:int(lz/2)]
zz = zz_full[0:int(lz/2)]

# Velocity profiles plots

#plt.plot(zz, U)
#plt.xlabel('z')
#plt.ylabel('U')
#plt.show()

#plt.plot(zz, V)
#plt.xlabel('z')
#plt.ylabel('V')
#plt.show()

# Protrusion heights computation
indx_start = 0
for ii in range(0, int(lz/2)-indx_start):
    poly_U = np.polyfit(zz[indx_start+ii:int(lz/2)], U[indx_start+ii:int(lz/2)], 1)
    poly_V = np.polyfit(zz[indx_start+ii:int(lz/2)], V[indx_start+ii:int(lz/2)], 1)
    z_height_U = -poly_U[1]/poly_U[0]
    z_height_V= -poly_V[1]/poly_V[0]
    err_U = sum(np.power(U[zz>-3*zz[0]]-zz[zz>-3*zz[0]]*poly_U[0] - poly_U[1], 2))/lz
    err_V = sum(np.power(V[zz>-3*zz[0]]-zz[zz>-3*zz[0]]*poly_V[0] - poly_V[1], 2))/lz
    if err_U < err_min_U:
        ii_opt = ii
        err_min_U = err_U
        l_parallel = abs(z_height_U - ratiohs*spacing/2)/spacing
    if err_V < err_min_V:
        jj_opt = ii
        err_min_V = err_V
        l_perpendicular = abs(z_height_V- ratiohs*spacing/2)/spacing

delta_PH = l_parallel - l_perpendicular
poly_U_opt = np.polyfit(zz[indx_start+ii_opt:int(lz/2)], U[indx_start+ii_opt:int(lz/2)], 1)
poly_V_opt = np.polyfit(zz[indx_start+jj_opt:int(lz/2)], V[indx_start+jj_opt:int(lz/2)], 1)
# PLots for checks
plt.plot(zz, np.polyval(poly_U_opt, zz))
plt.plot(zz, U)
#plt.plot(zz[20], U[20], 'o')
#plt.legend("P1", "U")
plt.show()
plt.plot(zz[-30:-1], U[-30:-1])
plt.plot(zz[-30:-1], np.polyval(poly_U_opt, zz[-30:-1]))
plt.show()
plt.plot(zz[60:90], U[60:90])
plt.plot(zz[60:90], np.polyval(poly_U_opt, zz[60:90]))
plt.show()
plt.plot(zz[2:20], U[2:20])
plt.plot(zz[2:20], np.polyval(poly_U_opt, zz[2:20]))
plt.show()
err_par = abs(100*(l_parallel-0.1715)/0.1715)
err_per = abs(100*(l_perpendicular-0.08099)/0.08099)
err_delta = abs(100*(delta_PH-0.09051)/0.09051)
print("The parallel protrusion height is", l_parallel)
print("The % error WRT Luchini 1991 is", err_par)
print("The perpendicular protrusion height is", l_perpendicular)
print("The % error WRT Luchini 1991 is", err_per)
print("The difference is", delta_PH)
print("The % error WRT Luchini 1991 is", err_delta)
#diff1 = U[30] - np.polyval(poly_U_opt, zz[30])
#diff2 = U[20] - np.polyval(poly_U_opt, zz[20])

diff = []
for i in range(22, int(lz/2) - indx_start):
    diff.append(abs(U[i] - np.polyval(poly_U_opt, zz[i])))

plt.plot(zz[22:int(lz/2)], diff)
