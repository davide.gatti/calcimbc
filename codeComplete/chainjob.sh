#!/bin/bash
####################################
## simple Slurm submitter script to setup   ## 
## a chain of jobs using Slurm                    ##
####################################
## ver.  : 2018-11-27, KIT, SCC

## Common job options
comopt="-p multiple_e -N 6 --ntasks-per-node=28 -J ref  --mail-type=ALL --time=72:00:00 --mail-user=davide.gatti@kit.edu --output='job_%j.out'"

## Define maximum number of jobs via positional parameter 1, default is 5
max_nojob=${1:-10}

## Define your jobscript (e.g. "~/chain_job.sh")
chain_link_job=${PWD}/submitompi.sh

## Define type of dependency via positional parameter 2, default is 'afterok'
dep_type="${2:-afterok}"
## -> List of all dependencies:
## https://slurm.schedmd.com/sbatch.html

myloop_counter=1
## Submit loop
while [ ${myloop_counter} -le ${max_nojob} ] ; do
   ##
   ## Differ msub_opt depending on chain link number
   if [ ${myloop_counter} -eq 1 ] ; then
      slurm_opt="${comopt}"
   else
      slurm_opt="${comopt} -d ${dep_type}:${jobID}"
   fi
   ##
   ## Print current iteration number and sbatch command
   echo "Chain job iteration = ${myloop_counter}"
   echo "   sbatch --export=myloop_counter=${myloop_counter} ${slurm_opt} ${chain_link_job}"
   ## Store job ID for next iteration by storing output of sbatch command with empty lines
   jobID=$(sbatch --export=ALL,myloop_counter=${myloop_counter} ${slurm_opt} ${chain_link_job} 2>&1 | sed 's/[S,a-z]* //g' | tee /dev/tty)
   #jobID=$(sbatch --export=ALL,myloop_counter=${myloop_counter} ${slurm_opt} ${chain_link_job} 2>&1 | tee /dev/tty)
   ##   
   ## Check if ERROR occured
   if [[ "${jobID}" =~ "ERROR" ]] ; then
      echo "   -> submission failed!" ; exit 1
   else
      echo "   -> job number = ${jobID}"
   fi
   ##
   ## Increase counter
   let myloop_counter+=1
done

