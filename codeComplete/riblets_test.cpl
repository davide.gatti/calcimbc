!      ___           ___           ___           ___           ___           ___           ___       ___           ___
!     /\  \         /\__\         /\  \         /\__\         /\__\         /\  \         /\__\     /\  \         /\  \
!    /::\  \       /:/  /        /::\  \       /::|  |       /::|  |       /::\  \       /:/  /    /::\  \       /::\  \
!   /:/\:\  \     /:/__/        /:/\:\  \     /:|:|  |      /:|:|  |      /:/\:\  \     /:/  /    /:/\:\  \     /:/\:\  \
!  /:/  \:\  \   /::\  \ ___   /::\~\:\  \   /:/|:|  |__   /:/|:|  |__   /::\~\:\  \   /:/  /    /::\~\:\  \   /:/  \:\__\
! /:/__/ \:\__\ /:/\:\  /\__\ /:/\:\ \:\__\ /:/ |:| /\__\ /:/ |:| /\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ /:/__/ \:|__|
! \:\  \  \/__/ \/__\:\/:/  / \/__\:\/:/  / \/__|:|/:/  / \/__|:|/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\ \/__/ \:\  \ /:/  /
!  \:\  \            \::/  /       \::/  /      |:/:/  /      |:/:/  /   \:\ \:\__\    \:\  \        \:\__\    \:\  /:/  /
!   \:\  \           /:/  /        /:/  /       |::/  /       |::/  /     \:\ \/__/     \:\  \        \/__/     \:\/:/  /
!    \:\__\         /:/  /        /:/  /        /:/  /        /:/  /       \:\__\        \:\__\                  \::/__/
!     \/__/         \/__/         \/__/         \/__/         \/__/         \/__/         \/__/                   ~~
!

! Some MPI variables
! -----------------------
INTEGER iprocx, nprocx=atoi(COMMANDLINE(1)), iprocy, nprocy=atoi(COMMANDLINE(2))
INTEGER iproc, nproc
BOOLEAN has_terminal

! Libraries to load
! -----------------------
USE complex
USE rbmat
!USE gnuplot
!USE rtchecks


!!!!!
#define dns
USE modules/dns				! DNS input and definitions
USE modules/riblets 			! Riblets input and definitions
USE modules/grid                        ! Staggered grid and difference coefficients
USE modules/imbc                        ! Immersed Boundary Coefficients (IMBC)
USE modules/parallel


!!!!!

!! TestFiles
!FILE testfile1 = CREATE("../../confronto/coeffChange/data/uimbcW.txt")
!FILE testfile2 = CREATE("../../confronto/coeffChange/data/vimbcW.txt")
!FILE testfile3 = CREATE("../../confronto/coeffChange/data/wimbcW.txt")
!FILE testfile4 = CREATE("../../confronto/quickFix/data/y_position_u.txt")
!FILE testfile5 = CREATE("../../confronto/quickFix/data/y_position_v.txt")

!WRITE TO testfile1 "x y imbc"
!WRITE TO testfile2 "x y imbc"
!WRITE TO testfile3 "x y imbc"
!WRITE TO testfile4 "x_uGrid y_uGrid x_vGrid y_vGrid"
! ------------------------

! Defines
! ------------------------

#define cpg
#define mpiverbose
#define chron
!#define randomDisturbance

! Inputs
! ------------------------
BOOLEAN continue
INTEGER non_laminar
REAL nu, headx, heady
INTEGER walls, waven
REAL spacing, ratiohs
INTEGER use_coco, extra
REAL radius, stokexp
ARRAY(0..6) OF REAL phiC, gC
INTEGER nx, ny, nz, it=0
REAL Lx, Ly
REAL cflmax, fouriermax, deltat, maxTime, deltat=0, time=0, Uconv=0
INTEGER it_save, it_stat, it_max, it_cfl, it_check
INTEGER it_runtimestats, it_start, it_end, iz_stats 
REAL tip_radius, zCirc, yTan, eqHeight, s_t
ARRAY(0..1) OF REAL cfl
INTEGER iN0


SUBROUTINE readribletsin()
	FILE ribletsin=OPENRO('./input/riblets.in')
		READ BY NAME FROM ribletsin non_laminar, nu, headx, heady
		READ BY NAME FROM ribletsin walls, spacing, ratiohs
		READ BY NAME FROM ribletsin waven
		lambda = NEW ARRAY(1..waven) OF REAL 
		ampl = NEW ARRAY(1..waven) OF REAL
		READ BY NAME FROM ribletsin lambda
		READ BY NAME FROM ribletsin ampl
		READ BY NAME FROM ribletsin use_coco, radius, stokexp, extra
		READ BY NAME FROM ribletsin phiC, gC
		READ BY NAME FROM ribletsin tip_radius, zCirc, yTan, eqHeight, s_t
		READ BY NAME FROM ribletsin nx, ny, nz, Lx, Ly
		READ BY NAME FROM ribletsin cflmax, fouriermax, deltat, maxTime
		READ BY NAME FROM ribletsin it_save, it_stat, it_max, it_cfl, it_check 
		READ BY NAME FROM ribletsin it_runtimestats, it_start, it_end, iz_stats 
		READ BY NAME FROM ribletsin continue
	CLOSE ribletsin
END readribletsin
readribletsin()

! If a restart file exists, override input parameters with those from field
! ------------------------
myname=basename(COMMANDLINE(0))
POINTER TO STORED STRUCTURE[INTEGER nxs,nys,nzs,its
        REAL Lxs,Lys,headxs,headys,nus,times] dnsinfo=OPENRO("./fields/riblets.field")
        

IF dnsinfo#NULL THEN
	nx=dnsinfo.nxs; ny=dnsinfo.nys; nz=dnsinfo.nzs
	IF continue THEN 
        	it=dnsinfo.its; headx=dnsinfo.headxs; heady=dnsinfo.headys
        	time=dnsinfo.times; nu=dnsinfo.nus; Lx=dnsinfo.Lxs; Ly=dnsinfo.Lys
	ELSE
        	time=0
	END IF
END IF

! Initialisations
! ------------------------
#ifdef chron
	USE wallclock
	REAL clock
#endif

VARS = STRUCTURED ARRAY(p,u,v,w) OF REAL
INCLUDE parallelbcs.h
REAL xd(-2..2*nx+5), yd(-2..2*ny+5), zd(0..2*nz) 
xx = ^xd(2*(*));   yy = ^yd(2*(*));   zz = ^zd(2*(*))
ARRAY[nxl(nx)-1..nxh(nx)+1,nyl(ny)-1..nyh(ny)+1,0..nz] OF VARS var = 0, old
STRUCTURED ARRAY(x,y) OF REAL flowrate = 0


! CoCo switch
BOOLEAN useCoco
IF use_coco = 1 THEN
	useCoco = YES
ELSE IF use_coco = 0 THEN
	useCoco = NO
ELSE
ERROR "ERROR: Invalid value for use_coco input!"
END IF

USE parallelbcs
USE iofiles
USE timestep


! Imbc
! ==================================== 
MODULE imbc

! Read parameters and grid from restart file
DNS dns
STRING filename=WRITE("./fields/riblets.field")
readRestartFile(dns, filename)

! Initialise MPI
mpiInit(dns)

! Read parameters from the riblet file
RIBLETS riblet
filename=WRITE("./input/riblets.in")
readInputFile(riblet,filename)

! Initialise grid
GRID uGrid, vGrid, wGrid, pGrid
initialiseGrid(uGrid, dns, [1,0,0])
initialiseGrid(vGrid, dns, [0,1,0])
initialiseGrid(wGrid, dns, [0,0,1])
initialiseGrid(pGrid, dns, [0,0,0])

! Allocate IBM coefficients
IMB uIMB, vIMB, wIMB
allocate(uIMB, dns, uGrid, riblet, useCoco)
allocate(vIMB, dns, vGrid, riblet, useCoco)
allocate(wIMB, dns, wGrid, riblet, useCoco)

! Populate IBM coefficients
populateCoeff(uIMB, dns, uGrid, pGrid, riblet, useCoco)
populateCoeff(vIMB, dns, vGrid, pGrid, riblet, useCoco)
populateCoeff(wIMB, dns, wGrid, pGrid, riblet, useCoco)

DO
	uimbc(ix,iy)=uIMB.imbc(ix,iy)
	vimbc(ix,iy)=vIMB.imbc(ix,iy)
	wimbc(ix,iy)=wIMB.imbc(ix,iy)
	!IF ix=3 AND iy<uIMB.imbc.HI2 THEN ! Coefficient study
	!	LOOP FOR zCoord=uimbc(ix,iy).LO TO uimbc(ix,iy).HI
	!		WRITE TO testfile1 200*uGrid.x(1,iy), 200*uGrid.x(2,zCoord), uimbc(ix,iy)(zCoord)
	!	REPEAT
	!	LOOP FOR zCoord=wimbc(ix,iy).LO TO wimbc(ix,iy).HI
	!		WRITE TO testfile3 200*wGrid.x(1,iy), 200*wGrid.x(2,zCoord), wimbc(ix,iy)(zCoord)
	!	REPEAT
!		!WRITE TO testfile4 200*yWave(riblet,uGrid.x(0,3))
!		!WRITE TO testfile5 200*yWave(riblet,vGrid.x(0,3))
	!END IF
	!IF ix=3 AND iy<uIMB.imbc.HI2-1 THEN
	!	LOOP FOR zCoord=vimbc(ix,iy).LO TO vimbc(ix,iy).HI
	!		WRITE TO testfile2 200*vGrid.x(1,iy), 200*vGrid.x(2,zCoord), vimbc(ix,iy)(zCoord)
	!	REPEAT
	!END IF
FOR ix=uIMB.imbc.LO1 TO uIMB.imbc.HI1 AND iy=uIMB.imbc.LO2 TO uIMB.imbc.HI2

!ARRAY(0..2) OF REAL coordinates=(wGrid.x(0,1), wGrid.x(1,7), wGrid.x(2, 15))
!WRITE BY NAME 200*coordinates
!WRITE BY NAME wimbc(1,7)(15), wimbc(1,9)(15)
END imbc

!CLOSE testfile1
!CLOSE testfile2
!CLOSE testfile3
!CLOSE testfile4
!CLOSE testfile5
! ====================================

#ifdef WALLS
#ifdef EXTRA
DO 
   	Uuimbc(ix,iy) = NEW ARRAY((nz-uimbc(ix,iy,*,0).HI)..(nz-uimbc(ix,iy,*,0).LO),0..4) OF REAL
     	DO Uuimbc(ix,iy,nz-iz,0) = uimbc(ix,iy,iz,0) FOR iz=uimbc(ix,iy,*,0).LO TO uimbc(ix,iy,*,0).HI
     	DO Uuimbc(ix,iy,nz-iz,1) = 0 FOR iz=uimbc(ix,iy,*,1).LO TO uimbc(ix,iy,*,1).HI
     	DO Uuimbc(ix,iy,nz-iz,2) = 0 FOR iz=uimbc(ix,iy,*,2).LO TO uimbc(ix,iy,*,2).HI
     	DO Uuimbc(ix,iy,nz-iz,3) = 0 FOR iz=uimbc(ix,iy,*,3).LO TO uimbc(ix,iy,*,3).HI
     	DO Uuimbc(ix,iy,nz-iz,4) = 0 FOR iz=uimbc(ix,iy,*,4).LO TO uimbc(ix,iy,*,4).HI
     
     	Uvimbc(ix,iy) = NEW ARRAY((nz-vimbc(ix,iy,*,0).HI)..(nz-vimbc(ix,iy,*,0).LO),0..4) OF REAL
	DO Uvimbc(ix,iy,nz-iz,0) = vimbc(ix,iy,iz,0) FOR iz=vimbc(ix,iy,*,0).LO TO vimbc(ix,iy,*,0).HI
	DO Uvimbc(ix,iy,nz-iz,1) = 0 FOR iz=vimbc(ix,iy,*,1).LO TO vimbc(ix,iy,*,1).HI
	DO Uvimbc(ix,iy,nz-iz,2) = 0 FOR iz=vimbc(ix,iy,*,2).LO TO vimbc(ix,iy,*,2).HI
	DO Uvimbc(ix,iy,nz-iz,3) = 0 FOR iz=vimbc(ix,iy,*,3).LO TO vimbc(ix,iy,*,3).HI
	DO Uvimbc(ix,iy,nz-iz,4) = 0 FOR iz=vimbc(ix,iy,*,4).LO TO vimbc(ix,iy,*,4).HI
     
     	Uwimbc(ix,iy) = NEW ARRAY((nz-1-wimbc(ix,iy,*,0).HI)..(nz-1-wimbc(ix,iy,*,0).LO),0..4) OF REAL
     	DO Uwimbc(ix,iy,nz-1-iz,0) = wimbc(ix,iy,iz,0) FOR iz=wimbc(ix,iy,*,0).LO TO wimbc(ix,iy,*,0).HI
     	DO Uwimbc(ix,iy,nz-1-iz,1) = 0 FOR iz=wimbc(ix,iy,*,1).LO TO wimbc(ix,iy,*,1).HI
     	DO Uwimbc(ix,iy,nz-1-iz,2) = 0 FOR iz=wimbc(ix,iy,*,2).LO TO wimbc(ix,iy,*,2).HI
     	DO Uwimbc(ix,iy,nz-1-iz,3) = 0 FOR iz=wimbc(ix,iy,*,3).LO TO wimbc(ix,iy,*,3).HI
     	DO Uwimbc(ix,iy,nz-1-iz,4) = 0 FOR iz=wimbc(ix,iy,*,4).LO TO wimbc(ix,iy,*,4).HI
     		
     	Uuimbc_rhs(ix,iy) = NEW ARRAY((nz-uimbc_rhs(ix,iy,*,0).HI)..(nz-uimbc_rhs(ix,iy,*,0).LO),0..4) OF REAL
     	DO Uuimbc_rhs(ix,iy,nz-iz,0) = uimbc_rhs(ix,iy,iz,0) FOR iz=uimbc_rhs(ix,iy,*,0).LO TO uimbc_rhs(ix,iy,*,0).HI
     	DO Uuimbc_rhs(ix,iy,nz-iz,1) = uimbc_rhs(ix,iy,iz,1) FOR iz=uimbc_rhs(ix,iy,*,1).LO TO uimbc_rhs(ix,iy,*,1).HI
     	DO Uuimbc_rhs(ix,iy,nz-iz,2) = uimbc_rhs(ix,iy,iz,2) FOR iz=uimbc_rhs(ix,iy,*,2).LO TO uimbc_rhs(ix,iy,*,2).HI
     	DO Uuimbc_rhs(ix,iy,nz-iz,3) = uimbc_rhs(ix,iy,iz,3) FOR iz=uimbc_rhs(ix,iy,*,3).LO TO uimbc_rhs(ix,iy,*,3).HI
     	DO Uuimbc_rhs(ix,iy,nz-iz,4) = uimbc_rhs(ix,iy,iz,4) FOR iz=uimbc_rhs(ix,iy,*,4).LO TO uimbc_rhs(ix,iy,*,4).HI
     
     	Uvimbc_rhs(ix,iy) = NEW ARRAY((nz-vimbc_rhs(ix,iy,*,0).HI)..(nz-vimbc_rhs(ix,iy,*,0).LO),0..4) OF REAL
     	DO Uvimbc_rhs(ix,iy,nz-iz,0) = vimbc_rhs(ix,iy,iz,0) FOR iz=vimbc_rhs(ix,iy,*,0).LO TO vimbc_rhs(ix,iy,*,0).HI
     	DO Uvimbc_rhs(ix,iy,nz-iz,1) = vimbc_rhs(ix,iy,iz,1) FOR iz=vimbc_rhs(ix,iy,*,1).LO TO vimbc_rhs(ix,iy,*,1).HI
     	DO Uvimbc_rhs(ix,iy,nz-iz,2) = vimbc_rhs(ix,iy,iz,2) FOR iz=vimbc_rhs(ix,iy,*,2).LO TO vimbc_rhs(ix,iy,*,2).HI
     	DO Uvimbc_rhs(ix,iy,nz-iz,3) = vimbc_rhs(ix,iy,iz,3) FOR iz=vimbc_rhs(ix,iy,*,3).LO TO vimbc_rhs(ix,iy,*,3).HI
     	DO Uvimbc_rhs(ix,iy,nz-iz,4) = vimbc_rhs(ix,iy,iz,4) FOR iz=vimbc_rhs(ix,iy,*,4).LO TO vimbc_rhs(ix,iy,*,4).HI

FOR ALL ix,iy

#else
DO 
   	Uuimbc(ix,iy) = NEW ARRAY((nz-uimbc(ix,iy).HI)..(nz-uimbc(ix,iy).LO)) OF REAL
     	DO Uuimbc(ix,iy,nz-iz) = uimbc(ix,iy,iz) FOR iz=uimbc(ix,iy).LO TO uimbc(ix,iy).HI
     
     	Uvimbc(ix,iy) = NEW ARRAY((nz-vimbc(ix,iy).HI)..(nz-vimbc(ix,iy).LO)) OF REAL
	DO Uvimbc(ix,iy,nz-iz) = vimbc(ix,iy,iz) FOR iz=vimbc(ix,iy).LO TO vimbc(ix,iy).HI
     
     	Uwimbc(ix,iy) = NEW ARRAY((nz-1-wimbc(ix,iy).HI)..(nz-1-wimbc(ix,iy).LO)) OF REAL
     	DO Uwimbc(ix,iy,nz-1-iz) = wimbc(ix,iy,iz) FOR iz=wimbc(ix,iy).LO TO wimbc(ix,iy).HI

FOR ALL ix,iy

#endif
#endif



IF NOT continue THEN 
! Imposing zero velocity in the geometry
! -------------------------
	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
#ifdef EXTRA
		DO var(ix,iy,iz).u = 0 FOR iz=0 TO uimbc(ix,iy,*,0).LO-1
   		DO var(ix,iy,iz).v = 0 FOR iz=0 TO vimbc(ix,iy,*,0).LO-1
   		DO var(ix,iy,iz).w = 0 FOR iz=0 TO wimbc(ix,iy,*,0).LO-1
#else
		DO var(ix,iy,iz).u = 0 FOR iz=0 TO uimbc(ix,iy).LO-1
   		DO var(ix,iy,iz).v = 0 FOR iz=0 TO vimbc(ix,iy).LO-1
   		DO var(ix,iy,iz).w = 0 FOR iz=0 TO wimbc(ix,iy).LO-1		
#endif
#ifdef WALLS
#ifdef EXTRA
   		DO var(ix,iy,iz).u = 0 FOR iz=Uuimbc(ix,iy,*,0).HI+1 TO nz
   		DO var(ix,iy,iz).v = 0 FOR iz=Uvimbc(ix,iy,*,0).HI+1 TO nz
   		DO var(ix,iy,iz).w = 0 FOR iz=Uwimbc(ix,iy,*,0).HI+1 TO nz-1
#else
   		DO var(ix,iy,iz).u = 0 FOR iz=Uuimbc(ix,iy).HI+1 TO nz
   		DO var(ix,iy,iz).v = 0 FOR iz=Uvimbc(ix,iy).HI+1 TO nz
   		DO var(ix,iy,iz).w = 0 FOR iz=Uwimbc(ix,iy).HI+1 TO nz-1
#endif
#endif
	REPEAT

! Check that the initial condition fulfills the boundary conditions
! -------------------------
#ifdef WALLS
	var(var.LO1..var.HI1,var.LO2..var.HI2,0)=0
	var(var.LO1..var.HI1,var.LO2..var.HI2,nz).u=0
	var(var.LO1..var.HI1,var.LO2..var.HI2,nz).v=0
	var(var.LO1..var.HI1,var.LO2..var.HI2,nz-1).w=0
	var(var.LO1..var.HI1,var.LO2..var.HI2,nz).p=0
#else
	var(var.LO1..var.HI1,var.LO2..var.HI2,nz-1).w=0
  	var(var.LO1..var.HI1,var.LO2..var.HI2,nz-1).p=0
  	var(var.LO1..var.HI1,var.LO2..var.HI2,0)=0
#endif

END IF



! Timeloop
! -------------------------
INTEGER it_max_new
REAL cflmax_it
SUBROUTINE time_loop()
	LOOP timeloop WHILE it<it_max
  	! Every it_cfl iterations, check the CFL and impose the new timestep
	! ------

		IF it MOD it_cfl = 0 THEN
			get_cfl()
			IF it < 1000 THEN cflmax_it = 0.1; ELSE IF it < 5000 THEN cflmax_it = 0.5; ELSE cflmax_it = cflmax;	END IF
			IF non_laminar=0 THEN cflmax_it = cflmax
			IF cflmax_it>0 THEN deltat=cflmax_it/cfl(0) END IF
			IF (cfl(1)*deltat)>fouriermax THEN deltat=fouriermax/cfl(1) END IF
		END IF
		#ifdef chron
  	  		IF has_terminal THEN clock=wallclock()
		#endif
	! Run the RK3 integration and check the BCs
	! ------
	
#ifdef WALLS
		IF non_laminar=0 THEN 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).u=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).u=1.0 FOR ALL ix,iy; 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).v=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).v=1.0 FOR ALL ix,iy 
		END IF
#else
		DO var(ix,iy,nz,0..2)=var(ix,iy,nz-1,0..2); var(ix,iy,nz).w=-var(ix,iy,nz-2).w FOR ALL ix,iy
		IF non_laminar=0 THEN DO var(ix,iy,nz).u=1.0 FOR ALL ix,iy; DO var(ix,iy,nz).v=1.0 FOR ALL ix,iy; END IF
#endif
		timestep(rai1)
#ifdef WALLS
		IF non_laminar=0 THEN 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).u=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).u=1.0 FOR ALL ix,iy; 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).v=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).v=1.0 FOR ALL ix,iy
		END IF
#else
		DO var(ix,iy,nz,0..2)=var(ix,iy,nz-1,0..2); var(ix,iy,nz).w=-var(ix,iy,nz-2).w FOR ALL ix,iy
		IF non_laminar=0 THEN DO var(ix,iy,nz).u=1.0 FOR ALL ix,iy; DO var(ix,iy,nz).v=1.0 FOR ALL ix,iy; END IF
#endif
 		timestep(rai2)
#ifdef WALLS
		IF non_laminar=0 THEN 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).u=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).u=1.0 FOR ALL ix,iy; 
			DO var(ix,iy,FLOOR(0.5*(nz+1))).v=1.0 FOR ALL ix,iy; DO var(ix,iy,FLOOR(0.5*(nz-1))).v=1.0 FOR ALL ix,iy
		END IF
#else
		DO var(ix,iy,nz,0..2)=var(ix,iy,nz-1,0..2); var(ix,iy,nz).w=-var(ix,iy,nz-2).w FOR ALL ix,iy
		IF non_laminar=0 THEN DO var(ix,iy,nz).u=1.0 FOR ALL ix,iy; DO var(ix,iy,nz).v=1.0 FOR ALL ix,iy; END IF
#endif
	 	timestep(rai3)
	  	INC it
		#ifdef randomDisturbance
	  		IF it<500 THEN DO var(ix,iy,iz).u=~+deltat*100*0.2*(RAND()-0.5) FOR ix=LO+1 TO HI-1 AND iy=LO+1 TO HI-1 AND iz=LO+1 TO HI-1
		#endif
		
	! Write the files
	! ------
	  	output
	  	outputRuntime
	  	IF it MOD it_save = 0 THEN savefield("./fields/"myname""it DIV it_save".field")
		#ifdef chron
	  		IF has_terminal THEN WRITE "elapsed time per timestep: "wallclock()-clock" s"
		#endif
  	  	time=~+deltat
	! Check the on the run input files to see if the user changed it_save, it_max or it_check
	! It's been checked every it_check times
	! ------
	  	IF it MOD it_check = 0 THEN 
			FILE rereadfile=OPENRO('reread')
			IF rereadfile#NULL THEN 
			    readribletsin()
			    CLOSE rereadfile
			    remove('reread')
			END IF
	  	END IF
	REPEAT timeloop
END time_loop
time_loop()
! Dump the field in the original file. This is done even if it_save and it do not match
! ------
savefield("./fields/riblets.field")
save_statsX()

CLOSE runtimedata
CLOSE runtimestats

