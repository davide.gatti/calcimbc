#!/bin/bash
sbatch -p multiple_il -N 4 --ntasks-per-node=64 -J 8_1_re200ch --time=72:00:00 --mail-type=ALL --mail-user=giacomo.ronchetti@mail.polimi.it --output="job_%j.out"  ./submitompi.sh
#sbatch -p multiple_e -N 4 --ntasks-per-node=24 -J 2deg  --time=18:00:00  --mail-type=ALL --mail-user=pw5576@partner.kit.edu --output="job_%j.out"  ./submitompi.sh
