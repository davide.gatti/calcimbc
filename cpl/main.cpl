! Libraries to load
! -----------------------
USE rtchecks
USE complex
USE rbmat
USE modules/dns				! DNS input and definitions
USE modules/riblets 			! Riblets input and definitions
USE modules/grid                        ! Staggered grid and difference coefficients
USE modules/imbc                        ! Immersed Boundary Coefficients (IMBC)
USE modules/parallel
USE modules/splash
USE modules/coco                        ! COrner COrrection

!! TestFiles
FILE testfile1 = CREATE("../../../postProc/confrontoSin/data/uimbc_new_new.txt")
FILE testfile2 = CREATE("../../../postProc/confrontoSin/data/vimbc_new_new.txt")
FILE testfile3 = CREATE("../../../postProc/confrontoSin/data/wimbc_new_new.txt")
FILE testfile4 = CREATE("../../../postProc/confrontoSin/data/y_position_u.txt")
FILE testfile5 = CREATE("../../../postProc/confrontoSin/data/y_position_v.txt")

WRITE TO testfile1 "x y imbc"
WRITE TO testfile2 "x y imbc"
WRITE TO testfile3 "x y imbc"

! Read parameters and grid from restart file
DNS dns
STRING filename=WRITE("./fields/riblets.field")
readRestartFile(dns, filename)

! Initialise MPI
mpiInit(dns)

! Read parameters from the riblet file
RIBLETS riblet
filename=WRITE("./input/riblets.in")
readInputFile(riblet,filename)

INTEGER use_coco=1

! Coco switch
BOOLEAN useCoco
IF use_coco = 1 THEN
	useCoco = YES
ELSE IF use_coco = 0 THEN
	useCoco = NO
ELSE
ERROR "ERROR: Invalid value for use_coco input!"
END IF

! Initialise grid
GRID uGrid, vGrid, wGrid, pGrid
initialiseGrid(uGrid, dns, [1,0,0])
initialiseGrid(vGrid, dns, [0,1,0])
initialiseGrid(wGrid, dns, [0,0,1])
initialiseGrid(pGrid, dns, [0,0,0])

! Allocate IBM coefficients
IMB uIMB, vIMB, wIMB
allocate(uIMB, dns, uGrid, riblet, useCoco)
allocate(vIMB, dns, vGrid, riblet, useCoco)
allocate(wIMB, dns, wGrid, riblet, useCoco)

! Populate IBM coefficients
populateCoeff(uIMB, dns, uGrid, pGrid, riblet, useCoco)
populateCoeff(vIMB, dns, vGrid, pGrid, riblet, useCoco)
populateCoeff(wIMB, dns, wGrid, pGrid, riblet, useCoco)

DO
	IF ix=3 AND iy<uIMB.imbc.HI2 THEN 

		LOOP FOR zCoord=uIMB.imbc(ix,iy).LO TO uIMB.imbc(ix,iy).HI
			WRITE TO testfile1 200*uGrid.x(1,iy), 200*uGrid.x(2,zCoord), uIMB.imbc(ix,iy)(zCoord)
		REPEAT
		LOOP FOR zCoord=wIMB.imbc(ix,iy).LO TO wIMB.imbc(ix,iy).HI
			WRITE TO testfile3 200*wGrid.x(1,iy), 200*wGrid.x(2,zCoord), wIMB.imbc(ix,iy)(zCoord)
		REPEAT
		WRITE TO testfile4 200*yWave(riblet,uGrid.x(0,3))
		WRITE TO testfile5 200*yWave(riblet,vGrid.x(0,3))
	END IF
	IF ix=3 AND iy<uIMB.imbc.HI2-1 THEN
		LOOP FOR zCoord=vIMB.imbc(ix,iy).LO TO vIMB.imbc(ix,iy).HI
			WRITE TO testfile2 200*vGrid.x(1,iy), 200*vGrid.x(2,zCoord), vIMB.imbc(ix,iy)(zCoord)
		REPEAT
	END IF
FOR ix=uIMB.imbc.LO1 TO uIMB.imbc.HI1 AND iy=uIMB.imbc.LO2 TO uIMB.imbc.HI2
