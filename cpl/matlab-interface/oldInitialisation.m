%%
addpath('./base/')
clc
disp('|-------------------')
disp('|     RIBLETS 3D   |')
disp('|-------------------')
disp('| ')
disp('| Simulation set-up ')
disp('| initialisation    ')
disp('| and checks        ')
disp('| ')
disp('  ')



%% Input parameters 
%  -------------------------

% GEOMETRY

rib.alfa 	= 60;			% rib tip angle
rib.s_h 	= 0.01;		 	% ratio between riblet spacing and channel height

% DNS SIMULATION

dns.type 	= 'CPG';               	% 'CFR'= constant flow rate; 'CPG'= constant pressure gradient
dns.walls 	= 2; 			% only the half channel is simulated -> one wall 
dns.extra    = 0;            % if = 1 then extra diagonal terms are considered
dns.domain 	= 'full';            	% 'full'= physical dimensions chosen by user, 'minimal'= minimal domain size
dns.laminar 	= true;		% this setting allow the field generator to chose among the possible laminar cases
dns.couette 	= true;		% if true, a couette flow is imposed with centerline velocity = 1 and headx=heady=0
dns.generateInitialField=true;  	% do you want to generate the initial field?
dns.fieldToInterpolate = '../../../../initialFields/riblets_iniziale.field';
%if dns.laminar == false 
     %if dns.walls == 1
         %dns.fieldToInterpolate='../fields/riblets.field'; % turbulent simulations, half channel
%     else
% %         dns.fieldToInterpolate='../../../../../roughness.field'; % turbulent simulations, full channel
%         dns.fieldToInterpolate='../../../../../../riblets20.field'; % turbulent simulations, full channel
     %end
% else
%     dns.fieldToInterpolate='../../../../../../riblets9.field'; % laminar simulations
% end
dns.init_plot	= 'true';		% if true, the initial field is plot in all its 3 components

% DNS AND MESH PARAMETERS
dns.re 		= 200;                  % re_bulk if CFR, re_tau if CPG
if dns.re == 400
    dns.fieldToInterpolate='../../../../../../riblets99.field';
end
dns.dxp 	= 2.0;                    % streamwise resolution in wall units
dns.dyp 	= 1.0;                    % maximum spanwise resolution
rib.nppr 	= 2;                   	% minimum number of points per riblet
dns.cellAR 	= 1.25;                 	% aspect ratio of the cells within the riblets (dy+/dz+)
%dns.cellAR 	= 0.625;                 	% aspect ratio of the cells within the riblets (dy+/dz+)
dns.zstretch 	= 3.0;               	% ratio of dz+_max/dz+_min
dns.cflmax 	= 1.2;               	% maximum cfl number
dns.fouriermax 	= 0.5;           	% maximum fourier number
dns.Lx 		= 7.5;                 % Relevant only if dns.type is 'full'
dns.Ly 		= 3.75;                 % Relevant only if dns.type is 'full'
%dns.Ly 		= 2*rib.s_h;                 % To simulate 2 riblets
rib.number  = round(dns.Ly/rib.s_h);
dns.Ly      = rib.number*rib.s_h;
if dns.laminar
      dns.Ly 	= rib.s_h; 		% to simulate one riblet
end
dns.zcp 	= 400;                  % height up to which you want an healthy mean velocity profile

% DNS EXECUTION, READING AND SAVING TIME

dns.it_save 	= 10000;          	% number of iteration between two flow snapshots. It can be changed in the ontherun.in file while tthe simulation is going
dns.it_stat 	= 100;              	% number of iteration between two 1d flow statistics
dns.it_max 	= 150000;           	% maximum number of iterations
dns.it_check 	= 100;			% number of iterations between two readings of the on the fly adaptation. This can also be changed on the run
dns.it_cfl 	= 10;                	% how often to compute CFL 
					% It can also be set to 0 to stop the simulation after it_check iterations and save the statistics.
dns.maxTime     = 250;                  % maximum time before the simulation stops, expressed in seconds
% ON THE RUN CHANGING PARAMETERS (to be added more in the future)

dns.it_max_new 	= dns.it_max;    	% maximum number of iterations for ontherun.in. It starts as it_max and it can be changed during the simulation.

% RIBLETS 3D PARAMETERS
rib.lambda1	= dns.Lx;		% lengthwave of the cosine applied in the streamwise direction. Set it as dns.Lx to have one period only
rib.ampl1 	= 0.0;			% max amplitude of the wave, in terms of multiples of rib.s_h
% rib.ampl1 	= 0.521044976361029;			% max amplitude of the wave, in terms of multiples of rib.s_h
% rib.ampl1 	= 0.7360;			% max amplitude of the wave, in terms of multiples of rib.s_h
rib.lambda2	= dns.Lx/6;		% lengthwave of the cosine applied in the streamwise direction. Set it as dns.Lx to have one period only
% rib.ampl2 	= 0.9423;			% max amplitude of the wave, in terms of multiples of rib.s_h
rib.ampl2 	= 0.0;			% max amplitude of the wave, in terms of multiples of rib.s_h
% rib.ampl2 	= 0.5285848;			% max amplitude of the wave, in terms of multiples of rib.s_h
rib.lambda3	= dns.Lx/3;		% lengthwave of the cosine applied in the streamwise direction. Set it as dns.Lx to have one period only
rib.ampl3 	= 0.0;			% max amplitude of the wave, in terms of multiples of rib.s_h

if dns.laminar
      dns.Lx 	= 6*dns.dxp/dns.re; 		% to simulate one riblet
end
dns.radius_plus = 2.0;
dns.radius = dns.radius_plus/dns.re;
% dns.radius = 0.011;
%% Derived quantities
% --------------------------
dns.nu 		= (1/dns.re);           % kinematic viscosity

if strcmp(dns.type, 'CFR')
    tmp.Cf 	= 0.073*((2*dns.re)^-(0.25));
    dns.retau 	= sqrt(0.5*tmp.Cf)*dns.re;
else
    dns.retau 	= dns.re;
end

dns.utau 	= dns.retau*dns.nu;

dns.deltani 	= dns.nu/dns.utau;
dns.tni 	= dns.nu/dns.utau^2;
dns.dt 		= (0.01*dns.tni);       	% timestep

rib.sp 		= rib.s_h * dns.retau;		% de-scaled riblet spacing 
rib.s_k		= 2*tan(pi*rib.alfa/2/180); % ratio between riblet spacing and height
rib.k_h 	= rib.s_h/rib.s_k;         	% ratio between riblet and channel height
tmp.phirib 	= atan(0.5*rib.s_k);		% semi-angle at the riblet tip
rib.phiwall = pi - tmp.phirib;          % angle wrt the horizontal wall
rib.stokdisp    = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);
options 	= optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);
rib.stokexp = fsolve( @(x) rib.stokdisp(x,rib.phiwall), 0.51, options); % Stokes eq. coefficient to evaluate the corner correction

if strcmp(dns.domain, 'minimal')
    dns.zcp 	= max([4*rib.sp/rib.s_k, 100]);
    dns.Lyp 	= ceil(max([0.5*rib.sp/0.4, dns.zcp/0.4])/rib.sp)*rib.sp; % spanwise size of the box in wall units:
   									  % criteria according to (eq. 2.3) of Endrikat et al (2021), JFM.
									  % Rounded so to have an integer number of riblets in the box.
    dns.Lxp 	= max([1000, 3*dns.Lyp]);   % streamwise size of the box in wall units: criteria according to (eq. 2.3) of Endrikat et al (2021), JFM
    dns.Ly 	= dns.Lyp*dns.deltani;       	% spanwise size of the box as fraction of h
    dns.Lx 	= dns.Lxp*dns.deltani;       	% streamwise size of the box as fraction of h
else
    dns.Lyp 	= dns.Ly/dns.deltani;       	% spanwise size of the box in wall units
    dns.Lxp 	= dns.Lx/dns.deltani;       	% streamwise size of the box in wall units
end 

rib.nrib 	= dns.Lyp/rib.sp;               % number of riblets in the box
dns.nx 		= ceil(dns.Lxp/dns.dxp);        % number of points in the streamwise direction
% dns.nx      = ceil(ceil(rib.lambda2*dns.retau/dns.dxp)/rib.lambda2*dns.Lx);
dns.nx		= dns.nx+mod(dns.nx,2);        	% even number of points correction
dns.ny 		= ceil(max([rib.nrib*rib.nppr, dns.Lyp/dns.dyp])); % nunmber of points in the spanwise direction
dns.ny		= dns.ny+mod(dns.ny,2);        	% even number of points correction
dns.dyp 	= dns.Lyp/dns.ny;               % rescaled delta_y given the lenght
dns.dxp 	= dns.Lxp/dns.nx;            	% rescaled delta_x given the lenght
dns.dzmin 	= dns.dyp/dns.cellAR;           % minimum resolution in the wall-normal direction in wall units

% PRESSURE GRADIENT DEFINITION (valid for CPG only)

dns.headx 	= 1.0;				% x-component of the pressure gradient
dns.heady 	= 0.0;  			% y-component of the pressure gradient

if dns.couette
     dns.headx 	= 0.0;
     dns.heady 	= 0.0;
end

% Z-DIRECTION STRETCHING

% The wall-normal grid will go from -k/2 to 1, so that the 
% meltdown height is exactly 1. For the wall-normal grid I use an
% equispaced grid until the riblet tip and then a stretching through a
% geometric series. The stretching ratio will be slightly adjusted, so that
% we get an integer number of points in the wall-normal direction. All
% support variable I need for the grid are saved in the structure tmp,
% which I will delete at the end.
% 

rib.k_h_splus = 0.065/rib.s_k;

tmp.nz_rib 	= ceil(2*rib.k_h_splus*dns.retau/dns.dzmin);

%if mod(tmp.nz_rib,2) == 0
%    dns.dzmin = dns.dzmin*(tmp.nz_rib + 1)/tmp.nz_rib;
%    dns.cellAR = dns.dyp/dns.dzmin;
%end

if mod(tmp.nz_rib,2) == 0
	tmp.nz_rib = tmp.nz_rib + 1;
end
dns.nz_rib = tmp.nz_rib;
dns.dzmin = (2*dns.retau*(rib.k_h_splus - 1/dns.retau*(0.15))/tmp.nz_rib); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dns.cellAR = dns.dyp/dns.dzmin;

tmp.z0 		= -rib.k_h_splus/2+(tmp.nz_rib*dns.dzmin*0.5)/dns.retau;
tmp.z1 		= 1;
tmp.L 	= (tmp.z1-tmp.z0)*dns.retau;
tmp.dzmax = 0.5*dns.dzmin*dns.zstretch;
tmp.beta = (tmp.L-0.5*dns.dzmin)/(tmp.L-tmp.dzmax);
tmp.n = ceil(1+log(dns.zstretch)/log(tmp.beta));
tmp.n = tmp.n + mod(tmp.n + tmp.nz_rib,2);
tmp.fun = @(x,n,L,dzmin) n-1-log(x)/log((L-dzmin)/(L-dzmin*x));
tmp.r = fsolve( @(x) tmp.fun(x,tmp.n-1,tmp.L,0.5*dns.dzmin), dns.zstretch,options);
tmp.beta = tmp.r^(1/(tmp.n-2));
tmp.nz = tmp.nz_rib+tmp.n;              
dns.dzmax = dns.dzmin*tmp.r;            % maximum grid spacing in the wall-normal direction

if dns.walls == 1
    dns.zd = zeros(tmp.nz+1,1);             % grid in the wall-normal direction (all nodes)
    for i=0:tmp.nz_rib
        dns.zd(i+1)=(0.5*i*dns.dzmin-rib.k_h_splus/2*dns.retau)/dns.retau;
    end
    for i=tmp.nz_rib+1:tmp.nz
        j=i-tmp.nz_rib-1;
        tmp.d = (0.5*dns.dzmin)*tmp.beta^j;
        dns.zd(i+1)=dns.zd(i)+tmp.d/dns.retau;
    end
    dns.zd(end-1)=1.0;
    dns.zd(end)=dns.zd(end-1)+(dns.zd(end-1)-dns.zd(end-2));

    dns.zc = dns.zd(1:2:end);               % grid in wall-normal direction (w nodes) (io penso siano quelli di u, v)
    dns.nz = numel(dns.zc)-1;               % number of points in wall-normal direction

else

    dns.zd = zeros(2*tmp.nz-1,1);             % grid in the wall-normal direction (all nodes)
    for i = 0:tmp.nz_rib
        dns.zd(i+1) = (0.5*i*dns.dzmin-rib.k_h_splus/2*dns.retau)/dns.retau;
        dns.zd(end-i) = (2*dns.retau-0.5*i*dns.dzmin+rib.k_h_splus/2*dns.retau)/dns.retau;
    end
    for i=tmp.nz_rib+1:tmp.nz
        j=i-tmp.nz_rib-1;
        tmp.d = (0.5*dns.dzmin)*tmp.beta^j;
        dns.zd(i+1)=dns.zd(i)+tmp.d/dns.retau;
        dns.zd(end-i)=dns.zd(end-i+1)-tmp.d/dns.retau;
    end
%     dns.zd(end-1)=1.0;
%     dns.zd(end)=dns.zd(end-1)+(dns.zd(end-1)-dns.zd(end-2));
% 
    dns.zc = dns.zd(1:2:end);               % grid in wall-normal direction (w nodes)
    dns.nz = numel(dns.zc)-1;               % number of points in wall-normal direction

end


%% Echo
% --------------------------
% clear tmp i j options
clear i j options
disp(' ')
disp('Flow parameters')
disp(['    nx = ' num2str(dns.nx)])
disp(['    ny = ' num2str(dns.ny)])
disp(['    nz = ' num2str(dns.nz)])
disp(['    nu = ' num2str(1/dns.retau)])
disp(['    Lx = ' num2str(dns.Lx) '  (Lx+ = ' num2str(dns.Lxp) ')'])
disp(['    Ly = ' num2str(dns.Ly) '  (Ly+ = ' num2str(dns.Lyp) ')'])
disp(['    Lz = ' num2str(dns.zd(end))])
disp(['    dx+ = ' num2str(dns.dxp)])
disp(['    dy+ = ' num2str(dns.dyp)])
disp(['    dz+min = ' num2str(min(abs(diff(dns.zc))*dns.retau))])
disp(['    dz+max = ' num2str(max(diff(dns.zc)*dns.retau))])
disp(['    deltat = ' num2str(dns.dt)])
disp(['    dt+ = ' num2str(dns.dt*dns.retau)])
disp(['    headx = ' num2str(dns.headx)])
disp(['    heady = ' num2str(dns.heady)])
disp(['    AR = ' num2str(dns.cellAR)])
disp(['    walls = ' num2str(dns.walls)])
disp(' ')
disp('Riblets parameters')
disp(['    alfa = ' num2str(rib.alfa) '°'])
disp(['    k/s = ' num2str(1/rib.s_k)])
disp(['    s/h = ' num2str(rib.s_h) '  (s+ = ' num2str(rib.sp) ')'])
disp(['    stokexp = ' num2str(rib.stokexp) '  (err = ' num2str(rib.stokdisp(rib.stokexp,rib.phiwall)) ')'])
disp(['    nppr = ' num2str(rib.sp/dns.dyp)])
%disp(['    cell z-exp_ratio = ' num2str(dns.exp_ratio)])

% FLOW PARAMETER TXT FILE (#table doesn't mix strings and floats in Octave. Left commented because it's no use
% values 		= [dns.nx; dns.ny; dns.nz; dns.retau; dns.Lx; dns.Ly; dns.zd(end); dns.dxp; dns.dyp; min(diff(dns.zc)*dns.retau); max(diff(dns.zc)*dns.retau); dns.dt; dns.dt*dns.retau; dns.headx; dns.heady; rib.alfa; 1/rib.s_k; rib.s_h; rib.stokexp; rib.sp/dns.dyp; dns.zcp];
% T 		= table(param,values);
% writetable(T, 'flow_param.txt');

%% Create initial condition
% --------------------------
if dns.generateInitialField
  disp(' ')
  disp('Generating initial field')
  field=fieldGenerator('../fields/riblets.field',dns.fieldToInterpolate,dns);
end

%% Create initial parameter file
% --------------------------
channel = 0;
f=fopen('../input/riblets.in','w+');
fprintf(f,'nx=%d\t\t\t ny=%d\t\t\t nz=%d\t\t walls=%d\t\t extra=%d\n',dns.nx, dns.ny, dns.nz, dns.walls, dns.extra);
fprintf(f,'nu=%f\t\t Lx=%f\t Ly=%f\t deltat=%f\t cflmax=%f\t fouriermax=%f\n',dns.nu, dns.Lx, dns.Ly, dns.dt, dns.cflmax, dns.fouriermax);
fprintf(f,'headx=%f\t heady=%f\n',dns.headx, dns.heady);
fprintf(f,'it_save=%d\t it_stat=%d\t\t it_max=%d\t\t it_cfl=%d\t\t it_check=%d\n',dns.it_save,dns.it_stat,dns.it_max,dns.it_cfl,dns.it_check);
fprintf(f,'continue=FALSE\n\n');
fprintf(f,'ratiohs=%.12f\t spacing=%.12f\t stokexp=%.12f\t radius=%.12f\n ',1/rib.s_k,rib.s_h,rib.stokexp,dns.radius);
fprintf(f,'lambda1=%.6f\t lambda2=%.6f\t lambda3=%.6f\n',rib.lambda1,rib.lambda2,rib.lambda3);
fprintf(f,'ampl1=%.6f\t ampl2=%.6f\t ampl3=%.6f\n',rib.ampl1,rib.ampl2,rib.ampl3);
fclose(f);
% clear f



%% Tschüß
% ---------------------------
disp(' ')
disp('Remember to check:  ')
disp('   1. that all input parameters are correct and as desired')
disp('   2. that the number of walls is set to one in riblets.cpl')
disp('   3. that the grid looks fine')
disp('   4. that the definition of the body and edge in riblets.cpl is as intended,')
disp('      in fact it is implemented for triangular riblets at the moment but can easily modified.')
disp('   5. that the HPC batch job scripts match what input in parallelbcs.h')
disp(' ')

disp('Tschüß!')
