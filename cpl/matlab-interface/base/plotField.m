function [out, f] = plotField(filenamein)

%% Channel reader - plots

i = fopen(filenamein,'r');
out.nx = fread(i,1,'int');
out.ny = fread(i,1,'int');
out.nz = fread(i,1,'int');
out.it = fread(i,1,'int');
out.Lx = fread(i,1,'double');
out.Ly = fread(i,1,'double');
out.headx = fread(i,1,'double');
out.heady = fread(i,1,'double');
out.nu = fread(i,1,'double');
out.dt = fread(i,1,'double');
out.zd=fread(i, 2*out.nz+1, 'double');
out.var=reshape(fread(i, 'double'), [4, out.nz+1, out.ny, out.nx]);

f.u = figure(1)
pcolor(squeeze(out.var(2,:,:,10))); shading interp
xlabel("spanwise")
ylabel("wall-normal")
title("u")
colorbar();

f.v = figure(2)
pcolor(squeeze(out.var(3,:,:,10))); shading interp
xlabel("spanwise")
ylabel("wall-normal")
title("v")
colorbar();


f.w = figure(3)
pcolor(squeeze(out.var(4,:,:,10))); shading interp
xlabel("spanwise")
ylabel("wall-normal")
title("w")
colorbar();