function var_new=fieldGenerator(filenameout,filenamein,dns)

%% STARTING FIELD CREATION FROM CHANNEL.FIELD     ~ BLADES

i = fopen(filenamein,'r');
nx = fread(i,1,'int');
ny = fread(i,1,'int');
nz = fread(i,1,'int');
it = fread(i,1,'int');
Lx = fread(i,1,'double');
Ly = fread(i,1,'double');
headx = fread(i,1,'double');
heady = fread(i,1,'double');
nu = fread(i,1,'double');
dt = fread(i,1,'double');
%fclose(i); i = fopen('./channel0.field','r');
zd=fread(i, 2*nz+1, 'double');
var=reshape(fread(i, 'double'), [4, nz+1, ny, nx]);
disp('    Input field read')
x=linspace(0,Lx,nx+1); x=x(1:end-1);
y=linspace(0,Ly,ny+1); y=y(1:end-1);
z = zd(1:2:end)'; 
if z(end-1)<1.5 
    zvec = [z(1:end-1)*(1-dns.zd(1))+dns.zd(1), 2-(z(end-1:-1:1)*(1-dns.zd(1))+dns.zd(1))];
    indx = [(1:(numel(z)-1)), ((numel(z)-1):-1:1)];
else
    zvec = z;
    indx = 1:numel(z);
end

%[Y,Z,X]=meshgrid(y,z,x);
%Fp=scatteredInterpolant(X(:),Y(:),Z(:),reshape(var(1,:,:,:),[prod([nz+1, ny, nx]),1]));
%Fu=scatteredInterpolant(X(:),Y(:),Z(:),reshape(var(2,:,:,:),[prod([nz+1, ny, nx]),1]));
%Fv=scatteredInterpolant(X(:),Y(:),Z(:),reshape(var(3,:,:,:),[prod([nz+1, ny, nx]),1]));
%Fw=scatteredInterpolant(X(:),Y(:),Z(:),reshape(var(4,:,:,:),[prod([nz+1, ny, nx]),1]));

if dns.laminar == false
    
    var = fft(fft(var,[],4),[],3);
    
    
    
    %disp('   Interpolant defined')
    %clear var
    
    nxn=dns.nx;
    nyn=dns.ny;
    nzn=dns.nz;
    %x_new=linspace(0,Lx,nxn+1); x_new=x_new(1:end-1);
    %y_new=linspace(0,Ly,nyn+1); y_new=y_new(1:end-1);
    zz_new=dns.zc;
    z_new=dns.zd;
    var_new=zeros(4,nzn+1,nyn,nxn);
    
    
    iiix = [0:floor(nx/2), -floor(nx/2)+1-mod(nx,2):-1];
    iiiy = [0:floor(ny/2), -floor(ny/2)+1-mod(ny,2):-1];
    
    for iV=1:4
        for ix=1:floor(min(nx,nxn)/2)+1
            if iiix(ix)<0
                IX=nxn+iiix(ix)+1;
            else
                IX=iiix(ix)+1;
            end
            for iy=1:min(ny,nyn)
                if iiiy(iy)<0
                    IY=nyn+iiiy(iy)+1;
                else
                    IY=iiiy(iy)+1;
                end
                if iV==4 || iV==1
                    vvec = [squeeze(var(iV,1:end-1,iy,ix))', -squeeze(var(iV,end-1:-1:1,iy,ix))'];
                else
                    vvec = var(iV,indx,iy,ix);
                end
                var_new(iV,:,IY,IX) = interp1(zvec,var(iV,indx,iy,ix),zz_new,'linear',0);
            end
        end
        disp(['    Component ' num2str(iV) ' interpolated'])
    end
    
    var_new = (ifft(ifft(var_new,[],3),[],4,'symmetric'))*nxn*nyn/nx/ny;
    var_new = real(var_new);
    
    if dns.walls==1
        var_new([1,4],:,:,end-1:end)=0;
        var_new(:,:,:,1)=0;
    else
        var_new([1,4],:,:,end-1:end)=0;
        var_new([2,3],:,:,end)=0;
        var_new(:,:,:,1)=0;
    end
    
else
   
    
    nxn=dns.nx;
    nyn=dns.ny;
    nzn=dns.nz;
    zz_new=dns.zc - dns.zc(1);
    z_new=dns.zd;
    
    if dns.couette == false
    	laminar_profile = -dns.headx*0.5/dns.nu*zz_new.*(zz_new - zz_new(end)*2); %parabola
    else
	laminar_profile = zz_new/zz_new(end);
    end

    var_new=zeros(4,nzn+1,nyn,nxn);
    for i = 1:nyn
        for j = 1:nxn
		var_new(2,:,i,j) = laminar_profile;
		var_new(3,:,i,j) = laminar_profile;
        end
    end
    
end
    

% 
% [Y_new,Z_new,X_new]=meshgrid(y_new,zz_new,x_new);
% P = Fp(X_new,Y_new,Z_new);
% var_new(1,:,:,:)=reshape(P,[4, nzn+1, nyn, nxn]);
% P = Fu(X_new,Y_new,Z_new);
% var_new(2,:,:,:)=reshape(P,[4, nzn+1, nyn, nxn]);
% P = Fv(X_new,Y_new,Z_new);
% var_new(3,:,:,:)=reshape(P,[4, nzn+1, nyn, nxn]);
% P = Fw(X_new,Y_new,Z_new);
% var_new(4,:,:,:)=reshape(P,[4, nzn+1, nyn, nxn]);
% disp('   Field interpolated')
% clear P

%% WRITE FILE
d = fopen(filenameout,'w');
it=0;
domain = [nxn,nyn,nzn,it]';
headx=dns.headx;
heady=dns.heady;
nu=dns.nu;
deltat=dns.dt;
Lx=dns.Lx;
Ly=dns.Ly;
inputs= [Lx,Ly,headx,heady,nu,deltat];
fwrite(d,domain,'int');
fwrite(d,inputs,'double');
fwrite(d,z_new,'double');
fwrite(d,var_new,'double');
fclose(d);
disp('    Field written')

%if dns.init_plot == 'true'
%	[out,g] = plotField(filenameout,dns);
%end
