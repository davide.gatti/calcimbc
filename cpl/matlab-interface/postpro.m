clear
clc
close all

% Example of a postprocessing: it just reads a file at the moment
addpath('./base/')
[field, dns] = readfield('../fields/riblets.field');
% [field1, dns1] = readfield('../../../channel.field');

% [field_new, dns_new] = readfield('../campo_iniziale/riblets_new.field');

%field=reshape(fread(i, 'double'), [4, dns.nz+1, dns.ny, dns.nx]);


% figure()
% pcolor(dns.y,dns.zc,squeeze(field(2,:,:,30)))
% shading interp
% axis equal
% colorbar()


% primo indice field: pressione(1), u(2), v(3), w(4) (penso)
% secondo indice field: distanza da parete
% terzo indice field: coordinata spanwise
% ultimo indice field: coordinata streamwise
% prova = squeeze(field(2,:,:,100));
% figure(1)
% pcolor(dns.y,dns.zc,prova)
% shading interp
% axis equal
% colorbar()

prova = squeeze(field(2,:,:,4));
figure(2)
pcolor(dns.y,dns.zc,prova)
shading interp
axis equal
colorbar()

u_3d = squeeze(field(2,:,:,:));

for i = 1:length(u_3d(:,1,1))
    for j = 1:length(u_3d(1,:,1))
        u_2d(i,j) = sum(u_3d(i,j,:))/length(u_3d(1,1,:));
    end
end

for i = 1:length(u_2d(:,1))
    u_1d(i) = sum(u_2d(i,:))/length(u_2d(1,:));
end

figure(3)
plot(dns.zc,u_1d)

% figure(4)
% hold on; grid on
% plot(dns.zc(1:length(u_1d)/2),u_1d(1:length(u_1d)/2),'b')
% plot(fliplr(dns.zc(1:length(u_1d)/2)),fliplr(u_1d(length(u_1d)/2+1:end)),'r')


