close all
clear
clc

addpath('./base/')
fields_name = dir('../fields/*.field');
size_names = size(fields_name);
size = size_names(1);

field_file = fields_name(1).name;
[field] = readfield("../fields/" + field_file);
nz = length(field(1,:,1,1));

lambda_im = zeros(round(nz/2),1);



clear field_file
clear field
clear dns
clear ny
clear nz


for namefile = 1:1
    field_file = fields_name(namefile).name;
    [field, dns] = readfield("../fields/" + field_file);

    nx = length(field(1,1,1,:));    
    ny = length(field(1,1,:,1));
    nz = length(field(1,:,1,1));

    dx = dns.Lx/dns.nx;
    dy = dns.Ly/dns.ny;
% 
%     u1 = field(2,20,10,10)
%     u2 = field(2,20,10,11)
%     v1 = field(3,20,10,10)
%     v2 = field(3,20,10,11)
%     w1 = field(4,20,10,10)
%     w2 = field(4,20,10,11)

    for i = 2:nx-1
        for j = 2:ny-1
            for k = 2:nz-1
    
                d1z = dns.zd(2*k+1)-dns.zd(2*k-1);
                d1zm = dns.zc(k)-dns.zc(k-1);
                d1zp = dns.zc(k+1)-dns.zc(k);
                den = d1zm^2+d1zm*d1zp-d1zp^2;

                
%                 dxz1 = sqrt((0.5*dx)^2+(0.5*dz1)^2);
%                 dxz2 = sqrt((0.5*dx)^2+(0.5*dz2)^2);
%                 dxz3 = sqrt((0.5*dx)^2+(0.5*dz3)^2);
%                 dyz1 = sqrt((0.5*dy)^2+(0.5*dz1)^2);
%                 dyz2 = sqrt((0.5*dy)^2+(0.5*dz2)^2);
%                 dyz3 = sqrt((0.5*dy)^2+(0.5*dz3)^2);            
% 
%                 weightx1 = (1/dxz1)/(2/dxz1+2/dxz2);
%                 weightx2 = (1/dxz2)/(2/dxz1+2/dxz2);
%                 weightx3 = (1/dxz2)/(2/dxz2+2/dxz3);
%                 weightx4 = (1/dxz3)/(2/dxz2+2/dxz3);
%                 weighty1 = (1/dyz1)/(2/dyz1+2/dyz2);
%                 weighty2 = (1/dyz2)/(2/dyz1+2/dyz2);
%                 weighty3 = (1/dyz2)/(2/dyz2+2/dyz3);
%                 weighty4 = (1/dyz3)/(2/dyz2+2/dyz3);

%                 if i==5 && j==10 && k==154
%                     weightx1
%                     weightx2
%                     weightx3
%                     weightx4
%                 end


                grad_vel = zeros(3,3);
                grad_vel(1,1) = (field(2,k,j,i)-field(2,k,j,i-1))/dx;
                grad_vel(1,2) = 0.25*(field(2,k,j+1,i)+field(2,k,j+1,i-1)-field(2,k,j-1,i)-field(2,k,j-1,i-1))/dy;
%                 grad_vel(1,3) = ((weightx4*(field(2,k+1,j,i)+field(2,k+1,j,i-1))+weightx3*(field(2,k,j,i)+field(2,k,j,i-1)))-(weightx2*(field(2,k,j,i)+field(2,k,j,i-1))+weightx1*(field(2,k-1,j,i)+field(2,k-1,j,i-1))))/dz2;
                grad_vel(1,3) = 0.5*(d1zm/den*(field(2,k-1,j,i-1)+field(2,k-1,j,i))-d1zp^2/d1zm/den*(field(2,k,j,i-1)+field(2,k,j,i))+(d1zp/den-1/d1zm)*(field(2,k+1,j,i-1)+field(2,k+1,j,i)));
                grad_vel(2,1) = 0.25*(field(3,k,j,i+1)+field(3,k,j-1,i+1)-field(3,k,j,i-1)-field(3,k,j-1,i-1))/dx;
                grad_vel(2,2) = (field(3,k,j,i)-field(3,k,j-1,i))/dy;
%                 grad_vel(2,3) = ((weighty4*(field(3,k+1,j,i)+field(3,k+1,j-1,i))+weighty3*(field(3,k,j,i)+field(3,k,j-1,i)))-(weighty2*(field(3,k,j,i)+field(3,k,j-1,i))+weighty1*(field(3,k-1,j,i)+field(3,k-1,j-1,i))))/dz2;
                grad_vel(2,3) = 0.5*(d1zm/den*(field(3,k-1,j-1,i)+field(3,k-1,j,i))-d1zp^2/d1zm/den*(field(3,k,j-1,i)+field(3,k,j,i))+(d1zp/den-1/d1zm)*(field(3,k+1,j-1,i)+field(3,k+1,j,i)));
                grad_vel(3,1) = 0.25*(field(4,k,j,i+1)+field(4,k-1,j,i+1)-field(4,k,j,i-1)-field(4,k-1,j,i-1))/dx;
                grad_vel(3,2) = 0.25*(field(4,k,j+1,i)+field(4,k-1,j+1,i)-field(4,k,j-1,i)-field(4,k-1,j-1,i))/dy;
                grad_vel(3,3) = (field(4,k,j,i)-field(4,k-1,j,i))/d1z;

                [V,D] = eig(grad_vel);

%                 if i==10 && j==10 && k==289
%                     grad_vel
%                     D
%                 end

%             im = 0;
%             real_eig = zeros(3,1);
%             real_av = 0;

                for l = 1:3
                    if k <= nz/2
                        lambda_im(k) = lambda_im(k) + abs(imag(D(l,l)));
                    else
                        lambda_im(nz+1-k) = lambda_im(nz+1-k) + abs(imag(D(l,l)));
                    end

%                 if abs(imag(D(l,l))) ~= 0
%                     real_eig(l) = 1;
%                 end
                end

%             if sum(real_eig) == 1
%                 for m = 1:3
%                     if real_eig(m) == 1
%                         real_av = m;
%                     end
%                 end
%             end

%             if lambda_im(i,j,k,1) ~= 0 && sum(real_eig) == 1
%                 lambda_im(i,j,k,2:4) = V(:,real_av);
%             end

            end
        end
        disp(['file number = ' num2str(namefile) 'ix = ' num2str(i)])
    end
end

lambda_im = lambda_im/((nx-2)*(ny-2))/2;

T = table(dns.zc(2:161), lambda_im(2:161), 'VariableNames', { 'z', 'l'} );
writetable(T, '/home/ws/og2387/Documenti/lambda_ci/lambda_matlab.txt')



% figure()
% pcolor(dns.y,dns.zc(1:nz/2),lambda_im')
% shading interp
% axis equal
% colorbar()
% 
% lambda_im_rib = zeros(16,nz/2);
% 
% for l = 1:16
%     for i = 1:ny
%         if mod(i-l,16) == 0
%             lambda_im_rib(l,:) = lambda_im_rib(l,:)+lambda_im(i,:);
%         end
%     end
% end
% 
% lambda_im_rib = lambda_im_rib/(ny/16);

% figure()
% pcolor(dns.y(1:16),dns.zc(1:nz/2),lambda_im_rib')
% shading interp
% axis equal
% colorbar()
% 
% 
% %%
% 
% 
% 
% figure(); hold on; grid on
% plot(z_w(1:161),lambda_im(1:161),'k*')







