# -*- coding: utf-8 -*-
#This Python script contains all the functions needed to compute the Immersed Boundary Correction Coefficients.
#These functions are:
#-Bisection: a function that is used to get closer to the point where the body crosses the numerical stencil
#-InBody: a function that is used to check whether a point lies in the body or not
#-CoefForZ: a function that uses Bisection and InBody to compute the IMBC coefficient for each Z value
#-ComputeIMBC: a function that scans the grid and uses CoefForZ to find the IMBC coefficients for each Z value among all X and Y values
#Finally, all these functions are then used in the complete Python script that gives as an output the IMBC coefficients vector for each point of the grid


# test 2



def CoefForZ(ix, iy, nz,  delta_x, delta_y, dirx, diry, dirz, z_d):
  #Input description: -ix: x index
  #                   -iy: y index
  #                   -delta_x: grid spacing in direction x
  #                   -delta_y: grid spacing in direction y
  #                   -dirx: scanning direction index: if 1, then direction x
  #                   -diry: scanning direction index: if 1, then direction y
  #                   -dirz: scanning direction index: if 1, then direction z
  #                   -z_d: vector containing the points of the grid in z direction (even points = p, u, v, odd points = w)
  #                   -zilo: z lower index
  #                   -zihi: z upper index

  # Modules to import:
  import numpy as np

  # Functions used:

  def InBody(xp, yp, zp):
  #Input description: -xp: x coordinate of the point to check
  #                   -yp: y coordinate of the point to check
  #                   -zp: z coordinate of the point to check
    import numpy as np
    import math
    h_s = 0.866025403784; s = 0.08;
    lambda1 =  7.5; lambda2 = 1.25; lambda3 = 2.5;
    amp1 = 0; amp2 = 0; amp3 = 0;
    h = h_s*s #riblet height
    edge_z = h/2 #riblet height in the actual reference frame
    sinusoidal_term = amp1*np.sin(2*np.pi/lambda1*xp) + amp2*np.sin(2*np.pi/lambda2*xp) + amp3*np.sin(2*np.pi/lambda3*xp) #term that includes all the sinusoids
    y_new = yp - sinusoidal_term #new y coordinate, corrected by the influence of sinusoidal term
    z_rib_at_p = abs(h_s*2*(y_new - s*math.floor(y_new/s + 0.5))) - h/2 #riblet height at the given point
    if zp < z_rib_at_p:
      return 1 #the given point is inside the riblet
    else:
      return 0 #the given point is not inside the riblet
    

  def Bisection(x1, x2, coor):
    # Input description: - x1: coordinate of the first point for bisection
    #                    - x2: coordinate of the second point for bisection
    #                    - coor: index to tell what x1 and x2 are: if = 1, then x; if = 2 then y; if = 3 then z
    
    toll = 1e-10
    
    def check_boundary(p):
        if coor == 1:
            return InBody(p, y, z)
        elif coor == 2:
            return InBody(x, p, z)
        elif coor == 3:
            return InBody(x, y, p)
    
    c1 = check_boundary(x1)
    c2 = check_boundary(x2)
    
    if c1 == c2:
        return 'Error, the point is not included in the interval between the two points'
    
    a, b = x1, x2
    
    while abs(b - a) > toll:
        bp = 0.5 * (a + b)
        b_check = check_boundary(bp)
        if b_check == c1:
            a = bp
        else:
            b = bp
            
    return bp

  
  def zboundaries(z_d, x, y):
    #This function computes zilo (lower z index) and zihi(upper z index) for a given (x, y) pairing
    zilo = 1 #zilo is initialized
    while InBody(x, y, z_d[2*zilo + dirz]):
      zilo+=1
    zihi = zilo #zihi is initialized
    for iz in range(zilo+1, nz - dirz):
        z = z_d[2*iz + dirz]
        if InBody(x-delta_x, y, z) or InBody(x+delta_x, y, z) or InBody(x, y-delta_y, z) or InBody(x, y+delta_y, z) or InBody(x, y, z_d[2*iz+2+dirz]) or InBody(x, y, z_d[2*iz+2+dirz]):
          zihi = iz  
    return zilo, zihi
  

  def compute_rsd(InBodyCheck, coordinates, coor, delta):
   d = delta
   if InBodyCheck:
    d = abs(coordinates[0] - Bisection(coordinates[0], coordinates[1], coor))
   
   if coor == 1 or coor == 2:
    x_try = coordinates[0]
    sign = 1
    if coordinates[0]>coordinates[1]:
      sign = -1
    for _ in range(0, 1002):
        x_try = x_try + sign*delta/1000
        if coor == 1:
          if InBody(x_try, y, z):
            break
        else:
          if InBody(x, x_try, z):
            break

    if abs(x_try - coordinates[0]) < delta:
        c = Bisection(x_try, coordinates[0], coor)
        d = abs(coordinates[0] - c)
    if d == 0:
        d = 1e-21
   return delta/d
  

  nu = 0.005
  imbc = np.array(()) #initialize the imbc array that will store the IB coefficient for a given z
  z_v = np.array(()) #array for z values
  x = (ix + dirx/2)*delta_x; y = (iy + diry/2)*delta_y; #assign values to x and y based on the indices ix and iy
  d2x = 1/(delta_x**2); d2y = 1/(delta_y**2);
  zilo, zihi = zboundaries(z_d, x, y)
  for iz in range(zilo, zihi+1):
    z = z_d[2*iz + dirz]
    delta_zm = (z - z_d[2*iz - 2 + dirz])
    delta_zp = (z_d[2*iz + 2 + dirz] - z)

    d2zm = (1/(delta_zm*(z_d[2*iz + dirz + 1] - z_d[2*iz + dirz - 1]))); #this value will be used in the coefficients construction
    d2zp = (1/(delta_zp*(z_d[2*iz + dirz + 1] - z_d[2*iz + dirz - 1]))); #this value will be used in the coefficients construction
    rsd = 0 #this variable will be used in the coefficient construction and will store all the different coefficients
    if InBody(x, y, z):
        rsd = 1e20 #if the point is inside the body, then we assign a sufficiently high value to rsd in order to set all variables = 0
    else:
        rsd_terms_x = compute_rsd(InBody(x-delta_x, y, z), np.array((x, x - delta_x)), 1, delta_x)*d2x + compute_rsd(InBody(x+delta_x, y, z), np.array((x, x + delta_x)), 1, delta_x)*d2x
        rsd_terms_y = compute_rsd(InBody(x, y-delta_y, z), np.array((y, y - delta_y)), 2, delta_y)*d2y + compute_rsd(InBody(x, y+delta_y, z), np.array((y, y + delta_y)), 2, delta_y)*d2y
        rsd_terms_z = compute_rsd(InBody(x, y, z-delta_zm), np.array((z, z-delta_zm)), 3, delta_zm)*d2zm + compute_rsd(InBody(x, y, z + delta_zp), np.array((z, z+ delta_zp)), 3, delta_zp)*d2zp
        rsd = rsd_terms_x + rsd_terms_y + rsd_terms_z
        imbc = np.append(imbc, (rsd - 2*d2x - 2*d2y - d2zp - d2zm)*nu)
        z_v = np.append(z_v, z)

  return imbc, z_v

#def ComputeIMBC(dirx, diry, dirz, xilo, xihi, yilo, yihi, zilo, zihi, z_d):
  #Input description: -dirx: scanning direction index: if 1, then direction x
  #                   -diry: scanning direction index: if 1, then direction y
  #                   -dirz: scanning direction index: if 1, then direction z
  #                   -xilo: x lower index
  #                   -xihi: x upper index
  #                   -yilo: y lower index
  #                   -yihi: y upper index
  #                   -zilo: z lower index
  #                   -zihi: z upper index
  #                   -z_d: vector for z coordinates
  # Functions to import:
  #from functions_storage import CoefForZ
  #first we must compute zilo and zihi

  imbc = [[[0 for _ in range(zihi - zilo + 1)] for _ in range(yihi - yilo + 1)] for _ in range(xihi - xilo + 1)] #inizialization of the 3D data structure, where IB coefficients will be stored
  for ix in range(xilo, xihi+1):
    for iy in range(yilo, yihi+1):
      x = (ix + dirx/2)*delta_x; y = (iy + diry/2)*delta_y; #assign values to x and y based on the indices ix and iy
      zilo = 1
      while InBody(x, y, z_d(2*zilo + dirz)):
        zilo +=1
      for iz in range(zilo+1, nz - dirz):
        z = z_d(2*iz + dirz)
        if InBody(x-delta_x, y, z) or InBody(x+delta_x, y, z) or InBody(x, y-delta_y, z) or InBody(x, y+delta_y, z) or InBody(x, y, z_d[2*iz+2+dirz]) or InBody(x, y, z_d[2*iz+2+dirz]):
          zihi = iz  
      for iz in range(zilo, zihi+1):  
        imbcz = CoefForZ(ix, iy, iz, dirx, diry, dirz) #the function CoefForZ is called for each point in the grid, giving the IB coefficient in the selected point
        imbc[ix - xilo][iy - yilo][iz - zilo] = imbcz

  return imbc #imbc is a 3D data structure



