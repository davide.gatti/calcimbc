#This Python script uses the functions included in 'functions_storage.py' and computes the IB coefficients given the geometry provided.
#Data:
from functions_storage_compact import CoefForZ
import numpy as np
ny = 16; Ly = 0.16; delta_y = Ly/ny;
nx = 100; Lx = 2; delta_x = Lx/nx;
nz = 154;
d2x = 1 / (delta_x**2); d2y = 1 / (delta_y**2);
h_s = 0.866025403784; s = 0.08;
z_d = np.loadtxt('../COEFFICIENTI_IMBC_SIMULAZIONE/Lx2Ly0.16s0.08Walss1/z_vector.txt')
lambda1 =  7.5; lambda2 = 1.25; lambda3 = 2.5;
amp1 = 0; amp2 = 0; amp3 = 0;
dirx = 0
diry = 0
dirz = 1
ix = 10
xilo = 0
xihi = 1
yilo = 0
yihi = 17
imbc = np.array(())
z_v = np.array(())
for iy in range(yilo, yihi):
    imbcv, zv = CoefForZ(ix, iy, nz, delta_x, delta_y, dirx, diry, dirz, z_d)
    imbc = np.hstack((imbc,imbcv))
    z_v = np.hstack((z_v, zv))

print(imbc)
#vettore_da_stampare = np.vstack((imbc, z_v))
#np.savetxt('vettore_vimbc2.txt', vettore_da_stampare)

#y = (iy + diry/2)*delta_y

#iy = 4
#imbz_err, zval = CoefForZ(ix, iy, nz, delta_x, delta_y, dirx, diry, dirz, z_d)
#print(imbz_err)