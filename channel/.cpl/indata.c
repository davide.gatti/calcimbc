/*-I/home/ws/rc8764/opt/openmpi/include -L/home/ws/rc8764/opt/openmpi/lib -Wl,-rpath -Wl,/home/ws/rc8764/opt/openmpi/lib -Wl,--enable-new-dtags -lmpi */

#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64

#define _LARGE_FILES

#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

extern char errortemp_[(80+1)];

struct arrptr{int l,h; ssize_t i; char *a;};struct dynptr{void* p; int t;};extern char INTERRUPT;
typedef 
 void (*traphandler_t)(const char *);struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;};extern struct jbtrap mainjb;
extern struct jbtrap * curjb;

#define traphandler curjb->tr
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};extern struct freefunc * freestack;

#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}

#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name

#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
extern void traprestore(void *ptr);
extern void condfree(void *ptr);
extern int friexecerror(char** s);
extern int (*friexec)(char** s);

#define mmovefrom(var,buf,type) *(type *)(buf)=*var

#define mmoveto(var,buf,type) *var=*(type *)(buf)

#define mainstart void default_traphandler(const char *errormessage){   if(errormessage[0]){     printERR;     freemem(NULL);     exit(EXIT_FAILURE);   }else{     freemem(NULL);     exit(EXIT_SUCCESS);   } } int main(int argc, char **argv){ struct freefunc* es; 			{struct sigaction act,oldact; act.sa_sigaction=trapsignal; sigemptyset(&act.sa_mask); act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; sigaction(SIGSEGV,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); sigaction(SIGFPE,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); sigaction(SIGILL,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); sigaction(SIGINT,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); /* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ else {traphandler=default_traphandler;       freestack=NULL;       feenableexcept(fpe);      }; } es=freestack;
extern int dynptrerr(int type);
extern void *errmalloc(void);
extern void ioerr(FILE *fil);
extern void errfclose(void *voidf);
extern void errfopen(FILE **f, const char *name, int mode);
extern int scanrec(FILE *f, const char *format, void *var) ;
extern int scanbool(FILE *f, const char *format, int *var) ;
extern int myfgets(char *name, char *var, char *varend, FILE *f) ;
extern int mygetline(char *name, char **var, FILE *f) ;
extern void trapsignal(int signum, siginfo_t *info, void *ucontext);





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
extern int iprocx_;
extern int nprocx_;
extern int iprocy_;
extern int nprocy_;

extern int iproc_;
extern int nproc_;

extern int has_terminal_;


/* Complex-number type definition and corresponding extensions to the ! */
/* CPL language                                                       ! */
/*                                                                    ! */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net               ! */
/* Released under the attached LICENSE.                               ! */
/*                                                                    ! */
/* Code maturity: green.                                              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

/* Library of common matrix algebra operations for real-valued matrices ! */
/* including suitable extensions to the CPL language to handle infix    ! */
/* notation for such functions. See matrix.info for usage.              ! */
/*                                                                      ! */
/* Copyright 1997-2021 Paolo Luchini http://CPLcode.net                 ! */
/* Released under the attached LICENSE.                                 ! */
/*                                                                      ! */
/* Code maturity: green.                                                ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

/* Transparently reshapes the operation of the CPL compiler so as to provide ! */
/* array bounds checking and more run-time checks, without any modifications ! */
/* to the original program (at the expense of a slower execution).           ! */
/* To activate, put "USE rtchecks" at the beginning of your program.         ! */
/* See infocpl rtchecks.                                                     ! */
/*                                                                           !  */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net                      ! */
/* Released under the attached LICENSE.                                      ! */
/*                                                                           ! */
/* Code maturity: mostly green but the TRACE command is orange.              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <sys/ioctl.h>                                                          
/*
<*#ifdef __GNUC__
  const
#endif
int cb(int lb, int ub, int index);
#ifdef __GNUC__
  const
#endif
char* cp(int inputpos);
#ifdef __GNUC__
  const
#endif
char *cr(char *lb, char *ub, char *index);
*>
*/
/* nota: #ifdef non passa in C SECTION */
#undef printERR
#define printERR fprintf(stderr,"\r%s in line %d of %s: PROGRAM HALTED  \n",errormessage,ln,fn);fflush(stderr)
extern volatile int ln;
extern char * volatile fn;
extern const int cb(int lb, int ub, int index);
extern const char * cp(void);
extern const int ca(void);
extern const unsigned char sigNAN[8];


/* CPL interface to the termios functions needed to turn on and off ! */
/* character-by-character terminal input                            ! */
/*                                                                  ! */
/* Copyright 2002-2023 Paolo Luchini http://CPLcode.net             ! */
/* Released under the attached LICENSE.                             ! */
/*                                                                  ! */
/* Code maturity: green.                                            ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <termios.h>
void CHARbyCHAR_1CHARbyCHAR(int descr_);
void CHARbyCHAR_2LINEbyLINE(void);

extern struct termios CHARbyCHAR_CHARbyCHAR_newsetting;
extern struct termios CHARbyCHAR_CHARbyCHAR_oldsetting;

extern int CHARbyCHAR_CHARbyCHAR_CbCdescr;

extern void CHARbyCHAR_1CHARbyCHAR(int descr_);


extern void CHARbyCHAR_2LINEbyLINE(void);

/* Library providing an interface to the select system call ! */
/* in order to detect if input is waiting to be read.       ! */
/* See infocpl INPUTREADY.                                  ! */
/*                                                          !  */
/* Copyright 2008-2020 Paolo Luchini http://CPLcode.net     ! */
/* Released under the attached LICENSE.                     ! */
/*                                                          ! */
/* Code maturity: green.                                    ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern int fd_input_ready(int fd, int sec, int usec);








extern int BlockLevel;

void singlestepper(int lineN_,char *line_);






extern FILE *rtchecks_SingleStep_grabin;

extern int rtchecks_SingleStep_StopLevel;
extern char rtchecks_SingleStep_LoopCount[(ssize_t)sizeof(int)*(100+1)];
extern int rtchecks_SingleStep_LastLine;

extern int rtchecks_SingleStep_paused;

extern int rtchecks_SingleStep_lastfnlength;
extern int rtchecks_SingleStep_lastrow;

extern char *rtchecks_SingleStep_lastfn;

extern int rtchecks_SingleStep_termwidth;
extern int rtchecks_SingleStep_termheight;

extern char *rtchecks_SingleStep_hotkeys;


extern void rtchecks_SingleStep_1RestoreScroll(void);


extern void TRON(void);


extern void singlestepper(int lineN_,char *line_);


/**/
struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)


int non_laminar_;

double nu_;
double headx_;
double heady_;

int walls_;
int waven_;

double spacing_;
double ratiohs_;

int use_coco_;
int extra_;

double radius_;
double stokexp_;

int nx_;
int ny_;
int nz_;

double Lx_;
double Ly_;

double cflmax_;
double fouriermax_;
double deltat_;
double maxTime_;
double time_;
double Uconv_;

int it_save_;
int it_stat_;
int it_max_;
int it_cfl_;
int it_check_;
int it_;

char cfl_[(ssize_t)sizeof(double)*(1+1)];

int continue_;


void indata_2readInputFile(struct RIBLETS_ *this_,char *fname_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="indata.cpl";{struct freefunc* es=freestack;
	FILE *ribletsin_;

	int _3h;
ssize_t _4i;
char *_5lambda;
int _7h;
ssize_t _8i;
char *_9ampl;
ribletsin_=NULL;ln=20;errfopen(&ribletsin_,fname_,O_RDONLY);ln=21;   if(!(scanrec( ribletsin_ ," non_laminar = %d",&non_laminar_)>0 &&scanrec( ribletsin_ ," nu = %lg",&nu_)>0 &&scanrec( ribletsin_ ," headx = %lg",&headx_)>0 &&scanrec( ribletsin_ ," heady = %lg",&heady_)>0))ioerr( ribletsin_ );
	ln=22;   if(!(scanrec( ribletsin_ ," walls = %d",&walls_)>0 &&scanrec( ribletsin_ ," spacing = %lg",&spacing_)>0 &&scanrec( ribletsin_ ," ratiohs = %lg",&ratiohs_)>0))ioerr( ribletsin_ );
	ln=23;   if(!(scanrec( ribletsin_ ," waven = %d",&waven_)>0))ioerr( ribletsin_ );
	ln=24;_3h=waven_;
ln=24;_4i=(ssize_t)sizeof(double)*_3h;

_5lambda=malloc(_4i);if(_5lambda==NULL)_5lambda=errmalloc();_5lambda-=(ssize_t)sizeof(double);{int i6;for(i6=1;i6<=_3h;i6++){memmove((double *)(_5lambda+i6*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}ln=24; 
	ln=25;_7h=waven_;
ln=25;_8i=(ssize_t)sizeof(double)*_7h;

_9ampl=malloc(_8i);if(_9ampl==NULL)_9ampl=errmalloc();_9ampl-=(ssize_t)sizeof(double);{int i10;for(i10=1;i10<=_7h;i10++){memmove((double *)(_9ampl+i10*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}ln=25; 
	ln=26;   if(!((scanrec( ribletsin_ ," lambda = ",NULL),({int i11=1; while(i11<=_3h){if(!scanrec( ribletsin_ ," %lg",(double *)(i11*(ssize_t)sizeof(double)+_5lambda))>0)ioerr( ribletsin_ ); i11++;}}),1)))ioerr( ribletsin_ );
	ln=27;   if(!((scanrec( ribletsin_ ," ampl = ",NULL),({int i12=1; while(i12<=_7h){if(!scanrec( ribletsin_ ," %lg",(double *)(i12*(ssize_t)sizeof(double)+_9ampl))>0)ioerr( ribletsin_ ); i12++;}}),1)))ioerr( ribletsin_ );
	ln=28;   if(!(scanrec( ribletsin_ ," use_coco = %d",&use_coco_)>0 &&scanrec( ribletsin_ ," radius = %lg",&radius_)>0 &&scanrec( ribletsin_ ," stokexp = %lg",&stokexp_)>0 &&scanrec( ribletsin_ ," extra = %d",&extra_)>0))ioerr( ribletsin_ );
	ln=29;   if(!(scanrec( ribletsin_ ," nx = %d",&nx_)>0 &&scanrec( ribletsin_ ," ny = %d",&ny_)>0 &&scanrec( ribletsin_ ," nz = %d",&nz_)>0 &&scanrec( ribletsin_ ," Lx = %lg",&Lx_)>0 &&scanrec( ribletsin_ ," Ly = %lg",&Ly_)>0))ioerr( ribletsin_ );
	ln=30;   if(!(scanrec( ribletsin_ ," cflmax = %lg",&cflmax_)>0 &&scanrec( ribletsin_ ," fouriermax = %lg",&fouriermax_)>0 &&scanrec( ribletsin_ ," deltat = %lg",&deltat_)>0 &&scanrec( ribletsin_ ," maxTime = %lg",&maxTime_)>0))ioerr( ribletsin_ );
	ln=31;   if(!(scanrec( ribletsin_ ," it_save = %d",&it_save_)>0 &&scanrec( ribletsin_ ," it_stat = %d",&it_stat_)>0 &&scanrec( ribletsin_ ," it_max = %d",&it_max_)>0 &&scanrec( ribletsin_ ," it_cfl = %d",&it_cfl_)>0 &&scanrec( ribletsin_ ," it_check = %d",&it_check_ )>0))ioerr( ribletsin_ );
	ln=32;   if(!(scanbool( ribletsin_ ," continue = %c%*4[A-Za-z] ",&continue_)))ioerr( ribletsin_ );
	ln=33; (*this_).h_s_ = ratiohs_;
	ln=34; (*this_).s_ = spacing_;
	ln=35; (*this_).stkexp_ = stokexp_;
	ln=36; (*this_).r_ = radius_;
	ln=37; (*this_).h_ = (*this_).h_s_*(*this_).s_;
	ln=38; (*this_).waven_ = waven_;
	ln=39; (*this_).lambda_.l=1;(*this_).lambda_.h=_3h;(*this_).lambda_.i=(ssize_t)sizeof(double);(*this_).lambda_.a=_5lambda;
	ln=40; (*this_).amp_.l=1;(*this_).amp_.h=_7h;(*this_).amp_.i=(ssize_t)sizeof(double);(*this_).amp_.a=_9ampl;
}fn=savefn; ln=saveln;}

/* Read parameters from the riblet file */
struct RIBLETS_ riblet_;

char *indata_3myname;
struct _indata_s4{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _indata_s4_s (ssize_t)sizeof(struct _indata_s4)
FILE *dnsinfo_;
FILE *indata_5;




double indata_16deltax;
double indata_17deltay;






#include <mpi.h>


/*INTEGER iprocx, nprocx=atoi(COMMANDLINE(1)), iprocy, nprocy=atoi(COMMANDLINE(2)) */
/*** typedef int (MPI_Datarep_extent_function)(MPI_Datatype, MPI_Aint *, void *) ununderstood ***/
/*** typedef int (MPI_Datarep_conversion_function)(void *, MPI_Datatype,
 int, void *, MPI_Offset, void *) ununderstood ***/
/*** typedef void (MPI_Comm_errhandler_function)(MPI_Comm *, int *, ...) ununderstood ***/
/*** typedef void (MPI_Session_errhandler_function) (MPI_Session *, int *, ...) ununderstood ***/
/*** typedef void (ompi_file_errhandler_function)(MPI_File *, int *, ...) ununderstood ***/
/*** typedef void (MPI_Win_errhandler_function)(MPI_Win *, int *, ...) ununderstood ***/
/*** typedef void (MPI_User_function)(void *, void *, int *, MPI_Datatype *) ununderstood ***/
/*** typedef int (MPI_Comm_copy_attr_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Comm_delete_attr_function)(MPI_Comm, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Type_copy_attr_function)(MPI_Datatype, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Type_delete_attr_function)(MPI_Datatype, int,
 void *, void *) ununderstood ***/
/*** typedef int (MPI_Win_copy_attr_function)(MPI_Win, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Win_delete_attr_function)(MPI_Win, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Grequest_query_function)(void *, MPI_Status *) ununderstood ***/
/*** typedef int (MPI_Grequest_free_function)(void *) ununderstood ***/
/*** typedef int (MPI_Grequest_cancel_function)(void *, int) ununderstood ***/
/*** typedef MPI_Comm_errhandler_function MPI_Comm_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_function MPI_File_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_function MPI_File_errhandler_function ununderstood ***/
/*** typedef MPI_Win_errhandler_function MPI_Win_errhandler_fn
  ununderstood ***/
/*** enum {
  /* MPI-1 *  /
 MPI_TAG_UB,
 MPI_HOST,
 MPI_IO,
 MPI_WTIME_IS_GLOBAL,

  /* MPI-2 *  /
 MPI_APPNUM,
 MPI_LASTUSEDCODE,
 MPI_UNIVERSE_SIZE,
 MPI_WIN_BASE,
 MPI_WIN_SIZE,
 MPI_WIN_DISP_UNIT,
 MPI_WIN_CREATE_FLAVOR,
 MPI_WIN_MODEL,

  /* MPI-4 *  /
 MPI_FT,  /* used by OPAL_ENABLE_FT_MPI *  /
 MPI_ATTR_PREDEFINED_KEY_MAX,
} ununderstood ***/
/*** typedef int (MPI_Copy_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Delete_function)(MPI_Comm, int, void *, void *) ununderstood ***/
int prevx_;
int nextx_;
int prevy_;
int nexty_;

/*INTEGER iproc, nproc,  */
int nreqs_;
int nsreqs_;


struct indata_s20 {int l,h; ssize_t i;struct arrptr a;};
char unpckbuf_[(ssize_t)sizeof(struct indata_s20)*(3+1)] ;

int szbuf_;


  MPI_Status status[4];
  MPI_Request request[4];
  MPI_Request srequest[4];
  MPI_Comm  cart_comm, cart_comm_x;
  int nproc, iproc, nprocx, nprocy, iprocx, iprocy, idim[2];
  int prevx, nextx, prevy, nexty;


void indata_21init_mpi(void){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="parallelbcs.h";{struct freefunc* es=freestack;
    
       MPI_Init(NULL,NULL);
       MPI_Comm_size(MPI_COMM_WORLD, &nproc); nproc_=nproc;
       MPI_Comm_rank(MPI_COMM_WORLD, &iproc); iproc_=iproc+1;
       int ndims=2, reorder=1, ierr;
       int dim_size[2], periods[2];
       dim_size[0] = nprocx_; nprocx=nprocx_;
       dim_size[1] = nprocy_; nprocy=nprocy_;
       periods[0] = 1;
       periods[1] = 1; 
       ierr =  MPI_Cart_create(MPI_COMM_WORLD,ndims,dim_size,
             periods,reorder,&cart_comm);
       MPI_Cart_coords(cart_comm, iproc, ndims, idim);
       iprocx_=idim[0]+1; iprocy_=idim[1]+1;
       iprocx=idim[0];  iprocy=idim[1];
       MPI_Cart_shift(cart_comm, 0, 1, &prevx, &nextx); prevx_=prevx+1; nextx_=nextx+1;
       MPI_Cart_shift(cart_comm, 1, 1, &prevy, &nexty); prevy_=prevy+1; nexty_=nexty+1;
       // Create a communicator with all processes that hold the same y-slab
       // this is useful for collecting x-averaged statistics, for instance
       MPI_Comm_split(cart_comm, iprocy_, iproc_, &cart_comm_x);
    
}fn=savefn; ln=saveln;} 

int indata_22nxl(int n_){struct freefunc* es=freestack;return (int)floor((double)((iprocx_-1)*n_)/(double)(nprocx_))+1;}
int indata_23nxh(int n_){struct freefunc* es=freestack;return (int)floor((double)(iprocx_*n_)/(double)(nprocx_));}
int indata_24nyl(int n_){struct freefunc* es=freestack;return (int)floor((double)((iprocy_-1)*n_)/(double)(nprocy_))+1;}
int indata_25nyh(int n_){struct freefunc* es=freestack;return (int)floor((double)(iprocy_*n_)/(double)(nprocy_));}
int indata_26M;
int indata_27h;
ssize_t indata_28i;
ssize_t indata_29i;
struct freefunc recvbuf_f;char *recvbuf_;

int indata_32h;
ssize_t indata_33i;
ssize_t indata_34i;
struct freefunc sendbuf_f;char *sendbuf_;



void indata_37MPI_Send(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="parallelbcs.h";{struct freefunc* es=freestack;
        ln=61;  {int i_=x_l;do{{int j_=x__l;do{ { 
           ln=60;(*(double *)(cb(0,indata_32h,(i_-x_l)*(x__h-x__l+1)+j_-x__l)*(ssize_t)sizeof(double)+cb(0,3,ireq_)*indata_33i+sendbuf_))=(*(double *)(cb(x__l,x__h,j_)*x__i+cb(x_l,x_h,i_)*x_i+x___) );
        }j_++;}while(j_<=x__h);}i_++;}while(i_<=x_h);}
     
        ssize_t L1_; 
        ssize_t L2_; 
        L1_= x_h-x_l+1; L2_= x__h-x__l+1;
        MPI_Isend((double *)(ireq_*((ssize_t)sizeof(double)*(szbuf_*(nz_+1)*4+1))+sendbuf_), L1_*L2_, MPI_DOUBLE, iP_-1, tag_,
              cart_comm, &srequest[ireq_]);
     
}fn=savefn; ln=saveln;}


void indata_38MPI_Recv(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="parallelbcs.h";{struct freefunc* es=freestack;
     
        ssize_t L1_;
        ssize_t L2_;
        L1_= x_h-x_l+1; L2_= x__h-x__l+1;
        MPI_Irecv((double *)(ireq_*((ssize_t)sizeof(double)*(szbuf_*(nz_+1)*4+1))+recvbuf_), L1_*L2_, MPI_DOUBLE, iP_-1, tag_,
              cart_comm, &request[ireq_]);
     
     ln=80;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).l=x_l;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).h=x_h;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).i=x_i;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).a.l=x__l;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).a.h=x__h;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).a.i=x__i;(*(struct indata_s20*)(cb(0,3,ireq_)*(ssize_t)sizeof(struct indata_s20)+unpckbuf_)).a.a=x___;
}fn=savefn; ln=saveln;}

void indata_39mpi_barrier(void){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="parallelbcs.h";{struct freefunc* es=freestack;
       
           MPI_Barrier(cart_comm);
       
}fn=savefn; ln=saveln;}



void indata_40pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);
void indata_41pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);
void indata_42pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);
void indata_43pbcwait(void);
int indata_44h;
ssize_t indata_45i;
struct freefunc zd_f;char *zd_;
int indata_47h;
ssize_t indata_48i;
struct freefunc yd_f;char *yd_;

ssize_t indata_50i;
char *indata_51zz;
ssize_t indata_52i;
char *indata_53yy;
double dzz_ ;

int indata_54l;
int indata_55h;
int indata_56l;
int indata_57h;
int indata_58h;
ssize_t indata_59i;
ssize_t indata_60i;
ssize_t indata_61i;
ssize_t indata_62st;
ssize_t indata_63st;
struct freefunc var_f;char *var_ ;
struct freefunc old_f;char *old_;

char flowrate_[(ssize_t)sizeof(double)*(/*SA68*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)] ;



/* CoCo switch */
int useCoco_;

