/*-I/home/ws/rc8764/opt/openmpi/include -L/home/ws/rc8764/opt/openmpi/lib -Wl,-rpath -Wl,/home/ws/rc8764/opt/openmpi/lib -Wl,--enable-new-dtags -lmpi */

#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#define _LARGE_FILES
#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

char errortemp_[(80+1)];


struct arrptr{int l,h; ssize_t i; char *a;};
struct dynptr{void* p; int t;};
char INTERRUPT=0;
typedef void (*traphandler_t)(const char *);
struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;} mainjb;
struct jbtrap* curjb=&mainjb;
#define traphandler curjb->tr

struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};
struct freefunc* freestack=NULL;
#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}
#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name
#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
void traprestore(void *ptr){curjb=ptr;}
void condfree(void *ptr){if(*(void **)ptr!=NULL)free(*(void **)ptr);}
int friexecerror(char** s){
  fprintf(stderr,"\rprogram must be run under icpl\n");
  sleep(1);
  return 0;
}
int (*friexec)(char** s)=friexecerror;

int dynptrerr(int type){
  snprintf(errortemp_,sizeof(errortemp_),"Argument of incompatible TYPE %d",type);
  traphandler(errortemp_);
  return 0;
}
void *errmalloc(void){
  traphandler("Not enough memory");
  return NULL;
}
void ioerr(FILE *fil){
  char where[11];
  off_t cn;
  int fn;
  if(fil==NULL){
    snprintf(errortemp_,sizeof(errortemp_),"file not found");
  } else {
    fn=fileno(fil);
    cn=ftello(fil);
    if(ferror(fil)){
      snprintf(errortemp_,sizeof(errortemp_),"%s in fd %d char %ld",strerror(errno),fn,(long)cn);
    } else if(feof(fil)){
      snprintf(errortemp_,sizeof(errortemp_),"End of file in fd %d char %ld",fn,(long)cn);
    } else {
      if(!(cn>=0 && fscanf(fil,"%10s",where)>0)) where[0]=0;
      snprintf(errortemp_,sizeof(errortemp_),"Unrecognized input \"%s\" from fd %d char %ld",where,fn,(long)cn);
    }
  }
  traphandler(errortemp_);
}
void errfclose(void *voidf){
  FILE **f=(FILE **)voidf;
  int fn;
  if(*f==NULL)return;
  fn=fileno(*f);
  if(fn>=0 && ftell(*f)>=0 && fclose(*f)!=0){
    snprintf(errortemp_,sizeof(errortemp_),"Error in closing fd %d: %s",fn,strerror(errno));
    traphandler(errortemp_);
    }
  *f=NULL;
}
void errfopen(FILE **f, const char *name, int mode){
  int fd;
/*  if(*f)errfclose(f);  causes segfault if used on copies of closed FILE descriptors */
  fd=open(name,mode,0666);
  if(fd>0 || mode==O_RDONLY){
    *f=fdopen(fd,mode==O_RDONLY?"r":"r+");
    if(*f!=NULL || mode==O_RDONLY)return;
    } 
  snprintf(errortemp_,sizeof(errortemp_),"Error in opening %s: %s",name,strerror(errno));
  traphandler(errortemp_);
}
int scanrec(FILE *f, const char *format, void *var) {
  if(f==NULL)return 0;
  off_t pos; int res=0;
  register int c;
  if (friexec!=friexecerror && isatty(fileno(f))){
    char *s="\357\201i\362\371\376";
    if (friexec(&s) && sscanf(s,format,var)==1) return 1;
  }
  pos=ftello(f);
  while((c=getc(f))<=' '&&(c!='\n')&&(c!=EOF)){};
  if(c!='\n')ungetc(c,f);
  while((c=getc(f))=='!') {while(!feof(f)&&getc(f)!='\n'){};};
  ungetc(c,f);
  res=fscanf(f,format,var);
  if (res<=0 && var!=NULL && pos>=0) fseeko(f,pos,SEEK_SET);
  return res;
}
int scanbool(FILE *f, const char *format, int *var) {
  char c;
  return scanrec(f,format,&c)&&((*var=(c=='T')||(c=='Y')||(c=='t')||(c=='y'))||(c=='F')||(c=='N')||(c=='f')||(c=='n'));
  }
int myfgets(char *name, char *var, char *varend, FILE *f) {
  off_t pos;
  register int c;
  if(f==NULL)return 0;
  pos=ftello(f);
  if(feof(f))return 0;
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  if(var<varend){
    do{c=getc(f); *var=c;}while(c!=EOF && c!='\n' && ++var < varend);
    *var=0;
  }
  return 1;
}
int mygetline(char *name, char **var, FILE *f) {
  off_t pos;
  int c,oldc;
  if(f==NULL)return 0;
  pos=ftello(f);
  if(feof(f))return 0;
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '&&c>=0){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  c=32; oldc=0;
  while(1){
  *var=realloc(*var,c);
  if (fgets(*var+oldc,c-oldc,f)==NULL) {fseeko(f,pos,SEEK_SET);return 0;};
  {char* c1=memchr(*var+oldc,'\n',c-oldc-1); if(c1) {*c1=0; return 1;}}
  oldc=c-1; c=2*c;
  }
  /*
  c=0;
  getline(var,&c,f);
  {char* c1=strchr(*var,'\n'); if(c1) *c1=0;}
  return 1;
  */
}
/* Note: fetestexcept always returns zero. Detecting the type of floating point
   exception requires testing the si_code field of sig_info (man sigaction) */
void trapsignal(int signum, siginfo_t *info, void *ucontext){
  feclearexcept(FE_ALL_EXCEPT);
  feenableexcept(fpe);
  if(signum==SIGINT) traphandler(&INTERRUPT);
  else if (signum==SIGFPE){
    switch(info->si_code){
    case FPE_INTDIV: traphandler("*** ERROR: integer division by zero");
    case FPE_FLTDIV: traphandler("*** ERROR: floating-point division by zero");
    case FPE_FLTOVF: traphandler("*** ERROR: floating-point overflow");
    default: traphandler("*** ERROR: invalid or unassigned floating-point value");
    }
  }
  else traphandler(strsignal(signum));
}
#if (defined __i386__ || defined __x86_64__)
#define mmovefrom(var,buf,type) *(type *)(buf)=*var
#define mmoveto(var,buf,type) *var=*(type *)(buf)
#else
#define mmovefrom(var,buf,type) memmove(buf,var,sizeof(type))
#define mmoveto(var,buf,type) memmove(var,buf,sizeof(type))
#endif
#define mainstart \
void default_traphandler(const char *errormessage){ \
  if(errormessage[0]){ \
    printERR; \
    freemem(NULL); \
    exit(EXIT_FAILURE); \
  }else{ \
    freemem(NULL); \
    exit(EXIT_SUCCESS); \
  } \
} \
int main(int argc, char **argv){ \
struct freefunc* es; \
			\
{struct sigaction act,oldact; \
act.sa_sigaction=trapsignal; \
sigemptyset(&act.sa_mask); \
act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; \
sigaction(SIGSEGV,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); \
sigaction(SIGFPE,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); \
sigaction(SIGILL,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); \
sigaction(SIGINT,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); \
/* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ \
else {traphandler=default_traphandler; \
      freestack=NULL; \
      feenableexcept(fpe); \
     }; \
} \
es=freestack;





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);


#include <sys/ioctl.h>                                                          
/*
<*#ifdef __GNUC__
  const
#endif
int cb(int lb, int ub, int index);
#ifdef __GNUC__
  const
#endif
char* cp(int inputpos);
#ifdef __GNUC__
  const
#endif
char *cr(char *lb, char *ub, char *index);
*>
*/
/* nota: #ifdef non passa in C SECTION */
#undef printERR
#define printERR fprintf(stderr,"\r%s in line %d of %s: PROGRAM HALTED  \n",errormessage,ln,fn);fflush(stderr)
extern volatile int ln;
extern char * volatile fn;
extern const int cb(int lb, int ub, int index);
extern const char * cp(void);
extern const int ca(void);
extern const unsigned char sigNAN[8];


/* CPL interface to the termios functions needed to turn on and off ! */
/* character-by-character terminal input                            ! */
/*                                                                  ! */
/* Copyright 2002-2023 Paolo Luchini http://CPLcode.net             ! */
/* Released under the attached LICENSE.                             ! */
/*                                                                  ! */
/* Code maturity: green.                                            ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <termios.h>
void CHARbyCHAR_1CHARbyCHAR(int descr_);
void CHARbyCHAR_2LINEbyLINE(void);

extern struct termios CHARbyCHAR_CHARbyCHAR_newsetting;
extern struct termios CHARbyCHAR_CHARbyCHAR_oldsetting;

extern int CHARbyCHAR_CHARbyCHAR_CbCdescr;

extern void CHARbyCHAR_1CHARbyCHAR(int descr_);


extern void CHARbyCHAR_2LINEbyLINE(void);

/* Library providing an interface to the select system call ! */
/* in order to detect if input is waiting to be read.       ! */
/* See infocpl INPUTREADY.                                  ! */
/*                                                          !  */
/* Copyright 2008-2020 Paolo Luchini http://CPLcode.net     ! */
/* Released under the attached LICENSE.                     ! */
/*                                                          ! */
/* Code maturity: green.                                    ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern int fd_input_ready(int fd, int sec, int usec);








extern int BlockLevel;

void singlestepper(int lineN_,char *line_);






extern FILE *rtchecks_SingleStep_grabin;

extern int rtchecks_SingleStep_StopLevel;
extern char rtchecks_SingleStep_LoopCount[(ssize_t)sizeof(int)*(100+1)];
extern int rtchecks_SingleStep_LastLine;

extern int rtchecks_SingleStep_paused;

extern int rtchecks_SingleStep_lastfnlength;
extern int rtchecks_SingleStep_lastrow;

extern char *rtchecks_SingleStep_lastfn;

extern int rtchecks_SingleStep_termwidth;
extern int rtchecks_SingleStep_termheight;

extern char *rtchecks_SingleStep_hotkeys;


extern void rtchecks_SingleStep_1RestoreScroll(void);


extern void TRON(void);


extern void singlestepper(int lineN_,char *line_);

void prova_2velocita(char *RESULT_,int bo_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="prova.cpl";{struct freefunc* es=freestack;
	ln=5; (*(double *)(0+RESULT_) )= (double)(bo_);
	ln=6; (*(double *)(0+(ssize_t)sizeof(double)+RESULT_) )= (double)(3*bo_);
	ln=7; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= (double)(4*bo_);
	ln=8; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= (*(double *)(0+RESULT_) )+ (*(double *)(0+(ssize_t)sizeof(double)+RESULT_));
}fn=savefn; ln=saveln;}

char di_[(ssize_t)sizeof(int)*3] ;


char risultato_[(ssize_t)sizeof(double)*(/*SA3*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)] ;

double velv_ ;

/*WRITE BY NAME velv */
/*!WRITE BY NAME risultato */
/*WRITE BY NAME risultato.beta */
/*!WRITE BY NAME velocita */
/*WRITE BY NAME velocita(0,0) */
/*!WRITE BY NAME velocita.v(2) */
/*WRITE BY NAME velocita(2) */
/*!WRITE BY NAME velocita.u */
/*!WRITE BY NAME velocita.v */
/*!WRITE BY NAME velocita.w */
/*ARRAY(0..2) OF REAL lambda= [4, 3, 5] */
char m_[(ssize_t)sizeof(double)*(2+1)];

/*ARRAY(0..2) OF REAL output */
double sinlambda_;

/*sinlambda = lambda($i)*SIN(m($i)) */
/*sinlambda($i) = SIN(2*PI/lambda($i)) */

/*sinlambda = 2*lambda */
/*WRITE BY NAME lambda */
/*WRITE BY NAME SIN(m) */

/*ARRAY(lambda.LO..lambda.HI) OF REAL FUNCTION ppppp(ARRAY(lambda.LO..lambda.HI) OF REAL vector) */
/*	LOOP FOR i = vector.LO TO vector.HI */
/*		WRITE BY NAME i */
/*		RESULT(i) = SIN(vector(i)) */
/*	REPEAT */
/*	WRITE BY NAME RESULT */
/*END ppppp */
/*!ARRAY(0..2) OF REAL vector */


void prova_4sinx(char *RESULT_,const int vector_l,const int vector_h,const ssize_t vector_i,char *vector__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="prova.cpl";{struct freefunc* es=freestack;
	ln=51; for(int i_=  vector_l ;i_<= vector_h;i_+=1){
		/*WRITE BY NAME i */
		ln=50; (*(double *)(cb(0,2,i_)*(ssize_t)sizeof(double)+RESULT_) )= sin((*(double *)(cb(vector_l,vector_h,i_)*vector_i+vector__)));
	}
}fn=savefn; ln=saveln;}

char lambda_[(ssize_t)sizeof(double)*(2+1)];

char lambda1_[(ssize_t)sizeof(double)*(2+1)];


double rr_;




/*ARRAY(0..2) OF REAL arg */
/*arg=2*PI/lambda */
/*rr = 2*m($i)/lambda($i)*sinx(arg($i)) */

void prova_5inv(char *RESULT_,const int vector_l,const int vector_h,const ssize_t vector_i,char *vector__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="prova.cpl";{struct freefunc* es=freestack;
	ln=68; for(int i_=  vector_l ;i_<= vector_h;i_+=1){
		ln=67; (*(double *)(cb(0,2,i_)*(ssize_t)sizeof(double)+RESULT_) )= 1./(*(double *)(cb(vector_l,vector_h,i_)*vector_i+vector__));
	}
}fn=savefn; ln=saveln;}

int xxx_ ;


char prova_6[(ssize_t)sizeof(double)*(2+1)];
double prova_7;
char risult_[(ssize_t)sizeof(double)*(2+1)];

double cosb_ ;

double sinb_;

char Lweight_[(ssize_t)sizeof(double)*(2+1)] ;
char Sweight_[(ssize_t)sizeof(double)*(2+1)] ;
char pSweight_[(ssize_t)sizeof(double)*(2+1)] ;

/*WRITE BY NAME Sweight */

int numero_ ;

int due_ ;

double risultato1_;


struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;struct arrptr lambdaa_;struct arrptr aamp_;};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

		   
struct RIBLETS_ riblet_;

char lllll_[(ssize_t)sizeof(double)*5];

char aaaaa_[(ssize_t)sizeof(double)*5];

void prova_11edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="prova.cpl";{struct freefunc* es=freestack;
	double sinusoidal_term_ ;

	double _12y_new;
ln=118; (*(double *)(0+RESULT_) )= (*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+0)*coordinates_i+coordinates__));
	ln=119; sinusoidal_term_= 0.;ln=122; for(int ii_=  1 ;ii_<= (this_.aamp_.h-(this_.aamp_.l)+1);ii_+=1){
		ln=121; sinusoidal_term_ +=  (*(double *)(cb(this_.aamp_.l,this_.aamp_.h,ii_)*this_.aamp_.i+this_.aamp_.a))*sin(2.*(3.14159265358979323846)/(*(double *)(cb(this_.lambdaa_.l,this_.lambdaa_.h,ii_)*this_.lambdaa_.i+this_.lambdaa_.a))*(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+0)*coordinates_i+coordinates__)));
	}
	ln=123;_12y_new=(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__)) - sinusoidal_term_;
ln=123; 
	ln=124; (*(double *)(0+(ssize_t)sizeof(double)+RESULT_) )= ((double)((int)floor(_12y_new/this_.s_))+ 0.5)*this_.s_ + sinusoidal_term_;
	ln=125; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= 0.5*this_.h_;
}fn=savefn; ln=saveln;}
char metti_[(ssize_t)sizeof(double)*(2+1)] ;

double vvvv_;

char dnei_[(ssize_t)sizeof(double)*(/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];

char prova_13[(ssize_t)sizeof(double)*(/*SA10*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
char bbbbb_[(ssize_t)sizeof(double)*5];


double matt_;

/*ARRAY(1..5) OF REAL ppp = lllll * aaaaa */
double matt1_;
double prova_15;


/*WRITE BY NAME ppp */
/*WRITE BY NAME matt */
/*WRITE BY NAME matt1 */



















/*POINTER TO ARRAY(*) OF REAL vediamo = ppppp(lambda) */

/*WRITE BY NAME vediamo */
mainstart


/* with "number" redefined to the respective numeric type.           ! */
/* See matrix.info for usage.                                        ! */
/*                                                                   ! */
/* Copyright 1999-2020 Paolo Luchini http://CPLcode.net              ! */
/* Released under the attached LICENSE.                              ! */
/*                                                                   ! */
/* Code maturity: green.                                             ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */



REAL_Lanczos_norm_=0.;
  rbmat_Lanczos_R:;

CHARbyCHAR_CHARbyCHAR_CbCdescr= - 1;CHARbyCHAR_CHARbyCHAR:;


rtchecks_SingleStep_grabin=NULL;rtchecks_SingleStep_grabin=NULL;rtchecks_SingleStep_StopLevel=INT_MAX;rtchecks_SingleStep_LastLine=0;(*(int *)(rtchecks_SingleStep_LoopCount))=1;
rtchecks_SingleStep_paused=0;rtchecks_SingleStep_lastfnlength=80;rtchecks_SingleStep_lastrow=1;rtchecks_SingleStep_lastfn=fn;rtchecks_SingleStep_termwidth=80;rtchecks_SingleStep_termheight=25;rtchecks_SingleStep_hotkeys="Tracing:(P)ause (Q)uit (T)roff (V)iew <tab>=(F)inish <C-c>=(I)cpl <space>=(S)tep";rtchecks_SingleStep:;fn="prova.cpl";
ln=11; (*(int *)(cb(1,3,1+0)*(ssize_t)sizeof(int)+di_-((ssize_t)sizeof(int))))=0;(*(int *)(cb(1,3,1+1)*(ssize_t)sizeof(int)+di_-((ssize_t)sizeof(int))))=1;(*(int *)(cb(1,3,1+2)*(ssize_t)sizeof(int)+di_-((ssize_t)sizeof(int))))=0;ln=14; {cb(0,/*SA3*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA1*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)+0);prova_2velocita((cb(/*SA1*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA1*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),(((ssize_t)sizeof(double)==(ssize_t)sizeof(double))?(/*SA3*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)-(0)):0)+0),risultato_ +(0)*(ssize_t)sizeof(double)+0-(0)),2);}
ln=15; velv_= (*(double *)(0+(ssize_t)sizeof(double)+risultato_));ln=27; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+m_))=1.;(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+m_))=1.;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+m_))=2.;memmove(&sinlambda_,&sigNAN,sizeof(double));ln=54; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+lambda_))=4.;(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+lambda_))=3.;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+lambda_))=5.;ln=55; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+lambda1_))=3.;(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+lambda1_))=2.;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+lambda1_))=4.;memmove(&rr_,&sigNAN,sizeof(double));ln=71; xxx_= 0;prova_5inv(prova_6,0,2,(ssize_t)sizeof(double),( xxx_==1  ?lambda_  :lambda1_)); prova_7=0.;  {int iii_=2;do{{ln=73;(*&prova_7)=(*(&prova_7!=NULL?&prova_7:(double *)cp()))+(*(double *)(cb(0,2,iii_)*(ssize_t)sizeof(double)+m_))*(*(double *)(cb(0,2,iii_)*(ssize_t)sizeof(double)+prova_6) );}iii_--;}while(iii_>=0);}ln=73; rr_ = atan(prova_7);

/*WRITE BY NAME rr */
/*WRITE BY NAME rr */
/*WRITE BY NAME ATAN(1) */
/*WRITE BY NAME ATAN(-0.5) */

{int i8;for(i8=0;i8<=2;i8++){memmove((double *)(risult_+i8*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}ln=81; {cb(0,2,2-(0)+0);prova_4sinx((cb(2,2,(((ssize_t)sizeof(double)==(ssize_t)sizeof(double))?(2-(0)-(0)):0)+0),risult_ +(0)*(ssize_t)sizeof(double)+0-(0)),0,2,(ssize_t)sizeof(double),lambda_);}

/*WRITE BY NAME risult */

ln=85; cosb_= 5.;ln=86; sinb_= 2.;ln=87; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+Lweight_))=(cosb_*cosb_);(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+Lweight_))=(sinb_*sinb_);(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+Lweight_))=0.;ln=87; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+Sweight_))=(sinb_*sinb_);(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+Sweight_))=(cosb_*cosb_);(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+Sweight_))=1.;ln=87; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+pSweight_))=sinb_;(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+pSweight_))=cosb_;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+pSweight_))=1.;ln=90; numero_= 10;ln=91; due_= 2;memmove(&risultato1_,&sigNAN,sizeof(double));ln=100;if( numero_ == 10 ){
	ln=95; risultato1_ = 1.;
}else{ int mtemp9;
if( (mtemp9=numero_ % due_ ,mtemp9>=0?mtemp9:mtemp9+due_ )== 0 ){
	ln=97; risultato1_ = 2.;
}else{ 
	ln=99; risultato1_ = 3.;
};};

/*WRITE BY NAME risultato1 */
/*WRITE numero MOD due */

ln=110; (*(double *)(cb(1,5,1+0)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))=5.;(*(double *)(cb(1,5,1+1)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))=1.;(*(double *)(cb(1,5,1+2)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))=3.;(*(double *)(cb(1,5,1+3)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))=1.2;(*(double *)(cb(1,5,1+4)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))=0.001;ln=111;riblet_.lambdaa_.l=1;riblet_.lambdaa_.h=5;riblet_.lambdaa_.i=(ssize_t)sizeof(double);riblet_.lambdaa_.a=lllll_-((ssize_t)sizeof(double));
ln=112; (*(double *)(cb(1,5,1+0)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))))=27.;(*(double *)(cb(1,5,1+1)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))))=0.000000;(*(double *)(cb(1,5,1+2)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))))=0.000000;(*(double *)(cb(1,5,1+3)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))))=0.045930;(*(double *)(cb(1,5,1+4)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))))=0.034811 ;ln=113; riblet_.aamp_.l=1;riblet_.aamp_.h=5;riblet_.aamp_.i=(ssize_t)sizeof(double);riblet_.aamp_.a=aaaaa_-((ssize_t)sizeof(double));
ln=114; riblet_.s_ = 1.;
ln=115; riblet_.h_ = 0.8;

ln=127; (*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+metti_))=1.;(*(double *)(cb(0,2,0+1)*(ssize_t)sizeof(double)+metti_))=2.;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+metti_))=0.1;memmove(&vvvv_,&sigNAN,sizeof(double));ln=129;{cb(0,/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA10*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)+0);prova_11edge((cb(/*SA10*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA10*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),(((ssize_t)sizeof(double)==(ssize_t)sizeof(double))?(/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)-(0)):0)+0),dnei_+(0)*(ssize_t)sizeof(double)+0-(0)),riblet_,0,2,(ssize_t)sizeof(double),metti_);}prova_11edge(prova_13,riblet_,0,2,(ssize_t)sizeof(double),metti_);ln=130; vvvv_ = (*(double *)(cb(0,/*SA10*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),1)*(ssize_t)sizeof(double)+prova_13));

/*WRITE BY NAME vvvv.x */
fprintf(stdout,"""dnei=""");ln=133; {int _14i_=0 ;do{{ ln=133; fprintf(stdout,"%g \t",(*(double *)(cb(0,/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),_14i_)*(ssize_t)sizeof(double)+dnei_)) );fflush(stdout); }_14i_+=1;}while(_14i_<= /*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1));}{}ln=133;putc('\n',stdout);
ln=134;fprintf(stdout,"""vvvv=""%g",vvvv_);putc('\n',stdout);

/*WRITE BY NAME vvvv */

ln=138; (*(double *)(cb(1,5,1+0)*(ssize_t)sizeof(double)+bbbbb_-((ssize_t)sizeof(double))))=-5.;(*(double *)(cb(1,5,1+1)*(ssize_t)sizeof(double)+bbbbb_-((ssize_t)sizeof(double))))=-1.;(*(double *)(cb(1,5,1+2)*(ssize_t)sizeof(double)+bbbbb_-((ssize_t)sizeof(double))))=31.;(*(double *)(cb(1,5,1+3)*(ssize_t)sizeof(double)+bbbbb_-((ssize_t)sizeof(double))))=11.2;(*(double *)(cb(1,5,1+4)*(ssize_t)sizeof(double)+bbbbb_-((ssize_t)sizeof(double))))=1111.;ln=140; matt_=0.;  {int i_=1;do{{ln=140;(*&matt_)=(*(&matt_!=NULL?&matt_:(double *)cp()))+(*(double *)(cb(1,5,i_)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))*(*(double *)(cb(1,5,i_)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))) );}i_++;}while(i_<=5);} prova_15=0.;  {int i_=1;do{{ln=142;(*&prova_15)=(*(&prova_15!=NULL?&prova_15:(double *)cp()))+(*(double *)(cb(1,5,i_)*(ssize_t)sizeof(double)+lllll_-((ssize_t)sizeof(double))))*(*(double *)(cb(1,5,i_)*(ssize_t)sizeof(double)+aaaaa_-((ssize_t)sizeof(double))) );}i_++;}while(i_<=5);}ln=142;matt1_=atan(prova_15)/**bbbbb($i))  */;return 0;}
