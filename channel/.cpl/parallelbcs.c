/*-I/opt/openmpi/openmpi_4.1.4_gcc/include -pthread -L/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,-rpath -Wl,/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,--enable-new-dtags -lmpi */

#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64

#define _LARGE_FILES

#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

extern char errortemp_[(80+1)];

struct arrptr{int l,h; ssize_t i; char *a;};struct dynptr{void* p; int t;};extern char INTERRUPT;
typedef 
 void (*traphandler_t)(const char *);struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;};extern struct jbtrap mainjb;
extern struct jbtrap * curjb;

#define traphandler curjb->tr
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};extern struct freefunc * freestack;

#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}

#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name

#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
extern void traprestore(void *ptr);
extern void condfree(void *ptr);
extern int friexecerror(char** s);
extern int (*friexec)(char** s);

#define mmovefrom(var,buf,type) *(type *)(buf)=*var

#define mmoveto(var,buf,type) *var=*(type *)(buf)

#define mainstart void default_traphandler(const char *errormessage){   if(errormessage[0]){     printERR;     freemem(NULL);     exit(EXIT_FAILURE);   }else{     freemem(NULL);     exit(EXIT_SUCCESS);   } } int main(int argc, char **argv){ struct freefunc* es; 			{struct sigaction act,oldact; act.sa_sigaction=trapsignal; sigemptyset(&act.sa_mask); act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; sigaction(SIGSEGV,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); sigaction(SIGFPE,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); sigaction(SIGILL,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); sigaction(SIGINT,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); /* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ else {traphandler=default_traphandler;       freestack=NULL;       feenableexcept(fpe);      }; } es=freestack;
extern int dynptrerr(int type);
extern void *errmalloc(void);
extern void ioerr(FILE *fil);
extern void errfclose(void *voidf);
extern void errfopen(FILE **f, const char *name, int mode);
extern int scanrec(FILE *f, const char *format, void *var) ;
extern int scanbool(FILE *f, const char *format, int *var) ;
extern int myfgets(char *name, char *var, char *varend, FILE *f) ;
extern int mygetline(char *name, char **var, FILE *f) ;
extern void trapsignal(int signum, siginfo_t *info, void *ucontext);





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/*      ___           ___           ___           ___           ___           ___           ___       ___           ___ */
/*     /\  \         /\__\         /\  \         /\__\         /\__\         /\  \         /\__\     /\  \         /\  \ */
/*    /::\  \       /:/  /        /::\  \       /::|  |       /::|  |       /::\  \       /:/  /    /::\  \       /::\  \ */
/*   /:/\:\  \     /:/__/        /:/\:\  \     /:|:|  |      /:|:|  |      /:/\:\  \     /:/  /    /:/\:\  \     /:/\:\  \ */
/*  /:/  \:\  \   /::\  \ ___   /::\~\:\  \   /:/|:|  |__   /:/|:|  |__   /::\~\:\  \   /:/  /    /::\~\:\  \   /:/  \:\__\ */
/* /:/__/ \:\__\ /:/\:\  /\__\ /:/\:\ \:\__\ /:/ |:| /\__\ /:/ |:| /\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ /:/__/ \:|__| */
/* \:\  \  \/__/ \/__\:\/:/  / \/__\:\/:/  / \/__|:|/:/  / \/__|:|/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\ \/__/ \:\  \ /:/  / */
/*  \:\  \            \::/  /       \::/  /      |:/:/  /      |:/:/  /   \:\ \:\__\    \:\  \        \:\__\    \:\  /:/  / */
/*   \:\  \           /:/  /        /:/  /       |::/  /       |::/  /     \:\ \/__/     \:\  \        \/__/     \:\/:/  / */
/*    \:\__\         /:/  /        /:/  /        /:/  /        /:/  /       \:\__\        \:\__\                  \::/__/ */
/*     \/__/         \/__/         \/__/         \/__/         \/__/         \/__/         \/__/                   ~~ */
/* */

/* Some MPI variables */
/* ----------------------- */
extern int iprocx_;
extern int nprocx_;
extern int iprocy_;
extern int nprocy_;

extern int iproc_;
extern int nproc_;

extern int has_terminal_;


/* Libraries to load */
/* ----------------------- */
/* Complex-number type definition and corresponding extensions to the ! */
/* CPL language                                                       ! */
/*                                                                    ! */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net               ! */
/* Released under the attached LICENSE.                               ! */
/*                                                                    ! */
/* Code maturity: green.                                              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

/* Library of common matrix algebra operations for real-valued matrices ! */
/* including suitable extensions to the CPL language to handle infix    ! */
/* notation for such functions. See matrix.info for usage.              ! */
/*                                                                      ! */
/* Copyright 1997-2021 Paolo Luchini http://CPLcode.net                 ! */
/* Released under the attached LICENSE.                                 ! */
/*                                                                      ! */
/* Code maturity: green.                                                ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);


struct DNS_{char size_[(ssize_t)sizeof(int)*(/*SA1*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char localSize_[(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)*(/*SA3*/(((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))/((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))-1)+1)];char leng_[(ssize_t)sizeof(double)*(/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];double nu_;struct arrptr zd_;};
#define DNS_s (ssize_t)sizeof(struct DNS_)
              
               
void dns_5readRestartFile(struct DNS_ *this_,char *fname_);     
         
extern void dns_5readRestartFile(struct DNS_ *this_,char *fname_);

struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;char phiC_[(ssize_t)sizeof(double)*(6+1)];char gC_[(ssize_t)sizeof(double)*(6+1)];};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

	
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);
double riblets_2yWave(struct RIBLETS_ this_,double x_);
void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);
int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);
int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

extern double riblets_riblets_11tol;
extern void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);

	
	extern void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);

	
        extern double riblets_2yWave(struct RIBLETS_ this_,double x_);


	extern int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);


	extern void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);

	
	extern int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
			



struct grid_s4 {int l,h; ssize_t i;struct arrptr a;};

struct grid_s6 {int l,h; ssize_t i;struct arrptr a;};
struct GRID_{char xd_[(ssize_t)sizeof(struct arrptr)*(/*SA1*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char x_[(ssize_t)sizeof(struct arrptr)*(/*SA2*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char di_[(ssize_t)sizeof(int)*(/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char delta_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];char d2_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];};
#define GRID_s (ssize_t)sizeof(struct GRID_)

   
void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
                                   
extern void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
 









int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);
void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__);
void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

extern int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

	



struct imbc_s1 {int l,h; ssize_t i;struct arrptr a;};
struct IMB_{struct imbc_s1 imbc_;};
#define IMB_s (ssize_t)sizeof(struct IMB_)

		
void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);
double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);
void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ pgrid_,struct RIBLETS_ riblet_,int useCoco_);

extern const int imbc_imbc_ca5[7];
extern const int imbc_imbc_ca6[3];
extern char imbc_imbc_7[(ssize_t)sizeof(int)*3*(8-1)];
extern void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);

	
	extern void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ prGrid_,struct RIBLETS_ riblet_,int useCoco_);

			

#include <mpi.h>
/*** typedef int (MPI_Datarep_extent_function)(MPI_Datatype, MPI_Aint *, void *) ununderstood ***/
/*** typedef int (MPI_Datarep_conversion_function)(void *, MPI_Datatype,
 int, void *, MPI_Offset, void *) ununderstood ***/
/*** typedef void (MPI_Comm_errhandler_function)(MPI_Comm *, int *, ...) ununderstood ***/
/*** typedef void (ompi_file_errhandler_fn)(MPI_File *, int *, ...) ununderstood ***/
/*** typedef void (MPI_Win_errhandler_function)(MPI_Win *, int *, ...) ununderstood ***/
/*** typedef void (MPI_User_function)(void *, void *, int *, MPI_Datatype *) ununderstood ***/
/*** typedef int (MPI_Comm_copy_attr_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Comm_delete_attr_function)(MPI_Comm, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Type_copy_attr_function)(MPI_Datatype, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Type_delete_attr_function)(MPI_Datatype, int,
 void *, void *) ununderstood ***/
/*** typedef int (MPI_Win_copy_attr_function)(MPI_Win, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Win_delete_attr_function)(MPI_Win, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Grequest_query_function)(void *, MPI_Status *) ununderstood ***/
/*** typedef int (MPI_Grequest_free_function)(void *) ununderstood ***/
/*** typedef int (MPI_Grequest_cancel_function)(void *, int) ununderstood ***/
/*** typedef MPI_Comm_errhandler_function MPI_Comm_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_function ununderstood ***/
/*** typedef MPI_Win_errhandler_function MPI_Win_errhandler_fn
  ununderstood ***/
/*** typedef int (MPI_Copy_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Delete_function)(MPI_Comm, int, void *, void *) ununderstood ***/


#include <fenv.h>



/* In this module, encapsulation through data STRUCTURE is not performed, */
/* since it is not compatible with the current readily-available MPI routines. */
/* To change this, write own C SECTION for MPI calls.  */



void parallel_4mpiInit(struct DNS_ *dns_);


	
	
	
/*#endif */

	extern void parallel_4mpiInit(struct DNS_ *dns_);



extern void f5(void);
extern void f6(void);
extern void f7(void);
extern void f8(void);
extern void f9(void);
extern void f10(void);
extern FILE *testfile1_;

extern FILE *testfile2_;

extern FILE *testfile3_;

extern FILE *testfile4_;

extern FILE *testfile5_;





/*#define randomDisturbance */

/* Inputs */
/* ------------------------ */
extern int continue_;

extern int non_laminar_;

extern double nu_;
extern double headx_;
extern double heady_;

extern int walls_;
extern int waven_;

extern double spacing_;
extern double ratiohs_;

extern int use_coco_;
extern int extra_;

extern double radius_;
extern double stokexp_;

extern char phiC_[(ssize_t)sizeof(double)*(6+1)];
extern char gC_[(ssize_t)sizeof(double)*(6+1)];

extern int nx_;
extern int ny_;
extern int nz_;
extern int it_;

extern double Lx_;
extern double Ly_;

extern double cflmax_;
extern double fouriermax_;
extern double deltat_;
extern double maxTime_;
extern double time_;
extern double Uconv_;

extern int it_save_;
extern int it_stat_;
extern int it_max_;
extern int it_cfl_;
extern int it_check_;

extern char cfl_[(ssize_t)sizeof(double)*(1+1)];

extern int iN0_;



extern void riblets_test_1readribletsin(void);

extern char *riblets_test_2myname;
struct _riblets_test_s3{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _riblets_test_s3_s (ssize_t)sizeof(struct _riblets_test_s3)
extern FILE *dnsinfo_;
extern FILE *riblets_test_4;

        


	double wallclock_1wallclock(void);

extern struct timeval wallclock_wallclock_startim;

  extern double wallclock_1wallclock(void);

extern double clock_;



extern double riblets_test_15deltax;
extern double riblets_test_16deltay;






#include <mpi.h>


/*INTEGER iprocx, nprocx=atoi(COMMANDLINE(1)), iprocy, nprocy=atoi(COMMANDLINE(2)) */
extern int prevx_;
extern int nextx_;
extern int prevy_;
extern int nexty_;

/*INTEGER iproc, nproc,  */
extern int nreqs_;
extern int nsreqs_;

extern char unpckbuf_[(ssize_t)sizeof(struct grid_s6)*(3+1)];

extern int szbuf_;

extern MPI_Status status[4];
extern MPI_Request request[4];
extern MPI_Request srequest[4];
extern MPI_Comm cart_comm,cart_comm_x;
extern int nproc,iproc,nprocx,nprocy,iprocx,iprocy,idim[2];
extern int prevx,nextx,prevy,nexty;


extern void riblets_test_18init_mpi(void);
 

extern int riblets_test_19nxl(int n_);

extern int riblets_test_20nxh(int n_);

extern int riblets_test_21nyl(int n_);

extern int riblets_test_22nyh(int n_);

extern int riblets_test_23M;
extern int riblets_test_24h;
extern ssize_t riblets_test_25i;
extern ssize_t riblets_test_26i;
extern struct freefunc recvbuf_f;
extern char *recvbuf_;

extern int riblets_test_27h;
extern ssize_t riblets_test_28i;
extern ssize_t riblets_test_29i;
extern struct freefunc sendbuf_f;
extern char *sendbuf_;



extern void riblets_test_30MPI_Send(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_);



extern void riblets_test_31MPI_Recv(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_);


extern void riblets_test_32mpi_barrier(void);





void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);
void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);
void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);
void riblets_test_36pbcwait(void);
extern int riblets_test_37h;
extern ssize_t riblets_test_38i;
extern struct freefunc zd_f;
extern char *zd_;
extern int riblets_test_39h;
extern ssize_t riblets_test_40i;
extern struct freefunc yd_f;
extern char *yd_;

extern ssize_t riblets_test_41i;
extern char *riblets_test_42zz;
extern ssize_t riblets_test_43i;
extern char *riblets_test_44yy;
extern int riblets_test_45l;
extern int riblets_test_46h;
extern int riblets_test_47l;
extern int riblets_test_48h;
extern int riblets_test_49h;
extern ssize_t riblets_test_50i;
extern ssize_t riblets_test_51i;
extern ssize_t riblets_test_52i;
extern ssize_t riblets_test_53st;
extern ssize_t riblets_test_54st;
extern struct freefunc var_f;
extern char *var_;
extern struct freefunc old_f;
extern char *old_;

extern char flowrate_[(ssize_t)sizeof(double)*(/*SA55*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];


/* CoCo switch */
extern int useCoco_;

/* note: this module must be USEd after all other SHARED declarations */
/* INCLUDE parallelbcs.h at the beginning */
/*INCLUDE parallelbcs.h */
/**/
struct _parallelbcs_parupdate_s1{int direction_;struct grid_s6 xl_;struct grid_s6 xl1_;struct grid_s6 xh_;struct grid_s6 xh1_;struct grid_s6 yl_;struct grid_s6 yl1_;struct grid_s6 yh_;struct grid_s6 yh1_;};
#define _parallelbcs_parupdate_s1_s (ssize_t)sizeof(struct _parallelbcs_parupdate_s1)
struct _parallelbcs_parupdate_s1 parallelbcs_parupdate_comm;


void parallelbcs_parupdate_2reader(void){{struct freefunc* es=freestack;
    nreqs_=0;
    if( parallelbcs_parupdate_comm.direction_>=0 ){
      /*IF prevx<=0 THEN comm.xl(*)=comm.xh1(*) ELSE */
        riblets_test_31MPI_Recv(parallelbcs_parupdate_comm.xl_.l,parallelbcs_parupdate_comm.xl_.h,parallelbcs_parupdate_comm.xl_.i,parallelbcs_parupdate_comm.xl_.a.l,parallelbcs_parupdate_comm.xl_.a.h,parallelbcs_parupdate_comm.xl_.a.i,parallelbcs_parupdate_comm.xl_.a.a,prevx_ ,1 ,nreqs_);  nreqs_+=1;
      /*END IF */
    };
    if( parallelbcs_parupdate_comm.direction_<=0 ){
      /*IF nextx<=0 THEN comm.xh(*)=comm.xl1(*) ELSE */
        riblets_test_31MPI_Recv(parallelbcs_parupdate_comm.xh_.l,parallelbcs_parupdate_comm.xh_.h,parallelbcs_parupdate_comm.xh_.i,parallelbcs_parupdate_comm.xh_.a.l,parallelbcs_parupdate_comm.xh_.a.h,parallelbcs_parupdate_comm.xh_.a.i,parallelbcs_parupdate_comm.xh_.a.a,nextx_ ,2 ,nreqs_);  nreqs_+=1;
      /*END IF */
    };
    if( parallelbcs_parupdate_comm.direction_>=0 ){
      /*IF prevy<=0 THEN comm.yl(*)=comm.yh1(*) ELSE */
        riblets_test_31MPI_Recv(parallelbcs_parupdate_comm.yl_.l,parallelbcs_parupdate_comm.yl_.h,parallelbcs_parupdate_comm.yl_.i,parallelbcs_parupdate_comm.yl_.a.l,parallelbcs_parupdate_comm.yl_.a.h,parallelbcs_parupdate_comm.yl_.a.i,parallelbcs_parupdate_comm.yl_.a.a,prevy_ ,3 ,nreqs_);  nreqs_+=1;
      /*END IF */
    };
    if( parallelbcs_parupdate_comm.direction_<=0 ){
      /*IF nexty<=0 THEN comm.yh(*)=comm.yl1(*) ELSE */
        riblets_test_31MPI_Recv(parallelbcs_parupdate_comm.yh_.l,parallelbcs_parupdate_comm.yh_.h,parallelbcs_parupdate_comm.yh_.i,parallelbcs_parupdate_comm.yh_.a.l,parallelbcs_parupdate_comm.yh_.a.h,parallelbcs_parupdate_comm.yh_.a.i,parallelbcs_parupdate_comm.yh_.a.a,nexty_ ,4 ,nreqs_);  nreqs_+=1;
      /*END IF */
    };
}}

void parallelbcs_parupdate_3writer(void){{struct freefunc* es=freestack;
    int nR_;

    nsreqs_=0  ;
    /* invia in x */
    if( parallelbcs_parupdate_comm.direction_>=0  ){
       riblets_test_30MPI_Send(parallelbcs_parupdate_comm.xh1_.l,parallelbcs_parupdate_comm.xh1_.h,parallelbcs_parupdate_comm.xh1_.i,parallelbcs_parupdate_comm.xh1_.a.l,parallelbcs_parupdate_comm.xh1_.a.h,parallelbcs_parupdate_comm.xh1_.a.i,parallelbcs_parupdate_comm.xh1_.a.a ,nextx_ ,1 ,nsreqs_);  nsreqs_+=1;
    };
    if( parallelbcs_parupdate_comm.direction_<=0  ){
       riblets_test_30MPI_Send(parallelbcs_parupdate_comm.xl1_.l,parallelbcs_parupdate_comm.xl1_.h,parallelbcs_parupdate_comm.xl1_.i,parallelbcs_parupdate_comm.xl1_.a.l,parallelbcs_parupdate_comm.xl1_.a.h,parallelbcs_parupdate_comm.xl1_.a.i,parallelbcs_parupdate_comm.xl1_.a.a ,prevx_ ,2 ,nsreqs_);  nsreqs_+=1;
    };
    /* controlla di aver ricevuto in x prima di mandare  */
    nR_=(( parallelbcs_parupdate_comm.direction_==0  ?1  :0));{
     MPI_Waitall(nR_+1, request, MPI_STATUSES_IGNORE); 
    }
     for(int iR_=0 ;iR_<= nR_;iR_+=1){
         for(int i_=(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).l ;i_<= (*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).h ;i_+=1){ for(int j_=(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l ;j_<= (*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.h  ;j_+=1){
           (*(double *)(j_*(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.i+i_*(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).i+(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.a))=(*(double *)(((i_-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).l)*((*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.h-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l+1)+j_-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l)*(ssize_t)sizeof(double)+iR_*riblets_test_25i+recvbuf_) );
        }}
    }
    /* invia in y */
    if( parallelbcs_parupdate_comm.direction_>=0  ){
       riblets_test_30MPI_Send(parallelbcs_parupdate_comm.yh1_.l,parallelbcs_parupdate_comm.yh1_.h,parallelbcs_parupdate_comm.yh1_.i,parallelbcs_parupdate_comm.yh1_.a.l,parallelbcs_parupdate_comm.yh1_.a.h,parallelbcs_parupdate_comm.yh1_.a.i,parallelbcs_parupdate_comm.yh1_.a.a ,nexty_ ,3 ,nsreqs_);  nsreqs_+=1;
    };
    if( parallelbcs_parupdate_comm.direction_<=0  ){
       riblets_test_30MPI_Send(parallelbcs_parupdate_comm.yl1_.l,parallelbcs_parupdate_comm.yl1_.h,parallelbcs_parupdate_comm.yl1_.i,parallelbcs_parupdate_comm.yl1_.a.l,parallelbcs_parupdate_comm.yl1_.a.h,parallelbcs_parupdate_comm.yl1_.a.i,parallelbcs_parupdate_comm.yl1_.a.a ,prevy_ ,4 ,nsreqs_);  nsreqs_+=1;
    };
}}

void parallelbcs_parupdate_4com(void){{struct freefunc* es=freestack;
    parallelbcs_parupdate_2reader();
    parallelbcs_parupdate_3writer();
}}

void riblets_test_36pbcwait(void) {{struct freefunc* es=freestack;
    int nR_;

    nR_=(( parallelbcs_parupdate_comm.direction_==0  ?1  :0));{
    
      MPI_Waitall(nR_+1, &request[1+nR_], MPI_STATUSES_IGNORE);
      MPI_Waitall(nsreqs_, srequest, MPI_STATUSES_IGNORE);
    
    }
     for(int iR_=1+nR_ ;iR_<= 1+2*nR_;iR_+=1){
         for(int i_=(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).l ;i_<= (*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).h ;i_+=1){ for(int j_=(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l ;j_<= (*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.h  ;j_+=1){
           (*(double *)(j_*(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.i+i_*(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).i+(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.a))=(*(double *)(((i_-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).l)*((*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.h-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l+1)+j_-(*(struct grid_s6*)(iR_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l)*(ssize_t)sizeof(double)+iR_*riblets_test_25i+recvbuf_) );
        }}
    }
}} 

void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____){{struct freefunc* es=freestack;
  parallelbcs_parupdate_comm.direction_=0;
  parallelbcs_parupdate_comm.xl_.l=f__l;parallelbcs_parupdate_comm.xl_.h=f__h;parallelbcs_parupdate_comm.xl_.i=f__i;parallelbcs_parupdate_comm.xl_.a.l=f___l;parallelbcs_parupdate_comm.xl_.a.h=f___h;parallelbcs_parupdate_comm.xl_.a.i=f___i;parallelbcs_parupdate_comm.xl_.a.a=f_l*f_i+f____;  parallelbcs_parupdate_comm.xh1_.l=f__l;parallelbcs_parupdate_comm.xh1_.h=f__h;parallelbcs_parupdate_comm.xh1_.i=f__i;parallelbcs_parupdate_comm.xh1_.a.l=f___l;parallelbcs_parupdate_comm.xh1_.a.h=f___h;parallelbcs_parupdate_comm.xh1_.a.i=f___i;parallelbcs_parupdate_comm.xh1_.a.a=(f_h-1)*f_i+f____;  parallelbcs_parupdate_comm.yl_.l=f_l;parallelbcs_parupdate_comm.yl_.h=f_h;parallelbcs_parupdate_comm.yl_.i=f_i;parallelbcs_parupdate_comm.yl_.a.l=f___l;parallelbcs_parupdate_comm.yl_.a.h=f___h;parallelbcs_parupdate_comm.yl_.a.i=f___i;parallelbcs_parupdate_comm.yl_.a.a=f__l*f__i+f____;  parallelbcs_parupdate_comm.yh1_.l=f_l;parallelbcs_parupdate_comm.yh1_.h=f_h;parallelbcs_parupdate_comm.yh1_.i=f_i;parallelbcs_parupdate_comm.yh1_.a.l=f___l;parallelbcs_parupdate_comm.yh1_.a.h=f___h;parallelbcs_parupdate_comm.yh1_.a.i=f___i;parallelbcs_parupdate_comm.yh1_.a.a=(f__h-1)*f__i+f____;
  parallelbcs_parupdate_comm.xl1_.l=f__l;parallelbcs_parupdate_comm.xl1_.h=f__h;parallelbcs_parupdate_comm.xl1_.i=f__i;parallelbcs_parupdate_comm.xl1_.a.l=f___l;parallelbcs_parupdate_comm.xl1_.a.h=f___h;parallelbcs_parupdate_comm.xl1_.a.i=f___i;parallelbcs_parupdate_comm.xl1_.a.a=(f_l+1)*f_i+f____;  parallelbcs_parupdate_comm.xh_.l=f__l;parallelbcs_parupdate_comm.xh_.h=f__h;parallelbcs_parupdate_comm.xh_.i=f__i;parallelbcs_parupdate_comm.xh_.a.l=f___l;parallelbcs_parupdate_comm.xh_.a.h=f___h;parallelbcs_parupdate_comm.xh_.a.i=f___i;parallelbcs_parupdate_comm.xh_.a.a=f_h*f_i+f____;  parallelbcs_parupdate_comm.yl1_.l=f_l;parallelbcs_parupdate_comm.yl1_.h=f_h;parallelbcs_parupdate_comm.yl1_.i=f_i;parallelbcs_parupdate_comm.yl1_.a.l=f___l;parallelbcs_parupdate_comm.yl1_.a.h=f___h;parallelbcs_parupdate_comm.yl1_.a.i=f___i;parallelbcs_parupdate_comm.yl1_.a.a=(f__l+1)*f__i+f____;  parallelbcs_parupdate_comm.yh_.l=f_l;parallelbcs_parupdate_comm.yh_.h=f_h;parallelbcs_parupdate_comm.yh_.i=f_i;parallelbcs_parupdate_comm.yh_.a.l=f___l;parallelbcs_parupdate_comm.yh_.a.h=f___h;parallelbcs_parupdate_comm.yh_.a.i=f___i;parallelbcs_parupdate_comm.yh_.a.a=f__h*f__i+f____;
  parallelbcs_parupdate_4com();
}}

void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_){{struct freefunc* es=freestack;
  parallelbcs_parupdate_comm.direction_=dir_;
  if( dir_>=0 ){ parallelbcs_parupdate_comm.xl_.l=u__l;parallelbcs_parupdate_comm.xl_.h=u__h;parallelbcs_parupdate_comm.xl_.i=u__i;parallelbcs_parupdate_comm.xl_.a.l=u___l;parallelbcs_parupdate_comm.xl_.a.h=u___h;parallelbcs_parupdate_comm.xl_.a.i=u___i;parallelbcs_parupdate_comm.xl_.a.a=u_l*u_i+u____; parallelbcs_parupdate_comm.xh1_.l=u__l;parallelbcs_parupdate_comm.xh1_.h=u__h;parallelbcs_parupdate_comm.xh1_.i=u__i;parallelbcs_parupdate_comm.xh1_.a.l=u___l;parallelbcs_parupdate_comm.xh1_.a.h=u___h;parallelbcs_parupdate_comm.xh1_.a.i=u___i;parallelbcs_parupdate_comm.xh1_.a.a=(u_h-1)*u_i+u____; parallelbcs_parupdate_comm.yl_.l=v_l;parallelbcs_parupdate_comm.yl_.h=v_h;parallelbcs_parupdate_comm.yl_.i=v_i;parallelbcs_parupdate_comm.yl_.a.l=v___l;parallelbcs_parupdate_comm.yl_.a.h=v___h;parallelbcs_parupdate_comm.yl_.a.i=v___i;parallelbcs_parupdate_comm.yl_.a.a=v__l*v__i+v____; parallelbcs_parupdate_comm.yh1_.l=v_l;parallelbcs_parupdate_comm.yh1_.h=v_h;parallelbcs_parupdate_comm.yh1_.i=v_i;parallelbcs_parupdate_comm.yh1_.a.l=v___l;parallelbcs_parupdate_comm.yh1_.a.h=v___h;parallelbcs_parupdate_comm.yh1_.a.i=v___i;parallelbcs_parupdate_comm.yh1_.a.a=(v__h-1)*v__i+v____;};
  if( dir_<=0 ){ parallelbcs_parupdate_comm.xl1_.l=u__l;parallelbcs_parupdate_comm.xl1_.h=u__h;parallelbcs_parupdate_comm.xl1_.i=u__i;parallelbcs_parupdate_comm.xl1_.a.l=u___l;parallelbcs_parupdate_comm.xl1_.a.h=u___h;parallelbcs_parupdate_comm.xl1_.a.i=u___i;parallelbcs_parupdate_comm.xl1_.a.a=(u_l+1)*u_i+u____; parallelbcs_parupdate_comm.xh_.l=u__l;parallelbcs_parupdate_comm.xh_.h=u__h;parallelbcs_parupdate_comm.xh_.i=u__i;parallelbcs_parupdate_comm.xh_.a.l=u___l;parallelbcs_parupdate_comm.xh_.a.h=u___h;parallelbcs_parupdate_comm.xh_.a.i=u___i;parallelbcs_parupdate_comm.xh_.a.a=u_h*u_i+u____; parallelbcs_parupdate_comm.yl1_.l=v_l;parallelbcs_parupdate_comm.yl1_.h=v_h;parallelbcs_parupdate_comm.yl1_.i=v_i;parallelbcs_parupdate_comm.yl1_.a.l=v___l;parallelbcs_parupdate_comm.yl1_.a.h=v___h;parallelbcs_parupdate_comm.yl1_.a.i=v___i;parallelbcs_parupdate_comm.yl1_.a.a=(v__l+1)*v__i+v____; parallelbcs_parupdate_comm.yh_.l=v_l;parallelbcs_parupdate_comm.yh_.h=v_h;parallelbcs_parupdate_comm.yh_.i=v_i;parallelbcs_parupdate_comm.yh_.a.l=v___l;parallelbcs_parupdate_comm.yh_.a.h=v___h;parallelbcs_parupdate_comm.yh_.a.i=v___i;parallelbcs_parupdate_comm.yh_.a.a=v__h*v__i+v____;};
  parallelbcs_parupdate_4com();
}}

void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_){{struct freefunc* es=freestack;
  int mtemp5;
int mtemp6;
int mtemp7;
int mtemp8;
int mtemp9;
int mtemp10;
int mtemp11;
int mtemp12;
parallelbcs_parupdate_comm.direction_=0;
  parallelbcs_parupdate_comm.xl_.l=(2==0?INT_MIN:(u__l-(u__l+(mtemp5=(u__l+u_l+1+parity_)%2,mtemp5>=0?mtemp5:mtemp5+2)))/2);parallelbcs_parupdate_comm.xl_.h=(2==0?INT_MAX:(u__h-(u__l+(mtemp5=(u__l+u_l+1+parity_)%2,mtemp5>=0?mtemp5:mtemp5+2)))/2);parallelbcs_parupdate_comm.xl_.i=(2*u__i);parallelbcs_parupdate_comm.xl_.a.l=u___l;parallelbcs_parupdate_comm.xl_.a.h=u___h;parallelbcs_parupdate_comm.xl_.a.i=u___i;parallelbcs_parupdate_comm.xl_.a.a=(u__l+(mtemp5=(u__l+u_l+1+parity_)%2,mtemp5>=0?mtemp5:mtemp5+2))*u__i+u_l*u_i+u____;
  parallelbcs_parupdate_comm.xh1_.l=(2==0?INT_MIN:(u__l-(u__l+(mtemp6=(u__l+u_h+parity_)%2,mtemp6>=0?mtemp6:mtemp6+2)))/2);parallelbcs_parupdate_comm.xh1_.h=(2==0?INT_MAX:(u__h-(u__l+(mtemp6=(u__l+u_h+parity_)%2,mtemp6>=0?mtemp6:mtemp6+2)))/2);parallelbcs_parupdate_comm.xh1_.i=(2*u__i);parallelbcs_parupdate_comm.xh1_.a.l=u___l;parallelbcs_parupdate_comm.xh1_.a.h=u___h;parallelbcs_parupdate_comm.xh1_.a.i=u___i;parallelbcs_parupdate_comm.xh1_.a.a=(u__l+(mtemp6=(u__l+u_h+parity_)%2,mtemp6>=0?mtemp6:mtemp6+2))*u__i+(u_h-1)*u_i+u____;
  parallelbcs_parupdate_comm.xh_.l=(2==0?INT_MIN:(u__l-(u__l+(mtemp7=(u__l+u_h+parity_)%2,mtemp7>=0?mtemp7:mtemp7+2)))/2);parallelbcs_parupdate_comm.xh_.h=(2==0?INT_MAX:(u__h-(u__l+(mtemp7=(u__l+u_h+parity_)%2,mtemp7>=0?mtemp7:mtemp7+2)))/2);parallelbcs_parupdate_comm.xh_.i=(2*u__i);parallelbcs_parupdate_comm.xh_.a.l=u___l;parallelbcs_parupdate_comm.xh_.a.h=u___h;parallelbcs_parupdate_comm.xh_.a.i=u___i;parallelbcs_parupdate_comm.xh_.a.a=(u__l+(mtemp7=(u__l+u_h+parity_)%2,mtemp7>=0?mtemp7:mtemp7+2))*u__i+u_h*u_i+u____;
  parallelbcs_parupdate_comm.xl1_.l=(2==0?INT_MIN:(u__l-(u__l+(mtemp8=(u__l+u_l+1+parity_)%2,mtemp8>=0?mtemp8:mtemp8+2)))/2);parallelbcs_parupdate_comm.xl1_.h=(2==0?INT_MAX:(u__h-(u__l+(mtemp8=(u__l+u_l+1+parity_)%2,mtemp8>=0?mtemp8:mtemp8+2)))/2);parallelbcs_parupdate_comm.xl1_.i=(2*u__i);parallelbcs_parupdate_comm.xl1_.a.l=u___l;parallelbcs_parupdate_comm.xl1_.a.h=u___h;parallelbcs_parupdate_comm.xl1_.a.i=u___i;parallelbcs_parupdate_comm.xl1_.a.a=(u__l+(mtemp8=(u__l+u_l+1+parity_)%2,mtemp8>=0?mtemp8:mtemp8+2))*u__i+(u_l+1)*u_i+u____;
  parallelbcs_parupdate_comm.yl_.l=(2==0?INT_MIN:(v_l-(v_l+(mtemp9=(v_l+v__l+1+parity_)%2,mtemp9>=0?mtemp9:mtemp9+2)))/2);parallelbcs_parupdate_comm.yl_.h=(2==0?INT_MAX:(v_h-(v_l+(mtemp9=(v_l+v__l+1+parity_)%2,mtemp9>=0?mtemp9:mtemp9+2)))/2);parallelbcs_parupdate_comm.yl_.i=(2*v_i);parallelbcs_parupdate_comm.yl_.a.l=v___l;parallelbcs_parupdate_comm.yl_.a.h=v___h;parallelbcs_parupdate_comm.yl_.a.i=v___i;parallelbcs_parupdate_comm.yl_.a.a=v__l*v__i+(v_l+(mtemp9=(v_l+v__l+1+parity_)%2,mtemp9>=0?mtemp9:mtemp9+2))*v_i+v____;
  parallelbcs_parupdate_comm.yh1_.l=(2==0?INT_MIN:(v_l-(v_l+(mtemp10=(v_l+v__h+parity_)%2,mtemp10>=0?mtemp10:mtemp10+2)))/2);parallelbcs_parupdate_comm.yh1_.h=(2==0?INT_MAX:(v_h-(v_l+(mtemp10=(v_l+v__h+parity_)%2,mtemp10>=0?mtemp10:mtemp10+2)))/2);parallelbcs_parupdate_comm.yh1_.i=(2*v_i);parallelbcs_parupdate_comm.yh1_.a.l=v___l;parallelbcs_parupdate_comm.yh1_.a.h=v___h;parallelbcs_parupdate_comm.yh1_.a.i=v___i;parallelbcs_parupdate_comm.yh1_.a.a=(v__h-1)*v__i+(v_l+(mtemp10=(v_l+v__h+parity_)%2,mtemp10>=0?mtemp10:mtemp10+2))*v_i+v____;
  parallelbcs_parupdate_comm.yh_.l=(2==0?INT_MIN:(v_l-(v_l+(mtemp11=(v_l+v__h+parity_)%2,mtemp11>=0?mtemp11:mtemp11+2)))/2);parallelbcs_parupdate_comm.yh_.h=(2==0?INT_MAX:(v_h-(v_l+(mtemp11=(v_l+v__h+parity_)%2,mtemp11>=0?mtemp11:mtemp11+2)))/2);parallelbcs_parupdate_comm.yh_.i=(2*v_i);parallelbcs_parupdate_comm.yh_.a.l=v___l;parallelbcs_parupdate_comm.yh_.a.h=v___h;parallelbcs_parupdate_comm.yh_.a.i=v___i;parallelbcs_parupdate_comm.yh_.a.a=v__h*v__i+(v_l+(mtemp11=(v_l+v__h+parity_)%2,mtemp11>=0?mtemp11:mtemp11+2))*v_i+v____;
  parallelbcs_parupdate_comm.yl1_.l=(2==0?INT_MIN:(v_l-(v_l+(mtemp12=(v_l+v__l+1+parity_)%2,mtemp12>=0?mtemp12:mtemp12+2)))/2);parallelbcs_parupdate_comm.yl1_.h=(2==0?INT_MAX:(v_h-(v_l+(mtemp12=(v_l+v__l+1+parity_)%2,mtemp12>=0?mtemp12:mtemp12+2)))/2);parallelbcs_parupdate_comm.yl1_.i=(2*v_i);parallelbcs_parupdate_comm.yl1_.a.l=v___l;parallelbcs_parupdate_comm.yl1_.a.h=v___h;parallelbcs_parupdate_comm.yl1_.a.i=v___i;parallelbcs_parupdate_comm.yl1_.a.a=(v__l+1)*v__i+(v_l+(mtemp12=(v_l+v__l+1+parity_)%2,mtemp12>=0?mtemp12:mtemp12+2))*v_i+v____;
  parallelbcs_parupdate_4com();
}}
void parallelbcs_5getmax(const int val_l,const int val_h,const ssize_t val_i,char *val__){{struct freefunc* es=freestack;
    
    MPI_Allreduce(MPI_IN_PLACE, (double *) val__, val_h-val_l+1,
                  MPI_DOUBLE, MPI_MAX, cart_comm);
    
}}


void parallelbcs_6accumulate(const int val_l,const int val_h,const ssize_t val_i,char *val__){{struct freefunc* es=freestack;
    
    MPI_Allreduce(MPI_IN_PLACE, (double *) val__, val_h-val_l+1,
                  MPI_DOUBLE, MPI_SUM, cart_comm);
    
}}

void parallelbcs_7accumulateX(const int valin_l,const int valin_h,const ssize_t valin_i,char *valin__,const int valout_l,const int valout_h,const ssize_t valout_i,char *valout__){{struct freefunc* es=freestack;
    
    MPI_Allreduce((double *) valin__, (double *) valout__, valin_h-valin_l+1,
                  MPI_DOUBLE, MPI_SUM, cart_comm_x);
    
}}
