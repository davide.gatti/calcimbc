/*-I/opt/openmpi/openmpi_4.1.4_gcc/include -pthread -L/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,-rpath -Wl,/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,--enable-new-dtags -lmpi */

#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#define _LARGE_FILES
#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

char errortemp_[(80+1)];


struct arrptr{int l,h; ssize_t i; char *a;};
struct dynptr{void* p; int t;};
char INTERRUPT=0;
typedef void (*traphandler_t)(const char *);
struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;} mainjb;
struct jbtrap* curjb=&mainjb;
#define traphandler curjb->tr

struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};
struct freefunc* freestack=NULL;
#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}
#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name
#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
void traprestore(void *ptr){curjb=ptr;}
void condfree(void *ptr){if(*(void **)ptr!=NULL)free(*(void **)ptr);}
int friexecerror(char** s){
  fprintf(stderr,"\rprogram must be run under icpl\n");
  sleep(1);
  return 0;
}
int (*friexec)(char** s)=friexecerror;

int dynptrerr(int type){
  snprintf(errortemp_,sizeof(errortemp_),"Argument of incompatible TYPE %d",type);
  traphandler(errortemp_);
  return 0;
}
void *errmalloc(void){
  traphandler("Not enough memory");
  return NULL;
}
void ioerr(FILE *fil){
  char where[11];
  off_t cn;
  int fn;
  if(fil==NULL){
    snprintf(errortemp_,sizeof(errortemp_),"file not found");
  } else {
    fn=fileno(fil);
    cn=ftello(fil);
    if(ferror(fil)){
      snprintf(errortemp_,sizeof(errortemp_),"%s in fd %d char %ld",strerror(errno),fn,(long)cn);
    } else if(feof(fil)){
      snprintf(errortemp_,sizeof(errortemp_),"End of file in fd %d char %ld",fn,(long)cn);
    } else {
      if(!(cn>=0 && fscanf(fil,"%10s",where)>0)) where[0]=0;
      snprintf(errortemp_,sizeof(errortemp_),"Unrecognized input \"%s\" from fd %d char %ld",where,fn,(long)cn);
    }
  }
  traphandler(errortemp_);
}
void errfclose(void *voidf){
  FILE **f=(FILE **)voidf;
  int fn;
  if(*f==NULL)return;
  fn=fileno(*f);
  if(fn>=0 && ftell(*f)>=0 && fclose(*f)!=0){
    snprintf(errortemp_,sizeof(errortemp_),"Error in closing fd %d: %s",fn,strerror(errno));
    traphandler(errortemp_);
    }
  *f=NULL;
}
void errfopen(FILE **f, const char *name, int mode){
  int fd;
/*  if(*f)errfclose(f);  causes segfault if used on copies of closed FILE descriptors */
  fd=open(name,mode,0666);
  if(fd>0 || mode==O_RDONLY){
    *f=fdopen(fd,mode==O_RDONLY?"r":"r+");
    if(*f!=NULL || mode==O_RDONLY)return;
    } 
  snprintf(errortemp_,sizeof(errortemp_),"Error in opening %s: %s",name,strerror(errno));
  traphandler(errortemp_);
}
int scanrec(FILE *f, const char *format, void *var) {
  if(f==NULL)return 0;
  off_t pos; int res=0;
  register int c;
  if (friexec!=friexecerror && isatty(fileno(f))){
    char *s="\357\201i\362\371\376";
    if (friexec(&s) && sscanf(s,format,var)==1) return 1;
  }
  pos=ftello(f);
  while((c=getc(f))<=' '&&(c!='\n')&&(c!=EOF)){};
  if(c!='\n')ungetc(c,f);
  while((c=getc(f))=='!') {while(!feof(f)&&getc(f)!='\n'){};};
  ungetc(c,f);
  res=fscanf(f,format,var);
  if (res<=0 && var!=NULL && pos>=0) fseeko(f,pos,SEEK_SET);
  return res;
}
int scanbool(FILE *f, const char *format, int *var) {
  char c;
  return scanrec(f,format,&c)&&((*var=(c=='T')||(c=='Y')||(c=='t')||(c=='y'))||(c=='F')||(c=='N')||(c=='f')||(c=='n'));
  }
int myfgets(char *name, char *var, char *varend, FILE *f) {
  off_t pos;
  register int c;
  if(f==NULL)return 0;
  pos=ftello(f);
  if(feof(f))return 0;
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  if(var<varend){
    do{c=getc(f); *var=c;}while(c!=EOF && c!='\n' && ++var < varend);
    *var=0;
  }
  return 1;
}
int mygetline(char *name, char **var, FILE *f) {
  off_t pos;
  int c,oldc;
  if(f==NULL)return 0;
  pos=ftello(f);
  if(feof(f))return 0;
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '&&c>=0){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  c=32; oldc=0;
  while(1){
  *var=realloc(*var,c);
  if (fgets(*var+oldc,c-oldc,f)==NULL) {fseeko(f,pos,SEEK_SET);return 0;};
  {char* c1=memchr(*var+oldc,'\n',c-oldc-1); if(c1) {*c1=0; return 1;}}
  oldc=c-1; c=2*c;
  }
  /*
  c=0;
  getline(var,&c,f);
  {char* c1=strchr(*var,'\n'); if(c1) *c1=0;}
  return 1;
  */
}
/* Note: fetestexcept always returns zero. Detecting the type of floating point
   exception requires testing the si_code field of sig_info (man sigaction) */
void trapsignal(int signum, siginfo_t *info, void *ucontext){
  feclearexcept(FE_ALL_EXCEPT);
  feenableexcept(fpe);
  if(signum==SIGINT) traphandler(&INTERRUPT);
  else if (signum==SIGFPE){
    switch(info->si_code){
    case FPE_INTDIV: traphandler("*** ERROR: integer division by zero");
    case FPE_FLTDIV: traphandler("*** ERROR: floating-point division by zero");
    case FPE_FLTOVF: traphandler("*** ERROR: floating-point overflow");
    default: traphandler("*** ERROR: invalid or unassigned floating-point value");
    }
  }
  else traphandler(strsignal(signum));
}
#if (defined __i386__ || defined __x86_64__)
#define mmovefrom(var,buf,type) *(type *)(buf)=*var
#define mmoveto(var,buf,type) *var=*(type *)(buf)
#else
#define mmovefrom(var,buf,type) memmove(buf,var,sizeof(type))
#define mmoveto(var,buf,type) memmove(var,buf,sizeof(type))
#endif
#define mainstart \
void default_traphandler(const char *errormessage){ \
  if(errormessage[0]){ \
    printERR; \
    freemem(NULL); \
    exit(EXIT_FAILURE); \
  }else{ \
    freemem(NULL); \
    exit(EXIT_SUCCESS); \
  } \
} \
int main(int argc, char **argv){ \
struct freefunc* es; \
			\
{struct sigaction act,oldact; \
act.sa_sigaction=trapsignal; \
sigemptyset(&act.sa_mask); \
act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; \
sigaction(SIGSEGV,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); \
sigaction(SIGFPE,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); \
sigaction(SIGILL,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); \
sigaction(SIGINT,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); \
/* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ \
else {traphandler=default_traphandler; \
      freestack=NULL; \
      feenableexcept(fpe); \
     }; \
} \
es=freestack;





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/*      ___           ___           ___           ___           ___           ___           ___       ___           ___ */
/*     /\  \         /\__\         /\  \         /\__\         /\__\         /\  \         /\__\     /\  \         /\  \ */
/*    /::\  \       /:/  /        /::\  \       /::|  |       /::|  |       /::\  \       /:/  /    /::\  \       /::\  \ */
/*   /:/\:\  \     /:/__/        /:/\:\  \     /:|:|  |      /:|:|  |      /:/\:\  \     /:/  /    /:/\:\  \     /:/\:\  \ */
/*  /:/  \:\  \   /::\  \ ___   /::\~\:\  \   /:/|:|  |__   /:/|:|  |__   /::\~\:\  \   /:/  /    /::\~\:\  \   /:/  \:\__\ */
/* /:/__/ \:\__\ /:/\:\  /\__\ /:/\:\ \:\__\ /:/ |:| /\__\ /:/ |:| /\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ /:/__/ \:|__| */
/* \:\  \  \/__/ \/__\:\/:/  / \/__\:\/:/  / \/__|:|/:/  / \/__|:|/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\ \/__/ \:\  \ /:/  / */
/*  \:\  \            \::/  /       \::/  /      |:/:/  /      |:/:/  /   \:\ \:\__\    \:\  \        \:\__\    \:\  /:/  / */
/*   \:\  \           /:/  /        /:/  /       |::/  /       |::/  /     \:\ \/__/     \:\  \        \/__/     \:\/:/  / */
/*    \:\__\         /:/  /        /:/  /        /:/  /        /:/  /       \:\__\        \:\__\                  \::/__/ */
/*     \/__/         \/__/         \/__/         \/__/         \/__/         \/__/         \/__/                   ~~ */
/* */

/* Some MPI variables */
/* ----------------------- */
int iprocx_;
int nprocx_;
int iprocy_;
int nprocy_;

int iproc_;
int nproc_;

int has_terminal_;


/* Libraries to load */
/* ----------------------- */
struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);


struct DNS_{char size_[(ssize_t)sizeof(int)*(/*SA1*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char localSize_[(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)*(/*SA3*/(((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))/((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))-1)+1)];char leng_[(ssize_t)sizeof(double)*(/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];double nu_;struct arrptr zd_;};
#define DNS_s (ssize_t)sizeof(struct DNS_)
              
               
void dns_5readRestartFile(struct DNS_ *this_,char *fname_);     
         
extern void dns_5readRestartFile(struct DNS_ *this_,char *fname_);

struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;char phiC_[(ssize_t)sizeof(double)*(6+1)];char gC_[(ssize_t)sizeof(double)*(6+1)];};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

	
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);
double riblets_2yWave(struct RIBLETS_ this_,double x_);
void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);
int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);
int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

extern double riblets_riblets_11tol;
extern void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);

	
	extern void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);

	
        extern double riblets_2yWave(struct RIBLETS_ this_,double x_);


	extern int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);


	extern void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);

	
	extern int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
			

struct grid_s4 {int l,h; ssize_t i;struct arrptr a;};

struct grid_s6 {int l,h; ssize_t i;struct arrptr a;};
struct GRID_{char xd_[(ssize_t)sizeof(struct arrptr)*(/*SA1*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char x_[(ssize_t)sizeof(struct arrptr)*(/*SA2*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char di_[(ssize_t)sizeof(int)*(/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char delta_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];char d2_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];};
#define GRID_s (ssize_t)sizeof(struct GRID_)

   
void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
                                   
extern void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
 

int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);
void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__);
void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

extern int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

	

struct imbc_s1 {int l,h; ssize_t i;struct arrptr a;};
struct IMB_{struct imbc_s1 imbc_;};
#define IMB_s (ssize_t)sizeof(struct IMB_)

		
void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);
double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);
void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ pgrid_,struct RIBLETS_ riblet_,int useCoco_);

extern const int imbc_imbc_ca5[7];
extern const int imbc_imbc_ca6[3];
extern char imbc_imbc_7[(ssize_t)sizeof(int)*3*(8-1)];
extern void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);

	
	extern void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ prGrid_,struct RIBLETS_ riblet_,int useCoco_);

			

#include <mpi.h>
/*** typedef int (MPI_Datarep_extent_function)(MPI_Datatype, MPI_Aint *, void *) ununderstood ***/
/*** typedef int (MPI_Datarep_conversion_function)(void *, MPI_Datatype,
 int, void *, MPI_Offset, void *) ununderstood ***/
/*** typedef void (MPI_Comm_errhandler_function)(MPI_Comm *, int *, ...) ununderstood ***/
/*** typedef void (ompi_file_errhandler_fn)(MPI_File *, int *, ...) ununderstood ***/
/*** typedef void (MPI_Win_errhandler_function)(MPI_Win *, int *, ...) ununderstood ***/
/*** typedef void (MPI_User_function)(void *, void *, int *, MPI_Datatype *) ununderstood ***/
/*** typedef int (MPI_Comm_copy_attr_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Comm_delete_attr_function)(MPI_Comm, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Type_copy_attr_function)(MPI_Datatype, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Type_delete_attr_function)(MPI_Datatype, int,
 void *, void *) ununderstood ***/
/*** typedef int (MPI_Win_copy_attr_function)(MPI_Win, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Win_delete_attr_function)(MPI_Win, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Grequest_query_function)(void *, MPI_Status *) ununderstood ***/
/*** typedef int (MPI_Grequest_free_function)(void *) ununderstood ***/
/*** typedef int (MPI_Grequest_cancel_function)(void *, int) ununderstood ***/
/*** typedef MPI_Comm_errhandler_function MPI_Comm_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_function ununderstood ***/
/*** typedef MPI_Win_errhandler_function MPI_Win_errhandler_fn
  ununderstood ***/
/*** typedef int (MPI_Copy_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Delete_function)(MPI_Comm, int, void *, void *) ununderstood ***/


#include <fenv.h>



/* In this module, encapsulation through data STRUCTURE is not performed, */
/* since it is not compatible with the current readily-available MPI routines. */
/* To change this, write own C SECTION for MPI calls.  */



void parallel_4mpiInit(struct DNS_ *dns_);


	
	
	
/*#endif */

	extern void parallel_4mpiInit(struct DNS_ *dns_);



extern void f5(void);
extern void f6(void);
extern void f7(void);
extern void f8(void);
extern void f9(void);
extern void f10(void);
FILE *testfile1_ ;

FILE *testfile2_ ;

FILE *testfile3_ ;

FILE *testfile4_ ;

FILE *testfile5_ ;





/*#define randomDisturbance */

/* Inputs */
/* ------------------------ */
int continue_;

int non_laminar_;

double nu_;
double headx_;
double heady_;

int walls_;
int waven_;

double spacing_;
double ratiohs_;

int use_coco_;
int extra_;

double radius_;
double stokexp_;

char phiC_[(ssize_t)sizeof(double)*(6+1)];
char gC_[(ssize_t)sizeof(double)*(6+1)];

int nx_;
int ny_;
int nz_;
int it_;

double Lx_;
double Ly_;

double cflmax_;
double fouriermax_;
double deltat_;
double maxTime_;
double time_;
double Uconv_;

int it_save_;
int it_stat_;
int it_max_;
int it_cfl_;
int it_check_;

char cfl_[(ssize_t)sizeof(double)*(1+1)];

int iN0_;



void riblets_test_1readribletsin(void){{struct freefunc* es=freestack;
	FILE *ribletsin_;

		int _2h;
ssize_t _3i;
char *_4lambda;
int _5h;
ssize_t _6i;
char *_7ampl;
ribletsin_=NULL;errfopen(&ribletsin_,"./input/riblets.in",O_RDONLY);   if(!(scanrec( ribletsin_ ," non_laminar = %d",&non_laminar_)>0 &&scanrec( ribletsin_ ," nu = %lg",&nu_)>0 &&scanrec( ribletsin_ ," headx = %lg",&headx_)>0 &&scanrec( ribletsin_ ," heady = %lg",&heady_)>0))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," walls = %d",&walls_)>0 &&scanrec( ribletsin_ ," spacing = %lg",&spacing_)>0 &&scanrec( ribletsin_ ," ratiohs = %lg",&ratiohs_)>0))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," waven = %d",&waven_)>0))ioerr( ribletsin_ );
		_2h=waven_;
_3i=(ssize_t)sizeof(double)*_2h;

_4lambda=malloc(_3i);if(_4lambda==NULL)_4lambda=errmalloc();_4lambda-=(ssize_t)sizeof(double); 
		_5h=waven_;
_6i=(ssize_t)sizeof(double)*_5h;

_7ampl=malloc(_6i);if(_7ampl==NULL)_7ampl=errmalloc();_7ampl-=(ssize_t)sizeof(double); 
		   if(!((scanrec( ribletsin_ ," lambda = ",NULL),({int i8=1; while(i8<=_2h){if(!scanrec( ribletsin_ ," %lg",(double *)(i8*(ssize_t)sizeof(double)+_4lambda))>0)ioerr( ribletsin_ ); i8++;}}),1)))ioerr( ribletsin_ );
		   if(!((scanrec( ribletsin_ ," ampl = ",NULL),({int i9=1; while(i9<=_5h){if(!scanrec( ribletsin_ ," %lg",(double *)(i9*(ssize_t)sizeof(double)+_7ampl))>0)ioerr( ribletsin_ ); i9++;}}),1)))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," use_coco = %d",&use_coco_)>0 &&scanrec( ribletsin_ ," radius = %lg",&radius_)>0 &&scanrec( ribletsin_ ," stokexp = %lg",&stokexp_)>0 &&scanrec( ribletsin_ ," extra = %d",&extra_)>0))ioerr( ribletsin_ );
		   if(!((scanrec( ribletsin_ ," phiC = ",NULL),({int i10=0; while(i10<=6){if(!scanrec( ribletsin_ ," %lg",(double *)(i10*(ssize_t)sizeof(double)+phiC_))>0)ioerr( ribletsin_ ); i10++;}}),1) &&(scanrec( ribletsin_ ," gC = ",NULL),({int i11=0; while(i11<=6){if(!scanrec( ribletsin_ ," %lg",(double *)(i11*(ssize_t)sizeof(double)+gC_))>0)ioerr( ribletsin_ ); i11++;}}),1)))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," nx = %d",&nx_)>0 &&scanrec( ribletsin_ ," ny = %d",&ny_)>0 &&scanrec( ribletsin_ ," nz = %d",&nz_)>0 &&scanrec( ribletsin_ ," Lx = %lg",&Lx_)>0 &&scanrec( ribletsin_ ," Ly = %lg",&Ly_)>0))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," cflmax = %lg",&cflmax_)>0 &&scanrec( ribletsin_ ," fouriermax = %lg",&fouriermax_)>0 &&scanrec( ribletsin_ ," deltat = %lg",&deltat_)>0 &&scanrec( ribletsin_ ," maxTime = %lg",&maxTime_)>0))ioerr( ribletsin_ );
		   if(!(scanrec( ribletsin_ ," it_save = %d",&it_save_)>0 &&scanrec( ribletsin_ ," it_stat = %d",&it_stat_)>0 &&scanrec( ribletsin_ ," it_max = %d",&it_max_)>0 &&scanrec( ribletsin_ ," it_cfl = %d",&it_cfl_)>0 &&scanrec( ribletsin_ ," it_check = %d",&it_check_ )>0))ioerr( ribletsin_ );
		   if(!(scanbool( ribletsin_ ," continue = %c%*4[A-Za-z] ",&continue_)))ioerr( ribletsin_ );
	errfclose(&ribletsin_);
}}
char *riblets_test_2myname;
struct _riblets_test_s3{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _riblets_test_s3_s (ssize_t)sizeof(struct _riblets_test_s3)
FILE *dnsinfo_;
FILE *riblets_test_4;

        


	double wallclock_1wallclock(void);

extern struct timeval wallclock_wallclock_startim;

  extern double wallclock_1wallclock(void);

double clock_;



double riblets_test_15deltax;
double riblets_test_16deltay;






#include <mpi.h>


/*INTEGER iprocx, nprocx=atoi(COMMANDLINE(1)), iprocy, nprocy=atoi(COMMANDLINE(2)) */
int prevx_;
int nextx_;
int prevy_;
int nexty_;

/*INTEGER iproc, nproc,  */
int nreqs_;
int nsreqs_;

char unpckbuf_[(ssize_t)sizeof(struct grid_s6)*(3+1)] ;

int szbuf_;


  MPI_Status status[4];
  MPI_Request request[4];
  MPI_Request srequest[4];
  MPI_Comm  cart_comm, cart_comm_x;
  int nproc, iproc, nprocx, nprocy, iprocx, iprocy, idim[2];
  int prevx, nextx, prevy, nexty;


void riblets_test_18init_mpi(void){{struct freefunc* es=freestack;
    
       MPI_Init(NULL,NULL);
       MPI_Comm_size(MPI_COMM_WORLD, &nproc); nproc_=nproc;
       MPI_Comm_rank(MPI_COMM_WORLD, &iproc); iproc_=iproc+1;
       int ndims=2, reorder=1, ierr;
       int dim_size[2], periods[2];
       dim_size[0] = nprocx_; nprocx=nprocx_;
       dim_size[1] = nprocy_; nprocy=nprocy_;
       periods[0] = 1;
       periods[1] = 1; 
       ierr =  MPI_Cart_create(MPI_COMM_WORLD,ndims,dim_size,
             periods,reorder,&cart_comm);
       MPI_Cart_coords(cart_comm, iproc, ndims, idim);
       iprocx_=idim[0]+1; iprocy_=idim[1]+1;
       iprocx=idim[0];  iprocy=idim[1];
       MPI_Cart_shift(cart_comm, 0, 1, &prevx, &nextx); prevx_=prevx+1; nextx_=nextx+1;
       MPI_Cart_shift(cart_comm, 1, 1, &prevy, &nexty); prevy_=prevy+1; nexty_=nexty+1;
       // Create a communicator with all processes that hold the same y-slab
       // this is useful for collecting x-averaged statistics, for instance
       MPI_Comm_split(cart_comm, iprocy_, iproc_, &cart_comm_x);
    
}} 

int riblets_test_19nxl(int n_){struct freefunc* es=freestack;return (int)floor((double)((iprocx_-1)*n_)/(double)(nprocx_))+1;}
int riblets_test_20nxh(int n_){struct freefunc* es=freestack;return (int)floor((double)(iprocx_*n_)/(double)(nprocx_));}
int riblets_test_21nyl(int n_){struct freefunc* es=freestack;return (int)floor((double)((iprocy_-1)*n_)/(double)(nprocy_))+1;}
int riblets_test_22nyh(int n_){struct freefunc* es=freestack;return (int)floor((double)(iprocy_*n_)/(double)(nprocy_));}
int riblets_test_23M;
int riblets_test_24h;
ssize_t riblets_test_25i;
ssize_t riblets_test_26i;
struct freefunc recvbuf_f;char *recvbuf_;

int riblets_test_27h;
ssize_t riblets_test_28i;
ssize_t riblets_test_29i;
struct freefunc sendbuf_f;char *sendbuf_;



void riblets_test_30MPI_Send(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_){{struct freefunc* es=freestack;
          {int i_=x_l;do{{int j_=x__l;do{ { 
           (*(double *)(((i_-x_l)*(x__h-x__l+1)+j_-x__l)*(ssize_t)sizeof(double)+ireq_*riblets_test_28i+sendbuf_))=(*(double *)(j_*x__i+i_*x_i+x___) );
        }j_++;}while(j_<=x__h);}i_++;}while(i_<=x_h);}
     
        ssize_t L1_; 
        ssize_t L2_; 
        L1_= x_h-x_l+1; L2_= x__h-x__l+1;
        MPI_Isend((double *)(ireq_*((ssize_t)sizeof(double)*(szbuf_*(nz_+1)*4+1))+sendbuf_), L1_*L2_, MPI_DOUBLE, iP_-1, tag_,
              cart_comm, &srequest[ireq_]);
     
}}


void riblets_test_31MPI_Recv(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_){{struct freefunc* es=freestack;
     
        ssize_t L1_;
        ssize_t L2_;
        L1_= x_h-x_l+1; L2_= x__h-x__l+1;
        MPI_Irecv((double *)(ireq_*((ssize_t)sizeof(double)*(szbuf_*(nz_+1)*4+1))+recvbuf_), L1_*L2_, MPI_DOUBLE, iP_-1, tag_,
              cart_comm, &request[ireq_]);
     
     (*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).l=x_l;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).h=x_h;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).i=x_i;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.l=x__l;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.h=x__h;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.i=x__i;(*(struct grid_s6*)(ireq_*(ssize_t)sizeof(struct grid_s6)+unpckbuf_)).a.a=x___;
}}

void riblets_test_32mpi_barrier(void){{struct freefunc* es=freestack;
       
           MPI_Barrier(cart_comm);
       
}}




void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);
void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);
void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);
void riblets_test_36pbcwait(void);
int riblets_test_37h;
ssize_t riblets_test_38i;
struct freefunc zd_f;char *zd_;
int riblets_test_39h;
ssize_t riblets_test_40i;
struct freefunc yd_f;char *yd_;

ssize_t riblets_test_41i;
char *riblets_test_42zz;
ssize_t riblets_test_43i;
char *riblets_test_44yy;
int riblets_test_45l;
int riblets_test_46h;
int riblets_test_47l;
int riblets_test_48h;
int riblets_test_49h;
ssize_t riblets_test_50i;
ssize_t riblets_test_51i;
ssize_t riblets_test_52i;
ssize_t riblets_test_53st;
ssize_t riblets_test_54st;
struct freefunc var_f;char *var_ ;
struct freefunc old_f;char *old_;

char flowrate_[(ssize_t)sizeof(double)*(/*SA55*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)] ;


/* CoCo switch */
int useCoco_;

struct _parallelbcs_parupdate_s1{int direction_;struct grid_s6 xl_;struct grid_s6 xl1_;struct grid_s6 xh_;struct grid_s6 xh1_;struct grid_s6 yl_;struct grid_s6 yl1_;struct grid_s6 yh_;struct grid_s6 yh1_;};
#define _parallelbcs_parupdate_s1_s (ssize_t)sizeof(struct _parallelbcs_parupdate_s1)
extern struct _parallelbcs_parupdate_s1 parallelbcs_parupdate_comm;


extern void parallelbcs_parupdate_2reader(void);


extern void parallelbcs_parupdate_3writer(void);


extern void parallelbcs_parupdate_4com(void);


extern void riblets_test_36pbcwait(void);
 

extern void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);


extern void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);


extern void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);

extern void parallelbcs_5getmax(const int val_l,const int val_h,const ssize_t val_i,char *val__);



extern void parallelbcs_6accumulate(const int val_l,const int val_h,const ssize_t val_i,char *val__);


extern void parallelbcs_7accumulateX(const int valin_l,const int valin_h,const ssize_t valin_i,char *valin__,const int valout_l,const int valout_h,const ssize_t valout_i,char *valout__);

extern FILE *runtimedata_;


extern int iofiles_1h;
extern int iofiles_2h;
extern int iofiles_3h;
extern int iofiles_4h;
extern off_t iofiles_5i;
extern off_t iofiles_6i;
extern off_t iofiles_8i;
extern off_t iofiles_9i;
extern off_t iofiles_10i;
extern off_t iofiles_11i;
extern off_t iofiles_12st;
struct _iofiles_s7{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _iofiles_s7_s (ssize_t)sizeof(struct _iofiles_s7)
extern FILE *stored_;
extern FILE *iofiles_13;

extern void iofiles_21savefield(char *name_);


extern int cnt_stats_;

/* Define the statistics file. Check the order and which quantities to store */
/* ------ */
extern struct freefunc statsfilename_f;
extern char *statsfilename_;


extern int iofiles_24h;
extern ssize_t iofiles_25i;
extern struct freefunc stats_f;
extern char *stats_;

extern int iofiles_26h;
extern ssize_t iofiles_27i;
extern ssize_t iofiles_28i;
extern ssize_t iofiles_29st;
extern struct freefunc statsX_f;
extern char *statsX_;
extern struct freefunc statslocX_f;
extern char *statslocX_;
extern struct freefunc statstmpX_f;
extern char *statstmpX_;

extern int iofiles_30h;
extern int iofiles_31h;
extern int iofiles_32h;
extern int iofiles_33h;
extern off_t iofiles_34i;
extern off_t iofiles_36i;
extern off_t iofiles_37i;
extern off_t iofiles_38i;
extern off_t iofiles_39i;
struct _iofiles_s35{};
#define _iofiles_s35_s (ssize_t)sizeof(struct _iofiles_s35)
extern FILE *diskstats_;
extern FILE *iofiles_40;

extern int statsExist_;

extern void iofiles_51save_statsX(void);


/* Write x-averaged statistics to file (running average) */
/* ------ */
extern void iofiles_52output(void);




/* Define structures to be called in the main */
/* ------ */

extern ssize_t timestep_1i;
extern ssize_t timestep_2i;
extern ssize_t timestep_3st;
extern ssize_t timestep_4st;
extern struct freefunc uimbc_f;
extern char *uimbc_;
extern struct freefunc vimbc_f;
extern char *vimbc_;
extern struct freefunc wimbc_f;
extern char *wimbc_;





extern double d1x_;
extern double d1y_;
extern double d2x_;
extern double d2y_;

struct _timestep_s5{double d2zp_;double d2zm_;double d2z0_;double d1z_;double d1zp_;double d2zpp_;double d2zpm_;double d2zp0_;};
#define _timestep_s5_s (ssize_t)sizeof(struct _timestep_s5)
extern int timestep_6h;
extern ssize_t timestep_7i;
extern struct freefunc zc_f;
extern char *zc_;


extern double timestep_8limited(double x_);


/* Define the structure containing the data related to the wall-normal direction grid */
/* ------ */
extern void timestep_11AdamsB(double *val_,double *oldrsd_,double rsd_);


extern void timestep_12rai1(double *val_,double *oldrsd_,double rsd_);


extern void timestep_13rai2(double *val_,double *oldrsd_,double rsd_);


extern void timestep_14rai3(double *val_,double *oldrsd_,double rsd_);


/* Linear step to use the imbc with the velocity */
extern int timestep_15h;



extern void timestep_16linestep(void (*timescheme_)(double *val_,double *oldrsd_,double rsd_),int ix_,int iy_,double dts_,char *new_);


/* Define the LU decomposition for dz */
/* ------ */
extern int timestep_17h;
extern ssize_t timestep_18i;
extern ssize_t timestep_19st;
extern ssize_t timestep_20st;
extern struct freefunc M_f;
extern char *M_;

extern void timestep_21pressurelinestep(int ix_,int iy_,double dts_);


/* Define the function to get the CFL and Fourier number + any quantity useful to debug */
/* This routine is called once every it_cfl iterations, so it is clever to use it to get some quantities of the current flow. */
/* Any number of scalars can be added do this array, but REMEMBER TO MODIFY THE DIMENSION OF THIS ARRAY AT THE inputs SECTION OF THE MAIN. */
/* ------ */
extern void timestep_22get_cfl(void);



/* Time step advance in time */
/* ------ */
extern void timestep_23timestep(void (*timescheme_)(double *val_,double *oldrsd_,double rsd_));

struct DNS_ riblets_test_imbc_dns;

struct freefunc riblets_test_imbc_filenamef;char *riblets_test_imbc_filename;

struct RIBLETS_ riblets_test_imbc_riblet;

struct GRID_ riblets_test_imbc_uGrid;
struct GRID_ riblets_test_imbc_vGrid;
struct GRID_ riblets_test_imbc_wGrid;
struct GRID_ riblets_test_imbc_pGrid;

const int riblets_test_imbc_ca56[3]={1,0,0};
const int riblets_test_imbc_ca57[3]={0,1,0};
const int riblets_test_imbc_ca58[3]={0,0,1};
const int riblets_test_imbc_ca59[3]={0,0,0};
struct IMB_ riblets_test_imbc_uIMB;
struct IMB_ riblets_test_imbc_vIMB;
struct IMB_ riblets_test_imbc_wIMB;





int it_max_new_;

double cflmax_it_;

void riblets_test_66time_loop(void){{struct freefunc* es=freestack;
	 while( it_<it_max_){
  	/* Every it_cfl iterations, check the CFL and impose the new timestep */
	/* ------ */

		int mtemp67;

  	  		
	/* Run the RK3 integration and check the BCs */
	/* ------ */
	

		
		
		
 		
		
	 	
		
	/* Write the files */
	/* ------ */
	  	int mtemp68;

	  		
  	  	int mtemp73;
if( (mtemp67=it_ % it_cfl_ ,mtemp67>=0?mtemp67:mtemp67+it_cfl_ )== 0 ){
			timestep_22get_cfl();
			if( it_ <1000 ){  cflmax_it_ = 0.1; }else{ if( it_ <5000 ){  cflmax_it_ = 0.5; }else{  cflmax_it_ = cflmax_;	};};
			if( non_laminar_==0 ){  cflmax_it_ = cflmax_;};
			if( cflmax_it_>0.){ deltat_=cflmax_it_/(*(double *)(cfl_) );};
			if( ((*(double *)((ssize_t)sizeof(double)+cfl_))*deltat_)>fouriermax_ ){ deltat_=fouriermax_/(*(double *)((ssize_t)sizeof(double)+cfl_) );};
		};
		if( has_terminal_ ){ clock_=wallclock_1wallclock();};
		  {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ memmove(nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(nz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(ssize_t)sizeof(double)*(2+1));  (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))= - (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(nz_-2)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) );}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}
		if( non_laminar_==0 ){   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);} };
timestep_23timestep(timestep_12rai1);
  {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ memmove(nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(nz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(ssize_t)sizeof(double)*(2+1));  (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))= - (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(nz_-2)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) );}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}
		if( non_laminar_==0 ){   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);} };
timestep_23timestep(timestep_13rai2);
  {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ memmove(nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(nz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_,(ssize_t)sizeof(double)*(2+1));  (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))= - (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(nz_-2)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) );}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}
		if( non_laminar_==0 ){   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);}   {int ix_=riblets_test_45l;do{{int iy_=riblets_test_47l;do{{ (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+nz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_))=1.0 ;}iy_++;}while(iy_<=riblets_test_48h);}ix_++;}while(ix_<=riblets_test_46h);} };
timestep_23timestep(timestep_14rai3)		;
	  	it_+=1;
		iofiles_52output();
	  	if( (mtemp68=it_ % it_save_ ,mtemp68>=0?mtemp68:mtemp68+it_save_ )== 0 ){ int dtemp69;
int _70h;
ssize_t _72i;
struct freefunc _71f;char *_71;
_70h=snprintf((char*)NULL,0,"./fields/""%s""""%d"".field""",riblets_test_2myname,(dtemp69=it_ ,(dtemp69>=0)==(it_save_>=0)?dtemp69:dtemp69-(it_save_)+1)/ it_save_)+1;
_72i=(_70h+1);

_71=malloc(_72i);if(_71==NULL)_71=errmalloc();atblockexit(_71f,free,_71);
snprintf(_71,_70h+1,"./fields/""%s""""%d"".field""",riblets_test_2myname,(dtemp69=it_ ,(dtemp69>=0)==(it_save_>=0)?dtemp69:dtemp69-(it_save_)+1)/ it_save_);iofiles_21savefield(_71);free(_71f.ptr);freestack=es;};
		if( has_terminal_ ){  fprintf(stdout,"""elapsed time per timestep: ""%g"" s""""\n",wallclock_1wallclock()-clock_);};
		time_+=deltat_;
	/* Check the on the run input files to see if the user changed it_save, it_max or it_check */
	/* It's been checked every it_check times */
	/* ------ */
	  	if( (mtemp73=it_ % it_check_ ,mtemp73>=0?mtemp73:mtemp73+it_check_ )== 0 ){ 
			FILE *rereadfile_;

			rereadfile_=NULL;errfopen(&rereadfile_,"reread",O_RDONLY);if( rereadfile_!=NULL ){ 
			    riblets_test_1readribletsin();
			    errfclose(&rereadfile_);
			    remove("reread");
			};
	  	};
	};timeloop:;
}}
mainstart
nprocx_=atoi((*(char**)((ssize_t)sizeof(char*)+(char*)argv)));nprocy_=atoi((*(char**)(2*(ssize_t)sizeof(char*)+(char*)argv)));

/* with "number" redefined to the respective numeric type.           ! */
/* See matrix.info for usage.                                        ! */
/*                                                                   ! */
/* Copyright 1999-2020 Paolo Luchini http://CPLcode.net              ! */
/* Released under the attached LICENSE.                              ! */
/*                                                                   ! */
/* Code maturity: green.                                             ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */



REAL_Lanczos_norm_=0.;
  rbmat_Lanczos_R:;
/*USE gnuplot */
/*USE rtchecks */


/*!!!! */

	dns_dns:;


        /* Geometrical tolerance */
	riblets_riblets_11tol=1.e-10;
 


	riblets_riblets:;

 
	grid_staggeredGrid:;


	coco_coco:;

	/*dir=((char*)imbc_imbc_ca5-(ssize_t)sizeof(int))*/
	memmove((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+(ssize_t)sizeof(int)*3,(ssize_t)sizeof(int)+((char*)imbc_imbc_ca6-(ssize_t)sizeof(int)),(ssize_t)sizeof(int)*3);(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+2*(ssize_t)sizeof(int)*3))= - 1;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+2*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+2*(ssize_t)sizeof(int)*3))=0;(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+3*(ssize_t)sizeof(int)*3))=1;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+3*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+3*(ssize_t)sizeof(int)*3))=0;(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+4*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+4*(ssize_t)sizeof(int)*3))= - 1;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+4*(ssize_t)sizeof(int)*3))=0;(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+5*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+5*(ssize_t)sizeof(int)*3))=1;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+5*(ssize_t)sizeof(int)*3))=0;(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+6*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+6*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+6*(ssize_t)sizeof(int)*3))= - 1;(*(int *)((ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+7*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+1)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+7*(ssize_t)sizeof(int)*3))=0;(*(int *)((1+2)*(ssize_t)sizeof(int)+imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))+7*(ssize_t)sizeof(int)*3))=1;/*neighbours=imbc_imbc_7-((ssize_t)sizeof(int)*3+(ssize_t)sizeof(int))*/
	
	imbc_imbc:;



#pragma message "Compiling for the DNS code!"


/*#ifndef dns */
	parallel_parallel:;atexit(f10);



/*!!!! */

/*! TestFiles */
testfile1_=NULL; errfopen(&testfile1_,"../../../postProc/confrontoSin/data/uimbc_new_new.txt",O_RDWR|O_CREAT|O_TRUNC);testfile2_=NULL; errfopen(&testfile2_,"../../../postProc/confrontoSin/data/vimbc_new_new.txt",O_RDWR|O_CREAT|O_TRUNC);testfile3_=NULL; errfopen(&testfile3_,"../../../postProc/confrontoSin/data/wimbc_new_new.txt",O_RDWR|O_CREAT|O_TRUNC);testfile4_=NULL; errfopen(&testfile4_,"../../../postProc/confrontoSin/data/y_position_u.txt",O_RDWR|O_CREAT|O_TRUNC);testfile5_=NULL; errfopen(&testfile5_,"../../../postProc/confrontoSin/data/y_position_v.txt",O_RDWR|O_CREAT|O_TRUNC); fprintf( testfile1_ ,"""x y imbc""""\n");
 fprintf( testfile2_ ,"""x y imbc""""\n");
 fprintf( testfile3_ ,"""x y imbc""""\n");
/*WRITE TO testfile4 "x_uGrid y_uGrid x_vGrid y_vGrid" */
/* ------------------------ */

/* Defines */
/* ------------------------ */

it_=0;deltat_=0.;time_=0.;Uconv_=0.;riblets_test_1readribletsin();

/* If a restart file exists, override input parameters with those from field */
/* ------------------------ */
riblets_test_2myname=basename((*(char**)(argv)));
 
dnsinfo_=NULL;riblets_test_4=NULL;errfopen(&riblets_test_4,"./fields/riblets.field",O_RDONLY);dnsinfo_=riblets_test_4;if( dnsinfo_!=NULL ){
	int _5;
int _6;
int _7;
if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).nxs_,SEEK_SET)==0&&fread(&_5,(ssize_t)sizeof(int),1,dnsinfo_)==1))ioerr(dnsinfo_);
nx_=_5;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).nys_,SEEK_SET)==0&&fread(&_6,(ssize_t)sizeof(int),1,dnsinfo_)==1))ioerr(dnsinfo_);
ny_=_6;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).nzs_,SEEK_SET)==0&&fread(&_7,(ssize_t)sizeof(int),1,dnsinfo_)==1))ioerr(dnsinfo_);
nz_=_7;
	if( continue_ ){ 
        	int _8;
double _9;
double _10;
double _11;
double _12;
double _13;
double _14;
if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).its_,SEEK_SET)==0&&fread(&_8,(ssize_t)sizeof(int),1,dnsinfo_)==1))ioerr(dnsinfo_);
it_=_8;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).headxs_,SEEK_SET)==0&&fread(&_9,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
headx_=_9;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).headys_,SEEK_SET)==0&&fread(&_10,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
heady_=_10;
        	if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).times_,SEEK_SET)==0&&fread(&_11,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
time_=_11;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).nus_,SEEK_SET)==0&&fread(&_12,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
nu_=_12;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).Lxs_,SEEK_SET)==0&&fread(&_13,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
Lx_=_13;  if(!(fseeko(dnsinfo_,(-(0))+(ssize_t)&(*(struct _riblets_test_s3 *)(0)).Lys_,SEEK_SET)==0&&fread(&_14,(ssize_t)sizeof(double),1,dnsinfo_)==1))ioerr(dnsinfo_);
Ly_=_14;
	}else{
        	time_=0.;
	};
};

/* Initialisations */
/* ------------------------ */

  gettimeofday(&wallclock_wallclock_startim,NULL);
  wallclock_wallclock:;
	riblets_test_15deltax=Lx_/(double)(nx_);
  	riblets_test_16deltay=Ly_/(double)(ny_);
 
prevx_= - 99;nextx_= - 99;prevy_= - 99;nexty_= - 99;riblets_test_18init_mpi();
 has_terminal_ = (iprocx_==nprocx_ )&&( iprocy_==nprocy_);
riblets_test_23M=riblets_test_22nyh(ny_)-riblets_test_21nyl(ny_)+3;
 szbuf_ =riblets_test_20nxh(nx_)-riblets_test_19nxl(nx_)+3;if(szbuf_ <riblets_test_23M)szbuf_ =riblets_test_23M;
riblets_test_24h=szbuf_*(nz_+1)*4;
riblets_test_25i=(ssize_t)sizeof(double)*(riblets_test_24h+1);
riblets_test_26i=riblets_test_25i*(3+1);

recvbuf_=malloc(riblets_test_26i);if(recvbuf_==NULL)recvbuf_=errmalloc();atblockexit(recvbuf_f,free,recvbuf_);
riblets_test_27h=szbuf_*(nz_+1)*4;
riblets_test_28i=(ssize_t)sizeof(double)*(riblets_test_27h+1);
riblets_test_29i=riblets_test_28i*(3+1);

sendbuf_=malloc(riblets_test_29i);if(sendbuf_==NULL)sendbuf_=errmalloc();atblockexit(sendbuf_f,free,sendbuf_);
if( has_terminal_ ){
 fprintf(stdout,"""      ___           ___           ___           ___           ___           ___           ___       ___           ___      """"\n");
 fprintf(stdout,"""     /\\  \\         /\\__\\         /\\  \\         /\\__\\         /\\__\\         /\\  \\         /\\__\\     /\\  \\         /\\  \\     """"\n");
 fprintf(stdout,"""    /::\\  \\       /:/  /        /::\\  \\       /::|  |       /::|  |       /::\\  \\       /:/  /    /::\\  \\       /::\\  \\    """"\n");
 fprintf(stdout,"""   /:/\\:\\  \\     /:/__/        /:/\\:\\  \\     /:|:|  |      /:|:|  |      /:/\\:\\  \\     /:/  /    /:/\\:\\  \\     /:/\\:\\  \\   """"\n");
 fprintf(stdout,"""  /:/  \\:\\  \\   /::\\  \\ ___   /::\\~\\:\\  \\   /:/|:|  |__   /:/|:|  |__   /::\\~\\:\\  \\   /:/  /    /::\\~\\:\\  \\   /:/  \\:\\__\\  """"\n");
 fprintf(stdout,""" /:/__/ \\:\\__\\ /:/\\:\\  /\\__\\ /:/\\:\\ \\:\\__\\ /:/ |:| /\\__\\ /:/ |:| /\\__\\ /:/\\:\\ \\:\\__\\ /:/__/    /:/\\:\\ \\:\\__\\ /:/__/ \\:|__| """"\n");
 fprintf(stdout,""" \\:\\  \\  \\/__/ \\/__\\:\\/:/  / \\/__\\:\\/:/  / \\/__|:|/:/  / \\/__|:|/:/  / \\:\\~\\:\\ \\/__/ \\:\\  \\    \\/__\\:\\ \\/__/ \\:\\  \\ /:/  / """"\n");
 fprintf(stdout,"""  \\:\\  \\            \\::/  /       \\::/  /      |:/:/  /      |:/:/  /   \\:\\ \\:\\__\\    \\:\\  \\        \\:\\__\\    \\:\\  /:/  /  """"\n");
 fprintf(stdout,"""   \\:\\  \\           /:/  /        /:/  /       |::/  /       |::/  /     \\:\\ \\/__/     \\:\\  \\        \\/__/     \\:\\/:/  /   """"\n");
 fprintf(stdout,"""    \\:\\__\\         /:/  /        /:/  /        /:/  /        /:/  /       \\:\\__\\        \\:\\__\\                  \\::/__/    """"\n");
 fprintf(stdout,"""     \\/__/         \\/__/         \\/__/         \\/__/         \\/__/         \\/__/         \\/__/                   ~~        """"\n");
 fprintf(stdout,"""""""\n");
 fprintf(stdout,"""""""\n");
 fprintf(stdout,"""MPI initialised! Recap of the process grid, some MPI infos...""""\n");
};
riblets_test_32mpi_barrier();
 for(int iP_=1 ;iP_<= nproc_;iP_+=1){
   if( iP_==iproc_ ){
     fprintf(stdout,"""iproc=""%d \t""nproc=""%d \t""iprocx=""%d \t""nprocx=""%d \t""prevx=""%d \t""nextx=""%d \t""iprocy=""%d \t""nprocy=""%d \t""prevy=""%d \t""nexty=""%d",iproc_ ,nproc_ ,iprocx_ ,nprocx_ ,prevx_ ,nextx_ ,iprocy_ ,nprocy_ ,prevy_ ,nexty_);putc('\n',stdout);
   };
   riblets_test_32mpi_barrier();
}
if( has_terminal_ ){
    fprintf(stdout,"""""""\n");
    fprintf(stdout,"""Echo of the input parameters""""\n");
   fprintf(stdout,"""nx=""%d \t""ny=""%d \t""nz=""%d \t""it=""%d",nx_,ny_,nz_,it_);putc('\n',stdout);
   fprintf(stdout,"""Lx=""%g \t""Ly=""%g \t""deltat=""%g \t""nu=""%g",Lx_ ,Ly_ ,deltat_ ,nu_);putc('\n',stdout);
   fprintf(stdout,"""headx=""%g \t""heady=""%g",headx_ ,heady_);putc('\n',stdout);
   fprintf(stdout,"""it_save=""%d \t""it_stat=""%d \t""it_max=""%d",it_save_ ,it_stat_ ,it_max_);putc('\n',stdout);
   fprintf(stdout,"""ratiohs=""%g",ratiohs_);putc('\n',stdout);
    fprintf(stdout,"""""""\n");
};


riblets_test_37h=2*nz_;
riblets_test_38i=(ssize_t)sizeof(double)*(riblets_test_37h+1);

zd_=malloc(riblets_test_38i);if(zd_==NULL)zd_=errmalloc();atblockexit(zd_f,free,zd_);
riblets_test_39h=2*ny_+2;
riblets_test_40i=(ssize_t)sizeof(double)*(riblets_test_39h+1);

yd_=malloc(riblets_test_40i);if(yd_==NULL)yd_=errmalloc();atblockexit(yd_f,free,yd_);
riblets_test_41i=2*(ssize_t)sizeof(double);
riblets_test_42zz=zd_;
     riblets_test_43i=2*(ssize_t)sizeof(double);
riblets_test_44yy=yd_;
 
riblets_test_45l=riblets_test_19nxl(nx_)-1;
riblets_test_46h=riblets_test_20nxh(nx_)+1;
riblets_test_47l=riblets_test_21nyl(ny_)-1;
riblets_test_48h=riblets_test_22nyh(ny_)+1;
riblets_test_49h=nz_;
riblets_test_50i=(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(riblets_test_49h+1);
riblets_test_51i=riblets_test_50i*(riblets_test_48h-riblets_test_47l+1);
riblets_test_52i=riblets_test_51i*(riblets_test_46h-riblets_test_45l+1);
riblets_test_53st=riblets_test_47l*riblets_test_50i;
riblets_test_54st=riblets_test_45l*riblets_test_51i+riblets_test_53st;

var_=malloc(riblets_test_52i);if(var_==NULL)var_=errmalloc();var_-=riblets_test_54st;atblockexit(var_f,free,var_+riblets_test_54st);
 memset(riblets_test_54st+var_,0,riblets_test_52i);
old_=malloc(riblets_test_52i);if(old_==NULL)old_=errmalloc();old_-=riblets_test_54st;atblockexit(old_f,free,old_+riblets_test_54st);
 memset(flowrate_,0,(ssize_t)sizeof(double)*(/*SA55*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1));if( use_coco_ == 1 ){
	 useCoco_ = 1;
}else{ if( use_coco_ == 0 ){
	 useCoco_ = 0;
}else{
traphandler("ERROR: Invalid value for use_coco input!");
};};


parallelbcs_parupdate:;
runtimedata_=NULL;{
  
     runtimedata_=fopen("./output/runtimedata.txt", "a");
  
}

/* -------\\ */

/* Read data from the .field file */
/* ------ */
iofiles_1h=2*nz_;
iofiles_2h=nx_;
iofiles_3h=ny_;
iofiles_4h=nz_;
iofiles_5i=(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_4h+1);
iofiles_6i=iofiles_5i*iofiles_3h;
iofiles_8i=(ssize_t)sizeof(double)*(iofiles_1h+1);
iofiles_9i=(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_4h+1);
iofiles_10i=iofiles_9i*iofiles_3h;
iofiles_11i=iofiles_10i*iofiles_2h;
iofiles_12st=iofiles_6i+iofiles_5i;
stored_=NULL;iofiles_13=NULL;errfopen(&iofiles_13,"./fields/riblets.field",O_RDONLY);stored_=iofiles_13;if( stored_!=NULL ){
	if( has_terminal_ ){  fprintf(stdout,"""Reading restart file ...""""\n");};
  	if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(stored_,(off_t)(_iofiles_s7_s+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fread(zd_,(ssize_t)sizeof(double),riblets_test_37h+1,stored_)==riblets_test_37h+1))ioerr(stored_);}else{  {int _14i_=riblets_test_37h;do{{double _15;
if(!(fseeko(stored_,_14i_*(off_t)(ssize_t)sizeof(double)+(off_t)(_iofiles_s7_s+(ssize_t)(char*)(struct _iofiles_s7*)(0)) ,SEEK_SET)==0&&fread(&_15,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);
(*(double *)(_14i_*(ssize_t)sizeof(double)+zd_))=_15;}_14i_--;}while(_14i_>=0);}};
  	  {int _16i_=riblets_test_45l+1;do{{  {int _17i_=riblets_test_47l+1;do{{if((ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)&&(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)){if(!(fseeko(stored_,_17i_*(off_t)iofiles_5i+_16i_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fread(_17i_*riblets_test_50i+_16i_*riblets_test_51i+var_,(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1),riblets_test_49h+1,stored_)==riblets_test_49h+1))ioerr(stored_);}else{  {int _18i_=riblets_test_49h;do{{if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(stored_,_18i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_17i_*(off_t)iofiles_5i+_16i_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fread(_18i_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_17i_*riblets_test_50i+_16i_*riblets_test_51i+var_,(ssize_t)sizeof(double),/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1,stored_)==/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1))ioerr(stored_);}else{  {int _19i_=/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{double _20;
if(!(fseeko(stored_,_19i_*(off_t)(ssize_t)sizeof(double)+_18i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_17i_*(off_t)iofiles_5i+_16i_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)) ,SEEK_SET)==0&&fread(&_20,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);
(*(double *)(_19i_*(ssize_t)sizeof(double)+_18i_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_17i_*riblets_test_50i+_16i_*riblets_test_51i+var_))=_20;}_19i_--;}while(_19i_>=0);}};}_18i_--;}while(_18i_>=0);}};}_17i_++;}while(_17i_<=riblets_test_48h-1);}}_16i_++;}while(_16i_<=riblets_test_46h-1);}
}else{ 
traphandler("ERROR: You need an initial field!");
};
if( stored_!=NULL ){ errfclose(&stored_);};
memmove(riblets_test_54st+old_,riblets_test_54st+var_,riblets_test_52i);


/* Save field -- parallel */
/* ------ */
cnt_stats_=0;statsfilename_=NULL;atblockexit(statsfilename_f,condfree,&statsfilename_);
 condfree(&statsfilename_);
statsfilename_=strdup("""./output/stats.bin");iofiles_24h=nz_;
iofiles_25i=(ssize_t)sizeof(double)*(/*SA23*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_24h+1);

stats_=malloc(iofiles_25i);if(stats_==NULL)stats_=errmalloc();atblockexit(stats_f,free,stats_);
memset(stats_,0,iofiles_25i); iofiles_26h=nz_;
iofiles_27i=(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_26h+1);
iofiles_28i=iofiles_27i*(riblets_test_48h-riblets_test_47l+1);
iofiles_29st=riblets_test_47l*iofiles_27i;

statsX_=malloc(iofiles_28i);if(statsX_==NULL)statsX_=errmalloc();statsX_-=iofiles_29st;atblockexit(statsX_f,free,statsX_+iofiles_29st);
memset(iofiles_29st+statsX_,0,iofiles_28i);
statslocX_=malloc(iofiles_28i);if(statslocX_==NULL)statslocX_=errmalloc();statslocX_-=iofiles_29st;atblockexit(statslocX_f,free,statslocX_+iofiles_29st);
memset(iofiles_29st+statslocX_,0,iofiles_28i);
statstmpX_=malloc(iofiles_28i);if(statstmpX_==NULL)statstmpX_=errmalloc();statstmpX_-=iofiles_29st;atblockexit(statstmpX_f,free,statstmpX_+iofiles_29st);
memset(iofiles_29st+statstmpX_,0,iofiles_28i);iofiles_30h=ny_+1;
iofiles_31h=nz_;
iofiles_32h=ny_+1;
iofiles_33h=nz_;
iofiles_34i=(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_33h+1);
iofiles_36i=(ssize_t)sizeof(double)*(iofiles_30h+1);
iofiles_37i=(ssize_t)sizeof(double)*(iofiles_31h+1);
iofiles_38i=(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(iofiles_33h+1);
iofiles_39i=iofiles_38i*(iofiles_32h+1);
diskstats_=NULL;iofiles_40=NULL;errfopen(&iofiles_40,statsfilename_,O_RDONLY);diskstats_=iofiles_40;statsExist_=diskstats_!=NULL; {int iy_= 0 ;do{{ (*(double *)(iy_*(ssize_t)sizeof(double)+yd_))=0.5*riblets_test_16deltay*(double)(iy_);}iy_+=1;}while(iy_<= 2*ny_+2     );}

/* Routine to evaluate already existing stats file */
/* ------ */
if( statsExist_ ){
  	if( has_terminal_ ){  fprintf(stdout,"""Reading statistics file : ""%s""\n",statsfilename_); fflush(stdout); };
  	{ 
      		ssize_t _41i;
ssize_t _42st;
int _47;
_41i=(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(nz_+1);
_42st=(riblets_test_47l+1)*_41i;
if(iofiles_27i==_41i&&iofiles_34i==_41i){if(!(fseeko(diskstats_,(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0))+_42st,SEEK_SET)==0&&fread(statsX_+_42st,_41i,riblets_test_48h-1-(riblets_test_47l+1)+1,diskstats_)==riblets_test_48h-1-(riblets_test_47l+1)+1))ioerr(diskstats_);}else{  {int _43i_=riblets_test_47l+1;do{{if((ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)&&(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)){if(!(fseeko(diskstats_,_43i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fread(_43i_*iofiles_27i+statsX_,(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1),nz_+1,diskstats_)==nz_+1))ioerr(diskstats_);}else{  {int _44i_=nz_;do{{if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(diskstats_,_44i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_43i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fread(_44i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_43i_*iofiles_27i+statsX_,(ssize_t)sizeof(double),/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1,diskstats_)==/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1))ioerr(diskstats_);}else{  {int _45i_=/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{double _46;
if(!(fseeko(diskstats_,_45i_*(off_t)(ssize_t)sizeof(double)+_44i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_43i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)) ,SEEK_SET)==0&&fread(&_46,(ssize_t)sizeof(double),1,diskstats_)==1))ioerr(diskstats_);
(*(double *)(_45i_*(ssize_t)sizeof(double)+_44i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_43i_*iofiles_27i+statsX_))=_46;}_45i_--;}while(_45i_>=0);}};}_44i_--;}while(_44i_>=0);}};}_43i_++;}while(_43i_<=riblets_test_48h-1);}};
      		if(!(fseeko(diskstats_,(off_t )(((iofiles_36i+iofiles_37i)+iofiles_39i)+(-(0))+(ssize_t)(char*)((void **)(0))),SEEK_SET)==0&&fread(&_47,(ssize_t)sizeof(int),1,diskstats_)==1))ioerr(diskstats_);
cnt_stats_=_47;
      		{  {int _48i_=riblets_test_47l;do{{int _49i_=iofiles_26h;do{{int _50i_=/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{(*(double *)(_50i_*(ssize_t)sizeof(double)+_49i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_48i_*iofiles_27i+statsX_))*=(double)(cnt_stats_);}_50i_--;}while(_50i_>=0);}_49i_--;}while(_49i_>=0);}_48i_++;}while(_48i_<=riblets_test_48h-(0));}}
  	errfclose(&diskstats_);
}};


/* Save statistics */
/* ------ */

timestep_1i=(ssize_t)sizeof(struct arrptr)*(riblets_test_48h-riblets_test_47l+1);
timestep_2i=timestep_1i*(riblets_test_46h-riblets_test_45l+1);
timestep_3st=riblets_test_47l*(ssize_t)sizeof(struct arrptr);
timestep_4st=riblets_test_45l*timestep_1i+timestep_3st;

uimbc_=malloc(timestep_2i);if(uimbc_==NULL)uimbc_=errmalloc();uimbc_-=timestep_4st;atblockexit(uimbc_f,free,uimbc_+timestep_4st);

vimbc_=malloc(timestep_2i);if(vimbc_==NULL)vimbc_=errmalloc();vimbc_-=timestep_4st;atblockexit(vimbc_f,free,vimbc_+timestep_4st);

wimbc_=malloc(timestep_2i);if(wimbc_==NULL)wimbc_=errmalloc();wimbc_-=timestep_4st;atblockexit(wimbc_f,free,wimbc_+timestep_4st);
d1x_=1./riblets_test_15deltax;d1y_=1./riblets_test_16deltay;d2x_=1./riblets_test_15deltax/riblets_test_15deltax;d2y_=1./riblets_test_16deltay/riblets_test_16deltay;timestep_6h=nz_-1;
timestep_7i=_timestep_s5_s*timestep_6h;

zc_=malloc(timestep_7i);if(zc_==NULL)zc_=errmalloc();zc_-=_timestep_s5_s;atblockexit(zc_f,free,zc_+_timestep_s5_s);
 {int iz_=1 ;do{{ {
	struct _timestep_s5 *_9w;
double _10d1zm;
_9w=(struct _timestep_s5 *)(iz_*_timestep_s5_s+zc_);
(*_9w).d1z_=1./((*(double *)((2*iz_+1)*(ssize_t)sizeof(double)+zd_))-(*(double *)((2*iz_-1)*(ssize_t)sizeof(double)+zd_)));
  	_10d1zm=1./((*(double *)(iz_*riblets_test_41i+riblets_test_42zz))-(*(double *)((iz_-1)*riblets_test_41i+riblets_test_42zz)));
 
  	(*_9w).d2zm_=(*_9w).d1z_*_10d1zm;
  	(*_9w).d1zp_=1./((*(double *)((iz_+1)*riblets_test_41i+riblets_test_42zz))-(*(double *)(iz_*riblets_test_41i+riblets_test_42zz)));
  	(*_9w).d2zp_=(*_9w).d1z_*(*_9w).d1zp_;
  	(*_9w).d2z0_=(*_9w).d2zm_+(*_9w).d2zp_+2.*d2x_+2.*d2y_;
  	if( iz_<nz_-1 ){
    		(*_9w).d2zpp_=1./((*(double *)((2*iz_+3)*(ssize_t)sizeof(double)+zd_))-(*(double *)((2*iz_+1)*(ssize_t)sizeof(double)+zd_)))*(*_9w).d1zp_;
    		(*_9w).d2zpm_=(*_9w).d1z_*(*_9w).d1zp_;
    		(*_9w).d2zp0_=(*_9w).d2zpm_+(*_9w).d2zpp_+2.*d2x_+2.*d2y_;
  	};
}}iz_+=1;}while(iz_<= nz_-1);}


/* Define the steps of the RK3 semi-explicit Runge-Kutta method and the Adams-Bashfort (unstable!) */
/* ------ */
timestep_15h=nz_-1;
timestep_17h=nz_-1;
timestep_18i=(ssize_t)sizeof(double)*(1-(-1)+1)*timestep_17h;
timestep_19st=(-1)*(ssize_t)sizeof(double);
timestep_20st=(ssize_t)sizeof(double)*(1-(-1)+1)+timestep_19st;

M_=malloc(timestep_18i);if(M_==NULL)M_=errmalloc();M_-=timestep_20st;atblockexit(M_f,free,M_+timestep_20st);
 for(int iz_=1 ;iz_<= nz_-1;iz_+=1){
  	(*(double *)(-(ssize_t)sizeof(double)+iz_*(ssize_t)sizeof(double)*(1-(-1)+1)+M_))=(*(struct _timestep_s5 *)(iz_*_timestep_s5_s+zc_)).d2zm_;
  	(*(double *)((ssize_t)sizeof(double)+iz_*(ssize_t)sizeof(double)*(1-(-1)+1)+M_))=(*(struct _timestep_s5 *)(iz_*_timestep_s5_s+zc_)).d2zp_;
  	(*(double *)(iz_*(ssize_t)sizeof(double)*(1-(-1)+1)+M_))= - (*(struct _timestep_s5 *)(iz_*_timestep_s5_s+zc_)).d2z0_;
}
(*(double *)((ssize_t)sizeof(double)*(1-(-1)+1)+M_))+=(*(double *)(-(ssize_t)sizeof(double)+(ssize_t)sizeof(double)*(1-(-1)+1)+M_));
(*(double *)(timestep_17h*(ssize_t)sizeof(double)*(1-(-1)+1)+M_))+=(*(double *)((ssize_t)sizeof(double)+timestep_17h*(ssize_t)sizeof(double)*(1-(-1)+1)+M_));
rbmat_6LUdecomp(1,timestep_17h,(ssize_t)sizeof(double)*(1-(-1)+1),(-1),1,(ssize_t)sizeof(double),M_);
/*sor=1.5*/

/* Linear step to upgrade the pressure field */
/* ------ */



/* Imbc */
/* ====================================  */


/* Read parameters and grid from restart file */
riblets_test_imbc_filename=NULL;atblockexit(riblets_test_imbc_filenamef,condfree,&riblets_test_imbc_filename);
condfree(&riblets_test_imbc_filename);
riblets_test_imbc_filename=strdup("""./fields/riblets.field");dns_5readRestartFile(&riblets_test_imbc_dns ,riblets_test_imbc_filename);

/* Initialise MPI */
parallel_4mpiInit(&riblets_test_imbc_dns);

/* Read parameters from the riblet file */
condfree(&riblets_test_imbc_filename);
riblets_test_imbc_filename=strdup("""./input/riblets.in");
riblets_1readInputFile(&riblets_test_imbc_riblet,riblets_test_imbc_filename);

/* Initialise grid */
grid_7initialiseGrid(&riblets_test_imbc_uGrid ,riblets_test_imbc_dns ,1,3,(ssize_t)sizeof(int),((char*)riblets_test_imbc_ca56-(ssize_t)sizeof(int)));
grid_7initialiseGrid(&riblets_test_imbc_vGrid ,riblets_test_imbc_dns ,1,3,(ssize_t)sizeof(int),((char*)riblets_test_imbc_ca57-(ssize_t)sizeof(int)));
grid_7initialiseGrid(&riblets_test_imbc_wGrid ,riblets_test_imbc_dns ,1,3,(ssize_t)sizeof(int),((char*)riblets_test_imbc_ca58-(ssize_t)sizeof(int)));
grid_7initialiseGrid(&riblets_test_imbc_pGrid ,riblets_test_imbc_dns ,1,3,(ssize_t)sizeof(int),((char*)riblets_test_imbc_ca59-(ssize_t)sizeof(int)));

/* Allocate IBM coefficients */
imbc_2allocate(&riblets_test_imbc_uIMB ,riblets_test_imbc_dns ,riblets_test_imbc_uGrid ,riblets_test_imbc_riblet ,useCoco_);
imbc_2allocate(&riblets_test_imbc_vIMB ,riblets_test_imbc_dns ,riblets_test_imbc_vGrid ,riblets_test_imbc_riblet ,useCoco_);
imbc_2allocate(&riblets_test_imbc_wIMB ,riblets_test_imbc_dns ,riblets_test_imbc_wGrid ,riblets_test_imbc_riblet ,useCoco_);

/* Populate IBM coefficients */
imbc_4populateCoeff(&riblets_test_imbc_uIMB ,riblets_test_imbc_dns ,riblets_test_imbc_uGrid ,riblets_test_imbc_pGrid ,riblets_test_imbc_riblet ,useCoco_);
imbc_4populateCoeff(&riblets_test_imbc_vIMB ,riblets_test_imbc_dns ,riblets_test_imbc_vGrid ,riblets_test_imbc_pGrid ,riblets_test_imbc_riblet ,useCoco_);
imbc_4populateCoeff(&riblets_test_imbc_wIMB ,riblets_test_imbc_dns ,riblets_test_imbc_wGrid ,riblets_test_imbc_pGrid ,riblets_test_imbc_riblet ,useCoco_);


 {int ix_=riblets_test_imbc_uIMB.imbc_.l ;do{ {int iy_=riblets_test_imbc_uIMB.imbc_.a.l ;do{{
	(*(struct arrptr *)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_))=(*(struct arrptr *)(iy_*riblets_test_imbc_uIMB.imbc_.a.i+ix_*riblets_test_imbc_uIMB.imbc_.i+riblets_test_imbc_uIMB.imbc_.a.a));
	(*(struct arrptr *)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_))=(*(struct arrptr *)(iy_*riblets_test_imbc_vIMB.imbc_.a.i+ix_*riblets_test_imbc_vIMB.imbc_.i+riblets_test_imbc_vIMB.imbc_.a.a));
	(*(struct arrptr *)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_))=(*(struct arrptr *)(iy_*riblets_test_imbc_wIMB.imbc_.a.i+ix_*riblets_test_imbc_wIMB.imbc_.i+riblets_test_imbc_wIMB.imbc_.a.a));
	if( (ix_==180 )&&( iy_<riblets_test_imbc_uIMB.imbc_.a.h )){ /* Coefficient study */
		/*WRITE BY NAME TO testfile1 uGrid.x(1,3) */
		/*WRITE BY NAME TO testfile1 iy, uimbc(ix,iy)(uimbc(ix,iy).LO) */
		 for(int zCoord_=(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_)).l ;zCoord_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_)).h;zCoord_+=1){
			 fprintf( testfile1_ ,"%g \t%g \t%g""\n",200.*(*(double *)(iy_*(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_uGrid.x_)).i+(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_uGrid.x_)).a)) ,200.*(*(double *)(zCoord_*(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_uGrid.x_)).i+(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_uGrid.x_)).a)) ,(*(double *)(zCoord_*(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_)).i+(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_)).a)));
		}
		 for(int zCoord_=(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_)).l ;zCoord_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_)).h;zCoord_+=1){
			 fprintf( testfile3_ ,"%g \t%g \t%g""\n",200.*(*(double *)(iy_*(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_wGrid.x_)).i+(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_wGrid.x_)).a)) ,200.*(*(double *)(zCoord_*(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_wGrid.x_)).i+(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_wGrid.x_)).a)) ,(*(double *)(zCoord_*(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_)).i+(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_)).a)));
		}
		 fprintf( testfile4_ ,"%g""\n",200.*riblets_2yWave(riblets_test_imbc_riblet,(*(double *)(180*(*(struct arrptr*)(riblets_test_imbc_uGrid.x_)).i+(*(struct arrptr*)(riblets_test_imbc_uGrid.x_)).a))));
		 fprintf( testfile5_ ,"%g""\n",200.*riblets_2yWave(riblets_test_imbc_riblet,(*(double *)(180*(*(struct arrptr*)(riblets_test_imbc_vGrid.x_)).i+(*(struct arrptr*)(riblets_test_imbc_vGrid.x_)).a))));
	};
	if( (ix_==180 )&&( iy_<riblets_test_imbc_uIMB.imbc_.a.h-1 )){
		 for(int zCoord_=(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_)).l ;zCoord_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_)).h;zCoord_+=1){
			 fprintf( testfile2_ ,"%g \t%g \t%g""\n",200.*(*(double *)(iy_*(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_vGrid.x_)).i+(*(struct arrptr*)((ssize_t)sizeof(struct arrptr)+riblets_test_imbc_vGrid.x_)).a)) ,200.*(*(double *)(zCoord_*(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_vGrid.x_)).i+(*(struct arrptr*)(2*(ssize_t)sizeof(struct arrptr)+riblets_test_imbc_vGrid.x_)).a)) ,(*(double *)(zCoord_*(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_)).i+(*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_)).a)));
		}
	};
}iy_+=1;}while(iy_<= riblets_test_imbc_uIMB.imbc_.a.h);}ix_+=1;}while(ix_<= riblets_test_imbc_uIMB.imbc_.h );}


riblets_test_imbc:;errfclose(&testfile1_);
errfclose(&testfile2_);
errfclose(&testfile3_);
errfclose(&testfile4_);
errfclose(&testfile5_);
/* ==================================== */

if( !(continue_ )){ 
/* Imposing zero velocity in the geometry */
/* ------------------------- */
	
	

 for(int ix_=riblets_test_45l+1 ;ix_<= riblets_test_46h-1 ;ix_+=1){ for(int iy_=riblets_test_47l+1 ;iy_<= riblets_test_48h-1;iy_+=1){

		

	 {int iz_=0 ;do{{  (*(double *)(0+(ssize_t)sizeof(double)+iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) )= 0.;}iz_+=1;}while(iz_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+uimbc_)).l-1);}
   		 {int iz_=0 ;do{{  (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) )= 0.;}iz_+=1;}while(iz_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+vimbc_)).l-1);}
   		 {int iz_=0 ;do{{  (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+var_) )= 0.;}iz_+=1;}while(iz_<= (*(struct arrptr*)(iy_*(ssize_t)sizeof(struct arrptr)+ix_*timestep_1i+wimbc_)).l-1		);}
}}

/* Check that the initial condition fulfills the boundary conditions */
/* ------------------------- */
{ {char *_60_=riblets_test_45l*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(nz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+var_;int _60_1=riblets_test_46h-riblets_test_45l; do{{ { {char *_61_=riblets_test_47l*riblets_test_50i+_60_;int _61_1=riblets_test_48h-riblets_test_47l; do{{ (*(double *)(_61_))=0.;}_61_+=riblets_test_50i;_61_1--;}while(_61_1>=0);}} }_60_+=riblets_test_51i;_60_1--;}while(_60_1>=0);}}
  	{ {char *_62_=riblets_test_45l*riblets_test_51i+(nz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+var_;int _62_1=riblets_test_46h-riblets_test_45l; do{{ { {char *_63_=riblets_test_47l*riblets_test_50i+_62_;int _63_1=riblets_test_48h-riblets_test_47l; do{{ (*(double *)(_63_))=0.;}_63_+=riblets_test_50i;_63_1--;}while(_63_1>=0);}} }_62_+=riblets_test_51i;_62_1--;}while(_62_1>=0);}}
  	{ {char *_64_=riblets_test_45l*riblets_test_51i+var_;int _64_1=riblets_test_46h-riblets_test_45l; do{{ { {char *_65_=riblets_test_47l*riblets_test_50i+_64_;int _65_1=riblets_test_48h-riblets_test_47l; do{{ memset(_65_,0,(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)); }_65_+=riblets_test_50i;_65_1--;}while(_65_1>=0);}} }_64_+=riblets_test_51i;_64_1--;}while(_64_1>=0);}}	
};



/* Timeloop */
/* ------------------------- */
riblets_test_66time_loop();
/* Dump the field in the original file. This is done even if it_save and it do not match */
/* ------ */
iofiles_21savefield("./fields/riblets.field");
iofiles_51save_statsX();

/* Finalisations */
/* ------------------------ */

  MPI_Finalize();
riblets_test_finalise:;errfclose(&runtimedata_);


condfree(riblets_test_imbc_filenamef.ptr);free(M_f.ptr);free(zc_f.ptr);free(wimbc_f.ptr);free(vimbc_f.ptr);free(uimbc_f.ptr);free(statstmpX_f.ptr);free(statslocX_f.ptr);free(statsX_f.ptr);free(stats_f.ptr);condfree(statsfilename_f.ptr);free(old_f.ptr);free(var_f.ptr);free(yd_f.ptr);free(zd_f.ptr);free(sendbuf_f.ptr);free(recvbuf_f.ptr);freestack=es;return 0;}
