/*-I/opt/openmpi/openmpi_4.1.4_gcc/include -pthread -L/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,-rpath -Wl,/opt/openmpi/openmpi_4.1.4_gcc/lib -Wl,--enable-new-dtags -lmpi */

#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64

#define _LARGE_FILES

#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

extern char errortemp_[(80+1)];

struct arrptr{int l,h; ssize_t i; char *a;};struct dynptr{void* p; int t;};extern char INTERRUPT;
typedef 
 void (*traphandler_t)(const char *);struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;};extern struct jbtrap mainjb;
extern struct jbtrap * curjb;

#define traphandler curjb->tr
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};extern struct freefunc * freestack;

#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}

#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name

#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
extern void traprestore(void *ptr);
extern void condfree(void *ptr);
extern int friexecerror(char** s);
extern int (*friexec)(char** s);

#define mmovefrom(var,buf,type) *(type *)(buf)=*var

#define mmoveto(var,buf,type) *var=*(type *)(buf)

#define mainstart void default_traphandler(const char *errormessage){   if(errormessage[0]){     printERR;     freemem(NULL);     exit(EXIT_FAILURE);   }else{     freemem(NULL);     exit(EXIT_SUCCESS);   } } int main(int argc, char **argv){ struct freefunc* es; 			{struct sigaction act,oldact; act.sa_sigaction=trapsignal; sigemptyset(&act.sa_mask); act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; sigaction(SIGSEGV,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); sigaction(SIGFPE,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); sigaction(SIGILL,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); sigaction(SIGINT,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); /* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ else {traphandler=default_traphandler;       freestack=NULL;       feenableexcept(fpe);      }; } es=freestack;
extern int dynptrerr(int type);
extern void *errmalloc(void);
extern void ioerr(FILE *fil);
extern void errfclose(void *voidf);
extern void errfopen(FILE **f, const char *name, int mode);
extern int scanrec(FILE *f, const char *format, void *var) ;
extern int scanbool(FILE *f, const char *format, int *var) ;
extern int myfgets(char *name, char *var, char *varend, FILE *f) ;
extern int mygetline(char *name, char **var, FILE *f) ;
extern void trapsignal(int signum, siginfo_t *info, void *ucontext);





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/*      ___           ___           ___           ___           ___           ___           ___       ___           ___ */
/*     /\  \         /\__\         /\  \         /\__\         /\__\         /\  \         /\__\     /\  \         /\  \ */
/*    /::\  \       /:/  /        /::\  \       /::|  |       /::|  |       /::\  \       /:/  /    /::\  \       /::\  \ */
/*   /:/\:\  \     /:/__/        /:/\:\  \     /:|:|  |      /:|:|  |      /:/\:\  \     /:/  /    /:/\:\  \     /:/\:\  \ */
/*  /:/  \:\  \   /::\  \ ___   /::\~\:\  \   /:/|:|  |__   /:/|:|  |__   /::\~\:\  \   /:/  /    /::\~\:\  \   /:/  \:\__\ */
/* /:/__/ \:\__\ /:/\:\  /\__\ /:/\:\ \:\__\ /:/ |:| /\__\ /:/ |:| /\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ /:/__/ \:|__| */
/* \:\  \  \/__/ \/__\:\/:/  / \/__\:\/:/  / \/__|:|/:/  / \/__|:|/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\ \/__/ \:\  \ /:/  / */
/*  \:\  \            \::/  /       \::/  /      |:/:/  /      |:/:/  /   \:\ \:\__\    \:\  \        \:\__\    \:\  /:/  / */
/*   \:\  \           /:/  /        /:/  /       |::/  /       |::/  /     \:\ \/__/     \:\  \        \/__/     \:\/:/  / */
/*    \:\__\         /:/  /        /:/  /        /:/  /        /:/  /       \:\__\        \:\__\                  \::/__/ */
/*     \/__/         \/__/         \/__/         \/__/         \/__/         \/__/         \/__/                   ~~ */
/* */

/* Some MPI variables */
/* ----------------------- */
extern int iprocx_;
extern int nprocx_;
extern int iprocy_;
extern int nprocy_;

extern int iproc_;
extern int nproc_;

extern int has_terminal_;


/* Libraries to load */
/* ----------------------- */
/* Complex-number type definition and corresponding extensions to the ! */
/* CPL language                                                       ! */
/*                                                                    ! */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net               ! */
/* Released under the attached LICENSE.                               ! */
/*                                                                    ! */
/* Code maturity: green.                                              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

/* Library of common matrix algebra operations for real-valued matrices ! */
/* including suitable extensions to the CPL language to handle infix    ! */
/* notation for such functions. See matrix.info for usage.              ! */
/*                                                                      ! */
/* Copyright 1997-2021 Paolo Luchini http://CPLcode.net                 ! */
/* Released under the attached LICENSE.                                 ! */
/*                                                                      ! */
/* Code maturity: green.                                                ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);


struct DNS_{char size_[(ssize_t)sizeof(int)*(/*SA1*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char localSize_[(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)*(/*SA3*/(((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))/((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))-1)+1)];char leng_[(ssize_t)sizeof(double)*(/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];double nu_;struct arrptr zd_;};
#define DNS_s (ssize_t)sizeof(struct DNS_)
              
               
void dns_5readRestartFile(struct DNS_ *this_,char *fname_);     
         
extern void dns_5readRestartFile(struct DNS_ *this_,char *fname_);

struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;char phiC_[(ssize_t)sizeof(double)*(6+1)];char gC_[(ssize_t)sizeof(double)*(6+1)];};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

	
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);
double riblets_2yWave(struct RIBLETS_ this_,double x_);
void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);
int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);
int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

extern double riblets_riblets_11tol;
extern void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);

	
	extern void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);

	
        extern double riblets_2yWave(struct RIBLETS_ this_,double x_);


	extern int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);


	extern void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);

	
	extern int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
			



struct grid_s4 {int l,h; ssize_t i;struct arrptr a;};

struct grid_s6 {int l,h; ssize_t i;struct arrptr a;};
struct GRID_{char xd_[(ssize_t)sizeof(struct arrptr)*(/*SA1*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char x_[(ssize_t)sizeof(struct arrptr)*(/*SA2*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char di_[(ssize_t)sizeof(int)*(/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char delta_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];char d2_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];};
#define GRID_s (ssize_t)sizeof(struct GRID_)

   
void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
                                   
extern void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
 









int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);
void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__);
void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

extern int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

	



struct imbc_s1 {int l,h; ssize_t i;struct arrptr a;};
struct IMB_{struct imbc_s1 imbc_;};
#define IMB_s (ssize_t)sizeof(struct IMB_)

		
void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);
double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);
void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ pgrid_,struct RIBLETS_ riblet_,int useCoco_);

extern const int imbc_imbc_ca5[7];
extern const int imbc_imbc_ca6[3];
extern char imbc_imbc_7[(ssize_t)sizeof(int)*3*(8-1)];
extern void imbc_2allocate(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct RIBLETS_ riblet_,int useCoco_);

	
	extern double imbc_3calcCoeff(struct GRID_ grid_,struct GRID_ prGrid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int ix_,int iy_,int iz_,int useCoco_);

	
	extern void imbc_4populateCoeff(struct IMB_ *this_,struct DNS_ dns_,struct GRID_ grid_,struct GRID_ prGrid_,struct RIBLETS_ riblet_,int useCoco_);

			

#include <mpi.h>
/*** typedef int (MPI_Datarep_extent_function)(MPI_Datatype, MPI_Aint *, void *) ununderstood ***/
/*** typedef int (MPI_Datarep_conversion_function)(void *, MPI_Datatype,
 int, void *, MPI_Offset, void *) ununderstood ***/
/*** typedef void (MPI_Comm_errhandler_function)(MPI_Comm *, int *, ...) ununderstood ***/
/*** typedef void (ompi_file_errhandler_fn)(MPI_File *, int *, ...) ununderstood ***/
/*** typedef void (MPI_Win_errhandler_function)(MPI_Win *, int *, ...) ununderstood ***/
/*** typedef void (MPI_User_function)(void *, void *, int *, MPI_Datatype *) ununderstood ***/
/*** typedef int (MPI_Comm_copy_attr_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Comm_delete_attr_function)(MPI_Comm, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Type_copy_attr_function)(MPI_Datatype, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Type_delete_attr_function)(MPI_Datatype, int,
 void *, void *) ununderstood ***/
/*** typedef int (MPI_Win_copy_attr_function)(MPI_Win, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Win_delete_attr_function)(MPI_Win, int, void *, void *) ununderstood ***/
/*** typedef int (MPI_Grequest_query_function)(void *, MPI_Status *) ununderstood ***/
/*** typedef int (MPI_Grequest_free_function)(void *) ununderstood ***/
/*** typedef int (MPI_Grequest_cancel_function)(void *, int) ununderstood ***/
/*** typedef MPI_Comm_errhandler_function MPI_Comm_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_fn
  ununderstood ***/
/*** typedef ompi_file_errhandler_fn MPI_File_errhandler_function ununderstood ***/
/*** typedef MPI_Win_errhandler_function MPI_Win_errhandler_fn
  ununderstood ***/
/*** typedef int (MPI_Copy_function)(MPI_Comm, int, void *,
 void *, void *, int *) ununderstood ***/
/*** typedef int (MPI_Delete_function)(MPI_Comm, int, void *, void *) ununderstood ***/


#include <fenv.h>



/* In this module, encapsulation through data STRUCTURE is not performed, */
/* since it is not compatible with the current readily-available MPI routines. */
/* To change this, write own C SECTION for MPI calls.  */



void parallel_4mpiInit(struct DNS_ *dns_);


	
	
	
/*#endif */

	extern void parallel_4mpiInit(struct DNS_ *dns_);



extern void f5(void);
extern void f6(void);
extern void f7(void);
extern void f8(void);
extern void f9(void);
extern void f10(void);
extern FILE *testfile1_;

extern FILE *testfile2_;

extern FILE *testfile3_;

extern FILE *testfile4_;

extern FILE *testfile5_;





/*#define randomDisturbance */

/* Inputs */
/* ------------------------ */
extern int continue_;

extern int non_laminar_;

extern double nu_;
extern double headx_;
extern double heady_;

extern int walls_;
extern int waven_;

extern double spacing_;
extern double ratiohs_;

extern int use_coco_;
extern int extra_;

extern double radius_;
extern double stokexp_;

extern char phiC_[(ssize_t)sizeof(double)*(6+1)];
extern char gC_[(ssize_t)sizeof(double)*(6+1)];

extern int nx_;
extern int ny_;
extern int nz_;
extern int it_;

extern double Lx_;
extern double Ly_;

extern double cflmax_;
extern double fouriermax_;
extern double deltat_;
extern double maxTime_;
extern double time_;
extern double Uconv_;

extern int it_save_;
extern int it_stat_;
extern int it_max_;
extern int it_cfl_;
extern int it_check_;

extern char cfl_[(ssize_t)sizeof(double)*(1+1)];

extern int iN0_;



extern void riblets_test_1readribletsin(void);

extern char *riblets_test_2myname;
struct _riblets_test_s3{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _riblets_test_s3_s (ssize_t)sizeof(struct _riblets_test_s3)
extern FILE *dnsinfo_;
extern FILE *riblets_test_4;

        


	double wallclock_1wallclock(void);

extern struct timeval wallclock_wallclock_startim;

  extern double wallclock_1wallclock(void);

extern double clock_;



extern double riblets_test_15deltax;
extern double riblets_test_16deltay;






#include <mpi.h>


/*INTEGER iprocx, nprocx=atoi(COMMANDLINE(1)), iprocy, nprocy=atoi(COMMANDLINE(2)) */
extern int prevx_;
extern int nextx_;
extern int prevy_;
extern int nexty_;

/*INTEGER iproc, nproc,  */
extern int nreqs_;
extern int nsreqs_;

extern char unpckbuf_[(ssize_t)sizeof(struct grid_s6)*(3+1)];

extern int szbuf_;

extern MPI_Status status[4];
extern MPI_Request request[4];
extern MPI_Request srequest[4];
extern MPI_Comm cart_comm,cart_comm_x;
extern int nproc,iproc,nprocx,nprocy,iprocx,iprocy,idim[2];
extern int prevx,nextx,prevy,nexty;


extern void riblets_test_18init_mpi(void);
 

extern int riblets_test_19nxl(int n_);

extern int riblets_test_20nxh(int n_);

extern int riblets_test_21nyl(int n_);

extern int riblets_test_22nyh(int n_);

extern int riblets_test_23M;
extern int riblets_test_24h;
extern ssize_t riblets_test_25i;
extern ssize_t riblets_test_26i;
extern struct freefunc recvbuf_f;
extern char *recvbuf_;

extern int riblets_test_27h;
extern ssize_t riblets_test_28i;
extern ssize_t riblets_test_29i;
extern struct freefunc sendbuf_f;
extern char *sendbuf_;



extern void riblets_test_30MPI_Send(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_);



extern void riblets_test_31MPI_Recv(const int x_l,const int x_h,const ssize_t x_i,const int x__l,const int x__h,const ssize_t x__i,char *x___,int iP_,int tag_,int ireq_);


extern void riblets_test_32mpi_barrier(void);





void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);
void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);
void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);
void riblets_test_36pbcwait(void);
extern int riblets_test_37h;
extern ssize_t riblets_test_38i;
extern struct freefunc zd_f;
extern char *zd_;
extern int riblets_test_39h;
extern ssize_t riblets_test_40i;
extern struct freefunc yd_f;
extern char *yd_;

extern ssize_t riblets_test_41i;
extern char *riblets_test_42zz;
extern ssize_t riblets_test_43i;
extern char *riblets_test_44yy;
extern int riblets_test_45l;
extern int riblets_test_46h;
extern int riblets_test_47l;
extern int riblets_test_48h;
extern int riblets_test_49h;
extern ssize_t riblets_test_50i;
extern ssize_t riblets_test_51i;
extern ssize_t riblets_test_52i;
extern ssize_t riblets_test_53st;
extern ssize_t riblets_test_54st;
extern struct freefunc var_f;
extern char *var_;
extern struct freefunc old_f;
extern char *old_;

extern char flowrate_[(ssize_t)sizeof(double)*(/*SA55*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];


/* CoCo switch */
extern int useCoco_;

/* note: this module must be USEd after all other SHARED declarations */
/* INCLUDE parallelbcs.h at the beginning */
/*INCLUDE parallelbcs.h */
struct _parallelbcs_parupdate_s1{int direction_;struct grid_s6 xl_;struct grid_s6 xl1_;struct grid_s6 xh_;struct grid_s6 xh1_;struct grid_s6 yl_;struct grid_s6 yl1_;struct grid_s6 yh_;struct grid_s6 yh1_;};
#define _parallelbcs_parupdate_s1_s (ssize_t)sizeof(struct _parallelbcs_parupdate_s1)
extern struct _parallelbcs_parupdate_s1 parallelbcs_parupdate_comm;


extern void parallelbcs_parupdate_2reader(void);


extern void parallelbcs_parupdate_3writer(void);


extern void parallelbcs_parupdate_4com(void);


extern void riblets_test_36pbcwait(void);
 

extern void riblets_test_33pbc(const int f_l,const int f_h,const ssize_t f_i,const int f__l,const int f__h,const ssize_t f__i,const int f___l,const int f___h,const ssize_t f___i,char *f____);


extern void riblets_test_34pbc(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int dir_);


extern void riblets_test_35pbcp(const int u_l,const int u_h,const ssize_t u_i,const int u__l,const int u__h,const ssize_t u__i,const int u___l,const int u___h,const ssize_t u___i,char *u____,const int v_l,const int v_h,const ssize_t v_i,const int v__l,const int v__h,const ssize_t v__i,const int v___l,const int v___h,const ssize_t v___i,char *v____,int parity_);

extern void parallelbcs_5getmax(const int val_l,const int val_h,const ssize_t val_i,char *val__);



extern void parallelbcs_6accumulate(const int val_l,const int val_h,const ssize_t val_i,char *val__);


extern void parallelbcs_7accumulateX(const int valin_l,const int valin_h,const ssize_t valin_i,char *valin__,const int valout_l,const int valout_h,const ssize_t valout_i,char *valout__);

/* ------ INPUT/OUTPUT MODULE ------ */

/* Insert new files to be created here */
/* //------ */
/**/
FILE *runtimedata_;


int iofiles_1h;
int iofiles_2h;
int iofiles_3h;
int iofiles_4h;
off_t iofiles_5i;
off_t iofiles_6i;
off_t iofiles_8i;
off_t iofiles_9i;
off_t iofiles_10i;
off_t iofiles_11i;
off_t iofiles_12st;
struct _iofiles_s7{int nxs_;int nys_;int nzs_;int its_;double Lxs_;double Lys_;double headxs_;double headys_;double nus_;double times_;};
#define _iofiles_s7_s (ssize_t)sizeof(struct _iofiles_s7)
FILE *stored_;
FILE *iofiles_13;

void iofiles_21savefield(char *name_){{struct freefunc* es=freestack;
	 for(int iP_=1 ;iP_<= nproc_;iP_+=1){
    		if( iP_==iproc_ ){
      			int _22h;
ssize_t _24i;
struct freefunc _23f;char *_23;
FILE *_25;
_22h=snprintf((char*)NULL,0,"""%s"".partial""",name_)+1;
_24i=(_22h+1);

_23=malloc(_24i);if(_23==NULL)_23=errmalloc();atblockexit(_23f,free,_23);
snprintf(_23,_22h+1,"""%s"".partial""",name_);_25=NULL;errfopen(&_25,_23,O_RDWR|O_CREAT);stored_=_25;
      			if( iproc_==1 ){
        			if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).nxs_,SEEK_SET)==0&&fwrite(&nx_,(ssize_t)sizeof(int),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).nys_,SEEK_SET)==0&&fwrite(&ny_,(ssize_t)sizeof(int),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).nzs_,SEEK_SET)==0&&fwrite(&nz_,(ssize_t)sizeof(int),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).its_,SEEK_SET)==0&&fwrite(&it_,(ssize_t)sizeof(int),1,stored_)==1))ioerr(stored_);
         			if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).headxs_,SEEK_SET)==0&&fwrite(&headx_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).headys_,SEEK_SET)==0&&fwrite(&heady_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).times_,SEEK_SET)==0&&fwrite(&time_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);
         			if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).nus_,SEEK_SET)==0&&fwrite(&nu_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).Lxs_,SEEK_SET)==0&&fwrite(&Lx_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);  if(!(fseeko(stored_,(ssize_t)&(*(struct _iofiles_s7*)(0)).Lys_,SEEK_SET)==0&&fwrite(&Ly_,(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);
         			if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(stored_,(off_t)(_iofiles_s7_s+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fwrite(zd_,(ssize_t)sizeof(double),iofiles_1h+1,stored_)==iofiles_1h+1))ioerr(stored_);}else{  {int _26i_=iofiles_1h;do{{if(!(fseeko(stored_,_26i_*(off_t)(ssize_t)sizeof(double)+(off_t)(_iofiles_s7_s+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fwrite((double*)(_26i_*(ssize_t)sizeof(double)+zd_),(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);}_26i_--;}while(_26i_>=0);}};
      			};
      			 for(int ix_=riblets_test_45l+1 ;ix_<= riblets_test_46h-1;ix_+=1){
        			int _27l;
int _28h;
int _29h;
ssize_t _30i;
ssize_t _31i;
ssize_t _32st;
struct freefunc iobuf_f;char *iobuf_ ;

        			_27l=riblets_test_47l+1;
_28h=riblets_test_48h-1;
_29h=nz_;
_30i=(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(_29h+1);
_31i=_30i*(_28h-_27l+1);
_32st=_27l*_30i;

iobuf_=malloc(_31i);if(iobuf_==NULL)iobuf_=errmalloc();iobuf_-=_32st;atblockexit(iobuf_f,free,iobuf_+_32st);
{char *_34_;

_34_=_27l*riblets_test_50i+ix_*riblets_test_51i+var_; {char *_33_=_27l*_30i+iobuf_;int _33_1=_28h-_27l; do{{ memmove(_33_,_34_,_30i); _34_ =riblets_test_50i+_34_;}_33_+=_30i;_33_1--;}while(_33_1>=0);}}   {int _35i_=riblets_test_47l+1;do{{if((ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)&&(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)){if(!(fseeko(stored_,_35i_*(off_t)iofiles_5i+ix_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fwrite(_35i_*_30i+iobuf_,(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1),iofiles_4h+1,stored_)==iofiles_4h+1))ioerr(stored_);}else{  {int _36i_=iofiles_4h;do{{if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(stored_,_36i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_35i_*(off_t)iofiles_5i+ix_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fwrite(_36i_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_35i_*_30i+iobuf_,(ssize_t)sizeof(double),/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1,stored_)==/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1))ioerr(stored_);}else{  {int _37i_=/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{if(!(fseeko(stored_,_37i_*(off_t)(ssize_t)sizeof(double)+_36i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_35i_*(off_t)iofiles_5i+ix_*(off_t)iofiles_6i+(off_t)((_iofiles_s7_s+iofiles_8i)-iofiles_12st+(ssize_t)(char*)(struct _iofiles_s7*)(0)),SEEK_SET)==0&&fwrite((double*)(_37i_*(ssize_t)sizeof(double)+_36i_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_35i_*_30i+iobuf_),(ssize_t)sizeof(double),1,stored_)==1))ioerr(stored_);}_37i_--;}while(_37i_>=0);}};}_36i_--;}while(_36i_>=0);}};}_35i_++;}while(_35i_<=riblets_test_48h-1);}
      			free(iobuf_f.ptr);freestack=&_23f;}
      			errfclose(&stored_);
    		free(_23f.ptr);freestack=es;};
    		riblets_test_32mpi_barrier();
  	}
  	if( has_terminal_ ){ int _38h;
ssize_t _40i;
struct freefunc _39f;char *_39;
_38h=snprintf((char*)NULL,0,"""%s"".partial""",name_)+1;
_40i=(_38h+1);

_39=malloc(_40i);if(_39==NULL)_39=errmalloc();atblockexit(_39f,free,_39);
snprintf(_39,_38h+1,"""%s"".partial""",name_);rename(_39 ,name_);free(_39f.ptr);freestack=es;};
}}

int cnt_stats_;

/* Define the statistics file. Check the order and which quantities to store */
/* ------ */
struct freefunc statsfilename_f;char *statsfilename_ ;


int iofiles_24h;
ssize_t iofiles_25i;
struct freefunc stats_f;char *stats_;

int iofiles_26h;
ssize_t iofiles_27i;
ssize_t iofiles_28i;
ssize_t iofiles_29st;
struct freefunc statsX_f;char *statsX_;
struct freefunc statslocX_f;char *statslocX_;
struct freefunc statstmpX_f;char *statstmpX_;

int iofiles_30h;
int iofiles_31h;
int iofiles_32h;
int iofiles_33h;
off_t iofiles_34i;
off_t iofiles_36i;
off_t iofiles_37i;
off_t iofiles_38i;
off_t iofiles_39i;
struct _iofiles_s35{};
#define _iofiles_s35_s (ssize_t)sizeof(struct _iofiles_s35)
FILE *diskstats_;
FILE *iofiles_40;

int statsExist_;

void iofiles_51save_statsX(void){{struct freefunc* es=freestack;
	 for(int iP_=1 ;iP_<= nproc_;iP_+=1){
      		if( iP_==iproc_ ){
        		if( iprocx_==nprocx_ ){ {
            			FILE *_52;
int miny_ ;

            			int maxy_ ;

            			double _53m;
ssize_t _57i;
ssize_t _58st;
_52=NULL;errfopen(&_52,statsfilename_,O_RDWR|O_CREAT);diskstats_=_52;
            			 miny_= riblets_test_47l+(iprocy_!=1); maxy_= riblets_test_48h-(iprocy_!=nprocy_);_53m=1./(double)(cnt_stats_);
{  {int _54i_=riblets_test_47l;do{{int _55i_=iofiles_26h;do{{int _56i_=/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{(*(double *)(_56i_*(ssize_t)sizeof(double)+_55i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_54i_*iofiles_27i+statsX_))*=_53m ;}_56i_--;}while(_56i_>=0);}_55i_--;}while(_55i_>=0);}_54i_++;}while(_54i_<=riblets_test_48h-(0));}}
            			_57i=(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)*(nz_+1);
_58st=miny_*_57i;
if(iofiles_34i==_57i&&iofiles_27i==_57i){if(!(fseeko(diskstats_,(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0))+_58st,SEEK_SET)==0&&fwrite(statsX_+_58st,_57i,maxy_-miny_+1,diskstats_)==maxy_-miny_+1))ioerr(diskstats_);}else{  {int _59i_=miny_;do{{if((ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)&&(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)==(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)){if(!(fseeko(diskstats_,_59i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite(_59i_*iofiles_27i+statsX_,(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1),nz_+1,diskstats_)==nz_+1))ioerr(diskstats_);}else{  {int _60i_=nz_;do{{if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&(ssize_t)sizeof(double)==(ssize_t)sizeof(double)){if(!(fseeko(diskstats_,_60i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_59i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite(_60i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_59i_*iofiles_27i+statsX_,(ssize_t)sizeof(double),/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1,diskstats_)==/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1))ioerr(diskstats_);}else{  {int _61i_=/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{if(!(fseeko(diskstats_,_61i_*(off_t)(ssize_t)sizeof(double)+_60i_*(off_t)(ssize_t)sizeof(double)*(off_t)(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_59i_*(off_t)iofiles_34i+(off_t)((iofiles_36i+iofiles_37i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite((double*)(_61i_*(ssize_t)sizeof(double)+_60i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_59i_*iofiles_27i+statsX_),(ssize_t)sizeof(double),1,diskstats_)==1))ioerr(diskstats_);}_61i_--;}while(_61i_>=0);}};}_60i_--;}while(_60i_>=0);}};}_59i_++;}while(_59i_<=maxy_);}};
            			if( iprocy_==1 ){ if(!(fseeko(diskstats_,(off_t)(((iofiles_36i+iofiles_37i)+iofiles_39i)+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite(&cnt_stats_,(ssize_t)sizeof(int),1,diskstats_)==1))ioerr(diskstats_); if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&riblets_test_43i==(ssize_t)sizeof(double)){if(!(fseeko(diskstats_,(off_t)(_iofiles_s35_s+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite(riblets_test_44yy,(ssize_t)sizeof(double),iofiles_30h+1,diskstats_)==iofiles_30h+1))ioerr(diskstats_);}else{  {int _62i_=iofiles_30h;do{{if(!(fseeko(diskstats_,_62i_*(off_t)(ssize_t)sizeof(double)+(off_t)(_iofiles_s35_s+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite((double*)(_62i_*riblets_test_43i+riblets_test_44yy),(ssize_t)sizeof(double),1,diskstats_)==1))ioerr(diskstats_);}_62i_--;}while(_62i_>=0);}}; if((ssize_t)sizeof(double)==(ssize_t)sizeof(double)&&riblets_test_41i==(ssize_t)sizeof(double)){if(!(fseeko(diskstats_,(off_t)(iofiles_36i+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite(riblets_test_42zz,(ssize_t)sizeof(double),iofiles_31h+1,diskstats_)==iofiles_31h+1))ioerr(diskstats_);}else{  {int _63i_=iofiles_31h;do{{if(!(fseeko(diskstats_,_63i_*(off_t)(ssize_t)sizeof(double)+(off_t)(iofiles_36i+(ssize_t)(char*)(void**)(0)),SEEK_SET)==0&&fwrite((double*)(_63i_*riblets_test_41i+riblets_test_42zz),(ssize_t)sizeof(double),1,diskstats_)==1))ioerr(diskstats_);}_63i_--;}while(_63i_>=0);}};};
            			errfclose(&diskstats_);
        		}};
      		};
      		riblets_test_32mpi_barrier();
    	}
}}

/* Write x-averaged statistics to file (running average) */
/* ------ */
void iofiles_52output(void){{struct freefunc* es=freestack;
	int mtemp53;
if( (mtemp53=it_ % it_stat_ ,mtemp53>=0?mtemp53:mtemp53+it_stat_ )== 0 ){
    		int _54m;
int _55;
int _56;
cnt_stats_+=1;
    		riblets_test_33pbc(riblets_test_45l,riblets_test_46h,riblets_test_51i,riblets_test_47l,riblets_test_48h,riblets_test_50i,0,(riblets_test_49h+1)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)-1,(ssize_t)sizeof(double),((ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)!=(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)?(traphandler("array must be contiguous"),NULL):var_));
    		memset(iofiles_29st+statslocX_,0,iofiles_28i);  memset(iofiles_29st+statstmpX_,0,iofiles_28i);
    		riblets_test_36pbcwait();
    		 {int ix_=riblets_test_45l+1 ;do{ _54m=ny_+1;
_55=riblets_test_48h;if(_55>_54m)_55=_54m; _56=riblets_test_48h;if(_56>_54m)_56=_54m; {int iy_=riblets_test_47l+1 ;do{ {int iz_=1 ;do{{ {
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+statslocX_))+=(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+var_))/(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+statslocX_))+=((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+(iy_-1)*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))/2./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)((iz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))/2./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=(((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+(ix_-1)*riblets_test_51i+(ssize_t)sizeof(double)+var_)))*((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+(ix_-1)*riblets_test_51i+(ssize_t)sizeof(double)+var_))))/4./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=(((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+(iy_-1)*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))*((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+(iy_-1)*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))))/4./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=(((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)((iz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))*((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)((iz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))))/4./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+(ix_-1)*riblets_test_51i+(ssize_t)sizeof(double)+var_)))*((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)((iz_-1)*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))/4./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
      			(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*iofiles_27i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+statslocX_))+=((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+(ix_-1)*riblets_test_51i+(ssize_t)sizeof(double)+var_)))*((*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+iy_*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_))+(*(double *)(iz_*(ssize_t)sizeof(double)*(/*SA17*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+(iy_-1)*riblets_test_50i+ix_*riblets_test_51i+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+var_)))/4./(double)((riblets_test_46h-riblets_test_45l-1))/(double)(nprocx_);
    		}}iz_+=1;}while(iz_<= nz_);}iy_+=1;}while(iy_<= _56);}ix_+=1;}while(ix_<= riblets_test_46h-1 );}
    			parallelbcs_7accumulateX(0,((riblets_test_48h-riblets_test_47l+1)*(iofiles_26h+1)-1+1)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)-1,(ssize_t)sizeof(double),(iofiles_27i!=iofiles_27i?(traphandler("array must be contiguous"),NULL):riblets_test_47l*iofiles_27i+statslocX_),0,((riblets_test_48h-riblets_test_47l+1)*(iofiles_26h+1)-1+1)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)-1,(ssize_t)sizeof(double),(iofiles_27i!=iofiles_27i?(traphandler("array must be contiguous"),NULL):riblets_test_47l*iofiles_27i+statstmpX_));  {  {int _57i_=riblets_test_47l;do{{int _58i_=iofiles_26h;do{{int _59i_=/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1);do{{(*(double *)(_59i_*(ssize_t)sizeof(double)+_58i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_57i_*iofiles_27i+statsX_))+=(*(double *)(_59i_*(ssize_t)sizeof(double)+_58i_*(ssize_t)sizeof(double)*(/*SA22*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)+_57i_*iofiles_27i+statstmpX_)) ;}_59i_--;}while(_59i_>=0);}_58i_--;}while(_58i_>=0);}_57i_++;}while(_57i_<=riblets_test_48h-(0));}}
  	};
  	if( has_terminal_ ){
    		 fprintf( runtimedata_ ,"""# ""%d \t%1.15g \t%1.15g \t%1.15g \t%1.12g \t%1.12g \t%1.12g \t%1.10g \t%1.10g \t%1.15g""\n",it_ ,time_ ,headx_ ,heady_ ,(*(double *)(0+flowrate_))/(double)(nprocx_)/(double)(nprocy_) ,(*(double *)(0+(ssize_t)sizeof(double)+flowrate_))/(double)(nprocx_)/(double)(nprocy_) ,((*(double *)(0+flowrate_))-(*(double *)(0+(ssize_t)sizeof(double)+flowrate_)))/(double)(nprocx_)/(double)(nprocy_),((*(double *)(cfl_))*deltat_) ,((*(double *)((ssize_t)sizeof(double)+cfl_))*deltat_) ,deltat_);
    		fflush(stdout);
    		fflush(runtimedata_);
  	};
}}
