/*-march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -isystem /home/ws/sb0817/anaconda3/envs/fenicsproject/include -I/home/ws/sb0817/anaconda3/envs/fenicsproject/include -pthread -I/home/ws/sb0817/anaconda3/envs/fenicsproject/include -L/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -L/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -Wl,-rpath,/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -lmpi */

#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64

#define _LARGE_FILES

#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

extern char errortemp_[(80+1)];

struct arrptr{int l,h; ssize_t i; char *a;};struct dynptr{void* p; int t;};extern char INTERRUPT;
typedef 
 void (*traphandler_t)(const char *);struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;};extern struct jbtrap mainjb;
extern struct jbtrap * curjb;

#define traphandler curjb->tr
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};extern struct freefunc * freestack;

#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}

#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name

#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
extern void traprestore(void *ptr);
extern void condfree(void *ptr);
extern int friexecerror(char** s);
extern int (*friexec)(char** s);

#define mmovefrom(var,buf,type) *(type *)(buf)=*var

#define mmoveto(var,buf,type) *var=*(type *)(buf)

#define mainstart void default_traphandler(const char *errormessage){   if(errormessage[0]){     printERR;     freemem(NULL);     exit(EXIT_FAILURE);   }else{     freemem(NULL);     exit(EXIT_SUCCESS);   } } int main(int argc, char **argv){ struct freefunc* es; 			{struct sigaction act,oldact; act.sa_sigaction=trapsignal; sigemptyset(&act.sa_mask); act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; sigaction(SIGSEGV,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); sigaction(SIGFPE,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); sigaction(SIGILL,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); sigaction(SIGINT,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); /* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ else {traphandler=default_traphandler;       freestack=NULL;       feenableexcept(fpe);      }; } es=freestack;
extern int dynptrerr(int type);
extern void *errmalloc(void);
extern void ioerr(FILE *fil);
extern void errfclose(void *voidf);
extern void errfopen(FILE **f, const char *name, int mode);
extern int scanrec(FILE *f, const char *format, void *var) ;
extern int scanbool(FILE *f, const char *format, int *var) ;
extern int myfgets(char *name, char *var, char *varend, FILE *f) ;
extern int mygetline(char *name, char **var, FILE *f) ;
extern void trapsignal(int signum, siginfo_t *info, void *ucontext);





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/* Libraries to load */
/* ----------------------- */
/* Transparently reshapes the operation of the CPL compiler so as to provide ! */
/* array bounds checking and more run-time checks, without any modifications ! */
/* to the original program (at the expense of a slower execution).           ! */
/* To activate, put "USE rtchecks" at the beginning of your program.         ! */
/* See infocpl rtchecks.                                                     ! */
/*                                                                           !  */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net                      ! */
/* Released under the attached LICENSE.                                      ! */
/*                                                                           ! */
/* Code maturity: mostly green but the TRACE command is orange.              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <sys/ioctl.h>                                                          
/*
<*#ifdef __GNUC__
  const
#endif
int cb(int lb, int ub, int index);
#ifdef __GNUC__
  const
#endif
char* cp(int inputpos);
#ifdef __GNUC__
  const
#endif
char *cr(char *lb, char *ub, char *index);
*>
*/
/* nota: #ifdef non passa in C SECTION */
#undef printERR
#define printERR fprintf(stderr,"\r%s in line %d of %s: PROGRAM HALTED  \n",errormessage,ln,fn);fflush(stderr)
extern volatile int ln;
extern char * volatile fn;
extern const int cb(int lb, int ub, int index);
extern const char * cp(void);
extern const int ca(void);
extern const unsigned char sigNAN[8];


/* CPL interface to the termios functions needed to turn on and off ! */
/* character-by-character terminal input                            ! */
/*                                                                  ! */
/* Copyright 2002-2023 Paolo Luchini http://CPLcode.net             ! */
/* Released under the attached LICENSE.                             ! */
/*                                                                  ! */
/* Code maturity: green.                                            ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <termios.h>
void CHARbyCHAR_1CHARbyCHAR(int descr_);
void CHARbyCHAR_2LINEbyLINE(void);

extern struct termios CHARbyCHAR_CHARbyCHAR_newsetting;
extern struct termios CHARbyCHAR_CHARbyCHAR_oldsetting;

extern int CHARbyCHAR_CHARbyCHAR_CbCdescr;

extern void CHARbyCHAR_1CHARbyCHAR(int descr_);


extern void CHARbyCHAR_2LINEbyLINE(void);

/* Library providing an interface to the select system call ! */
/* in order to detect if input is waiting to be read.       ! */
/* See infocpl INPUTREADY.                                  ! */
/*                                                          !  */
/* Copyright 2008-2020 Paolo Luchini http://CPLcode.net     ! */
/* Released under the attached LICENSE.                     ! */
/*                                                          ! */
/* Code maturity: green.                                    ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern int fd_input_ready(int fd, int sec, int usec);








extern int BlockLevel;

void singlestepper(int lineN_,char *line_);






extern FILE *rtchecks_SingleStep_grabin;

extern int rtchecks_SingleStep_StopLevel;
extern char rtchecks_SingleStep_LoopCount[(ssize_t)sizeof(int)*(100+1)];
extern int rtchecks_SingleStep_LastLine;

extern int rtchecks_SingleStep_paused;

extern int rtchecks_SingleStep_lastfnlength;
extern int rtchecks_SingleStep_lastrow;

extern char *rtchecks_SingleStep_lastfn;

extern int rtchecks_SingleStep_termwidth;
extern int rtchecks_SingleStep_termheight;

extern char *rtchecks_SingleStep_hotkeys;


extern void rtchecks_SingleStep_1RestoreScroll(void);


extern void TRON(void);


extern void singlestepper(int lineN_,char *line_);

/* Complex-number type definition and corresponding extensions to the ! */
/* CPL language                                                       ! */
/*                                                                    ! */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net               ! */
/* Released under the attached LICENSE.                               ! */
/*                                                                    ! */
/* Code maturity: green.                                              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

/* Library of common matrix algebra operations for real-valued matrices ! */
/* including suitable extensions to the CPL language to handle infix    ! */
/* notation for such functions. See matrix.info for usage.              ! */
/*                                                                      ! */
/* Copyright 1997-2021 Paolo Luchini http://CPLcode.net                 ! */
/* Released under the attached LICENSE.                                 ! */
/*                                                                      ! */
/* Code maturity: green.                                                ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct DNS_{char size_[(ssize_t)sizeof(int)*(/*SA1*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char localSize_[(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)*(/*SA3*/(((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))/((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))-1)+1)];char leng_[(ssize_t)sizeof(double)*(/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];double nu_;struct arrptr zd_;};
#define DNS_s (ssize_t)sizeof(struct DNS_)
              
               
void dns_5readRestartFile(struct DNS_ *this_,char *fname_);     
         
extern void dns_5readRestartFile(struct DNS_ *this_,char *fname_);

struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;char phiC_[(ssize_t)sizeof(double)*(6+1)];char gC_[(ssize_t)sizeof(double)*(6+1)];};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

	
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);
double riblets_2yWave(struct RIBLETS_ this_,double x_);
void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);
int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);
int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

extern double riblets_riblets_11tol;
extern void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);

	
	extern void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);

	
        extern double riblets_2yWave(struct RIBLETS_ this_,double x_);


	extern int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
	extern void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);


	extern void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);

	
	extern int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

	
			



struct grid_s4 {int l,h; ssize_t i;struct arrptr a;};

struct grid_s6 {int l,h; ssize_t i;struct arrptr a;};
struct GRID_{char xd_[(ssize_t)sizeof(struct arrptr)*(/*SA1*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char x_[(ssize_t)sizeof(struct arrptr)*(/*SA2*/(((ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr)+(ssize_t)sizeof(struct arrptr))/((ssize_t)sizeof(struct arrptr))-1)+1)];char di_[(ssize_t)sizeof(int)*(/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char delta_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];char d2_[(ssize_t)sizeof(struct grid_s6)*(/*SA5*/(((ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4)+(ssize_t)sizeof(struct grid_s4))/((ssize_t)sizeof(struct grid_s4))-1)+1)];};
#define GRID_s (ssize_t)sizeof(struct GRID_)

   
void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
                                   
extern void grid_7initialiseGrid(struct GRID_ *this_,struct DNS_ dns_,const int di_l,const int di_h,const ssize_t di_i,char *di__);
 









/**/
int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_);
void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__);
void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_);

int coco_1applyCoco(const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,struct GRID_ grid_,struct DNS_ dns_,struct RIBLETS_ riblet_,int useCoco_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/coco.cpl";{struct freefunc* es=freestack;int RESULT_;
                int iN0_	;

                ln=16;  {int ii_=/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1);do{
                   ln=15;if( (*(int *)(cb(0,/*SA3*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1),ii_)*(ssize_t)sizeof(int)+grid_.di_)) != 0 ){   iN0_ = ii_									/* To find the equation we are computing the coefficients for */;};
                ii_--;}while(ii_>=0);}
                ln=33;if( !(useCoco_ )){ 
                   ln=18; RESULT_ = 0;
                }else{
                   ln=32;if( (iN0_==2 )&&( ((fabs((*(double *)(cb(coord_l,coord_h,coord_l+1)*coord_i+coord__))-(*(double *)(cb(eCoord_l,eCoord_h,eCoord_l+1)*eCoord_i+eCoord__)))<1.e-5 )||( (*(double *)(cb(coord_l,coord_h,coord_l+2)*coord_i+coord__))-(*(double *)(cb(eCoord_l,eCoord_h,eCoord_l+2)*eCoord_i+eCoord__))>0.3*dns_.nu_)) )){	/* To avoid having wimbc=inf when points lie above riblet tip */
                      ln=21; RESULT_ = 0;
                   }else{ 
                      char _5[(ssize_t)sizeof(double)*(/*SA2*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
double _7;
coco_3stokesSolution(_5,dns_,riblet_,coord_l,coord_h,coord_i,coord__); _7=0.;  {int _6i_=riblet_.amp_.l;do{{ln=23;(*&_7)=(*(&_7!=NULL?&_7:(double *)cp()))+(*(double *)(cb(riblet_.amp_.l,riblet_.amp_.h,_6i_)*riblet_.amp_.i+riblet_.amp_.a) );}_6i_++;}while(_6i_<=riblet_.amp_.h);}ln=31;if( ((iN0_==1 )&&( (*(double *)(0+_5))<0.))&&( _7>0.)){			/* To account for incorrect b.c. coming from Luchini correction for small riblets */
                         ln=24; RESULT_ = 0;
                      }else{
                         char _8[(ssize_t)sizeof(double)*(/*SA2*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
coco_3stokesSolution(_8,dns_,riblet_,coord_l,coord_h,coord_i,coord__);ln=30;if( (iN0_==0 )&&( (*(double *)(0+_8))<0.)){						/* To account for incorrect b.c. coming from Luchini correction for small riblets */
                            ln=27; RESULT_ = 0;
                         }else{
                            ln=29; RESULT_ = (((((*(double *)(cb(coord_l,coord_h,0+coord_l)*coord_i+coord__))-(*(double *)(cb(eCoord_l,eCoord_h,0)*eCoord_i+eCoord__)))/riblet_.r_)*(((*(double *)(cb(coord_l,coord_h,0+coord_l)*coord_i+coord__))-(*(double *)(cb(eCoord_l,eCoord_h,0)*eCoord_i+eCoord__)))/riblet_.r_))+ ((((*(double *)(cb(coord_l,coord_h,1+coord_l)*coord_i+coord__)) - (*(double *)(cb(eCoord_l,eCoord_h,1)*eCoord_i+eCoord__)))/riblet_.r_)*(((*(double *)(cb(coord_l,coord_h,1+coord_l)*coord_i+coord__)) - (*(double *)(cb(eCoord_l,eCoord_h,1)*eCoord_i+eCoord__)))/riblet_.r_))+ ((((*(double *)(cb(coord_l,coord_h,2+coord_l)*coord_i+coord__)) - (*(double *)(cb(eCoord_l,eCoord_h,2)*eCoord_i+eCoord__)))/riblet_.r_)*(((*(double *)(cb(coord_l,coord_h,2+coord_l)*coord_i+coord__)) - (*(double *)(cb(eCoord_l,eCoord_h,2)*eCoord_i+eCoord__)))/riblet_.r_)))<=1.;
                         };
                      };
                   };
                };
	fn=savefn; ln=saveln;return RESULT_;}}
	
	void coco_3stokesSolution(char *RESULT_,struct DNS_ dns_,struct RIBLETS_ riblet_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/coco.cpl";{struct freefunc* es=freestack;
		/* This function calcultes the Laplace and Stokes solutions @coordinates. (phi, g) are calculates from (phi0, beta) using a Taylor series (usiing coeff phiC, gC) */
		int iN0_;

		double dzz_;

		char xe_[(ssize_t)sizeof(double)*(/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];

		double phi_	;

		double phi_w_	;

		double laplExp_	;

		double g_ 		;

		double D3_		;

		double r_		;

		double rPres_	;

		double theta_	;

		double thetaPres_ ;

  		double _7r;
double _9r_8zvar;
double _10i_8zvar;
struct COMPLEX_ _11;
struct COMPLEX_ _12;
struct COMPLEX_ _13;
ln=39;dzz_=(*(double *)(cb(dns_.zd_.l,dns_.zd_.h,1)*dns_.zd_.i+dns_.zd_.a))-(*(double *)(cb(dns_.zd_.l,dns_.zd_.h,0)*dns_.zd_.i+dns_.zd_.a));ln=40;{cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA7*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)+0);riblets_8findEdge((cb(/*SA7*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),/*SA7*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),(((ssize_t)sizeof(double)==(ssize_t)sizeof(double))?(/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)-(0)-(0)):0)+0),xe_+(0)*(ssize_t)sizeof(double)+0-(0)),riblet_ ,dns_ ,coordinates_l,coordinates_h,coordinates_i,coordinates__);}ln=41; phi_= (*(double *)(cb(0,6,0)*(ssize_t)sizeof(double)+riblet_.phiC_))*pow((*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_)),6)+(*(double *)(cb(0,6,1)*(ssize_t)sizeof(double)+riblet_.phiC_))*pow((*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_)),5)+(*(double *)(cb(0,6,2)*(ssize_t)sizeof(double)+riblet_.phiC_))*pow((*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_)),4)+(*(double *)(cb(0,6,3)*(ssize_t)sizeof(double)+riblet_.phiC_))*pow((*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_)),3)+(*(double *)(cb(0,6,4)*(ssize_t)sizeof(double)+riblet_.phiC_))*((*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_))*(*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_)))+(*(double *)(cb(0,6,5)*(ssize_t)sizeof(double)+riblet_.phiC_))*(*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),3)*(ssize_t)sizeof(double)+xe_))+(*(double *)(cb(0,6,6)*(ssize_t)sizeof(double)+riblet_.phiC_));ln=42; phi_w_= (3.14159265358979323846) - 0.5*phi_;ln=43; laplExp_= (3.14159265358979323846)/(2.*phi_w_);ln=44; g_= (*(double *)(cb(0,6,0)*(ssize_t)sizeof(double)+riblet_.gC_))*pow(phi_,6)+(*(double *)(cb(0,6,1)*(ssize_t)sizeof(double)+riblet_.gC_))*pow(phi_,5)+(*(double *)(cb(0,6,2)*(ssize_t)sizeof(double)+riblet_.gC_))*pow(phi_,4)+(*(double *)(cb(0,6,3)*(ssize_t)sizeof(double)+riblet_.gC_))*pow(phi_,3)+(*(double *)(cb(0,6,4)*(ssize_t)sizeof(double)+riblet_.gC_))*(phi_*phi_)+(*(double *)(cb(0,6,5)*(ssize_t)sizeof(double)+riblet_.gC_))*phi_+(*(double *)(cb(0,6,6)*(ssize_t)sizeof(double)+riblet_.gC_));ln=45; D3_=  - cos((g_+1.)*phi_w_)/cos((g_-1.)*phi_w_);ln=46; r_= sqrt((((*(double *)(cb(coordinates_l,coordinates_h,0+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),0)*(ssize_t)sizeof(double)+xe_)))*((*(double *)(cb(coordinates_l,coordinates_h,0+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),0)*(ssize_t)sizeof(double)+xe_))))+ (((*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),1)*(ssize_t)sizeof(double)+xe_)))*((*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),1)*(ssize_t)sizeof(double)+xe_))))+ (((*(double *)(cb(coordinates_l,coordinates_h,2+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_)))*((*(double *)(cb(coordinates_l,coordinates_h,2+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_)))));ln=47; rPres_= ( r_ <dzz_  ?r_*pow(dzz_,(g_-2.) ) :pow(r_,(g_-1.)));ln=48; theta_= atan2((*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),1)*(ssize_t)sizeof(double)+xe_)) ,(*(double *)(cb(coordinates_l,coordinates_h,2+coordinates_l)*coordinates_i+coordinates__))-(*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_)));ln=49; thetaPres_= theta_;ln=54;if( theta_>phi_w_ ){ 
   			ln=51; thetaPres_ = phi_w_/(phi_w_-(3.14159265358979323846))*(theta_-(3.14159265358979323846)) ;
  		}else{ if( theta_< - phi_w_ ){ 
   			ln=53; thetaPres_ = phi_w_/(phi_w_-(3.14159265358979323846))*(theta_+(3.14159265358979323846));
  		};};
		ln=55;_7r=fabs((*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__)) - (*(double *)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),1)*(ssize_t)sizeof(double)+xe_)));
ln=55;_9r_8zvar=(*(double*)(cb(coordinates_l,coordinates_h,2+coordinates_l)*coordinates_i+coordinates__));
ln=55;_10i_8zvar=_7r;
ln=55;/*zvar=_9r_8zvar,_10i_8zvar*/
		complex_8POWER(&_11,(_9r_8zvar-(*(double*)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_))),(_10i_8zvar),laplExp_,0.);complex_8POWER(&_12,(_9r_8zvar+(*(double*)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_))*laplExp_/(1.-laplExp_)),(_10i_8zvar),(1.-laplExp_),0.);complex_8POWER(&_13,(_9r_8zvar+(*(double*)(cb(0,/*SA6*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),2)*(ssize_t)sizeof(double)+xe_))*laplExp_/(1.-laplExp_)),(_10i_8zvar),(1.-laplExp_),0.);ln=56; (*(double *)(0+RESULT_) )= ((_11.REAL_*_13.REAL_-_11.IMAG_*_13.IMAG_))			/* Laplace solution for streamwise velocity component (finite edge) */;
		ln=57; (*(double *)(0+(ssize_t)sizeof(double)+RESULT_) )= pow(r_,g_)*((g_+1.+D3_)*cos(g_*theta_) + g_*D3_*cos((g_-2.)*theta_))						/* Stokes solution for spanwise velocity component */;
		ln=58; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= pow(r_,g_)*((g_+1.-D3_)*sin(g_*theta_) + g_*D3_*sin((g_-2.)*theta_))						/* Stokes solution for wall-normal velocity component */;
		ln=59; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= D3_*4.*g_*rPres_*sin((g_-1.)*thetaPres_)								/* Stokes solution for pressure */;
	}fn=savefn; ln=saveln;}
	
	void coco_4signP(char *RESULT_,struct RIBLETS_ riblet_,const int coord_l,const int coord_h,const ssize_t coord_i,char *coord__,const int coord_p1_l,const int coord_p1_h,const ssize_t coord_p1_i,char *coord_p1__,const int coord_p2_l,const int coord_p2_h,const ssize_t coord_p2_i,char *coord_p2__,const int eCoord_l,const int eCoord_h,const ssize_t eCoord_i,char *eCoord__,double p1_,double p2_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/coco.cpl";{struct freefunc* es=freestack;
		/* Function needed to change the sign of p1 and p2 when computing deltaP, to account for the non periodicity of pressure Stokes solution */
		ln=76;if( (*(double *)(cb(coord_l,coord_h,1)*coord_i+coord__))-(*(double *)(cb(eCoord_l,eCoord_h,1)*eCoord_i+eCoord__)) <0.){
			ln=69;if( ((( (p1_)>=0. ?1  : -1))!=(( (p2_)>=0. ?1  : -1)) )&&( riblets_10ribletsCrossed(riblet_,coord_p1_l,coord_p1_h,coord_p1_i,coord_p1__)==riblets_10ribletsCrossed(riblet_,coord_p2_l,coord_p2_h,coord_p2_i,coord_p2__) )){
				ln=66; (*(double *)(cb(0,1,0+0)*(ssize_t)sizeof(double)+RESULT_ ))=-1.;(*(double *)(cb(0,1,0+1)*(ssize_t)sizeof(double)+RESULT_ ))=-1.;
			}else{
				ln=68; (*(double *)(cb(0,1,0+0)*(ssize_t)sizeof(double)+RESULT_ ))=1.;(*(double *)(cb(0,1,0+1)*(ssize_t)sizeof(double)+RESULT_ ))=-1.;
			};
		}else{
			ln=75;if( (((( (p1_)>=0. ?1  : -1))!=(( (p2_)>=0. ?1  : -1)) )&&( riblets_10ribletsCrossed(riblet_,coord_p1_l,coord_p1_h,coord_p1_i,coord_p1__)==riblets_10ribletsCrossed(riblet_,coord_p2_l,coord_p2_h,coord_p2_i,coord_p2__) ))&&( fabs(p1_)>1.e-8 )){
				ln=72; (*(double *)(cb(0,1,0+0)*(ssize_t)sizeof(double)+RESULT_ ))=1.;(*(double *)(cb(0,1,0+1)*(ssize_t)sizeof(double)+RESULT_ ))=1.;
			}else{
				ln=74; (*(double *)(cb(0,1,0+0)*(ssize_t)sizeof(double)+RESULT_ ))=1.;(*(double *)(cb(0,1,0+1)*(ssize_t)sizeof(double)+RESULT_ ))=-1.;
			};
		};
	}fn=savefn; ln=saveln;}
	
