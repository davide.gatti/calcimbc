/*-march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -isystem /home/ws/sb0817/anaconda3/envs/fenicsproject/include -I/home/ws/sb0817/anaconda3/envs/fenicsproject/include -pthread -I/home/ws/sb0817/anaconda3/envs/fenicsproject/include -L/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -L/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -Wl,-rpath,/home/ws/sb0817/anaconda3/envs/fenicsproject/lib -lmpi */

#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64

#define _LARGE_FILES

#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinf (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnan (double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf (float __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinfl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanl (long double __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32 (_Float32 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64 (_Float64 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf128 (_Float128 __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimumf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_magf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaximum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminimum_mag_numf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf32x (_Float32x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
   extern int isinff64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
   extern int isnanf64x (_Float64x __value) 
 __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

extern char errortemp_[(80+1)];

struct arrptr{int l,h; ssize_t i; char *a;};struct dynptr{void* p; int t;};extern char INTERRUPT;
typedef 
 void (*traphandler_t)(const char *);struct jbtrap{traphandler_t tr; sigjmp_buf jb; struct freefunc *fs;};extern struct jbtrap mainjb;
extern struct jbtrap * curjb;

#define traphandler curjb->tr
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};extern struct freefunc * freestack;

#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}

#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name

#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
extern void traprestore(void *ptr);
extern void condfree(void *ptr);
extern int friexecerror(char** s);
extern int (*friexec)(char** s);

#define mmovefrom(var,buf,type) *(type *)(buf)=*var

#define mmoveto(var,buf,type) *var=*(type *)(buf)

#define mainstart void default_traphandler(const char *errormessage){   if(errormessage[0]){     printERR;     freemem(NULL);     exit(EXIT_FAILURE);   }else{     freemem(NULL);     exit(EXIT_SUCCESS);   } } int main(int argc, char **argv){ struct freefunc* es; 			{struct sigaction act,oldact; act.sa_sigaction=trapsignal; sigemptyset(&act.sa_mask); act.sa_flags=SA_RESTART|SA_SIGINFO|SA_NODEFER; sigaction(SIGSEGV,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); sigaction(SIGFPE,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); sigaction(SIGILL,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); sigaction(SIGINT,&act,&oldact); if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); /* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ else {traphandler=default_traphandler;       freestack=NULL;       feenableexcept(fpe);      }; } es=freestack;
extern int dynptrerr(int type);
extern void *errmalloc(void);
extern void ioerr(FILE *fil);
extern void errfclose(void *voidf);
extern void errfopen(FILE **f, const char *name, int mode);
extern int scanrec(FILE *f, const char *format, void *var) ;
extern int scanbool(FILE *f, const char *format, int *var) ;
extern int myfgets(char *name, char *var, char *varend, FILE *f) ;
extern int mygetline(char *name, char **var, FILE *f) ;
extern void trapsignal(int signum, siginfo_t *info, void *ucontext);





/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */






/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/* Libraries to load */
/* ----------------------- */
/* Transparently reshapes the operation of the CPL compiler so as to provide ! */
/* array bounds checking and more run-time checks, without any modifications ! */
/* to the original program (at the expense of a slower execution).           ! */
/* To activate, put "USE rtchecks" at the beginning of your program.         ! */
/* See infocpl rtchecks.                                                     ! */
/*                                                                           !  */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net                      ! */
/* Released under the attached LICENSE.                                      ! */
/*                                                                           ! */
/* Code maturity: mostly green but the TRACE command is orange.              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <sys/ioctl.h>                                                          
/*
<*#ifdef __GNUC__
  const
#endif
int cb(int lb, int ub, int index);
#ifdef __GNUC__
  const
#endif
char* cp(int inputpos);
#ifdef __GNUC__
  const
#endif
char *cr(char *lb, char *ub, char *index);
*>
*/
/* nota: #ifdef non passa in C SECTION */
#undef printERR
#define printERR fprintf(stderr,"\r%s in line %d of %s: PROGRAM HALTED  \n",errormessage,ln,fn);fflush(stderr)
extern volatile int ln;
extern char * volatile fn;
extern const int cb(int lb, int ub, int index);
extern const char * cp(void);
extern const int ca(void);
extern const unsigned char sigNAN[8];


/* CPL interface to the termios functions needed to turn on and off ! */
/* character-by-character terminal input                            ! */
/*                                                                  ! */
/* Copyright 2002-2023 Paolo Luchini http://CPLcode.net             ! */
/* Released under the attached LICENSE.                             ! */
/*                                                                  ! */
/* Code maturity: green.                                            ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */


#include <termios.h>
void CHARbyCHAR_1CHARbyCHAR(int descr_);
void CHARbyCHAR_2LINEbyLINE(void);

extern struct termios CHARbyCHAR_CHARbyCHAR_newsetting;
extern struct termios CHARbyCHAR_CHARbyCHAR_oldsetting;

extern int CHARbyCHAR_CHARbyCHAR_CbCdescr;

extern void CHARbyCHAR_1CHARbyCHAR(int descr_);


extern void CHARbyCHAR_2LINEbyLINE(void);

/* Library providing an interface to the select system call ! */
/* in order to detect if input is waiting to be read.       ! */
/* See infocpl INPUTREADY.                                  ! */
/*                                                          !  */
/* Copyright 2008-2020 Paolo Luchini http://CPLcode.net     ! */
/* Released under the attached LICENSE.                     ! */
/*                                                          ! */
/* Code maturity: green.                                    ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern int fd_input_ready(int fd, int sec, int usec);








extern int BlockLevel;

void singlestepper(int lineN_,char *line_);






extern FILE *rtchecks_SingleStep_grabin;

extern int rtchecks_SingleStep_StopLevel;
extern char rtchecks_SingleStep_LoopCount[(ssize_t)sizeof(int)*(100+1)];
extern int rtchecks_SingleStep_LastLine;

extern int rtchecks_SingleStep_paused;

extern int rtchecks_SingleStep_lastfnlength;
extern int rtchecks_SingleStep_lastrow;

extern char *rtchecks_SingleStep_lastfn;

extern int rtchecks_SingleStep_termwidth;
extern int rtchecks_SingleStep_termheight;

extern char *rtchecks_SingleStep_hotkeys;


extern void rtchecks_SingleStep_1RestoreScroll(void);


extern void TRON(void);


extern void singlestepper(int lineN_,char *line_);

/* Complex-number type definition and corresponding extensions to the ! */
/* CPL language                                                       ! */
/*                                                                    ! */
/* Copyright 1996-2023 Paolo Luchini http://CPLcode.net               ! */
/* Released under the attached LICENSE.                               ! */
/*                                                                    ! */
/* Code maturity: green.                                              ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

struct COMPLEX_{double REAL_;double IMAG_;};
#define COMPLEX_s (ssize_t)sizeof(struct COMPLEX_)


extern void complex_1INV(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_2EXP(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_3SINH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_4COSH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_5TANH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);

extern void complex_6COTH(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);






extern void complex_7LOG(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_8POWER(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG,double y_REAL,double y_IMAG);


extern void complex_9SQRT(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);


extern void complex_10ACOS(struct COMPLEX_ *RESULT_,double x_REAL,double x_IMAG);




extern void complex_11CRAND(struct COMPLEX_ *RESULT_);

extern void complex_12CGAUSS(struct COMPLEX_ *RESULT_);

/* Library of common matrix algebra operations for real-valued matrices ! */
/* including suitable extensions to the CPL language to handle infix    ! */
/* notation for such functions. See matrix.info for usage.              ! */
/*                                                                      ! */
/* Copyright 1997-2021 Paolo Luchini http://CPLcode.net                 ! */
/* Released under the attached LICENSE.                                 ! */
/*                                                                      ! */
/* Code maturity: green.                                                ! */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

extern double DotProduct(const int a_l,const int a_h,const ssize_t a_i,char *a__,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_1LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,char *B___);




struct REALINVMAT_{int lo_;int hi_;};
#define REALINVMAT_s (ssize_t)sizeof(struct REALINVMAT_)





extern void rbmat_2MatEqu(const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,char *c___,double a_);


extern void rbmat_3MatEqu(FILE *c_f,const int c_l,const int c_h,const ssize_t c_i,const int c__l,const int c__h,const ssize_t c__i,off_t c___,double a_);


extern void rbmat_4LeftMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int b_l,const int b_h,const ssize_t b_i,char *b__);


extern void rbmat_5RightMult(const int c_l,const int c_h,const ssize_t c_i,char *c__,const int a_l,const int a_h,const ssize_t a_i,char *a__,FILE *B_f,const int B_l,const int B_h,const ssize_t B_i,const int B__l,const int B__h,const ssize_t B__i,off_t B___);


extern void rbmat_6LUdecomp(const int AA_l,const int AA_h,const ssize_t AA_i,const int AA__l,const int AA__h,const ssize_t AA__i,char *AA___);


extern void rbmat_7LUdecomp(FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);


extern void rbmat_8LeftLDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_9LeftUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);




extern void rbmat_10LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_11RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,char *A___);


extern void rbmat_12RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,FILE *A_f,const int A_l,const int A_h,const ssize_t A_i,const int A__l,const int A__h,const ssize_t A__i,off_t A___);




/*
inclusa separatamente in cbmat e rbmat
REAL Lanczos_norm=0
SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   SUBROUTINE[ARRAY(*) OF number v2^,v1] A,B
                   ARRAY(*) OF number y1; REAL eps)
  ARRAY(x.LO..x.HI) OF number v=0,v1
  ARRAY(y1.LO..y1.HI) OF number y=y1,r=0,r1
  REAL n=1
  x=0
  INTEGER count=2*LENGTH(y)
  DO
    B(v1,y)
    A(r1,v1)
    l1=r|r1/n; r=r1-l1*r; v=v1-l1*v
    n=NORM(r)
    l2=r|y/n; y=y-l2*r; x=x+l2*v
    DEC count
    Lanczos_norm=NORM(y)
  UNTIL Lanczos_norm<eps*eps OR count=0
END Lanczos

SUBROUTINE Lanczos(POINTER TO ARRAY(*) OF number x
                   ARRAY(*,*) OF number M
                   ARRAY(*) OF number y1; REAL eps)
  SUBROUTINE A[ARRAY(*) OF number v2^,v1]
    v2=M*v1
  END A  
  SUBROUTINE B[ARRAY(*) OF number v2^,v1]
    v2=v1*M
  END B
  Lanczos(x,A,B,y1,eps)
END Lanczos
*/

extern void rbmat_13PLU(const int m_l,const int m_h,const ssize_t m_i,const int m__l,const int m__h,const ssize_t m__i,char *m___,struct REALINVMAT_ *RESULT_);




extern void rbmat_14LeftLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,struct REALINVMAT_ *m_,const int t_l,const int t_h,const ssize_t t_i,char *t__);


extern void rbmat_15RightLUDiv(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int t_l,const int t_h,const ssize_t t_i,char *t__,struct REALINVMAT_ *m_);


extern void rbmat_16INV(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int RESULT_l,const int RESULT_h,const ssize_t RESULT_i,const int RESULT__l,const int RESULT__h,const ssize_t RESULT__i,char *RESULT___);


extern double rbmat_17DET(const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___);

extern double REAL_Lanczos_norm_;

extern void rbmat_18Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,void (*A_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),void (*B_)(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__),const int y1_l,const int y1_h,const ssize_t y1_i,char *y1__,double eps_);


void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct rbmat_Lanczos_R_s20 {int l,h; ssize_t i;struct arrptr a;};
extern struct rbmat_Lanczos_R_s20 rbmat_Lanczos_R_Lanczos_mat;

  extern void rbmat_Lanczos_R_21A(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  extern void rbmat_Lanczos_R_22B(const int v2_l,const int v2_h,const ssize_t v2_i,char *v2__,const int v1_l,const int v1_h,const ssize_t v1_i,char *v1__);

  
  extern void rbmat_19Lanczos(const int x_l,const int x_h,const ssize_t x_i,char *x__,const int mat_l,const int mat_h,const ssize_t mat_i,const int mat__l,const int mat__h,const ssize_t mat__i,char *mat___,const int y_l,const int y_h,const ssize_t y_i,char *y__,double eps_);

struct DNS_{char size_[(ssize_t)sizeof(int)*(/*SA1*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)];char localSize_[(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)*(/*SA3*/(((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1)+(ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))/((ssize_t)sizeof(int)*(/*SA2*/(((ssize_t)sizeof(int)+(ssize_t)sizeof(int))/((ssize_t)sizeof(int))-1)+1))-1)+1)];char leng_[(ssize_t)sizeof(double)*(/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];double nu_;struct arrptr zd_;};
#define DNS_s (ssize_t)sizeof(struct DNS_)
              
               
void dns_5readRestartFile(struct DNS_ *this_,char *fname_);     
         
extern void dns_5readRestartFile(struct DNS_ *this_,char *fname_);

/**/
struct RIBLETS_{double h_s_;double s_;double h_;double stkexp_;double r_;int waven_;struct arrptr lambda_;struct arrptr amp_;char phiC_[(ssize_t)sizeof(double)*(6+1)];char gC_[(ssize_t)sizeof(double)*(6+1)];};
#define RIBLETS_s (ssize_t)sizeof(struct RIBLETS_)

	
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_);
double riblets_2yWave(struct RIBLETS_ this_,double x_);
void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_);
int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);
void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__);
int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__);

double riblets_riblets_11tol;
void riblets_1readInputFile(struct RIBLETS_ *this_,char *fname_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;
		FILE *ribletsin_;

		int non_laminar_;

		double nu_;
double headx_;
double heady_;

		int walls_;
int waven_;

		double spacing_;
double ratiohs_;

		int use_coco_;
int extra_;

		double radius_;
double stokexp_;

		char phiC_[(ssize_t)sizeof(double)*(6+1)];
char gC_[(ssize_t)sizeof(double)*(6+1)];

		int nx_;
int ny_;
int nz_;

		double Lx_;
double Ly_;

		double cflmax_;
double fouriermax_;
double deltat_;
double maxTime_;

		int it_save_;
int it_stat_;
int it_max_;
int it_cfl_;
int it_check_;

		int continue_;

		int _14h;
ssize_t _15i;
char *_16lambda;
int _18h;
ssize_t _19i;
char *_20ampl;
ribletsin_=NULL;ln=23;errfopen(&ribletsin_,fname_,O_RDONLY);memmove(&nu_,&sigNAN,sizeof(double));memmove(&headx_,&sigNAN,sizeof(double));memmove(&heady_,&sigNAN,sizeof(double));memmove(&spacing_,&sigNAN,sizeof(double));memmove(&ratiohs_,&sigNAN,sizeof(double));memmove(&radius_,&sigNAN,sizeof(double));memmove(&stokexp_,&sigNAN,sizeof(double));{int i12;for(i12=0;i12<=6;i12++){memmove((double *)(phiC_+i12*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}{int i13;for(i13=0;i13<=6;i13++){memmove((double *)(gC_+i13*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}memmove(&Lx_,&sigNAN,sizeof(double));memmove(&Ly_,&sigNAN,sizeof(double));memmove(&cflmax_,&sigNAN,sizeof(double));memmove(&fouriermax_,&sigNAN,sizeof(double));memmove(&deltat_,&sigNAN,sizeof(double));memmove(&maxTime_,&sigNAN,sizeof(double));ln=36;   if(!(scanrec( ribletsin_ ," non_laminar = %d",&non_laminar_)>0 &&scanrec( ribletsin_ ," nu = %lg",&nu_)>0 &&scanrec( ribletsin_ ," headx = %lg",&headx_)>0 &&scanrec( ribletsin_ ," heady = %lg",&heady_)>0))ioerr( ribletsin_ );
		ln=37;   if(!(scanrec( ribletsin_ ," walls = %d",&walls_)>0 &&scanrec( ribletsin_ ," spacing = %lg",&spacing_)>0 &&scanrec( ribletsin_ ," ratiohs = %lg",&ratiohs_)>0))ioerr( ribletsin_ );
		ln=38;   if(!(scanrec( ribletsin_ ," waven = %d",&waven_)>0))ioerr( ribletsin_ );
		ln=39;_14h=waven_;
ln=39;_15i=(ssize_t)sizeof(double)*_14h;

_16lambda=malloc(_15i);if(_16lambda==NULL)_16lambda=errmalloc();_16lambda-=(ssize_t)sizeof(double);{int i17;for(i17=1;i17<=_14h;i17++){memmove((double *)(_16lambda+i17*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}ln=39; 
		ln=40;_18h=waven_;
ln=40;_19i=(ssize_t)sizeof(double)*_18h;

_20ampl=malloc(_19i);if(_20ampl==NULL)_20ampl=errmalloc();_20ampl-=(ssize_t)sizeof(double);{int i21;for(i21=1;i21<=_18h;i21++){memmove((double *)(_20ampl+i21*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}ln=40; 
		ln=41;   if(!((scanrec( ribletsin_ ," lambda = ",NULL),({int i22=1; while(i22<=_14h){if(!scanrec( ribletsin_ ," %lg",(double *)(i22*(ssize_t)sizeof(double)+_16lambda))>0)ioerr( ribletsin_ ); i22++;}}),1)))ioerr( ribletsin_ );
		ln=42;   if(!((scanrec( ribletsin_ ," ampl = ",NULL),({int i23=1; while(i23<=_18h){if(!scanrec( ribletsin_ ," %lg",(double *)(i23*(ssize_t)sizeof(double)+_20ampl))>0)ioerr( ribletsin_ ); i23++;}}),1)))ioerr( ribletsin_ );
		ln=43;   if(!(scanrec( ribletsin_ ," use_coco = %d",&use_coco_)>0 &&scanrec( ribletsin_ ," radius = %lg",&radius_)>0 &&scanrec( ribletsin_ ," stokexp = %lg",&stokexp_)>0 &&scanrec( ribletsin_ ," extra = %d",&extra_)>0))ioerr( ribletsin_ );
		ln=44;   if(!((scanrec( ribletsin_ ," phiC = ",NULL),({int i24=0; while(i24<=6){if(!scanrec( ribletsin_ ," %lg",(double *)(i24*(ssize_t)sizeof(double)+phiC_))>0)ioerr( ribletsin_ ); i24++;}}),1) &&(scanrec( ribletsin_ ," gC = ",NULL),({int i25=0; while(i25<=6){if(!scanrec( ribletsin_ ," %lg",(double *)(i25*(ssize_t)sizeof(double)+gC_))>0)ioerr( ribletsin_ ); i25++;}}),1)))ioerr( ribletsin_ );
		ln=45;   if(!(scanrec( ribletsin_ ," nx = %d",&nx_)>0 &&scanrec( ribletsin_ ," ny = %d",&ny_)>0 &&scanrec( ribletsin_ ," nz = %d",&nz_)>0 &&scanrec( ribletsin_ ," Lx = %lg",&Lx_)>0 &&scanrec( ribletsin_ ," Ly = %lg",&Ly_)>0))ioerr( ribletsin_ );
		ln=46;   if(!(scanrec( ribletsin_ ," cflmax = %lg",&cflmax_)>0 &&scanrec( ribletsin_ ," fouriermax = %lg",&fouriermax_)>0 &&scanrec( ribletsin_ ," deltat = %lg",&deltat_)>0 &&scanrec( ribletsin_ ," maxTime = %lg",&maxTime_)>0))ioerr( ribletsin_ );
		ln=47;   if(!(scanrec( ribletsin_ ," it_save = %d",&it_save_)>0 &&scanrec( ribletsin_ ," it_stat = %d",&it_stat_)>0 &&scanrec( ribletsin_ ," it_max = %d",&it_max_)>0 &&scanrec( ribletsin_ ," it_cfl = %d",&it_cfl_)>0 &&scanrec( ribletsin_ ," it_check = %d",&it_check_ )>0))ioerr( ribletsin_ );
		ln=48;   if(!(scanbool( ribletsin_ ," continue = %c%*4[A-Za-z] ",&continue_)))ioerr( ribletsin_ );
		ln=49; (*this_).h_s_ = ratiohs_;
		ln=50; (*this_).s_ = spacing_;
		ln=51; (*this_).stkexp_ = stokexp_;
		ln=52; (*this_).r_ = radius_;
		ln=53; (*this_).h_ = (*this_).h_s_*(*this_).s_;
		ln=54; (*this_).waven_ = waven_;
		ln=55; (*this_).lambda_.l=1;(*this_).lambda_.h=_14h;(*this_).lambda_.i=(ssize_t)sizeof(double);(*this_).lambda_.a=_16lambda;
		ln=56; (*this_).amp_.l=1;(*this_).amp_.h=_18h;(*this_).amp_.i=(ssize_t)sizeof(double);(*this_).amp_.a=_20ampl;
		ln=57; cb(0,6,6-(0)+0);memmove((*this_).phiC_,phiC_,(ssize_t)sizeof(double)*(6+1));
		ln=58; cb(0,6,6-(0)+0);memmove((*this_).gC_,gC_,(ssize_t)sizeof(double)*(6+1));
	}fn=savefn; ln=saveln;}
	
	void riblets_3Bisection(char *RESULT_,int (*f_)(char *s_),char *x1_,char *x2_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;
		char xl_[(ssize_t)sizeof(double)*(2+1)] ;
char xr_[(ssize_t)sizeof(double)*(2+1)] ;

		int _12fl;
int _13fr;
ln=62; cb(0,2,2-(0)+0);memmove(xl_,x1_,(ssize_t)sizeof(double)*(2+1));ln=62; cb(0,2,2-(0)+0);memmove(xr_,x2_,(ssize_t)sizeof(double)*(2+1));ln=63;_12fl=(*(f_!=NULL?f_:(int (*)(char *s_))cp()))(xl_);
ln=63;   _13fr=(*(f_!=NULL?f_:(int (*)(char *s_))cp()))(xr_);
 
		ln=64;if( _12fl == _13fr ){ traphandler("Bisection point not included in bisection limits");};
			ln=69;
			do{double _16m;
char _19[(ssize_t)sizeof(double)*(2+1)];
double _21;
ln=66;_16m=1./2.;
ln=66; {cb(0,2,2-(0)+0);ln=66;  {int _14i_=(0||0!=0?ca():0);do{{ln=66;(*(double *)(cb(0,2-(0),_14i_)*(ssize_t)sizeof(double)+RESULT_ +(0)*(ssize_t)sizeof(double)))=((*(double *)(cb(0,2,_14i_)*(ssize_t)sizeof(double)+xl_))+(*(double *)(cb(0,2,_14i_)*(ssize_t)sizeof(double)+xr_)))*_16m ;}_14i_++;}while(_14i_<=(0||2-(0)!=2?ca():2-(0)));}}
			ln=67;  {int _17i_=2;do{{ln=67;(*(double *)(cb(0,2,_17i_)*(ssize_t)sizeof(double)+_19))=(*(double *)(cb(0,2,_17i_)*(ssize_t)sizeof(double)+xl_))-(*(double *)(cb(0,2,_17i_)*(ssize_t)sizeof(double)+xr_)) ;}_17i_--;}while(_17i_>=0);} _21=0.;  {int _20i_=2;do{{(*&_21)=(*(&_21!=NULL?&_21:(double *)cp()))+(((*(double *)(cb(0,2,_20i_)*(ssize_t)sizeof(double)+_19)))*((*(double *)(cb(0,2,_20i_)*(ssize_t)sizeof(double)+_19)))) ;}_20i_--;}while(_20i_>=0);}ln=67;if( sqrt(_21)<riblets_riblets_11tol ){  fn=savefn; ln=saveln;return;};
			ln=68;if( (*(f_!=NULL?f_:(int (*)(char *s_))cp()))(RESULT_) == _12fl ){  cb(0,2,2-(0)+0);memmove(xl_,RESULT_,(ssize_t)sizeof(double)*(2+1));}else{  cb(0,2,2-(0)+0);memmove(xr_,RESULT_,(ssize_t)sizeof(double)*(2+1));};
		}while(1);halve:;
	}fn=savefn; ln=saveln;}
	
        double riblets_2yWave(struct RIBLETS_ this_,double x_){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;double RESULT_;
                /* This function gives the local shift at position x of the  */
                /* sinusoidal riblet crest */
		ln=75;  RESULT_ =0.; {int ii_=1 ;do{{ln=75;(*&RESULT_ )=(*(&RESULT_ !=NULL?&RESULT_ :(double *)cp()))+(*(double *)(cb(this_.amp_.l,this_.amp_.h,ii_)*this_.amp_.i+this_.amp_.a))*sin(2.*(3.14159265358979323846)/(*(double *)(cb(this_.lambda_.l,this_.lambda_.h,ii_)*this_.lambda_.i+this_.lambda_.a))*x_) ;}ii_+=1;}while(ii_<= this_.waven_);}
        fn=savefn; ln=saveln;return RESULT_;}}

	int riblets_4isInBody(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;int RESULT_;
		double _12yNew;
double _13z_at_p;
ln=79;_12yNew=(*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__)) - riblets_2yWave(this_,(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l)*coordinates_i+coordinates__)));
ln=79; 
		ln=80;_13z_at_p=fabs(this_.h_s_*2.*(_12yNew - this_.s_*(double)((int)floor(_12yNew/this_.s_+0.5)))) - this_.h_/2.;
ln=80; 
		ln=81; RESULT_ = (*(double *)(cb(coordinates_l,coordinates_h,2+coordinates_l)*coordinates_i+coordinates__)) <_13z_at_p;
	fn=savefn; ln=saveln;return RESULT_;}}
	
	void riblets_6edge(char *RESULT_,struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;
                /* This function returns the closest edge to a point of coordinates */
                /* along the y-direction */
		double _13yShift;
double _14yNew;
ln=87; (*(double *)(0+RESULT_) )= (*(double *)(cb(coordinates_l,coordinates_h,coordinates_l)*coordinates_i+coordinates__));
                ln=88;_13yShift=riblets_2yWave(this_,(*(double *)(0+RESULT_)));
ln=88; 
		ln=89;_14yNew=(*(double*)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__))-_13yShift;
ln=89; 
		ln=90; (*(double *)(0+(ssize_t)sizeof(double)+RESULT_) )= ((double)((int)floor(_14yNew/this_.s_))+ 0.5)*this_.s_ + _13yShift;
		ln=91; (*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_) )= 0.5*this_.h_;
	}fn=savefn; ln=saveln;}
	
	void riblets_8findEdge(char *RESULT_,struct RIBLETS_ this_,struct DNS_ dns_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;
                /* This function finds the coordinate of point located on the closes edge */
                /* along the direction normal to the closest edge */
		char xe_[(ssize_t)sizeof(double)*(3+1)];
char dsq_[(ssize_t)sizeof(double)*(3+1)];
char newCoord_[(ssize_t)sizeof(double)*(2+1)];
double trigArg_;

  		char _18[(ssize_t)sizeof(double)*(/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
char _21[(ssize_t)sizeof(double)*(/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
{int i14;for(i14=0;i14<=3;i14++){memmove((double *)(xe_+i14*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}{int i15;for(i15=0;i15<=3;i15++){memmove((double *)(dsq_+i15*(ssize_t)sizeof(double)),&sigNAN,sizeof(double));}}{char *_17_;

ln=97;_17_=coordinates__;ln=97; {char *_16_=newCoord_;int _16_1=2; do{{ ln=97;(*(double *)(cb(0+(int)(((ssize_t)(newCoord_-(_16_)))/(ssize_t)((ssize_t)sizeof(double))),2+(int)(((ssize_t)(newCoord_-(_16_)))/(ssize_t)((ssize_t)sizeof(double))),0)*(ssize_t)sizeof(double)+_16_))=(*(double *)(cb(coordinates_l+(int)(((ssize_t)(coordinates__-(_17_)))/(ssize_t)(coordinates_i)),coordinates_h+(int)(((ssize_t)(coordinates__-(_17_)))/(ssize_t)(coordinates_i)),0)*coordinates_i+_17_)); _17_ =coordinates_i+_17_;}_16_+=(ssize_t)sizeof(double);_16_1--;}while(_16_1>=0);}}ln=97;trigArg_=0.;ln=98;(*(double *)(cb(0,2,0+2)*(ssize_t)sizeof(double)+newCoord_))=0.5*this_.h_;  (*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+0)*coordinates_i+coordinates__));  riblets_6edge(_18,this_,0,2,(ssize_t)sizeof(double),newCoord_);(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+dsq_))=(((*(double *)(0+(ssize_t)sizeof(double)+_18))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__)))*((*(double *)(0+(ssize_t)sizeof(double)+_18))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__))));
  		ln=99;(*(double *)(cb(0,3,1)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(0,/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),0)*(ssize_t)sizeof(double)+dns_.leng_))/2.;  (*(double *)(cb(0,3,3)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))+(*(double *)(cb(0,/*SA4*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1),0)*(ssize_t)sizeof(double)+dns_.leng_))/2.;
  		ln=109;while( (*(double *)(cb(0,3,3)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(0,3,1)*(ssize_t)sizeof(double)+xe_))>riblets_riblets_11tol){
    			int _19m;
char _20[(ssize_t)sizeof(double)*(/*SA12*/(((ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double))/((ssize_t)sizeof(double))-1)+1)];
ln=101;_19m=( (*(double *)(cb(0,3,3)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))>(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(0,3,1)*(ssize_t)sizeof(double)+xe_) ) ?3  :1);
ln=101; 
    			ln=102;(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_))=0.382*(*(double *)(cb(0,3,_19m)*(ssize_t)sizeof(double)+xe_))+0.618*(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_));
    			ln=103;(*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+newCoord_))=(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_));
			riblets_6edge(_20,this_,0,2,(ssize_t)sizeof(double),newCoord_);ln=104;(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+dsq_))=(((*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l)*coordinates_i+coordinates__)))*((*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l)*coordinates_i+coordinates__))))+(((*(double *)(0+(ssize_t)sizeof(double)+_20))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__)))*((*(double *)(0+(ssize_t)sizeof(double)+_20))-(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l+1)*coordinates_i+coordinates__))));
    			ln=108;if( (*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+dsq_))>=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+dsq_) )){ (*(double *)(cb(0,3,_19m)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_)); (*(double *)(cb(0,3,_19m)*(ssize_t)sizeof(double)+dsq_))=(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+dsq_) );}else{
      				ln=106;(*(double *)(cb(0,3,4-_19m)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_));  (*(double *)(cb(0,3,4-_19m)*(ssize_t)sizeof(double)+dsq_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+dsq_));
      				ln=107;(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))=(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+xe_));  (*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+dsq_))=(*(double *)(cb(0,3,0)*(ssize_t)sizeof(double)+dsq_));
    			};
  		}
  		ln=110;(*(double *)(cb(0,2,0+0)*(ssize_t)sizeof(double)+newCoord_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_));
		ln=111;(*(double *)(0+RESULT_))=(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_));
		riblets_6edge(_21,this_,0,2,(ssize_t)sizeof(double),newCoord_);ln=112;(*(double *)(0+(ssize_t)sizeof(double)+RESULT_))=(*(double *)(0+(ssize_t)sizeof(double)+_21));
		ln=113;(*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_))=0.5*this_.h_;
		ln=116; for(int ii_=  1 ;ii_<= this_.waven_;ii_+=1){
  			ln=115; trigArg_ +=  2.*(3.14159265358979323846)*(*(double *)(cb(this_.amp_.l,this_.amp_.h,ii_)*this_.amp_.i+this_.amp_.a))/(*(double *)(cb(this_.lambda_.l,this_.lambda_.h,ii_)*this_.lambda_.i+this_.lambda_.a))*cos(2.*(3.14159265358979323846)*(*(double *)(cb(0,3,2)*(ssize_t)sizeof(double)+xe_))/(*(double *)(cb(this_.lambda_.l,this_.lambda_.h,ii_)*this_.lambda_.i+this_.lambda_.a)));
  		}
		ln=117;(*(double *)(0+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+(ssize_t)sizeof(double)+RESULT_))=atan(trigArg_) /* beta */;
	}fn=savefn; ln=saveln;}

	void riblets_9cordMod(struct RIBLETS_ this_,const int pA_l,const int pA_h,const ssize_t pA_i,char *pA__,const int pB_l,const int pB_h,const ssize_t pB_i,char *pB__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;
                /* This function corrects the coordinate of the neighbour point  */
                /* pB if it crosses a complete riblet crest */
		int _14nA;
int _15nB;
ln=123;_14nA=riblets_10ribletsCrossed(this_ ,pA_l,pA_h,pA_i,pA__);
ln=123; 
        	ln=124;_15nB=riblets_10ribletsCrossed(this_ ,pB_l,pB_h,pB_i,pB__);
ln=124; 
        	ln=127;if( ((_14nA!=_15nB )&&( (*(double *)(cb(pA_l,pA_h,2+pA_l)*pA_i+pA__))<=this_.h_/2.))&&( (*(double *)(cb(pB_l,pB_h,2+pB_l)*pB_i+pB__))<=this_.h_/2.)){
			ln=126;(*(double *)(cb(pB_l,pB_h,1+pB_l)*pB_i+pB__))=((double)(_15nB)+(( _14nA>_15nB  ?0.500001  : -0.500001)))*this_.s_+riblets_2yWave(this_,(*(double *)(cb(pB_l,pB_h,1+pB_l)*pB_i+pB__)));
		};
	}fn=savefn; ln=saveln;}
	
	int riblets_10ribletsCrossed(struct RIBLETS_ this_,const int coordinates_l,const int coordinates_h,const ssize_t coordinates_i,char *coordinates__){char* volatile savefn; volatile int saveln; savefn=fn; saveln=ln; fn="modules/riblets.cpl";{struct freefunc* es=freestack;int RESULT_;
                /* This function gives the number of riblet crests that are  */
                /* on the left of the point located at coordinates */
		ln=133; RESULT_ = (int)floor(((*(double *)(cb(coordinates_l,coordinates_h,1+coordinates_l)*coordinates_i+coordinates__))-riblets_2yWave(this_,(*(double *)(cb(coordinates_l,coordinates_h,coordinates_l)*coordinates_i+coordinates__)))+this_.s_/2.)/this_.s_) ;
	fn=savefn; ln=saveln;return RESULT_;}}
	
			
