#!/bin/bash
walls=$(awk -F'walls=' '{print substr($2,0,1)}' input/riblets.in)
extra=$(awk -F'extra=' '{print substr($2,0,1)}' input/riblets.in)
if [ $walls == 2 ]; then
    echo '#define WALLS' | cat - riblets_test.cpl > tmp
    mv tmp riblets_test.cpl
fi
if [ $extra == 1 ]; then
    echo '#define EXTRA' | cat - riblets_test.cpl > tmp
    mv tmp riblets_test.cpl
fi
#cat riblets_test.cpl
echo "walls=$walls"
echo "extra=$extra"
mpicpl make riblets_test.cpl
if [ $walls == 2 ]; then
   tail -n +2 riblets_test.cpl > tmp
   mv tmp riblets_test.cpl
fi
if [ $extra == 1 ]; then
   tail -n +2 riblets_test.cpl > tmp
   mv tmp riblets_test.cpl
fi
