close all
clear
clc

tic

addpath('./base/')
addpath('/home/ws/og2387/vtkwrite/')
fields_name = dir('../fields/*.field');

field_file = fields_name(4).name;
[field, dns] = readfield("../fields/" + field_file);
[X,Y,Z] = meshgrid(dns.y,dns.x,dns.zc);

z_w = dns.zd(2:2:end);
z_w(length(z_w)+1) = z_w(length(z_w))+(z_w(length(z_w))-z_w(length(z_w)-1));

nx = length(field(1,1,1,:));    
ny = length(field(1,1,:,1));
nz = length(field(1,:,1,1));

lambda_im = zeros(nx,ny,nz);

dx = dns.Lx/dns.nx;
dy = dns.Ly/dns.ny;

parpool('local')

for i = 3:nx-1
    for j = 3:ny-1
        for k = 3:nz-1
    
            dz1 = z_w(k-1)-z_w(k-2);
            dz2 = z_w(k)-z_w(k-1);
            dz3 = z_w(k+1)-z_w(k);

            dxz1 = sqrt((0.5*dx)^2+(0.5*dz1)^2);
            dxz2 = sqrt((0.5*dx)^2+(0.5*dz2)^2);
            dxz3 = sqrt((0.5*dx)^2+(0.5*dz3)^2);
            dyz1 = sqrt((0.5*dy)^2+(0.5*dz1)^2);
            dyz2 = sqrt((0.5*dy)^2+(0.5*dz2)^2);
            dyz3 = sqrt((0.5*dy)^2+(0.5*dz3)^2);            

            weightx1 = (1/dxz1)/(2/dxz1+2/dxz2);
            weightx2 = (1/dxz2)/(2/dxz1+2/dxz2);
            weightx3 = (1/dxz2)/(2/dxz2+2/dxz3);
            weightx4 = (1/dxz3)/(2/dxz2+2/dxz3);
            weighty1 = (1/dyz1)/(2/dyz1+2/dyz2);
            weighty2 = (1/dyz2)/(2/dyz1+2/dyz2);
            weighty3 = (1/dyz2)/(2/dyz2+2/dyz3);
            weighty4 = (1/dyz3)/(2/dyz2+2/dyz3);

            grad_vel = zeros(3,3);
            grad_vel(1,1) = (field(2,k,j,i)-field(2,k,j,i-1))/dx;
            grad_vel(1,2) = 0.25*(field(2,k,j+1,i)+field(2,k,j+1,i-1)-field(2,k,j-1,i)-field(2,k,j-1,i-1))/dy;
            grad_vel(1,3) = ((weightx4*(field(2,k+1,j,i)+field(2,k+1,j,i-1))+weightx3*(field(2,k,j,i)+field(2,k,j,i-1)))-(weightx2*(field(2,k,j,i)+field(2,k,j,i-1))+weightx1*(field(2,k-1,j,i)+field(2,k-1,j,i-1))))/dz2;
            grad_vel(2,1) = 0.25*(field(3,k,j,i+1)+field(3,k,j-1,i+1)-field(3,k,j,i-1)-field(3,k,j-1,i-1))/dx;
            grad_vel(2,2) = (field(3,k,j,i)-field(3,k,j-1,i))/dy;
            grad_vel(2,3) = ((weighty4*(field(3,k+1,j,i)+field(3,k+1,j-1,i))+weighty3*(field(3,k,j,i)+field(3,k,j-1,i)))-(weighty2*(field(3,k,j,i)+field(3,k,j-1,i))+weighty1*(field(3,k-1,j,i)+field(3,k-1,j-1,i))))/dz2;
            grad_vel(3,1) = 0.25*(field(4,k,j,i+1)+field(4,k-1,j,i+1)-field(4,k,j,i-1)-field(4,k-1,j,i-1))/dx;
            grad_vel(3,2) = 0.25*(field(4,k,j+1,i)+field(4,k-1,j+1,i)-field(4,k,j-1,i)-field(4,k-1,j-1,i))/dy;
            grad_vel(3,3) = (field(4,k,j,i)-field(4,k-1,j,i))/dz2;

            [V,D] = eig(grad_vel);

%             im = 0;
%             real_eig = zeros(3,1);
%             real_av = 0;

            for l = 1:3
                lambda_im(i,j,k) = lambda_im(i,j,k) + abs(imag(D(l,l)));
%                 if abs(imag(D(l,l))) ~= 0
%                     real_eig(l) = 1;
%                 end
            end

%             if sum(real_eig) == 1
%                 for m = 1:3
%                     if real_eig(m) == 1
%                         real_av = m;
%                     end
%                 end
%             end

%             if lambda_im(i,j,k,1) ~= 0 && sum(real_eig) == 1
%                 lambda_im(i,j,k,2:4) = V(:,real_av);
%             end

        end
    end
    i
end


prova = squeeze(lambda_im(550,:,:));
figure()
pcolor(dns.y,dns.zc,prova')
shading interp
axis equal
colorbar()

toc
% vtkwrite('lambda.vtk','structured_grid',X,Y,Z,'scalars','lambda',lambda_im);

