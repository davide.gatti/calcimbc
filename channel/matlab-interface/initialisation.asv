%%
addpath('./base/')
clc
disp('|-------------------')
disp('|     RIBLETS 3D   |')
disp('|-------------------')
disp('| ')
disp('| Simulation set-up ')
disp('| initialisation    ')
disp('| and checks        ')
disp('| ')
disp('  ')


%% Physical Inputs

% LAMINAR or TURBULENT ?

dns.laminar         = true;             % this setting allow the field generator to chose among the possible laminar cases
dns.couette 	    = true;             % if true, a couette flow is imposed with centerline velocity = 1 and headx=heady=0

% CONSTANT PRESSURE GRADIENT or FLOW RATE ?

dns.type 	        = 'CPG';            % 'CFR'= constant flow rate; 'CPG'= constant pressure gradient

dns.headx 	        = 1.0;				% x-component of the pressure gradient
dns.heady 	        = 0.0;  			% y-component of the pressure gradient

% REYNOLDS NUMBER ?

dns.re 		        = 200;              % re_bulk if CFR, re_tau if CPG


%% Geometrical Inputs

% HALF or FULL CHANNEL ?

dns.walls           = 2;                % one wall -> half channel, two walls -> full channel

% DOMAIN SIZE ?

dns.domain          = 'full';           % 'full'= physical dimensions chosen by user, 'minimal'= minimal domain size

dns.Lx_plus         = 1500;             % relevant only if dns.type is 'full' (wall-units)
dns.Ly_plus         = 750;              % relevant only if dns.type is 'full' (wall-units)

% RIBLETS GEOMETRY

rib.alfa            = 60;               % rib tip angle (degrees)
rib.s_plus          = 8;               % riblets spacing (wall-units)

% rib.lambda_plus     = ones(1,100);      % initial values for lambda (will be modified subsequently)
rib.lambda_plus = [];
rib.max_beta        = -1*ones(1,100);   % initial values for max angles (will be modified subsequently)

 rib.lambda_plus(1)  = dns.Lx_plus;      % values for the riblets wavelengths (wall-units)
 rib.lambda_plus(2)  = dns.Lx_plus/6;
 rib.lambda_plus(3)  = dns.Lx_plus/3;
% rib.lambda_plus(4)  = dns.Lx_plus/6;
% rib.lambda_plus(5)  = dns.Lx_plus/3;


rib.max_beta(1)     = 1;                % values for the maximum riblet angles (degrees), at least one value for max_beta has to be defined
rib.max_beta(2)     = 0;
rib.max_beta(3)     = 0;
rib.max_beta(4)     = 0;
rib.max_beta(5)     = 0;


%% Corner Correction Inputs

dns.useCoco         = 'yes';            % if 'yes' then corner correction is applied

dns.radius_plus     = 2;                % CoCo's radius of aplication around the tip (wall-units)

dns.extra_diag      = 'no';             % if 'yes' then the extra-diagonal terms of the corner correction are applied


%% Numerical Inputs

% SPATIAL RESOLUTION

dns.dx_plus         = 2.0;              % streamwise resolution (wall-units)
dns.dy_plus         = 1.0;              % spanwise resolution (wall-units)

rib.nppr            = 2;                % minimum number of points per riblet
dns.cellAR          = 1.25;             % aspect ratio of the cells within the riblets (dy+/dz+)
dns.zstretch        = 3.0;              % ratio dz_max/dz_min

% TEMPORAL RESOLUTION

dns.cflmax          = 1.2;              % maximum cfl number
dns.fouriermax      = 0.5;              % maximum fourier number

% CHECKS and SAVES

dns.it_save         = 10000;            % number of iterations between two flow snapshots. It can be changed in the ontherun.in file while tthe simulation is going
dns.it_stat         = 100;              % number of iteration between two 1d flow statistics
dns.it_max          = 100000;           % maximum number of iterations
dns.it_check        = 100;              % number of iterations between two readings of the on the fly adaptation. This can also be changed on the run
dns.it_cfl          = 10;               % how often to compute CFL 
					                    % it can also be set to 0 to stop the simulation after it_check iterations and save the statistics.
dns.maxTime         = 250;              % maximum time before the simulation stops, expressed in seconds


%% Starting Field Input

dns.generateInitialField    = true;                         % do you want to generate the initial field?
dns.fieldToInterpolate      = '../../../../initialFields/riblets200.field';
dns.init_plot               = true;                         % if true, the initial field is plot in all its 3 components


%% Derived Quantities

% WALL-UNITS

dns.nu          = (1/dns.re);                               % kinematic viscosity

if strcmp(dns.type,'CFR')
    tmp.Cf      = 0.073*((2*dns.re)^-(0.25));
    dns.retau   = sqrt(0.5*tmp.Cf)*dns.re;
else
    dns.retau   = dns.re;
end

dns.utau        = dns.retau*dns.nu;
dns.deltani     = dns.nu/dns.utau;
dns.tni         = dns.nu/dns.utau^2;
dns.dt          = (0.01*dns.tni);

index = 0;                                                  % create arrays containing values for lambda, amp; this is here because it is needed before defining laminar domain
if abs(rib.max_beta) ~= rib.max_beta
    error('Negative angle values not allowed!')
end
rib.lambda = [];
while index < length(rib.lambda_plus)
    rib.lambda = [rib.lambda rib.lambda_plus(index+1)*dns.nu/dns.utau];
    if mod(dns.Lx_plus,rib.lambda_plus(index+1)) ~= 0
        error('Incorrect lambda value: lost periodicity along the streamwise direction');
    end
    index = index+1;
    if rib.max_beta(index) < 0
        rib.lambda = rib.lambda(1:end-1);
        final_index = index-1;
        break
    end
end
if isempty(rib.lambda)
    rib.lambda = 0;
    rib.amp = 0;
else
    rib.amp = rib.lambda.*tan(rib.max_beta(1:final_index)*pi/180)/2/pi;
end
rib.wave_n = length(rib.amp);                               % number of wavelength in the geometry


% LAMINAR FLOWS

if dns.couette
     dns.headx 	= 0.0;
     dns.heady 	= 0.0;
end

if dns.laminar
    dns.nonLaminar = 0;
    dns.Ly_plus = rib.s_plus;                               % to simulate one riblet
    if rib.amp == zeros(1,length(rib.amp))
        dns.Lx_plus = 6*dns.dx_plus;                        % to use a smaller domain
    end
end


% GEOMETRY

rib.s_h         = rib.s_plus*dns.nu/dns.utau;               % ratio between riblet spacing and channel height

dns.Lx          = dns.Lx_plus*dns.nu/dns.utau;              % relevant only if dns.type is 'full'
dns.Ly          = dns.Ly_plus*dns.nu/dns.utau;              % relevant only if dns.type is 'full'

%dns.radius      = dns.radius_plus/dns.re;
dns.radius = 0.011;

rib.number      = round(dns.Ly/rib.s_h);
dns.Ly          = rib.number*rib.s_h;                       % to assure periodicity

rib.s_k         = 2*tan(pi*rib.alfa/2/180);                 % ratio between riblet spacing and height
rib.k_h         = rib.s_h/rib.s_k;                          % ratio between riblet and channel height


% COCO VARIABLES

tmp.phirib      = atan(0.5*rib.s_k);                        % semi-angle at the riblet tip
rib.phiwall     = pi - tmp.phirib;                          % angle wrt the horizontal wall
rib.stokdisp    = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);
options         = optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);
rib.stokexp     = fsolve( @(x) rib.stokdisp(x,rib.phiwall), 0.51, options); % Stokes eq. coefficient to evaluate the corner correction

if strcmp(dns.useCoco,'yes')
    dns.coco = true;                                        % corner correction activated
else
    dns.coco = false;
end

if strcmp(dns.extra_diag,'yes')
    dns.extra = true;                                       % extra diagonal terms considered
else
    dns.extra = false;
end


% COEFFICIENTS TO FIND TIP ANGLE FOR SINUSOIDAL RIBLETS

coeff.beta = zeros(10000,1);
coeff.phi = zeros(10000,1);

coeff.findEdge = @(x,xp,yp,A,lambda,ae) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-ae);
coeff.findValley = @(x,xp,yp,A,lambda,av) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-av);
options = optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);

coeff.A = 1.1677582641186;
coeff.lambda = 7.5;
coeff.ae = 0.04;
coeff.av = coeff.ae-rib.s_h/2;

for ii = 1:10000

    coeff.xp = coeff.lambda/10000*ii;
    coeff.yp = coeff.A*sin(2*pi/coeff.lambda*coeff.xp)+0.5*(coeff.ae+coeff.av)+(rand-0.5)*2*rib.s_h;
    
    coeff.edgex = fsolve(@(x) coeff.findEdge(x,coeff.xp,coeff.yp,coeff.A,coeff.lambda,coeff.ae), coeff.xp, options); 
    coeff.edgey = coeff.A*sin(2*pi/coeff.lambda*coeff.edgex)+coeff.ae;

    coeff.vallx = fsolve( @(x) coeff.findValley(x,coeff.xp,coeff.yp,coeff.A,coeff.lambda,coeff.av), coeff.xp, options);
    coeff.vally = coeff.A*sin(2*pi/coeff.lambda*coeff.edgex)+coeff.av;

    coeff.beta(ii) = atan((coeff.edgex-coeff.vallx)/(coeff.vally-coeff.edgey));
    coeff.phi(ii) = 2*atan(abs(coeff.edgey-coeff.vally)/cos(coeff.beta(ii))/rib.k_h);

end

coeff.coeff_phi = polyfit(coeff.beta,coeff.phi,6);


% COEFFICIENTS TO FIND STOKES EXPONENT FOR SINUSOIDAL RIBLETS

coeff.input_angle = linspace(10*pi/180,110*pi/180,10000);
coeff.stokesexp = zeros(1,10000);

coeff.stokdisp = @(x,phiwall) sin(2*x*phiwall)+x*sin(2*phiwall);

for ii = 1:10000
    coeff.phiwall = pi-0.5*coeff.input_angle(ii);
    coeff.stokesexp(ii) = fsolve( @(x) coeff.stokdisp(x,coeff.phiwall), 0.51, options);
end

coeff.coeff_gamma = polyfit(coeff.input_angle,coeff.stokesexp,6);


% GRID IN PERIODIC DIRECTIONS

dns.zcp         = 400;                                      % height up to which you want an healthy mean velocity profile (minimal domain)

if strcmp(dns.domain,'minimal')
    dns.zcp 	= max([4*rib.s_plus/rib.s_k, 100]);
    dns.Ly_plus = ceil(max([0.5*rib.s_plus/0.4, dns.zcp/0.4])/rib.s_plus)*rib.s_plus;   % Spanwise size of the box in wall units.
                                                                                        % Criteria according to (eq. 2.3) of Endrikat et al (2021), JFM.
									                                                    % Rounded so to have an integer number of riblets in the box.
    dns.Lx_plus = max([1000, 3*dns.Ly_plus]);               % streamwise size of the box in wall units: criteria according to (eq. 2.3) of Endrikat et al (2021), JFM
    dns.Ly      = dns.Ly_plus*dns.deltani;                  % spanwise size of the box as fraction of h
    dns.Lx      = dns.Lx_plus*dns.deltani;                  % streamwise size of the box as fraction of h
end 

rib.nrib        = dns.Ly_plus/rib.s_plus;                   % number of riblets in the box
dns.nx          = ceil(dns.Lx_plus/dns.dx_plus);            % number of points in the streamwise direction
dns.nx          = dns.nx+mod(dns.nx,2);                     % even number of points correction
dns.ny          = ceil(max([rib.nrib*rib.nppr, dns.Ly_plus/dns.dy_plus])); % nunmber of points in the spanwise direction
dns.ny          = dns.ny+mod(dns.ny,2);                     % even number of points correction
dns.dy_plus 	= dns.Ly_plus/dns.ny;                       % rescaled delta_y given the lenght
dns.dx_plus 	= dns.Lx_plus/dns.nx;                       % rescaled delta_x given the lenght
dns.dzmin       = dns.dy_plus/dns.cellAR;                   % minimum resolution in the wall-normal direction in wall units


% Z-DIRECTION STRETCHING

% The wall-normal grid will go from -k/2 to 1, so that the 
% meltdown height is exactly 1. For the wall-normal grid I use an
% equispaced grid until the riblet tip and then a stretching through a
% geometric series. The stretching ratio will be slightly adjusted, so that
% we get an integer number of points in the wall-normal direction. All
% support variable I need for the grid are saved in the structure tmp,
% which I will delete at the end.

rib.k_h_splus = 0.065/rib.s_k;

tmp.nz_rib 	= ceil(2*rib.k_h_splus*dns.retau/dns.dzmin);

if mod(tmp.nz_rib,2) == 0
	tmp.nz_rib = tmp.nz_rib + 1;
end
dns.nz_rib = tmp.nz_rib;
dns.dzmin = (2*dns.retau*(rib.k_h_splus - 1/dns.retau*(0.15))/tmp.nz_rib);              % modify dz_min with this parameter multiplying retau
dns.cellAR = dns.dy_plus/dns.dzmin;

tmp.z0 		= -rib.k_h_splus/2+(tmp.nz_rib*dns.dzmin*0.5)/dns.retau;
tmp.z1 		= 1;
tmp.L 	= (tmp.z1-tmp.z0)*dns.retau;
tmp.dzmax = 0.5*dns.dzmin*dns.zstretch;
tmp.beta = (tmp.L-0.5*dns.dzmin)/(tmp.L-tmp.dzmax);
tmp.n = ceil(1+log(dns.zstretch)/log(tmp.beta));
tmp.n = tmp.n + mod(tmp.n + tmp.nz_rib,2);
tmp.fun = @(x,n,L,dzmin) n-1-log(x)/log((L-dzmin)/(L-dzmin*x));
tmp.r = fsolve( @(x) tmp.fun(x,tmp.n-1,tmp.L,0.5*dns.dzmin), dns.zstretch,options);
tmp.beta = tmp.r^(1/(tmp.n-2));
tmp.nz = tmp.nz_rib+tmp.n;              
dns.dzmax = dns.dzmin*tmp.r;                                                            % maximum grid spacing in the wall-normal direction

if dns.walls == 1
    dns.zd = zeros(tmp.nz+1,1);                                                         % grid in the wall-normal direction (all nodes)
    for i=0:tmp.nz_rib
        dns.zd(i+1)=(0.5*i*dns.dzmin-rib.k_h_splus/2*dns.retau)/dns.retau;
    end
    for i=tmp.nz_rib+1:tmp.nz
        j=i-tmp.nz_rib-1;
        tmp.d = (0.5*dns.dzmin)*tmp.beta^j;
        dns.zd(i+1)=dns.zd(i)+tmp.d/dns.retau;
    end
    dns.zd(end-1)=1.0;
    dns.zd(end)=dns.zd(end-1)+(dns.zd(end-1)-dns.zd(end-2));

    dns.zc = dns.zd(1:2:end);                                                           % grid in wall-normal direction (w nodes) (io penso siano quelli di u, v)
    dns.nz = numel(dns.zc)-1;                                                           % number of points in wall-normal direction

else

    dns.zd = zeros(2*tmp.nz-1,1);                                                       % grid in the wall-normal direction (all nodes)
    for i = 0:tmp.nz_rib
        dns.zd(i+1) = (0.5*i*dns.dzmin-rib.k_h_splus/2*dns.retau)/dns.retau;
        dns.zd(end-i) = (2*dns.retau-0.5*i*dns.dzmin+rib.k_h_splus/2*dns.retau)/dns.retau;
    end
    for i=tmp.nz_rib+1:tmp.nz
        j=i-tmp.nz_rib-1;
        tmp.d = (0.5*dns.dzmin)*tmp.beta^j;
        dns.zd(i+1)=dns.zd(i)+tmp.d/dns.retau;
        dns.zd(end-i)=dns.zd(end-i+1)-tmp.d/dns.retau;
    end

    dns.zc = dns.zd(1:2:end);                                                           % grid in wall-normal direction (w nodes)
    dns.nz = numel(dns.zc)-1;                                                           % number of points in wall-normal direction

end


%% Echo
% --------------------------
% clear tmp i j options
clear i j options
disp(' ')
disp('Flow parameters')
disp(['    nx = ' num2str(dns.nx)])
disp(['    ny = ' num2str(dns.ny)])
disp(['    nz = ' num2str(dns.nz)])
disp(['    nu = ' num2str(1/dns.retau)])
disp(['    Lx = ' num2str(dns.Lx) '  (Lx+ = ' num2str(dns.Lx_plus) ')'])
disp(['    Ly = ' num2str(dns.Ly) '  (Ly+ = ' num2str(dns.Ly_plus) ')'])
disp(['    Lz = ' num2str(dns.zd(end))])
disp(['    dx+ = ' num2str(dns.dx_plus)])
disp(['    dy+ = ' num2str(dns.dy_plus)])
disp(['    dz+min = ' num2str(min(abs(diff(dns.zc))*dns.retau))])
disp(['    dz+max = ' num2str(max(diff(dns.zc)*dns.retau))])
disp(['    deltat = ' num2str(dns.dt)])
disp(['    dt+ = ' num2str(dns.dt*dns.retau)])
disp(['    headx = ' num2str(dns.headx)])
disp(['    heady = ' num2str(dns.heady)])
disp(['    AR = ' num2str(dns.cellAR)])
disp(['    walls = ' num2str(dns.walls)])
disp(' ')
disp('Riblets parameters')
disp(['    alfa = ' num2str(rib.alfa) '°'])
disp(['    k/s = ' num2str(1/rib.s_k)])
disp(['    s/h = ' num2str(rib.s_h) '  (s+ = ' num2str(rib.s_plus) ')'])
disp(['    stokexp = ' num2str(rib.stokexp) '  (err = ' num2str(rib.stokdisp(rib.stokexp,rib.phiwall)) ')'])
disp(['    nppr = ' num2str(rib.s_plus/dns.dy_plus)])


%% Create initial condition
% --------------------------
if dns.generateInitialField
  disp(' ')
  disp('Generating initial field')
  field=fieldGenerator('../fields/riblets.field',dns.fieldToInterpolate,dns);
end


%% Create initial parameter file
% --------------------------
channel = 1;
f=fopen('../input/riblets.in','w+');
fprintf(f,'non_laminar = %d\t\t nu = %f\t\t headx = %f\t\t heady = %f\n\n',dns.nonLaminar, dns.nu, dns.headx, dns.heady);
fprintf(f,'walls = %d\t\t spacing = %f\t ratiohs = %.12f\n',dns.walls, rib.s_h, channel/rib.s_k);
fprintf(f,'waven = %d\n',rib.wave_n);
fprintf(f,'lambda\t= ');
if length(rib.lambda) ~= 1
    fprintf(f,'%f \t',rib.lambda(1:end-1));
    fprintf(f,'%f \n',rib.lambda(end));
else
    fprintf(f,'%f \n',rib.lambda(1));
end
fprintf(f,'ampl\t= ');
if length(rib.lambda) ~= 1
    fprintf(f,'%f \t',rib.amp(1:end-1));
    fprintf(f,'%f \n\n',rib.amp(end));
else
    fprintf(f,'%f \n\n',rib.amp(1));
end
fprintf(f,'use_coco = %d\t\t radius = %f\t stokexp = %.12f\t extra = %d\n',dns.coco, dns.radius, rib.stokexp, dns.extra);
fprintf(f,'phiC\t= ');
fprintf(f,'%f \t',coeff.coeff_phi(1:end-1));
fprintf(f,'%f \n',coeff.coeff_phi(end));
fprintf(f,'gC\t= ');
fprintf(f,'%f \t',coeff.coeff_gamma(1:end-1));
fprintf(f,'%f \n\n',coeff.coeff_gamma(end));
fprintf(f,'nx = %d\t\t ny = %d\t\t nz = %d\t\t\t Lx = %f\t\t Ly = %f\n',dns.nx, dns.ny, dns.nz, dns.Lx, dns.Ly);
fprintf(f,'cflmax = %g\t\t fouriermax = %g\t deltat = %f\t\t maxTime = %g\n',dns.cflmax, dns.fouriermax, dns.dt, dns.maxTime);
fprintf(f,'it_save=%d\t\t it_stat=%d\t\t it_max=%d\t\t\t it_cfl=%d\t\t it_check=%d\n',dns.it_save,dns.it_stat,dns.it_max,dns.it_cfl,dns.it_check);
fprintf(f,'continue=FALSE\n');
fclose(f);
% clear f



%% Tschüß
% ---------------------------
disp(' ')
disp('Remember to check:  ')
disp('   1. that all input parameters are correct and as desired')
disp('   2. that the grid looks fine')
disp('   3. that the definition of the body and edge in riblets.cpl is as intended,')
disp('      in fact it is implemented for triangular riblets at the moment but can easily modified.')
disp('   4. that the HPC batch job scripts match what input in parallelbcs.h')
disp(' ')

disp('Tschüß!')
