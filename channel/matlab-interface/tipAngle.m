clear 
clc
close all

beta1 = zeros(10000,1);
phi1 = zeros(10000,1);
beta2 = zeros(10000,1);
phi2 = zeros(10000,1);
beta3 = zeros(10000,1);
phi3 = zeros(10000,1);
edgex1v = zeros(10000,1);
edgex2v = zeros(10000,1);
edgey1v = zeros(10000,1);
edgey2v = zeros(10000,1);
vallx1v = zeros(10000,1);
vallx2v = zeros(10000,1);
vally1v = zeros(10000,1);
vally2v = zeros(10000,1);


yp_v1 = zeros(10000,1);

findEdge = @(x,xp,yp,A,lambda,ae) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-ae);
findValley = @(x,xp,yp,A,lambda,av) 2*(x-xp)-4*pi/lambda*A*cos(2*pi/lambda*x)*(yp-A*sin(2*pi/lambda*x)-av);
options = optimset('Display','off','TolFun',1e-20,'MaxIter',1e7,'TolX',1e-20);

lg = 8/200;

phi0_1 = 40*pi/180;
phi0_2 = 60*pi/180;
phi0_3 = 80*pi/180;

h1 = lg/sqrt(tan(phi0_1/2));
s1 = 2*lg*sqrt(tan(phi0_1/2));
h2 = lg/sqrt(tan(phi0_2/2));
s2 = 2*lg*sqrt(tan(phi0_2/2));
h3 = lg/sqrt(tan(phi0_3/2));
s3 = 2*lg*sqrt(tan(phi0_3/2));

A = 1.1677582641186;
lambda = 7.5;
ae1 = 0.04;
av1 = ae1-s1/2;
ae2 = 0.04;
av2 = ae2-s2/2;
ae3 = 0.04;
av3 = ae3-s3/2;

for ii = 1:10000

    xp = lambda/10000*ii;
    yp1 = A*sin(2*pi/lambda*xp)+0.5*(ae1+av1)+(rand-0.5)*2*s1;
    yp_v1(ii) = yp1;
    yp2 = A*sin(2*pi/lambda*xp)+0.5*(ae2+av2)+(rand-0.5)*2*s2;
    yp3 = A*sin(2*pi/lambda*xp)+0.5*(ae3+av3)+(rand-0.5)*2*s3;
    
    edgex1 = fsolve(@(x) findEdge(x,xp,yp1,A,lambda,ae1), xp, options); 
    edgey1 = A*sin(2*pi/lambda*edgex1)+ae1;
    edgex2 = fsolve(@(x) findEdge(x,xp,yp2,A,lambda,ae2), xp, options); 
    edgey2 = A*sin(2*pi/lambda*edgex2)+ae2;
    edgex3 = fsolve(@(x) findEdge(x,xp,yp3,A,lambda,ae3), xp, options); 
    edgey3 = A*sin(2*pi/lambda*edgex3)+ae3;
    edgex1v(ii) = edgex1;
    edgex2v(ii) = edgex2;
    edgey1v(ii) = edgey1;
    edgey2v(ii) = edgey2;

    vallx1 = fsolve( @(x) findValley(x,xp,yp1,A,lambda,av1), xp, options);
    vally1 = A*sin(2*pi/lambda*edgex1)+av1;
    vallx2 = fsolve( @(x) findValley(x,xp,yp2,A,lambda,av2), xp, options);
    vally2 = A*sin(2*pi/lambda*edgex2)+av2;
    vallx3 = fsolve( @(x) findValley(x,xp,yp3,A,lambda,av3), xp, options);
    vally3 = A*sin(2*pi/lambda*edgex3)+av3;
    vallx1v(ii) = vallx1;
    vallx2v(ii) = vallx2;
    vally1v(ii) = vally1;
    vally2v(ii) = vally2;

    beta1(ii)        = atan((edgex1-vallx1)/(vally1-edgey1));
    phi1(ii)         = 2*atan(abs(edgey1-vally1)/cos(beta1(ii))/h1);
    beta2(ii)        = atan((edgex2-vallx2)/(vally2-edgey2));
    phi2(ii)         = 2*atan(abs(edgey2-vally2)/cos(beta2(ii))/h2);
    beta3(ii)        = atan((edgex3-vallx3)/(vally3-edgey3));
    phi3(ii)         = 2*atan(abs(edgey3-vally3)/cos(beta3(ii))/h3);
% 
%     if ii == 2500
%         xp
%         yp2
%         edgex2
%         edgey2
%         vallx2
%         vally2
%         beta2(ii)
%         phi2(ii)
%     end

end

beta_range1 = linspace(min(beta1),max(beta1),10000);
beta_range2 = linspace(min(beta2),max(beta2),10000);
beta_range3 = linspace(min(beta3),max(beta3),10000);

coeff_phi1 = polyfit(beta1,phi1,6);
coeff_phi2 = polyfit(beta2,phi2,6);
coeff_phi3 = polyfit(beta3,phi3,6);
phi_fit1 = polyval(coeff_phi1,beta_range1);
phi_fit2 = polyval(coeff_phi2,beta_range2);
phi_fit3 = polyval(coeff_phi3,beta_range3);

figure() 
hold on; grid on
plot(beta1,(phi1-phi0_1)*180/pi,'b*')
plot(beta_range1,(phi_fit1-phi0_1)*180/pi,'g')
plot(beta2,(phi2-phi0_2)*180/pi,'k*')
plot(beta_range2,(phi_fit2-phi0_2)*180/pi,'r')
plot(beta3,(phi3-phi0_3)*180/pi,'y*')
plot(beta_range3,(phi_fit3-phi0_3)*180/pi,'k')

xxx = linspace(0,lambda,10000);
xp_v = linspace(0,lambda,10000);

figure(); hold on; grid on
plot(phi0_1*180/pi,coeff_phi1(1),'*')
plot(phi0_2*180/pi,coeff_phi2(1),'*')
plot(phi0_3*180/pi,coeff_phi3(1),'*')

figure(); hold on; grid on
plot(phi0_1*180/pi,coeff_phi1(3),'*')
plot(phi0_2*180/pi,coeff_phi2(3),'*')
plot(phi0_3*180/pi,coeff_phi3(3),'*')

figure(); hold on; grid on
plot(phi0_1*180/pi,coeff_phi1(5),'*')
plot(phi0_2*180/pi,coeff_phi2(5),'*')
plot(phi0_3*180/pi,coeff_phi3(5),'*')

figure(); hold on; grid on
plot(phi0_1*180/pi,coeff_phi1(7),'*')
plot(phi0_2*180/pi,coeff_phi2(7),'*')
plot(phi0_3*180/pi,coeff_phi3(7),'*')

% figure() 
% hold on; grid on
% plot(xxx,A*sin(2*pi/lambda*xxx)+ae1)
% plot(xp_v,yp_v1,'*')
% 
% figure() 
% hold on; grid on
% plot(edgey1v-vally1v)
% plot(edgey2v-vally2v)




%%

gamma = zeros(1,length(beta_range));
coeff_phi = polyfit(beta1,phi1,6);
phi_fit = polyval(coeff_phi,beta_range);

stk_coeff = [-4.80373496142125e-06 0.000497194575060760 0.000421759877989030 0.00946215921980583 0.000268762967067000 -6.98982939129716e-05 0.500006626183304];

for ii = 1:length(beta_range)
    gamma(ii) = stk_coeff(1)*phi_fit(ii)^6+stk_coeff(2)*phi_fit(ii)^5+stk_coeff(3)*phi_fit(ii)^4+stk_coeff(4)*phi_fit(ii)^3+stk_coeff(5)*phi_fit(ii)^2+stk_coeff(6)*phi_fit(ii)+stk_coeff(7);
end


figure(1) 
hold on; grid on
plot(beta_range,gamma)
% plot(beta_range,phi_fit2,'b')
% plot(beta_range,phi_fit4,'c')
% % plot(beta_range,phi_fit4,'m')


