close all
clear
clc

addpath('/home/ws/og2387/vtkwrite/')

nx = 10;
ny = 8;
nz = 6;

x = linspace(0,2,10);
y = linspace(0,1,8);
z = linspace(0,0.5,6);

[X,Y,Z] = meshgrid(y,x,z);

campo = zeros(nx,ny,nz);

campo(:,:,1) = 1;
campo(:,:,2) = 2;
campo(:,:,3) = 3;
campo(:,:,4) = 4;
campo(:,:,5) = 5;
campo(:,:,6) = 6;

vtkwrite('prova.vtk','structured_grid',X,Y,Z,'scalars','lambda',campo);