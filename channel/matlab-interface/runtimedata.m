clear
close all
clc

M = readmatrix('../output/runtimedata.txt');
time = M(:,3);
Ub = M(:,6);
% tresh = 0.001*ones(length(Ub),1);


figure(1)
plot(time,Ub)

Cf = 2./(Ub/2).^2;


iniz = 130000;
writematrix(Cf(iniz:end), "Cf.txt");


Ub_avg = sum(Ub(iniz:end))/length(Ub(iniz:end));
Cf_avg = sum(Cf(iniz:end))/length(Ub(iniz:end));

err = (Cf_avg-0.007797872024888)/0.007797872024888*100;


