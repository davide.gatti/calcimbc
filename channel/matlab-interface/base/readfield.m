function [field,dns]=readfield(filename)

i = fopen(filename,'r');
dns.nx = fread(i,1,'int');
dns.ny = fread(i,1,'int');
dns.nz = fread(i,1,'int');
dns.it = fread(i,1,'int');
dns.Lx = fread(i,1,'double');
dns.Ly = fread(i,1,'double');
dns.headx = fread(i,1,'double');
dns.heady = fread(i,1,'double');
dns.nu = fread(i,1,'double');
dns.dt = fread(i,1,'double');
dns.zd=fread(i, 2*dns.nz+1, 'double');
dns.zc=dns.zd(1:2:end);
dns.x=dns.Lx*(0:dns.nx-1)/dns.nx;
dns.y=dns.Ly*(0:dns.ny-1)/dns.ny;
field=reshape(fread(i, 'double'), [4, dns.nz+1, dns.ny, dns.nx]);
