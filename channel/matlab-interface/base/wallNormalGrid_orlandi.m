function dns = wallNormalGrid_orlandi(dns)

tmp.alfa = dns.alfa;    % orlandi 1.5,  it defines the centerline resolution
tmp.cnu = 0.8;          
tmp.jb = dns.jb;        % orlandi 16,   it defines the blending position 
tmp.ywp = dns.ywp;      % 

% Create grid for (u,v,p) up to centreline in viscous units
zp = []; cont=true; j=0;
while  cont 
    zp = [zp, 1/(1+(j/tmp.jb)^2)*(tmp.ywp*j+((3*tmp.alfa*tmp.cnu*j/4)^(4/3))*((j/tmp.jb)^2) )];
    if zp(end)>=dns.retau
        cont=false;
    end
    j = j+1;
end
zp = zp-zp(1); z = zp/zp(end)*(1+0.5*dns.meanK); z = z-dns.meanK*0.5;
tmp.dd = zp(end)-zp(end-1);
z = zp*2/(2-tmp.dd);

% When you have two walls, mirror grid
if dns.walls==2
    z = [z, 2-z(end-1:-1:1)];
end


% Find zd (the grid that contains both the points for u,v,p and for w)
zd = [];
for i = 1:numel(z)
    if i==1 
      zd = [zd, z(i)];
    else
      zd = [zd, 0.5*(z(i)+z(i-1)), z(i)];
    end
end

% Check that zd has an odd number of elements (it always goes from 0 to 2*nz)
if mod(numel(zd),2)==0; error('The number of element in dns.zd must be odd!'); end

% Output
dns.zd = zd;
dns.zc = z;
dns.nz = numel(dns.zc)-1;               % number of points in wall-normal direction
dns.dzmin = (z(2)-z(1))*dns.retau;
dns.dzmax = (max(diff(z)))*dns.retau;
dns.zstretch = dns.dzmax/dns.dzmin;
