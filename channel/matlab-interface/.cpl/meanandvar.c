/*-Dstandalone */

#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#define _LARGE_FILES
#define printERR fprintf(stderr,"\r%s: PROGRAM HALTED  \n",errormessage);fflush(stderr)

/* #define __NO_INLINE__ ! why was it here ? */

#include <unistd.h>

#include <stdlib.h>
/*** typedef _Complex float __cfloat128 __attribute__ ((__mode__ (__TC__))) ununderstood ***/
/*** typedef __float128 _Float128 ununderstood ***/
/*** extern _Float128 strtof128 (const char *restrict __nptr,
			 char **restrict __endptr)
   ununderstood ***/
/*** extern _Float128 strtof128_l (const char *restrict __nptr,
			 char **restrict __endptr,
			 locale_t __loc)
   ununderstood ***/

#include <stdio.h>

#include <fcntl.h>

#include <math.h>
/*** enum
 {
 FP_INT_UPWARD =

 0,
 FP_INT_DOWNWARD =

 1,
 FP_INT_TOWARDZERO =

 2,
 FP_INT_TONEARESTFROMZERO =

 3,
 FP_INT_TONEAREST =

 4,
 } ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinf (double __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnan (double __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff (float __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf (float __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinfl (long double __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !0
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanl (long double __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff32 (_Float32 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf32 (_Float32 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff64 (_Float64 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf64 (_Float64 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 __atan2f128 (_Float128 __y, _Float128 __x)  ununderstood ***/
/*** extern _Float128 cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cosf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __coshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __tanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __acoshf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __asinhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __atanhf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 __frexpf128 (_Float128 __x, int *__exponent)  ununderstood ***/
/*** extern _Float128 ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 __ldexpf128 (_Float128 __x, int __exponent)  ununderstood ***/
/*** extern _Float128 logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 modff128 (_Float128 __x, _Float128 *__iptr)  ununderstood ***/
/*** extern _Float128 __modff128 (_Float128 __x, _Float128 *__iptr)   ununderstood ***/
/*** extern _Float128 exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp10f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __expm1f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log1pf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __logbf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __exp2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __log2f128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __powf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __sqrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __hypotf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __cbrtf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __ceilf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fabsf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __floorf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fmodf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff128 (_Float128 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __copysignf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 nanf128 (const char *__tagb)  ununderstood ***/
/*** extern _Float128 __nanf128 (const char *__tagb)  ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf128 (_Float128 __value)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __j1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __jnf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y0f128 (_Float128)  ununderstood ***/
/*** extern _Float128 y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 __y1f128 (_Float128)  ununderstood ***/
/*** extern _Float128 ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 __ynf128 (int, _Float128)  ununderstood ***/
/*** extern _Float128 erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erff128 (_Float128)  ununderstood ***/
/*** extern _Float128 erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __erfcf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __lgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 __tgammaf128 (_Float128)  ununderstood ***/
/*** extern _Float128 lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 __lgammaf128_r (_Float128, int *__signgamp)  ununderstood ***/
/*** extern _Float128 rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __rintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __nextafterf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextdownf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nextupf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __remainderf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 __scalbnf128 (_Float128 __x, int __n)  ununderstood ***/
/*** extern _Float128 scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 __scalblnf128 (_Float128 __x, long int __n)  ununderstood ***/
/*** extern _Float128 nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 __nearbyintf128 (_Float128 __x)  ununderstood ***/
/*** extern _Float128 roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __truncf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 __remquof128 (_Float128 __x, _Float128 __y, int *__quo)  ununderstood ***/
/*** extern _Float128 fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 __fdimf128 (_Float128 __x, _Float128 __y)  ununderstood ***/
/*** extern _Float128 fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 __fmaf128 (_Float128 __x, _Float128 __y, _Float128 __z)  ununderstood ***/
/*** extern _Float128 roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __roundevenf128 (_Float128 __x)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fmaxmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 __fminmagf128 (_Float128 __x, _Float128 __y)  __attribute__ ((__const__)) ununderstood ***/
/*** extern _Float128 getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 __getpayloadf128 (const _Float128 *__x)  ununderstood ***/
/*** extern _Float128 scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** extern _Float128 __scalbf128 (_Float128 __x, _Float128 __n)  ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff32x (_Float32x __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf32x (_Float32x __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return 0 if VALUE is finite or NaN, +1 if it
   is +Infinity, -1 if it is -Infinity.  *  /
 extern int isinff64x (_Float64x __value)  __attribute__ ((__const__)) ununderstood ***/
/*** && !1
 /* Return nonzero if VALUE is not a number.  *  /
 extern int isnanf64x (_Float64x __value)  __attribute__ ((__const__)) ununderstood ***/

#include <limits.h>

#include <float.h>

#include <string.h>


#include <time.h>

#include <sys/time.h>

#include <sys/types.h>

#include <sys/stat.h>

#include <setjmp.h>

#include <errno.h>

#include <signal.h>
#ifdef nofenv
  #define feenableexcept(fpe)
  #define feclearexcept(fpe)
#else
  #include <fenv.h>
  #ifdef modfenv
    #include "feenableexceptosx.h"
  #endif
  #define fpe FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO
#endif

char errortemp_[(80+1)];


struct arrptr{int l,h; ssize_t i; char *a;};
struct dynptr{void* p; int t;};
char INTERRUPT=0;
void (*traphandler)(const char *);
struct freefunc{struct freefunc *next; void (*f)(void *); void *ptr;};
struct freefunc* freestack=NULL;
#define freemem(upto) while(freestack!=upto){freestack->f(freestack->ptr); freestack=freestack->next;}
#define atblockexit(name,func,p) name.f=func;name.ptr=p;name.next=freestack;freestack=&name
#define commablockexit(name,func,p) name.f=func,name.ptr=p,name.next=freestack,freestack=&name
void traprestore(void *ptr){traphandler=ptr;}
void condfree(void *ptr){if(*(void **)ptr!=NULL)free(*(void **)ptr);}
int friexecerror(char** s){
  fprintf(stderr,"\rprogram must be run under icpl\n");
  sleep(1);
  return 0;
}
int (*friexec)(char** s)=friexecerror;

int dynptrerr(int type){
  snprintf(errortemp_,sizeof(errortemp_),"Argument of incompatible TYPE %d",type);
  traphandler(errortemp_);
  return 0;
}
void *errmalloc(void){
  traphandler("Not enough memory");
  return NULL;
}
void ioerr(FILE *fil){
  char where[11];
  off_t cn;
  int fn;
  fn=fileno(fil);
  cn=ftello(fil);
  if(ferror(fil)){
    snprintf(errortemp_,sizeof(errortemp_),"%s in fd %d char %ld",strerror(errno),fn,(long)cn);
  } else if(feof(fil)){
    snprintf(errortemp_,sizeof(errortemp_),"End of file in fd %d char %ld",fn,(long)cn);
  } else {
    if(!(cn>=0 && fscanf(fil,"%10s",where)>0)) where[0]=0;
    snprintf(errortemp_,sizeof(errortemp_),"Unrecognized input \"%s\" from fd %d char %ld",where,fn,(long)cn);
  }
  traphandler(errortemp_);
}
void errfclose(void *voidf){
  FILE **f=(FILE **)voidf;
  int fn;
  if(*f==NULL)return;
  fn=fileno(*f);
  if(fn>=0 && ftell(*f)>=0 && fclose(*f)!=0){
    snprintf(errortemp_,sizeof(errortemp_),"Error in closing fd %d: %s",fn,strerror(errno));
    traphandler(errortemp_);
    }
  *f=NULL;
}
void errfopen(FILE **f, const char *name, int mode){
  int fd;
/*  if(*f)errfclose(f);  causes segfault if used on copies of closed FILE descriptors */
  fd=open(name,mode,0666);
  if(fd>0 || mode==O_RDONLY){
    *f=fdopen(fd,mode==O_RDONLY?"r":"r+");
    if(*f!=NULL || mode==O_RDONLY)return;
    } 
  snprintf(errortemp_,sizeof(errortemp_),"Error in opening %s: %s",name,strerror(errno));
  traphandler(errortemp_);
}
int scanrec(FILE *f, const char *format, void *var) {
  off_t pos; int res;
  register int c;
  if (friexec!=friexecerror && isatty(fileno(f))){
    char *s="\357\201i\362\371\376";
    if (friexec(&s) && sscanf(s,format,var)==1) return 1;
  }
  pos=ftello(f);
  while((c=getc(f))<=' '&&(c!='\n')&&(c!=EOF)){};
  if(c!='\n')ungetc(c,f);
  while((c=getc(f))=='!') {while(!feof(f)&&getc(f)!='\n'){};};
  ungetc(c,f);
  res=fscanf(f,format,var);
  if (res<=0 && var!=NULL && pos>=0) fseeko(f,pos,SEEK_SET);
  return res;
}
int scanbool(FILE *f, const char *format, int *var) {
  char c;
  return scanrec(f,format,&c)&&((*var=(c=='T')||(c=='Y')||(c=='t')||(c=='y'))||(c=='F')||(c=='N')||(c=='f')||(c=='n'));
  }
int myfgets(char *name, char *var, char *varend, FILE *f) {
  off_t pos;
  register int c;
  pos=ftello(f);
  if(feof(f))return 0;
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  if(var<varend){
    do{c=getc(f); *var=c;}while(c!=EOF && c!='\n' && ++var < varend);
    *var=0;
  }
  return 1;
}
int mygetline(char *name, char **var, FILE *f) {
  off_t pos;
  int c,oldc;
  pos=ftello(f);
  while(*name!=0){
    if(*name==' '){while((c=getc(f))<=' '){};ungetc(c,f);} else
    if(*name!=getc(f)){fseeko(f,pos,SEEK_SET);return 0;};
    name++;
  };
  c=32; oldc=0;
  while(1){
  *var=realloc(*var,c);
  if (fgets(*var+oldc,c-oldc,f)==NULL) {fseeko(f,pos,SEEK_SET);return 0;};
  {char* c1=memchr(*var+oldc,'\n',c-oldc-1); if(c1) {*c1=0; return 1;}}
  oldc=c-1; c=2*c;
  }
  /*
  c=0;
  getline(var,&c,f);
  {char* c1=strchr(*var,'\n'); if(c1) *c1=0;}
  return 1;
  */
}
/* Note: fetestexcept always returns zero. Detecting the type of floating point
   exception requires testing the si_code field of sig_info (man sigaction) */
void trapsignal(int signum, siginfo_t *info, void *ucontext){
  feclearexcept(FE_ALL_EXCEPT);
  feenableexcept(fpe);
  if(signum==SIGINT) traphandler(&INTERRUPT);
  else if (signum==SIGFPE){
    switch(info->si_code){
    case FPE_INTDIV: traphandler("*** ERROR: integer division by zero");
    case FPE_FLTDIV: traphandler("*** ERROR: floating-point division by zero");
    case FPE_FLTOVF: traphandler("*** ERROR: floating-point overflow");
    default: traphandler("*** ERROR: invalid or unassigned floating-point value");
    }
  }
  else traphandler(strsignal(signum));
}
#if (defined __i386__ || defined __x86_64__)
#define mmovefrom(var,buf,type) *(type *)(buf)=*var
#define mmoveto(var,buf,type) *var=*(type *)(buf)
#else
#define mmovefrom(var,buf,type) memmove(buf,var,sizeof(type))
#define mmoveto(var,buf,type) memmove(var,buf,sizeof(type))
#endif
#define mainstart \
void default_traphandler(const char *errormessage){ \
  if(errormessage[0]){ \
    printERR; \
    freemem(NULL); \
    exit(EXIT_FAILURE); \
  }else{ \
    freemem(NULL); \
    exit(EXIT_SUCCESS); \
  } \
} \
int main(int argc, char **argv){ \
struct freefunc* es; \
			\
{struct sigaction act,oldact; \
act.sa_sigaction=trapsignal; \
sigemptyset(&act.sa_mask); \
act.sa_flags=SA_RESTART|SA_SIGINFO; \
sigaction(SIGSEGV,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGSEGV,&oldact,NULL); \
sigaction(SIGFPE,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGFPE,&oldact,NULL); \
sigaction(SIGILL,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGILL,&oldact,NULL); \
sigaction(SIGINT,&act,&oldact); \
if (oldact.sa_handler!=SIG_DFL)sigaction(SIGINT,&oldact,NULL); \
/* \
{void (*sig)(int); \
if((sig=signal(SIGSEGV,trapsignal))!=SIG_DFL)signal(SIGSEGV,sig); \
if((sig=signal(SIGFPE,trapsignal))!=SIG_DFL)signal(SIGFPE,sig); \
if((sig=signal(SIGILL,trapsignal))!=SIG_DFL)signal(SIGILL,sig); \
if((sig=signal(SIGINT,trapsignal))!=SIG_DFL)signal(SIGINT,sig); \
*/ \
else {traphandler=default_traphandler; \
      freestack=NULL; \
      feenableexcept(fpe); \
     }; \
} \
es=freestack;






/* INTEGER LIBRARY FUNCTION INTEGER[(int)rint](REAL x) */
/* INTEGER LIBRARY FUNCTION int[(int)](REAL x) */





/* to-do list
1) modificare STRUCTURED ARRAY in modo da evitare malloc quando possibile
2) separare il #define CPL da quello C
*/
/* compile with -Dstandalone for standalone version */

/* meanandvar -- Copyright 2015 Paolo Luchini */
/* http://CPLcode.net/Applications/Numerical/MeanandVar */

/* Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

int autots_;


struct MEANANDVAR_{double mean_;char bm_[(ssize_t)sizeof(double)*(1+1)];char bt_[(ssize_t)sizeof(double)*(1+1)];int nt_;int bs_;int ts_;};
#define MEANANDVAR_s (ssize_t)sizeof(struct MEANANDVAR_)


void meanandvar_1mvinit(struct MEANANDVAR_ *mvp_) {{struct freefunc* es=freestack;{
  (*mvp_).mean_=0.;  memset((*mvp_).bm_,0,(ssize_t)sizeof(double)*(1+1));  memset((*mvp_).bt_,0,(ssize_t)sizeof(double)*(1+1));
  (*mvp_).nt_=0;  (*mvp_).bs_=0;  (*mvp_).ts_=1;
}}}

void meanandvar_2meanandvar(struct MEANANDVAR_ *mvp_,double xx_) {{struct freefunc* es=freestack;{
  (*(double *)((*mvp_).bm_))=xx_-(*mvp_).mean_+(*(double *)((*mvp_).bm_));
  (*mvp_).bs_+=1;
  if( (*mvp_).bs_>=(*mvp_).ts_ ){
    (*mvp_).nt_+=(*mvp_).bs_;
    (*mvp_).mean_+=(*(double *)((*mvp_).bm_))/(double)((*mvp_).nt_);
    (*(double *)((ssize_t)sizeof(double)+(*mvp_).bt_))+=(*(double *)((*mvp_).bm_))*(*(double *)((ssize_t)sizeof(double)+(*mvp_).bm_));
    (*(double *)((ssize_t)sizeof(double)+(*mvp_).bm_))=(*(double *)((*mvp_).bm_))*(1.-(double)((*mvp_).bs_)/(double)((*mvp_).nt_));
    (*(double *)((*mvp_).bt_))+=(*(double *)((*mvp_).bm_))*(*(double *)((ssize_t)sizeof(double)+(*mvp_).bm_));
    if( (((*(double *)((ssize_t)sizeof(double)+(*mvp_).bt_))>0.5*(*(double *)((*mvp_).bt_) ))&&( autots_ ))&&( (*mvp_).nt_>(*mvp_).bs_ )){ (*mvp_).ts_=(int)ceil(1.05*(double)((*mvp_).ts_));};
    (*mvp_).bs_=0;
    (*(double *)((*mvp_).bm_))=0.;
  };
}}}

double meanandvar_3var(struct MEANANDVAR_ *mvp_) {{struct freefunc* es=freestack;double RESULT_;{
    if( (*mvp_).nt_-3*(*mvp_).ts_<=0 ){ RESULT_=0.;return RESULT_;};
    RESULT_=((*(double *)((*mvp_).bt_))+2.*(*(double *)((ssize_t)sizeof(double)+(*mvp_).bt_)))/(double)((*mvp_).nt_)/(double)(((*mvp_).nt_-3*(*mvp_).ts_));
    if( (*(double *)((ssize_t)sizeof(double)+(*mvp_).bt_))>0.){ RESULT_*=(1.+0.234*pow((2.*(*(double *)((ssize_t)sizeof(double)+(*mvp_).bt_))/(*(double *)((*mvp_).bt_))),3.17));};
}return RESULT_;}}

double meanandvar_4rms(struct MEANANDVAR_ *mvp_){struct freefunc* es=freestack;return ( meanandvar_3var(mvp_)>0. ?sqrt(meanandvar_3var(mvp_))  :0.);}
double meanandvar_5sum(struct MEANANDVAR_ *mvp_){struct freefunc* es=freestack;return (*mvp_).mean_*(double)((*mvp_).nt_);}
double meanandvar_6sumvar(struct MEANANDVAR_ *mvp_){struct freefunc* es=freestack;return meanandvar_3var(mvp_)*(double)(((*mvp_).nt_*(*mvp_).nt_));}
double meanandvar_7sumrms(struct MEANANDVAR_ *mvp_){struct freefunc* es=freestack;return meanandvar_4rms(mvp_)*(double)((*mvp_).nt_);}



int startcolumn_;
int numcolumn_;

int meanandvar_10skip;
int meanandvar_11h;
ssize_t meanandvar_12i;
struct freefunc mv_f;char *mv_;

double xx_;


mainstart
/*TEST=0*/
autots_=1;startcolumn_=1;numcolumn_=1;if( (argc-1)>=1 ){
  ssize_t _8i;
char *_9mpos;
_8i=((int)(strlen((*(char**)((ssize_t)sizeof(char*)+(char*)argv)))-1)+1);
if( strncmp((*(char **)((ssize_t)sizeof(char*)+(char*)argv))+0,"-h" -(0)+0,_8i)==0){
     fprintf(stdout,"%s""\n","    \n"
"Usage: meanandvar [-h] [col[-tocol]] [skip] [bsize]\n"
"\n"
"Estimates the mean and the standard deviation of the estimate of the mean from\n"
"finite time series extracted from a stationary stochastic process. The process\'\n"
"correlation function is assumed to be stationary and integrable but otherwise\n"
"unspecified. The estimate of the standard deviation is obtained by an unbiased\n"
"method of batch means with internally calculated adaptive batch size.\n"
"\n"
"Input is accepted from stdin in a multicolumn format, possibly prefixed by\n"
"comment lines starting with \"#\". Each column is assumed to be a separate time\n"
"series. The first commandline parameter, if present, specifies the column, or\n"
"range of columns, to be operated on (default: 1). The second parameter, if\n"
"present, is a number of lines to be skipped at the beginning of the file\n"
"(default: 0). The third parameter, if present, is a fixed batch size to be used\n"
"(default: adaptive).\n"
"\n"
"Output is its estimate of the mean and standard deviation for each selected\n"
"column, one per line. A final line contains the total number of samples used\n"
"for the estimate and the automatic batch size (roughly proportional\n"
"to correlation time). The number of samples used may differ from the number of\n"
"samples received by the size of the last partially filled batch.\n"
"");
    traphandler("");
  };
  startcolumn_=atoi((*(char**)((ssize_t)sizeof(char*)+(char*)argv)));
  _9mpos=strchr((*(char**)((ssize_t)sizeof(char*)+(char*)argv)),(int)(unsigned char)'-');
 
  if( _9mpos!=NULL ){
    numcolumn_=atoi((1+(char*)_9mpos))-startcolumn_+1;
  };
};
meanandvar_10skip=( (argc-1)>=2  ?atoi((*(char**)(2*(ssize_t)sizeof(char*)+(char*)argv)))  :0);
 
meanandvar_11h=numcolumn_;
meanandvar_12i=MEANANDVAR_s*meanandvar_11h;

mv_=malloc(meanandvar_12i);if(mv_==NULL)mv_=errmalloc();mv_-=MEANANDVAR_s;atblockexit(mv_f,free,mv_+MEANANDVAR_s);memset(MEANANDVAR_s+mv_,0,meanandvar_12i);  {int i_=1;do{{ meanandvar_1mvinit((struct MEANANDVAR_ *)(i_*MEANANDVAR_s+mv_)) ;}i_++;}while(i_<=meanandvar_11h);}
if( (argc-1)>=3 ){ (*(struct MEANANDVAR_ *)(MEANANDVAR_s+mv_)).ts_=atoi((*(char**)(3*(ssize_t)sizeof(char*)+(char*)argv))); autots_=0;};
while( ungetc(getc(stdin),stdin)==(int)(unsigned char)'#'){ while(!feof(stdin)&&getc(stdin)!='\n'){};}
 {int i_=1  ;while(i_<=meanandvar_10skip){ while(!feof(stdin)&&getc(stdin)!='\n'){};i_+=1;}}
while(  (scanrec(stdin," %lg",&xx_)>0)){
   {int i_=2  ;while(i_<=startcolumn_){  if(!(scanrec(stdin," %lg",&xx_)>0))ioerr(stdin);i_+=1;}}
   {int col_=1  ;while(col_<=numcolumn_){
    meanandvar_2meanandvar((struct MEANANDVAR_ *)(col_*MEANANDVAR_s+mv_),xx_);
    if( col_<numcolumn_ ){  if(!(scanrec(stdin," %lg",&xx_)>0))ioerr(stdin);};
  col_+=1;}}
  while(!feof(stdin)&&getc(stdin)!='\n'){};
  if( (*(struct MEANANDVAR_ *)(MEANANDVAR_s+mv_)).bs_==0 ){
    {
    struct MEANANDVAR_ *_13w;
_13w=(struct MEANANDVAR_ *)(MEANANDVAR_s+mv_);
if( (0 )&&( (*_13w).nt_-3*(*_13w).ts_>0 )){
      double _14sigma;
double _15sigma10;
double _16sigma0;
_14sigma=meanandvar_3var(_13w);
 
      _15sigma10=((*(double *)((*_13w).bt_))+(*(double *)((ssize_t)sizeof(double)+(*_13w).bt_)))/(double)((*_13w).nt_)/(double)(((*_13w).nt_-2*(*_13w).ts_));
 
      _16sigma0=(*(double *)((*_13w).bt_))/(double)((*_13w).nt_)/(double)(((*_13w).nt_-(*_13w).ts_));
 
       fprintf(stdout,"%d \t%g \t%g \t%g \t%g \t%d \t%g \t%g \t%g""\n",(*_13w).nt_,(*_13w).mean_,_14sigma*(double)((*_13w).nt_),_15sigma10*(double)((*_13w).nt_),_16sigma0*(double)((*_13w).nt_),(*_13w).ts_ ,( _14sigma>0. ?sqrt(_14sigma)  :0.) ,( _15sigma10>0. ?sqrt(_15sigma10)  :0.) ,( (*(double *)((*_13w).bt_))>0. ?(*(double *)((ssize_t)sizeof(double)+(*_13w).bt_))/(*(double *)((*_13w).bt_) ) :0.));
    };
  }};
}
if( !(0 )){
   {int col_=1  ;while(col_<=meanandvar_11h){
    double _17sigma;
_17sigma=meanandvar_3var((struct MEANANDVAR_ *)(col_*MEANANDVAR_s+mv_));
 
     fprintf(stdout,"%g \t%g \t",(*(struct MEANANDVAR_ *)(col_*MEANANDVAR_s+mv_)).mean_ ,( _17sigma>0. ?sqrt(_17sigma)  :_17sigma) );fflush(stdout);
  col_+=1;}}  putc('\n',stdout);
   fprintf( stderr ,"%d \t%d""\n",(*(struct MEANANDVAR_ *)(MEANANDVAR_s+mv_)).nt_,(*(struct MEANANDVAR_ *)(MEANANDVAR_s+mv_)).ts_);
};
freemem(es);return 0;}
