! ------ TIMESTEP ROUTINE ------
! ------

! Define operators
! ------
#define lapl(f) [f(1,0,0)+f(-1,0,0)]*d2x+[f(0,1,0)+f(0,-1,0)]*d2y+f(0,0,1)*d2zp+f(0,0,-1)*d2zm-f(0,0,0)*d2z0
#define laplp(f) [f(1,0,0)+f(-1,0,0)]*d2x+[f(0,1,0)+f(0,-1,0)]*d2y+f(0,0,1)*d2zpp+f(0,0,-1)*d2zpm-f(0,0,0)*d2zp0

! Define structures to be called in the main
! ------
#ifdef EXTRA
ARRAY[var.LO1..var.HI1,var.LO2..var.HI2] OF POINTER TO ARRAY(*,*) OF REAL uimbc,vimbc,wimbc,uimbc_rhs,vimbc_rhs
#ifdef WALLS
ARRAY[var.LO1..var.HI1,var.LO2..var.HI2] OF POINTER TO ARRAY(*,*) OF REAL Uuimbc,Uvimbc,Uwimbc,Uuimbc_rhs,Uvimbc_rhs
#endif
#else
ARRAY[var.LO1..var.HI1,var.LO2..var.HI2] OF POINTER TO ARRAY(*) OF REAL uimbc,vimbc,wimbc
#ifdef WALLS
ARRAY[var.LO1..var.HI1,var.LO2..var.HI2] OF POINTER TO ARRAY(*) OF REAL Uuimbc,Uvimbc,Uwimbc
#endif
#endif


REAL d1x=1/deltax,d1y=1/deltay,d2x=1/deltax/deltax,d2y=1/deltay/deltay
STRUCTURE(REAL d2zp,d2zm,d2z0,d1z,d1zp,d2zpp,d2zpm,d2zp0) zc(1..nz-1)

REAL FUNCTION limited(REAL x)= IF x<-1 THEN -0.9 ELSE x

! Define the structure containing the data related to the wall-normal direction grid
! ------
DO WITH zc(iz)
	d1z=1/[zd(2*iz+1)-zd(2*iz-1)]
  	d1zm=1/[zz(iz)-zz(iz-1)]
  	d2zm=d1z*d1zm
  	d1zp=1/[zz(iz+1)-zz(iz)]
  	d2zp=d1z*d1zp
  	d2z0=d2zm+d2zp+2*d2x+2*d2y
  	IF iz<nz-1 THEN
    		d2zpp=1/[zd(2*iz+3)-zd(2*iz+1)]*d1zp
    		d2zpm=d1z*d1zp
    		d2zp0=d2zpm+d2zpp+2*d2x+2*d2y
  	END IF
FOR iz=1 TO nz-1


! Define the steps of the RK3 semi-explicit Runge-Kutta method and the Adams-Bashfort (unstable!)
! ------
SUBROUTINE AdamsB(REAL val^,oldrsd^,rsd)
	val=val+deltat*[1.5*rsd-0.5*oldrsd]
  	oldrsd=rsd
END AdamsB

SUBROUTINE rai1(REAL val^,oldrsd^,rsd)
  	val=val+deltat*64/120*rsd
  	oldrsd=rsd
END rai1

SUBROUTINE rai2(REAL val^,oldrsd^,rsd)
  	val=val+deltat*(50/120*rsd-34/120*oldrsd)
  	oldrsd=rsd
END rai2

SUBROUTINE rai3(REAL val^,oldrsd^,rsd)
  	val=val+deltat*(90/120*rsd-50/120*oldrsd)
END rai3

! Linear step to use the imbc with the velocity
newARRAY=ARRAY(1..nz-1,1..3) OF REAL


SUBROUTINE linestep[SUBROUTINE(REAL val^,oldrsd^,rsd) timescheme; INTEGER ix,iy; REAL dts; POINTER TO newARRAY new]
	LOOP FOR iz=1 TO nz-1
    		new(iz)=var(ix,iy,iz,1..3)
    		WITH var(ix+*,iy+*,iz+*),zc(iz)
#ifdef EXTRA	
#ifdef WALLS
      		IF iz>=uimbc(ix,iy,*,0).LO AND iz<=Uuimbc(ix,iy,*,0).HI THEN ! XXX
			! ATTENTION: to run laminar simulation and deactivate non-linear terms, 
			! check the $non_laminar$ parameter and the beginning of riblets.cpl
			! ------ 
  			REAL rsdu=lapl(u)*nu-({[u(1,0,0)+u(0,0,0)]^2-[u(-1,0,0)+u(0,0,0)]^2}*d1x+
          			{[u(0,1,0)+u(0,0,0)]*[v(1,0,0)+v(0,0,0)]-[u(0,-1,0)+u(0,0,0)]*[v(1,-1,0)+v(0,-1,0)]}*d1y+
	   			{[u(0,0,1)+u(0,0,0)]*[w(1,0,0)+w(0,0,0)]-[u(0,0,-1)+u(0,0,0)]*[w(1,0,-1)+w(0,0,-1)]}*d1z)/4*non_laminar	 
	   		IF iz<=uimbc(ix,iy,*,0).HI THEN rsdu = ~ + extra*uimbc_rhs(ix,iy,iz,0) * [uimbc_rhs(ix,iy,iz,1)*v(0,0,0)+uimbc_rhs(ix,iy,iz,2)*v(1,0,0)+uimbc_rhs(ix,iy,iz,3)*v(1,-1,0)+uimbc_rhs(ix,iy,iz,4)*v(0,-1,0)]
	   		IF iz>=Uuimbc(ix,iy,*,0).LO THEN rsdu = ~ + extra*Uuimbc_rhs(ix,iy,iz,0) * [Uuimbc_rhs(ix,iy,iz,1)*v(0,0,0)+Uuimbc_rhs(ix,iy,iz,2)*v(1,0,0)+Uuimbc_rhs(ix,iy,iz,3)*v(1,-1,0)+Uuimbc_rhs(ix,iy,iz,4)*v(0,-1,0)]
      			timescheme(new(iz,1),old(ix,iy,iz).u,rsdu)
			new(iz,1)=~+(headx-[p(1,0,0)-p(0,0,0)]*d1x)*dts		
      			IF iz<=uimbc(ix,iy,*,0).HI THEN new(iz,1)=~/[1+limited[dts*uimbc(ix,iy,iz,0)]]
      			IF iz>=Uuimbc(ix,iy,*,0).LO THEN new(iz,1)=~/[1+limited[dts*Uuimbc(ix,iy,iz,0)]]
    		END IF
    		
   		IF iz>=vimbc(ix,iy,*,0).LO AND iz<=Uvimbc(ix,iy,*,0).HI THEN ! XXX
      	   		REAL rsdv=lapl(v)*nu-({[v(0,1,0)+v(0,0,0)]^2-[v(0,-1,0)+v(0,0,0)]^2}*d1y+
      				{[v(1,0,0)+v(0,0,0)]*[u(0,1,0)+u(0,0,0)]-[v(-1,0,0)+v(0,0,0)]*[u(-1,1,0)+u(-1,0,0)]}*d1x+
	   			{[v(0,0,1)+v(0,0,0)]*[w(0,1,0)+w(0,0,0)]-[v(0,0,-1)+v(0,0,0)]*[w(0,1,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
	   		IF iz<=vimbc(ix,iy,*,0).HI THEN rsdv = ~ + extra*vimbc_rhs(ix,iy,iz,0) * [vimbc_rhs(ix,iy,iz,1)*u(0,0,0)+vimbc_rhs(ix,iy,iz,2)*u(0,1,0)+vimbc_rhs(ix,iy,iz,3)*u(-1,1,0)+vimbc_rhs(ix,iy,iz,4)*u(-1,0,0)]
	   		IF iz>=Uvimbc(ix,iy,*,0).LO THEN rsdv = ~ + extra*Uvimbc_rhs(ix,iy,iz,0) * [Uvimbc_rhs(ix,iy,iz,1)*u(0,0,0)+Uvimbc_rhs(ix,iy,iz,2)*u(0,1,0)+Uvimbc_rhs(ix,iy,iz,3)*u(-1,1,0)+Uvimbc_rhs(ix,iy,iz,4)*u(-1,0,0)]
			timescheme(new(iz,2),old(ix,iy,iz).v,rsdv)
     			new(iz,2)=~+(heady-[p(0,1,0)-p(0,0,0)]*d1y)*dts	
     			IF iz<=vimbc(ix,iy,*,0).HI THEN new(iz,2)=~/[1+limited[dts*vimbc(ix,iy,iz,0)]]
			IF iz>=Uvimbc(ix,iy,*,0).LO THEN new(iz,2)=~/[1+limited[dts*Uvimbc(ix,iy,iz,0)]]
    		END IF
    		
   		IF iz>=wimbc(ix,iy,*,0).LO AND iz<=Uwimbc(ix,iy,*,0).HI THEN ! XXX 
      			REAL rsdw=laplp(w)*nu-({[w(0,0,1)+w(0,0,0)]^2-[w(0,0,-1)+w(0,0,0)]^2}*d1zp+
	   			{[w(1,0,0)+w(0,0,0)]*[u(0,0,1)+u(0,0,0)]-[w(-1,0,0)+w(0,0,0)]*[u(-1,0,1)+u(-1,0,0)]}*d1x+
	   			{[w(0,1,0)+w(0,0,0)]*[v(0,0,1)+v(0,0,0)]-[w(0,-1,0)+w(0,0,0)]*[v(0,-1,1)+v(0,-1,0)]}*d1y)/4*non_laminar
      			timescheme(new(iz,3),old(ix,iy,iz).w,rsdw)
      			new(iz,3)=~-[p(0,0,1)-p(0,0,0)]*d1zp*dts
     			IF iz<=wimbc(ix,iy,*,0).HI THEN new(iz,3)=~/[1+limited[dts*wimbc(ix,iy,iz,0)]]
     			IF iz>=Uwimbc(ix,iy,*,0).LO THEN new(iz,3)=~/[1+limited[dts*Uwimbc(ix,iy,iz,0)]]
    		END IF	
#else
		IF iz>=uimbc(ix,iy,*,0).LO THEN ! XXX
			! ATTENTION: to run laminar simulation and deactivate non-linear terms, 
			! check the $non_laminar$ parameter and the beginning of riblets.cpl
			! ------ 
  			REAL rsdu=lapl(u)*nu-({[u(1,0,0)+u(0,0,0)]^2-[u(-1,0,0)+u(0,0,0)]^2}*d1x+
          			{[u(0,1,0)+u(0,0,0)]*[v(1,0,0)+v(0,0,0)]-[u(0,-1,0)+u(0,0,0)]*[v(1,-1,0)+v(0,-1,0)]}*d1y+
	   			{[u(0,0,1)+u(0,0,0)]*[w(1,0,0)+w(0,0,0)]-[u(0,0,-1)+u(0,0,0)]*[w(1,0,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
	   		IF iz<=uimbc(ix,iy,*,0).HI THEN rsdu = ~ + extra*uimbc_rhs(ix,iy,iz,0) * [uimbc_rhs(ix,iy,iz,1)*v(0,0,0)+uimbc_rhs(ix,iy,iz,2)*v(1,0,0)+uimbc_rhs(ix,iy,iz,3)*v(1,-1,0)+uimbc_rhs(ix,iy,iz,4)*v(0,-1,0)]
      			timescheme(new(iz,1),old(ix,iy,iz).u,rsdu)
			new(iz,1)=~+(headx-[p(1,0,0)-p(0,0,0)]*d1x)*dts		
      			IF iz<=uimbc(ix,iy,*,0).HI THEN new(iz,1)=~/[1+limited[dts*uimbc(ix,iy,iz,0)]]
    		END IF
    		
   		IF iz>=vimbc(ix,iy,*,0).LO THEN ! XXX
      	   		REAL rsdv=lapl(v)*nu-({[v(0,1,0)+v(0,0,0)]^2-[v(0,-1,0)+v(0,0,0)]^2}*d1y+
      				{[v(1,0,0)+v(0,0,0)]*[u(0,1,0)+u(0,0,0)]-[v(-1,0,0)+v(0,0,0)]*[u(-1,1,0)+u(-1,0,0)]}*d1x+
	   			{[v(0,0,1)+v(0,0,0)]*[w(0,1,0)+w(0,0,0)]-[v(0,0,-1)+v(0,0,0)]*[w(0,1,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
	   		IF iz<=vimbc(ix,iy,*,0).HI THEN rsdv = ~ + extra*vimbc_rhs(ix,iy,iz,0) * [vimbc_rhs(ix,iy,iz,1)*u(0,0,0)+vimbc_rhs(ix,iy,iz,2)*u(0,1,0)+vimbc_rhs(ix,iy,iz,3)*u(-1,1,0)+vimbc_rhs(ix,iy,iz,4)*u(-1,0,0)]
   			timescheme(new(iz,2),old(ix,iy,iz).v,rsdv)
     			new(iz,2)=~+(heady-[p(0,1,0)-p(0,0,0)]*d1y)*dts	
     			IF iz<=vimbc(ix,iy,*,0).HI THEN new(iz,2)=~/[1+limited[dts*vimbc(ix,iy,iz,0)]]
    		END IF
    		
   		IF iz>=wimbc(ix,iy,*,0).LO AND iz<nz-1 THEN ! XXX 
      			REAL rsdw=laplp(w)*nu-({[w(0,0,1)+w(0,0,0)]^2-[w(0,0,-1)+w(0,0,0)]^2}*d1zp+
	   			{[w(1,0,0)+w(0,0,0)]*[u(0,0,1)+u(0,0,0)]-[w(-1,0,0)+w(0,0,0)]*[u(-1,0,1)+u(-1,0,0)]}*d1x+
	   			{[w(0,1,0)+w(0,0,0)]*[v(0,0,1)+v(0,0,0)]-[w(0,-1,0)+w(0,0,0)]*[v(0,-1,1)+v(0,-1,0)]}*d1y)/4*non_laminar
      			timescheme(new(iz,3),old(ix,iy,iz).w,rsdw)
      			new(iz,3)=~-[p(0,0,1)-p(0,0,0)]*d1zp*dts
     			IF iz<=wimbc(ix,iy,*,0).HI THEN new(iz,3)=~/[1+limited[dts*wimbc(ix,iy,iz,0)]]
    		END IF
#endif
#else
#ifdef WALLS
      		IF iz>=uimbc(ix,iy).LO AND iz<=Uuimbc(ix,iy).HI THEN ! XXX
			! ATTENTION: to run laminar simulation and deactivate non-linear terms, 
			! check the $non_laminar$ parameter and the beginning of riblets.cpl
			! ------ 
  			REAL rsdu=lapl(u)*nu-({[u(1,0,0)+u(0,0,0)]^2-[u(-1,0,0)+u(0,0,0)]^2}*d1x+
          			{[u(0,1,0)+u(0,0,0)]*[v(1,0,0)+v(0,0,0)]-[u(0,-1,0)+u(0,0,0)]*[v(1,-1,0)+v(0,-1,0)]}*d1y+
	   			{[u(0,0,1)+u(0,0,0)]*[w(1,0,0)+w(0,0,0)]-[u(0,0,-1)+u(0,0,0)]*[w(1,0,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
      			timescheme(new(iz,1),old(ix,iy,iz).u,rsdu)
			new(iz,1)=~+(headx-[p(1,0,0)-p(0,0,0)]*d1x)*dts		
      			IF iz<=uimbc(ix,iy).HI THEN new(iz,1)=~/[1+limited[dts*uimbc(ix,iy,iz)]]
      			IF iz>=Uuimbc(ix,iy).LO THEN new(iz,1)=~/[1+limited[dts*Uuimbc(ix,iy,iz)]]
    		END IF
    		
   		IF iz>=vimbc(ix,iy).LO AND iz<=Uvimbc(ix,iy).HI THEN ! XXX
      	   		REAL rsdv=lapl(v)*nu-({[v(0,1,0)+v(0,0,0)]^2-[v(0,-1,0)+v(0,0,0)]^2}*d1y+
      				{[v(1,0,0)+v(0,0,0)]*[u(0,1,0)+u(0,0,0)]-[v(-1,0,0)+v(0,0,0)]*[u(-1,1,0)+u(-1,0,0)]}*d1x+
	   			{[v(0,0,1)+v(0,0,0)]*[w(0,1,0)+w(0,0,0)]-[v(0,0,-1)+v(0,0,0)]*[w(0,1,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
			timescheme(new(iz,2),old(ix,iy,iz).v,rsdv)
     			new(iz,2)=~+(heady-[p(0,1,0)-p(0,0,0)]*d1y)*dts	
     			IF iz<=vimbc(ix,iy).HI THEN new(iz,2)=~/[1+limited[dts*vimbc(ix,iy,iz)]]
			IF iz>=Uvimbc(ix,iy).LO THEN new(iz,2)=~/[1+limited[dts*Uvimbc(ix,iy,iz)]]
    		END IF
    		
   		IF iz>=wimbc(ix,iy).LO AND iz<=Uwimbc(ix,iy).HI THEN ! XXX 
      			REAL rsdw=laplp(w)*nu-({[w(0,0,1)+w(0,0,0)]^2-[w(0,0,-1)+w(0,0,0)]^2}*d1zp+
	   			{[w(1,0,0)+w(0,0,0)]*[u(0,0,1)+u(0,0,0)]-[w(-1,0,0)+w(0,0,0)]*[u(-1,0,1)+u(-1,0,0)]}*d1x+
	   			{[w(0,1,0)+w(0,0,0)]*[v(0,0,1)+v(0,0,0)]-[w(0,-1,0)+w(0,0,0)]*[v(0,-1,1)+v(0,-1,0)]}*d1y)/4*non_laminar
      			timescheme(new(iz,3),old(ix,iy,iz).w,rsdw)
      			new(iz,3)=~-[p(0,0,1)-p(0,0,0)]*d1zp*dts
     			IF iz<=wimbc(ix,iy).HI THEN new(iz,3)=~/[1+limited[dts*wimbc(ix,iy,iz)]]
     			IF iz>=Uwimbc(ix,iy).LO THEN new(iz,3)=~/[1+limited[dts*Uwimbc(ix,iy,iz)]]
    		END IF	
#else
		IF iz>=uimbc(ix,iy).LO THEN ! XXX
			! ATTENTION: to run laminar simulation and deactivate non-linear terms, 
			! check the $non_laminar$ parameter and the beginning of riblets.cpl
			! ------ 
  			REAL rsdu=lapl(u)*nu-({[u(1,0,0)+u(0,0,0)]^2-[u(-1,0,0)+u(0,0,0)]^2}*d1x+
          			{[u(0,1,0)+u(0,0,0)]*[v(1,0,0)+v(0,0,0)]-[u(0,-1,0)+u(0,0,0)]*[v(1,-1,0)+v(0,-1,0)]}*d1y+
	   			{[u(0,0,1)+u(0,0,0)]*[w(1,0,0)+w(0,0,0)]-[u(0,0,-1)+u(0,0,0)]*[w(1,0,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
      			timescheme(new(iz,1),old(ix,iy,iz).u,rsdu)
			new(iz,1)=~+(headx-[p(1,0,0)-p(0,0,0)]*d1x)*dts		
      			IF iz<=uimbc(ix,iy).HI THEN new(iz,1)=~/[1+limited[dts*uimbc(ix,iy,iz)]]
    		END IF
    		
   		IF iz>=vimbc(ix,iy).LO THEN ! XXX
      	   		REAL rsdv=lapl(v)*nu-({[v(0,1,0)+v(0,0,0)]^2-[v(0,-1,0)+v(0,0,0)]^2}*d1y+
      				{[v(1,0,0)+v(0,0,0)]*[u(0,1,0)+u(0,0,0)]-[v(-1,0,0)+v(0,0,0)]*[u(-1,1,0)+u(-1,0,0)]}*d1x+
	   			{[v(0,0,1)+v(0,0,0)]*[w(0,1,0)+w(0,0,0)]-[v(0,0,-1)+v(0,0,0)]*[w(0,1,-1)+w(0,0,-1)]}*d1z)/4*non_laminar
   			timescheme(new(iz,2),old(ix,iy,iz).v,rsdv)
     			new(iz,2)=~+(heady-[p(0,1,0)-p(0,0,0)]*d1y)*dts	
     			IF iz<=vimbc(ix,iy).HI THEN new(iz,2)=~/[1+limited[dts*vimbc(ix,iy,iz)]]
    		END IF
    		
   		IF iz>=wimbc(ix,iy).LO AND iz<nz-1 THEN ! XXX 
      			REAL rsdw=laplp(w)*nu-({[w(0,0,1)+w(0,0,0)]^2-[w(0,0,-1)+w(0,0,0)]^2}*d1zp+
	   			{[w(1,0,0)+w(0,0,0)]*[u(0,0,1)+u(0,0,0)]-[w(-1,0,0)+w(0,0,0)]*[u(-1,0,1)+u(-1,0,0)]}*d1x+
	   			{[w(0,1,0)+w(0,0,0)]*[v(0,0,1)+v(0,0,0)]-[w(0,-1,0)+w(0,0,0)]*[v(0,-1,1)+v(0,-1,0)]}*d1y)/4*non_laminar
      			timescheme(new(iz,3),old(ix,iy,iz).w,rsdw)
      			new(iz,3)=~-[p(0,0,1)-p(0,0,0)]*d1zp*dts
     			IF iz<=wimbc(ix,iy).HI THEN new(iz,3)=~/[1+limited[dts*wimbc(ix,iy,iz)]]
    		END IF
#endif
#endif

  	REPEAT
END linestep

! Define the LU decomposition for dz
! ------
REAL M(1..nz-1,-1..1)
LOOP FOR iz=1 TO nz-1
  	M(iz,-1)=zc(iz).d2zm
  	M(iz,1)=zc(iz).d2zp
  	M(iz,0)=-zc(iz).d2z0
REPEAT
M(LO,0)=~+M(LO,-1)
M(HI,0)=~+M(HI,1)
LUdecomp M
sor=1.5

! Linear step to upgrade the pressure field
! ------
SUBROUTINE pressurelinestep(INTEGER ix,iy; REAL dts)
	ARRAY(1..nz-1) OF REAL phi
  	LOOP FOR iz=1 TO nz-1
    		WITH var(ix+*,iy+*,iz+*),zc(iz)
    		phi(iz)={[u(0,0,0)-u(-1,0,0)]*d1x+[v(0,0,0)-v(0,-1,0)]*d1y+[w(0,0,0)-w(0,0,-1)]*d1z}*sor
  	REPEAT
  	phi=M\~
  	LOOP FOR iz=1 TO nz-1
    		BOOLEAN updatep=NO
    		WITH var(ix+*,iy+*,iz+*)
#ifdef EXTRA    		
#ifdef WALLS
    		IF iz>=uimbc(ix,iy,*,0).LO AND iz<=Uuimbc(ix,iy,*,0).HI THEN updatep=YES; u(0,0,0)=~+phi(iz)*{IF iz<=uimbc(ix,iy,*,0).HI THEN d1x/[1+dts*uimbc(ix,iy,iz,0)] ELSE IF iz>=Uuimbc(ix,iy,*,0).LO THEN d1x/[1+dts*Uuimbc(ix,iy,iz,0)] ELSE d1x}
    		IF iz>=uimbc(ix-1,iy,*,0).LO AND iz<=Uuimbc(ix-1,iy,*,0).HI THEN updatep=YES; u(-1,0,0)=~-phi(iz)*{IF iz<=uimbc(ix-1,iy,*,0).HI THEN d1x/[1+dts*uimbc(ix-1,iy,iz,0)] ELSE IF iz>=Uuimbc(ix-1,iy,*,0).LO THEN d1x/[1+dts*Uuimbc(ix-1,iy,iz,0)] ELSE d1x}
   
    		IF iz>=vimbc(ix,iy,*,0).LO AND iz<=Uvimbc(ix,iy,*,0).HI THEN updatep=YES; v(0,0,0)=~+phi(iz)*{IF iz<=vimbc(ix,iy,*,0).HI THEN d1y/[1+dts*vimbc(ix,iy,iz,0)] ELSE IF iz>=Uvimbc(ix,iy,*,0).LO THEN d1y/[1+dts*Uvimbc(ix,iy,iz,0)] ELSE d1y}
    		IF iz>=vimbc(ix,iy-1,*,0).LO AND iz<=Uvimbc(ix,iy-1,*,0).HI THEN updatep=YES; v(0,-1,0)=~-phi(iz)*{IF iz<=vimbc(ix,iy-1,*,0).HI THEN d1y/[1+dts*vimbc(ix,iy-1,iz,0)] ELSE IF iz>=Uvimbc(ix,iy-1,*,0).LO THEN d1y/[1+dts*Uvimbc(ix,iy-1,iz,0)] ELSE d1y}
        
    		IF iz>=wimbc(ix,iy,*,0).LO AND iz<=Uwimbc(ix,iy,*,0).HI THEN updatep=YES; w(0,0,0)=~+phi(iz)*{IF iz<=wimbc(ix,iy,*,0).HI THEN zc(iz).d1zp/[1+dts*wimbc(ix,iy,iz,0)] ELSE IF iz>=Uwimbc(ix,iy,*,0).LO THEN zc(iz).d1zp/[1+dts*Uwimbc(ix,iy,iz,0)] ELSE zc(iz).d1zp}
    		IF iz-1>=wimbc(ix,iy,*,0).LO AND iz-1<=Uwimbc(ix,iy,*,0).HI THEN updatep=YES; w(0,0,-1)=~-phi(iz)*{IF iz-1<=wimbc(ix,iy,*,0).HI THEN zc(iz-1).d1zp/[1+dts*wimbc(ix,iy,iz-1,0)] ELSE IF iz-1>=Uwimbc(ix,iy,*,0).LO THEN zc(iz-1).d1zp/[1+dts*Uwimbc(ix,iy,iz-1,0)] ELSE zc(iz-1).d1zp}  		
#else
    		IF iz>=uimbc(ix,iy,*,0).LO THEN updatep=YES; u(0,0,0)=~+phi(iz)*{IF iz<=uimbc(ix,iy,*,0).HI THEN d1x/[1+limited[dts*uimbc(ix,iy,iz,0)]] ELSE d1x}
    		IF iz>=uimbc(ix-1,iy,*,0).LO THEN updatep=YES; u(-1,0,0)=~-phi(iz)*{IF iz<=uimbc(ix-1,iy,*,0).HI THEN d1x/[1+limited[dts*uimbc(ix-1,iy,iz,0)]] ELSE d1x}
    		IF iz>=vimbc(ix,iy,*,0).LO THEN updatep=YES; v(0,0,0)=~+phi(iz)*{IF iz<=vimbc(ix,iy,*,0).HI THEN d1y/[1+limited[dts*vimbc(ix,iy,iz,0)]] ELSE d1y}
    		IF iz>=vimbc(ix,iy-1,*,0).LO THEN updatep=YES; v(0,-1,0)=~-phi(iz)*{IF iz<=vimbc(ix,iy-1,*,0).HI THEN d1y/[1+limited[dts*vimbc(ix,iy-1,iz,0)]] ELSE d1y}
    		IF iz<nz-1 AND iz>=wimbc(ix,iy,*,0).LO THEN updatep=YES; w(0,0,0)=~+phi(iz)*{IF iz<=wimbc(ix,iy,*,0).HI THEN zc(iz).d1zp/[1+limited[dts*wimbc(ix,iy,iz,0)]] ELSE zc(iz).d1zp}
    		IF iz-1>=wimbc(ix,iy,*,0).LO THEN updatep=YES; w(0,0,-1)=~-phi(iz)*{IF iz-1<=wimbc(ix,iy,*,0).HI THEN zc(iz-1).d1zp/[1+limited[dts*wimbc(ix,iy,iz-1,0)]] ELSE zc(iz-1).d1zp}		
#endif
#else
#ifdef WALLS
    		IF iz>=uimbc(ix,iy).LO AND iz<=Uuimbc(ix,iy).HI THEN updatep=YES; u(0,0,0)=~+phi(iz)*{IF iz<=uimbc(ix,iy).HI THEN d1x/[1+dts*uimbc(ix,iy,iz)] ELSE IF iz>=Uuimbc(ix,iy).LO THEN d1x/[1+dts*Uuimbc(ix,iy,iz)] ELSE d1x}
    		IF iz>=uimbc(ix-1,iy).LO AND iz<=Uuimbc(ix-1,iy).HI THEN updatep=YES; u(-1,0,0)=~-phi(iz)*{IF iz<=uimbc(ix-1,iy).HI THEN d1x/[1+dts*uimbc(ix-1,iy,iz)] ELSE IF iz>=Uuimbc(ix-1,iy).LO THEN d1x/[1+dts*Uuimbc(ix-1,iy,iz)] ELSE d1x}
  
    		IF iz>=vimbc(ix,iy).LO AND iz<=Uvimbc(ix,iy).HI THEN updatep=YES; v(0,0,0)=~+phi(iz)*{IF iz<=vimbc(ix,iy).HI THEN d1y/[1+dts*vimbc(ix,iy,iz)] ELSE IF iz>=Uvimbc(ix,iy).LO THEN d1y/[1+dts*Uvimbc(ix,iy,iz)] ELSE d1y}
    		IF iz>=vimbc(ix,iy-1).LO AND iz<=Uvimbc(ix,iy-1).HI THEN updatep=YES; v(0,-1,0)=~-phi(iz)*{IF iz<=vimbc(ix,iy-1).HI THEN d1y/[1+dts*vimbc(ix,iy-1,iz)] ELSE IF iz>=Uvimbc(ix,iy-1).LO THEN d1y/[1+dts*Uvimbc(ix,iy-1,iz)] ELSE d1y}
        
    		IF iz>=wimbc(ix,iy).LO AND iz<=Uwimbc(ix,iy).HI THEN updatep=YES; w(0,0,0)=~+phi(iz)*{IF iz<=wimbc(ix,iy).HI THEN zc(iz).d1zp/[1+dts*wimbc(ix,iy,iz)] ELSE IF iz>=Uwimbc(ix,iy).LO THEN zc(iz).d1zp/[1+dts*Uwimbc(ix,iy,iz)] ELSE zc(iz).d1zp}
    		IF iz-1>=wimbc(ix,iy).LO AND iz-1<=Uwimbc(ix,iy).HI THEN updatep=YES; w(0,0,-1)=~-phi(iz)*{IF iz-1<=wimbc(ix,iy).HI THEN zc(iz-1).d1zp/[1+dts*wimbc(ix,iy,iz-1)] ELSE IF iz-1>=Uwimbc(ix,iy).LO THEN zc(iz-1).d1zp/[1+dts*Uwimbc(ix,iy,iz-1)] ELSE zc(iz-1).d1zp}  		
#else
    		IF iz>=uimbc(ix,iy).LO THEN updatep=YES; u(0,0,0)=~+phi(iz)*{IF iz<=uimbc(ix,iy).HI THEN d1x/[1+limited[dts*uimbc(ix,iy,iz)]] ELSE d1x}
    		IF iz>=uimbc(ix-1,iy).LO THEN updatep=YES; u(-1,0,0)=~-phi(iz)*{IF iz<=uimbc(ix-1,iy).HI THEN d1x/[1+limited[dts*uimbc(ix-1,iy,iz)]] ELSE d1x}
    		IF iz>=vimbc(ix,iy).LO THEN updatep=YES; v(0,0,0)=~+phi(iz)*{IF iz<=vimbc(ix,iy).HI THEN d1y/[1+limited[dts*vimbc(ix,iy,iz)]] ELSE d1y}
    		IF iz>=vimbc(ix,iy-1).LO THEN updatep=YES; v(0,-1,0)=~-phi(iz)*{IF iz<=vimbc(ix,iy-1).HI THEN d1y/[1+limited[dts*vimbc(ix,iy-1,iz)]] ELSE d1y}
    		IF iz<nz-1 AND iz>=wimbc(ix,iy).LO THEN updatep=YES; w(0,0,0)=~+phi(iz)*{IF iz<=wimbc(ix,iy).HI THEN zc(iz).d1zp/[1+limited[dts*wimbc(ix,iy,iz)]] ELSE zc(iz).d1zp}
    		IF iz-1>=wimbc(ix,iy).LO THEN updatep=YES; w(0,0,-1)=~-phi(iz)*{IF iz-1<=wimbc(ix,iy).HI THEN zc(iz-1).d1zp/[1+limited[dts*wimbc(ix,iy,iz-1)]] ELSE zc(iz-1).d1zp}		
#endif
#endif    		
    		IF updatep THEN p(0,0,0)=~+phi(iz)/dts
  	REPEAT
END pressurelinestep

! Define the function to get the CFL and Fourier number + any quantity useful to debug
! This routine is called once every it_cfl iterations, so it is clever to use it to get some quantities of the current flow.
! Any number of scalars can be added do this array, but REMEMBER TO MODIFY THE DIMENSION OF THIS ARRAY AT THE inputs SECTION OF THE MAIN.
! ------
SUBROUTINE get_cfl() ! XXX Should we consider the IBM in the definition of the CFL?
		     ! Still open doubt, but cflmax = 0.5 and fouriermax = 0.5 work.
	cfl=0
#ifdef EXTRA
#ifdef WALLS
     	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
       		DO WITH var(ix,iy,iz):
			! Evaluate the CFL
         		cfl(0)=MAX{cfl(0),MAX[ABS(u)]*d1x+MAX[ABS(v)]*d1y+MAX[ABS(w)]*zc(iz).d1z}
         		! Evaluate the Fourier number
			!cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0)}
			cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0), nu*(0.5*d1y*d1y), nu*(0.5*d1x*d1x) }
         		! Evaluate the residual of the maximum value of u
			!cfl(2)=MAX{cfl(2),ABS(u-varold(ix,iy,iz).u)}
       		FOR iz=vimbc(ix,iy,*,0).LO TO nz-vimbc(ix,iy,*,0).LO
     	REPEAT
#else
     	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
		DO WITH var(ix,iy,iz):
			! Evaluate the CFL
         		cfl(0)=MAX{cfl(0),MAX[ABS(u)]*d1x+MAX[ABS(v)]*d1y+MAX[ABS(w)]*zc(iz).d1z}
         		! Evaluate the Fourier number
			!cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0)}
			cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0), nu*(0.5*d1y*d1y), nu*(0.5*d1x*d1x) }
         		! Evaluate the residual of the maximum value of u
			!cfl(2)=MAX{cfl(2),ABS(u-varold(ix,iy,iz).u)}
       		FOR iz=vimbc(ix,iy,*,0).LO TO nz-1
     	REPEAT
#endif
#else
#ifdef WALLS
     	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
       		DO WITH var(ix,iy,iz):
			! Evaluate the CFL
         		cfl(0)=MAX{cfl(0),MAX[ABS(u)]*d1x+MAX[ABS(v)]*d1y+MAX[ABS(w)]*zc(iz).d1z}
         		! Evaluate the Fourier number
			!cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0)}
			cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0), nu*(0.5*d1y*d1y), nu*(0.5*d1x*d1x) }
         		! Evaluate the residual of the maximum value of u
			!cfl(2)=MAX{cfl(2),ABS(u-varold(ix,iy,iz).u)}
       		FOR iz=vimbc(ix,iy).LO TO nz-vimbc(ix,iy).LO
     	REPEAT
#else
     	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
		DO WITH var(ix,iy,iz):
			! Evaluate the CFL
         		cfl(0)=MAX{cfl(0),MAX[ABS(u)]*d1x+MAX[ABS(v)]*d1y+MAX[ABS(w)]*zc(iz).d1z}
         		! Evaluate the Fourier number
			!cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0)}
			cfl(1)=MAX{cfl(1),nu*(0.5*zc(iz).d2z0), nu*(0.5*d1y*d1y), nu*(0.5*d1x*d1x) }
         		! Evaluate the residual of the maximum value of u
			!cfl(2)=MAX{cfl(2),ABS(u-varold(ix,iy,iz).u)}
       		FOR iz=vimbc(ix,iy).LO TO nz-1
     	REPEAT
#endif
#endif
     	getmax(cfl)
END get_cfl


! Time step advance in time
! ------
SUBROUTINE timestep[SUBROUTINE(REAL val^,oldrsd^,rsd) timescheme]
  	pbc(var(*,*,**))
  	REAL dts=0, testval=1
  	timescheme(dts,testval,testval)
  	newARRAY new(var.LO1+1..var.HI1-1,var.LO2+1..var.HI2-1)
  	LOOP FOR ix=var.LO1+2 TO var.HI1-2 AND iy=var.LO2+2 TO var.HI2-2
    		linestep(timescheme,ix,iy,dts,new(ix,iy))
  	REPEAT
  	pbcwait
  	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
    		EXCEPT ix>var.LO1+1 AND ix<var.HI1-1 AND iy>var.LO2+1 AND iy<var.HI2-1
    		linestep(timescheme,ix,iy,dts,new(ix,iy))
  	REPEAT
  	var(LO+1..HI-1,LO+1..HI-1,1..nz-1,1..3)=new
  	pbc(var(*,*,**),var(*,*,**),-1)
  	pbcwait
  	LOOP FOR 3 TIMES AND parity=0 TO 1
    		pbcp(var.u,var.v,parity)
    		LOOP FOR ix=var.LO1+2 TO var.HI1-1 AND iy=var.LO2+2+(var.LO2+ix+parity) MOD 2 TO var.HI2-1 BY 2
      			pressurelinestep(ix,iy,dts)
    		REPEAT
    		pbcwait
    		LOOP FOR ix=var.LO1+1 TO var.HI1 AND iy=var.LO2+2-(var.LO2+ix+parity) MOD 2 TO var.HI2 BY 2
      			EXCEPT ix>var.LO1+1 AND ix<var.HI1 AND iy>var.LO2+1 AND iy<var.HI2
      			pressurelinestep(ix,iy,dts)
    		REPEAT
  	REPEAT
	! Define the flowrate, to be upgraded only at this point.
	! ------
  	flowrate=0
  	INTEGER count=0
  	LOOP FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1
    		INC count
#ifdef EXTRA
#ifdef WALLS
    		flowrate.x=~+({SUM [var(ix,iy,iz).u]/zc(iz).d1z FOR iz=uimbc(ix,iy,*,0).LO TO Uuimbc(ix,iy,*,0).HI}-flowrate.x)/count
    		flowrate.y=~+({SUM [var(ix,iy,iz).v]/zc(iz).d1z FOR iz=vimbc(ix,iy,*,0).LO TO Uvimbc(ix,iy,*,0).HI}-flowrate.y)/count
#else
    		flowrate.x=~+({SUM [var(ix,iy,iz).u]/zc(iz).d1z FOR iz=uimbc(ix,iy,*,0).LO TO nz-1}-flowrate.x)/count
    		flowrate.y=~+({SUM [var(ix,iy,iz).v]/zc(iz).d1z FOR iz=vimbc(ix,iy,*,0).LO TO nz-1}-flowrate.y)/count
#endif
#else
#ifdef WALLS
    		flowrate.x=~+({SUM [var(ix,iy,iz).u]/zc(iz).d1z FOR iz=uimbc(ix,iy).LO TO Uuimbc(ix,iy).HI}-flowrate.x)/count
    		flowrate.y=~+({SUM [var(ix,iy,iz).v]/zc(iz).d1z FOR iz=vimbc(ix,iy).LO TO Uvimbc(ix,iy).HI}-flowrate.y)/count
#else
    		flowrate.x=~+({SUM [var(ix,iy,iz).u]/zc(iz).d1z FOR iz=uimbc(ix,iy).LO TO nz-1}-flowrate.x)/count
    		flowrate.y=~+({SUM [var(ix,iy,iz).v]/zc(iz).d1z FOR iz=vimbc(ix,iy).LO TO nz-1}-flowrate.y)/count
#endif
#endif
  	REPEAT
  	accumulate(flowrate)
! If a constant flow rate simulation is running, the pressure gradient must be corrected to keep UFR and VFR constant.
! If a constant pressure gradient simulation is running, this IF is skipped
! ------
	#ifndef cpg
  		dum=(UFR-flowrate.x/nprocx/nprocy)/zd(2*nz)
  		dvm=(VFR-flowrate.y/nprocx/nprocy)/zd(2*nz)
  		DO var(ix,iy,iz).u=~+(IF iz>uimbc(ix,iy,*,0).HI THEN
			dum
		ELSE dum/[1+limited[dts*uimbc(ix,iy,iz,0)]]) FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1 AND iz=uimbc(ix,iy,*,0).LO TO nz-1
  		headx=~+dum/dts
  		DO var(ix,iy,iz).v=~+(IF iz>vimbc(ix,iy,*,0).HI THEN
			dvm
		ELSE dvm/[1+limited[dts*vimbc(ix,iy,iz,0)]]) FOR ix=var.LO1+1 TO var.HI1-1 AND iy=var.LO2+1 TO var.HI2-1 AND iz=vimbc(ix,iy,*,0).LO TO nz-1
  		heady=~+dvm/dts
	#endif
END timestep
