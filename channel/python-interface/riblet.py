#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import numpy as np
import os

def read_input(filename):
    with open(filename) as fileHandle:
        lines = fileHandle.readlines()
    dns = {
        'nx' : int(re.search('nx=(.*)\t\t\t ny=', lines[0]).group(1)),
        'ny' : int(re.search('ny=(.*)\t\t\t nz=', lines[0]).group(1)),
        'nz' : int(re.search('nz=(.*)\t walls=', lines[0]).group(1)),
        'nu' : float(re.search('nu=(.*)\t\t Lx=', lines[1]).group(1)),
    }
    return dns

def read_2dstats(filename,dns):
    ny = dns["ny"]; nz=dns["nz"]
    fileHandler = open(filename,"rb")
    stats = {
        'yy' : np.fromfile(fileHandler,count=ny+2)[1:-1],
        'zz' : np.fromfile(fileHandler,count=nz+1),
        }
    fields = (np.fromfile(fileHandler,count=((ny+2)*(nz+1)*8)).reshape([ny+2,nz+1,8]))[1:-1,:,:]
    stats['sampleNumber']=np.fromfile(fileHandler,dtype=np.int32,count=1)
    stats['U'] = fields[:,:,0]
    stats['V'] = fields[:,:,1]
    stats['W'] = fields[:,:,2]
    stats['uu'] = fields[:,:,3]-fields[:,:,0]**2
    stats['vv'] = fields[:,:,4]-fields[:,:,1]**2
    stats['ww'] = fields[:,:,5]-fields[:,:,2]**2
    stats['uw'] = fields[:,:,6]-fields[:,:,0]*fields[:,:,2]
    return stats

def compute_1dstats(stats2d,dns):
    stats = {
          'zz': stats2d['zz'],
          'U' : np.mean(stats2d['U'],axis=0),
          'uw': np.mean(stats2d['uw'],axis=0),
          'uwdisp' : np.mean(stats2d['U']*stats2d['W'],axis=0),
        }
    stats['uc']  = 0.5*(stats['U'][-1]+stats['U'][-2])
    stats['ub']  = np.trapz(stats['U'], x=stats['zz'])-0.5*(stats['zz'][-1]-1.0)*(stats['U'][-1]+stats['uc'])
    stats['dU/dz'] = d1(stats['U'],stats['zz'])
    stats['d(uw)/dz'] = d1(stats['uw'],stats['zz'])
    stats['mombal'] = { 'visc' : dns['nu']*stats['dU/dz'], 'turb' : -stats['uw'], 'disp' : -stats['uwdisp']}
    stats['mombal']['tot'] = stats['mombal']['visc']+stats['mombal']['turb']+stats['mombal']['disp']
    i0 = np.where(stats['zz']>(-stats['zz'][0]))[0][0]
    stats['mombal']['P'] = np.polynomial.polynomial.Polynomial.fit(stats['zz'][i0:],stats['mombal']['tot'][i0:],1,domain=[])
    return stats

def d1(f,x):
    df = np.zeros(f.shape)
    for i in range(f.size):
      j = i + int(i==0)-int(i==f.size-1)
      dxp = x[j+1]-x[i]; dx0 = x[j] - x[i]; dxm = x[j-1]-x[i]
      M = np.array([[1, 1, 1], [dxm, dx0, dxp], [dxm**2, dx0**2, dxp**2]])
      t = [0, 1, 0]; c = np.linalg.solve(M,t)
      df[i] = f[j-1]*c[0] + f[j]*c[1] + f[j+1]*c[2]
    return df

def d2(f,x):
    df = np.zeros(f.shape)
    for i in range(f.size):
      j = i + int(i==0)-int(i==f.size-1)
      dxp = x[j+1]-x[i]; dx0 = x[j] - x[i]; dxm = x[j-1]-x[i]
      M = np.array([[1, 1, 1], [dxm, dx0, dxp], [dxm**2, dx0**2, dxp**2]])
      t = [0, 0, 2]; c = np.linalg.solve(M,t)
      df[i] = f[j-1]*c[0] + f[j]*c[1] + f[j+1]*c[2]
    return df

def write_list(protr_height_opt,direction):
    wd = os.getcwd()
    start = wd.find('ppr') - 2
    end = wd.find('ppr', start)
    nppr = wd[start:end]
    height_str = str(protr_height_opt)
    collect_data = open("../../../list_h.txt","w+")
    lines = collect_data.readlines()
    flag = 0
    index = 0
    for line in lines:
        index = index + 1
        if nppr+"ppr" in line:
            flag = 1
            break
    temp = nppr + "ppr: h_"+direction+" = " + height_str
    if flag == 0:
        collect_data.write("\n")
        collect_data.write(temp)
    else:
        with open("../../../list_h.txt",'a+') as write_file:
            for line in lines:
                if currentLine == index:
                    write_file.write(temp)
                else:
                    write_file.write(line)

                currentLine += 1

