#import matplotlib.pyplot as plt
from riblet import * 

#%% PARALLEL FLOW - PROTRUSION HEIGHTS EVALUATION 
dns = read_input('../input/riblets.in')
ratiohs = 0.866025403784
spacing = 0.08

#%%
stats2d = read_2dstats('../output/stats.bin',dns)

#%% Some statistics
stats = compute_1dstats(stats2d,dns)


#%% Plot two-dimensional mean velocity profile
#plt.figure()
#plt.pcolormesh(stats2d['yy'],stats2d['zz'],stats2d['U'].transpose(),shading='auto')
#plt.xlabel('y/h')
#plt.ylabel('z/h')
#cbar = plt.colorbar()
#cbar.ax.set_title('U') 
#plt.show()

#%% Plot mean velocity profile with riblet
#plt.figure()
#plt.semilogx(stats2d['zz'],np.mean(stats2d['U'],axis=0))
#plt.xlabel('z/h')
#plt.ylabel('U')
# plt.show()


#%% Plot the variances
#plt.figure()
#plt.title('variances')
#plt.plot(stats2d['zz'],np.mean(stats2d['uu'],axis=0), label='<uu>')
#plt.plot(stats2d['zz'],np.mean(stats2d['vv'],axis=0), label='<vv>')
#plt.plot(stats2d['zz'],np.mean(stats2d['ww'],axis=0), label='<ww>')
#plt.plot(stats2d['zz'],np.mean(stats2d['uw'],axis=0), label='<uw>')
#plt.legend()
#plt.xlabel('z/h')
#plt.ylabel('U')
# plt.show()

#%% Plot the total stress
#plt.figure()
#plt.title('total stress budget')
#plt.plot(-np.array([1,1])*stats['zz'][0],np.array([0,1]), 'k--', linewidth=1)
#plt.plot(stats['zz'],stats['mombal']['turb'] , label='<uw>')
#plt.plot(stats['zz'],stats['mombal']['disp'] ,  label='<UW>')
#plt.plot(stats['zz'],stats['mombal']['visc'] ,  label='d<U>/dz')
#plt.plot(stats['zz'],stats['mombal']['tot'] , label='tot')
#plt.plot(stats['zz'],np.polynomial.polynomial.polyval(stats['zz'],stats['mombal']['P'].coef), label = 'lin')
#plt.legend()
# plt.show()


#%% Some results

print("U_bulk = ",stats['ub'])
print("C_f = ", 2/stats['ub']**2)


