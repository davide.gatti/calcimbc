import matplotlib.pyplot as plt
from riblet import * 

#%% PERPENDICULAR FLOW - PROTRUSION HEIGHTS EVALUATION 
dns = read_input('../input/riblets.in')
ratiohs = 0.866025403784
spacing = 0.08

#%%
stats2d = read_2dstats('../output/stats.bin',dns)

#%% Some statistics
stats = compute_1dstats(stats2d,dns)


#%% Plot two-dimensional mean velocity profile
plt.figure()
plt.pcolormesh(stats2d['yy'],stats2d['zz'],stats2d['V'].transpose(),shading='auto')
plt.xlabel('y/h')
plt.ylabel('z/h')
cbar = plt.colorbar()
cbar.ax.set_title('V') 

#%% Plot mean velocity profile with riblet
plt.figure()
plt.semilogx(stats2d['zz'],np.mean(stats2d['V'],axis=0))
plt.xlabel('z/h')
plt.ylabel('V')
plt.grid()

#%% Plot mean velocity profile and extraction of protrusion heights
v_plot = np.mean(stats2d['V'], axis=0)
zz = stats2d['zz']
z_plot = np.linspace(zz[0], zz[-1], 10000)
sqr_err_min = 1000
j_opt = 0
ll = len(zz)
for j in range(2,int(np.floor(ll/2))):
    line_profile_v = np.polyfit(zz[j+5:],v_plot[j+5:],1)
    z_height_u = -line_profile_v[1]/line_profile_v[0]
    sqr_err = sum(np.power(v_plot[zz>-1*zz[0]] - zz[zz>-1*zz[0]]*line_profile_v[0] - line_profile_v[1],2))/len(zz[zz>-1*zz[0]])
    protr_height_v = abs(z_height_u - ratiohs*spacing/2)/spacing
    if sqr_err<sqr_err_min:
        sqr_err_min = sqr_err
        line_plot_v = z_plot*line_profile_v[0] + line_profile_v[1]
        j_opt = j
        protr_height_opt = protr_height_v

#FIGURES

plt.figure()
plt.plot(v_plot, zz)
plt.plot(line_plot_v, z_plot, '--')
plt.ylabel('z/h')
plt.xlabel('V')
plt.grid()

plt.figure()
plt.plot(zz, v_plot)
plt.plot(z_plot, line_plot_v, '--')
plt.xlabel('z/h')
plt.ylabel('V')
plt.grid()

plt.figure()
plt.plot(zz[1:], np.diff(v_plot)/np.diff(zz))
plt.xlabel('z/h')
plt.ylabel('dV/dz')
plt.grid()

print("")
print("The minumun error occurs by statring the interpolation at z = ",zz[j_opt+5],)
print("Its value is ", sqr_err_min)
print("The perpendicular protrusion height is h = ",protr_height_opt)
print("")

plt.show()
#%% Plot the variances
#plt.figure()
#plt.title('total stress budget')
#plt.plot(stats2d['zz'],np.mean(stats2d['uu'],axis=0), label='<uu>')
#plt.plot(stats2d['zz'],np.mean(stats2d['vv'],axis=0), label='<vv>')
#plt.plot(stats2d['zz'],np.mean(stats2d['ww'],axis=0), label='<ww>')
#plt.plot(stats2d['zz'],np.mean(stats2d['uw'],axis=0), label='<uw>')
#plt.legend()
#plt.xlabel('z/h')
#plt.ylabel('U')
#plt.show()

#%% Plot the total stress
#plt.figure()
#plt.title('total stress budget')
#plt.plot(-np.array([1,1])*stats['zz'][0],np.array([0,1]), 'k--', linewidth=1)
#plt.plot(stats['zz'],stats['mombal']['turb'] , label='<uw>')
#plt.plot(stats['zz'],stats['mombal']['disp'] ,  label='<UW>')
#plt.plot(stats['zz'],stats['mombal']['visc'] ,  label='d<U>/dz')
#plt.plot(stats['zz'],stats['mombal']['tot'] , label='tot')
#plt.plot(stats['zz'],np.polynomial.polynomial.polyval(stats['zz'],stats['mombal']['P'].coef), label = 'lin')
#plt.legend()
#plt.show()


#%% 
