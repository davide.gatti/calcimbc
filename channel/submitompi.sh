#!/bin/bash
module load compiler/gnu/12.1
module load mpi/openmpi/4.1
source $HOME/openmpi-4.1.5/initMPI.sh
bash compile.sh
mpirun --use-hwthread-cpus --bind-to core --map-by core -report-bindings `pwd`/riblets_test 16 16
wait
